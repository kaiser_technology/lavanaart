<?php
class MY_Loader extends CI_Loader {
    public function template($template_name, $vars = array(), $return = FALSE)
    {
		
         $CI =& get_instance();
		if(isset($_COOKIE['currenty'])){
			define("CURRENCY",$_COOKIE['currenty']);
		}else {
			define("CURRENCY","INR");
		}
		$vars['category'] = $this->getmenu();
	//	$vars['session_data'] = $CI->session->userdata('userDetail');
	//	$vars['storeinfo'] = $this->storeInfo();
		$vars['pageName'] =ucfirst($CI->uri->segment(1));
        
		//print_r($vars);exit;
        if($return):
        $content  = $this->view('templates/header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('templates/footer', $vars, $return);

        return $content;
    else:
        $this->view('include/header', $vars);
        $this->view($template_name, $vars);
        $this->view('include/footer', $vars);
    endif;
    }
	 public function templateTwo($template_name, $vars = array(), $return = FALSE)
    {
         $CI =& get_instance();
		 if(isset($_COOKIE['currenty'])){
			define("CURRENCY",$_COOKIE['currenty']);
		}else {
			define("CURRENCY","INR");
		}
		//$vars['scripts']=$this->jsCss();
		$vars['category'] = $this->getmenu();
	//	$vars['session_data'] = $CI->session->userdata('userDetail');
	//	$vars['storeinfo'] = $this->storeInfo();
		$vars['pageName'] =ucfirst($CI->uri->segment(1));
        
		//print_r($vars);exit;
        if($return):
        $content  = $this->view('templates/headerTwo', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('templates/footer', $vars, $return);

        return $content;
    else:
        $this->view('include/headerTwo', $vars);
        $this->view($template_name, $vars);
        $this->view('include/footer', $vars);
    endif;
    }
	public function getmenu()
	{
		$param= array();
       $param['where'] =array(
                    "store_key" => STORE_KEY,
					"cat_status" => "Active"
                );
		$this->model('CorModel');
		 $CI =& get_instance();
		$menu = $CI->CorModel->getData('getCategory',$param);

	return $menu;
		
	}
	public function storeInfo()
	{
		$param= array();
       $param['where'] =array(
                    "store_key" => STORE_KEY
                );
		$this->model('CorModel');
		 $CI =& get_instance();
		$menu = $CI->CorModel->getData('storeInfo',$param);

	return $menu;
		
	}
    public function isSignedIn($page = "")
	{
		$CI =& get_instance();
		if(!$CI->session->userdata('userDetail'))
		{
			if($page !=""){
			 redirect(base_url().'login/index/'.$page);
			}else{
			 redirect(base_url().'login');
			}
		}
		else
		{
			$session_data = $CI->session->userdata('userDetail');
			return $session_data;
		}
	}
	
}
?>