<?PHP
Class CorModel extends CI_Model{
	
   public function getData($method,$param)
   {
		$param['storekey'] = STORE_KEY;
		$data = json_encode($param);
		$response = $this->$method($data);
		return json_decode($response);
	
   }
   function productList($param)
	{
		
		$params = json_decode($param);
			$stkey            = $params->storekey;
			$dbparma['table'] = "store_information";
			$dbparma['where'] = (array)$params->where;
			if(!empty($params->cid)){
			$dbparma['inarray'] = $this->getchield($params->cid,$stkey);
			}
			$result           = $this->productdata($dbparma);
			$data             = json_encode(array(
				"response" => "true",
				"detail" => $result
			));
			return $data;
		}
	 function productDetail($param)
	{
		
		$params = json_decode($param);
			$stkey            = $params->storekey;
			$dbparma['table'] = "store_information";
			$dbparma['where'] = (array)$params->where;
			if(!empty($params->cid)){
			$dbparma['inarray'] = $this->getchield($params->cid,$stkey);
			}
			$result           = $this->productdata($dbparma);
			$data             = json_encode(array(
				"response" => "true",
				"detail" => $result
			));
			return $data;
		}	
	function productdata($dbparma)
		{
			//return $dbparma;
			$date= date("Y-m-d");

		  $CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('product_tbl');
		$CI->db->join('product_price_tbl','product_tbl.pro_id = product_price_tbl.pro_id');
		
		$CI->db->join('prodcut_detail_tbl','product_tbl.pro_id = prodcut_detail_tbl.pro_id');
		$CI->db->where("product_tbl.pro_display_date <=", $date);
		//$this->db->where("product_tbl.pro_cat",$id);
		$CI->db->where("pro_status",'Active');
		/*if($bid!=0)
		{
		$this->db->where("brand",$bid);
		}	*/	
		
		$CI->db->where($dbparma['where']);
		if(!empty($dbparma['inarray']))
		{
			$CI->db->where_in("product_tbl.pro_cat",$dbparma['inarray']);
		}
		
		//$CI->db->order_by("product_price_tbl.pro_qty", "desc");
		$CI->db->order_by("product_tbl.pro_display_date", " ASC"); 
			//$this->db->limit($resultsPerPage, $ord);
		 $CI->db->group_by('product_tbl.pro_id');
   		 $query = $CI->db->get_where();
		
	  return $query->result_array();
		
		}
	function getdatas($dbparma)
        {
			//print_R($dbparma['table']);exit;
            $CI =& get_instance();
            $query  = $CI->db->get_where($dbparma['table'], $dbparma['where']);
            $result = $query->result_array();
			//return json_encode($CI->db->last_query());
            return $result;
        }
		function insertData($dbparma)
		{
			$CI =& get_instance();
            $CI->db->insert($dbparma['table'], $dbparma['data']);
            $insert_id = $CI->db->insert_id();

            return $insert_id;	
		}
		function updateData($dbparma)
		{
			$CI =& get_instance();
            $CI->db->update($dbparma['table'], $dbparma['data'],$dbparma['where']);
            
            return true;	
		}

function getchield($cid,$stkey)
		{
			 $CI =& get_instance();
			 $where = array(
                    "store_key" => $stkey,
					"parent_id" => $cid
                );
			$CI->db->select('cat_id');
            $query  = $CI->db->get_where('category_tbl', $where);
            $result = $query->result_array();
			$catarray[]=$cid;
			foreach($result as $res){ $catarray[] = $res['cat_id'];}
            return $catarray;
		}
        function checkcredential($k)
        {
            $a                = explode("|", unserialize(gzuncompress(base64_decode($k))));
            $dbparma['table'] = "store_webservices_credential";
            $dbparma['where'] = array(
                "api_key" => $a[0],
                "secret_key" => $a[1],
                "store_key" => $a[2]
            );
            $result           = $this->getdatas($dbparma);
            if (!empty($result)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
		
        function getCategory($param)
        {
			//return $param;
            $params = json_decode($param);
         
                $stkey            = $params->where;
                $dbparma['table'] = "category_tbl";
				
                $dbparma['where'] = (array)$stkey;
                $result           = $this->getdatas($dbparma);
                $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
                return $data;
           
        } 
		function customListing($param)
        {
			//return $param;
            $params = json_decode($param);
            ////$k      = $params->akey;
           
                $stkey            = $params->where;
                $dbparma['table'] = $params->table;;
				
                $dbparma['where'] = (array)$stkey;
                $result           = $this->getdatas($dbparma);
               if($result)
				{
					$data             = json_encode(array(
						"response" => "true",
						"detail" => $result
					));
				}else{
					$data             = json_encode(array(
						"response" => "false",
						"detail" => array()
					));
				}
                return $data;
           
        }
		function updateinfo($param)
		{
				return $param;
		}
		function CustomListings($param)
        {
			//return $param;
            $params = json_decode($param);
           
                $stkey            = $params->where;
                $dbparma['table'] = $params->table;;
				
                $dbparma['where'] = (array)$stkey;
				$dbparma['data'] = (array)$params->data;
                $result           = $this->updateData($dbparma);
				
				if($result)
				{
					$data             = json_encode(array(
						"response" => "true",
						"detail" => $result
					));
				}else{
					$data             = json_encode(array(
						"response" => "false",
						"detail" => array()
					));
				}					
                return $data;
           
        }
		function storeInfo($param)
        {
            $params = json_decode($param);
            
          
                $stkey            = $params->storekey;
                $dbparma['table'] = "store_information";
                $dbparma['where'] = array(
                    "store_key" => $stkey
                );
							$result           = $this->getdatas($dbparma);
							$data             = json_encode(array(
								"response" => "true",
                    "detail" => $result
                ));
                return $data;
            
        }
		function bannerList($param)
        {
            $params = json_decode($param);
            //$k      = $params->akey;
           
                $stkey            = $params->storekey;
                $dbparma['table'] = "banner";
                $dbparma['where'] = array(
                    "store_key" => $stkey,
					"status" => "Active"
                );
                $result           = $this->getdatas($dbparma);
                $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));

                return $data;
           
        }
		function userRagistration($param)
		{
			//return $param;
			 $params = json_decode($param);
			 $dbparma['table']		= "user_tbl";
			 $dbparma['data']    	= $params->udata;
			 $this->insertData( $dbparma);
			 unset($dbparma);
			 $dbparma['table'] 		= "login_credential";
			 $dbparma['data']		= $params->ldata;
			 $result	=	$this->insertData( $dbparma);
			  $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
				return $data;
		}
		
		function addShipping($param)
		{		
			
			 $params = json_decode($param);
			 $dbparma['table']		= $params->table;
			 $dbparma['billing']    	= $params->billing;
			 $this->insertData( $dbparma);
			 $dbparma['shipping']    	= $params->shipping;			 
			 $result	=	$this->insertData( $dbparma);
			  $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
				return $data;
		}

	function document($param)
        {
			
            $params = json_decode($param);
            //$k      = $params->akey;
			
			
				//return json_encode($params);
                $stkey            = $params->where;
				$updata  			=$params->urec;
                $dbparma['table'] = $params->table;
				
                $dbparma['where'] = (array)$stkey;
				$dbparma['data'] = (array)$updata;
				
                $result           = $this->updateData($dbparma);
				
				if($result){
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "true",
						"detail" => $result
					));
				}else{
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "false",
						"detail" => ""
					));
				}				
                return $data;
           
        }
		function instDocument($param){
		
			$params = json_decode($param);
            //$k      = $params->akey;
			
				
				$dbparma['table'] = $params->table;
			   	$dbparma['data'] = $params->shipping;;
				
				$result           = $this->insertData($dbparma);
				
				if($result){
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "true",
						"detail" => $result
					));
				}else{
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "false",
						"detail" => ""
					));
				}				
                return $data;
				
		}











		
   public function getDocument($method,$param)
   {
		$this->load->library("Nusoap_library");
		$client = new soapclient("http://navavyapar.com/index.php/SoapserversAction?wsdl"); 
	//print_r($client->call);exit;
		$param['akey'] = base64_encode(gzcompress(serialize(APICONF)));
		$param['storekey'] = STORE_KEY;
		$data = json_encode($param);
		//print_r($data);
	//echo $method;
		//$response = $client->__soapCall($method,array('param'=>$data)); 
		$response = $this->$method($data);
		return json_decode($response);
	
   }
   function getrec($tbl)
   {
	  $query=$this->db->get($tbl);
	    return $query -> result_array();
   }
   public function getRecords($params){
	   if(isset($params['select']))
	   {
			$this->db->select($params['select'],false);
	   }
	   if(isset($params['jointbl']))
	   {
			$cnt=count($params['jointbl']);
			
			for($i=0;$i<$cnt;$i++)
			{
					$this->db->join($params['jointbl'][$i],$params['joinfield']);
			}
	   
	   }
		
		if(isset($params['where']))
	   {
			$this->db->where($params['where']);
	   }
		$this->db->from($params['table']);
	 
		  $query=$this->db->get();
	 
	  
	  
	  return $query -> result_array();
	}
	public function insert($params){
	   
	  $query = $this->db->insert($params['table'],$params['data']);
		
	  if($query)
		   return $this->db->insert_id();
	  else
		  return false;
   }
   public function update($params){
	   
	  $query=$this->db->update($params['table'],$params['data'],$params['where']);
	  if($query)
		  return true;
	  else
		  return false;			
   }
}
?>
