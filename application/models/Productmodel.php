<?php
class Productmodel extends CI_Model
{
	function productdetail($cid,$search,$skey)
	{
		$inarry=array();
		if($cid != ""){
		$inarry = $this->getchield($cid,$skey);
		}
		$date= date("Y-m-d");
		$this->db->select('*');
		$this->db->from('product_tbl');
		$this->db->join('category_tbl','product_tbl.pro_cat = category_tbl.cat_id');
		$this->db->join('product_price_tbl','product_tbl.pro_id = product_price_tbl.pro_id');
		$this->db->join('prodcut_detail_tbl','product_tbl.pro_id = prodcut_detail_tbl.pro_id');
		$this->db->where('product_tbl.store_key',$skey);
		$this->db->where("product_tbl.pro_display_date <=", $date);
		$this->db->where("pro_status",'Active');
		//$this->db->where("product_tbl.offer",'0');
		//$this->db->where("category_tbl.cat_id",$cid);
		if(!empty($inarry))
		{
			$this->db->where_in("product_tbl.pro_cat",$inarry);
		}
		
		$this->db->like('product_tbl.pro_name', $search);
		$this->db->order_by("product_tbl.pro_id","desc");
		$query=$this->db->get();
		// echo $this->db->last_query();exit;
		return $query->result_array();
	}
	function getchield($cid,$stkey)
		{
			 $CI =& get_instance();
			 $where = array(
                    "store_key" => $stkey,
					"parent_id" => $cid
                );
			$CI->db->select('cat_id');
            $query  = $CI->db->get_where('category_tbl', $where);
            $result = $query->result_array();
			$catarray[]=$cid;
			foreach($result as $res){ $catarray[] = $res['cat_id'];}
            return $catarray;
		}
	function code($tab)
	{
		$this->db->order_by("pro_code","desc");	
		$query=$this->db->get($tab);
		return $query->result_array();
	}
	function insertpro($tab,$data)
	{
		$this->db->insert($tab,$data);
		$insert_id = $this->db->insert_id();
	return $insert_id;
	}
	function addpro($tab,$data)
	{
		$this->db->insert($tab,$data);
	}
	function maincategory($tab,$where)
	{
		$query=$this->db->get_where($tab,$where);
		return $query->result_array();
	}
	function viewbrand($tab)
	{
		$query=$this->db->get($tab);
		return $query->result_array();
	}
	function get_sub_cat($tab,$data)
	{
		$query=$this->db->get_where($tab,$data);
						
		if($query->result())
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$options[$row->cat_id] = $row->cat_name;
			}   
			return $options;
		} 
	}
	function get_pro($tab,$data)
	{
		$query=$this->db->get_where($tab,$data);
							
		if($query->result())
		{
			$result = $query->result();
			$options[]='Select';
			foreach($result as $row)
			{
				$options[$row->pro_id] = $row->pro_name; 
			}   
			return $options;
		} 
	}
	
	function viewdetailpro($id)
	{
		$this->db->select('product_tbl.*,manufacturer.*');
		$this->db->from('product_tbl');
		$this->db->join('manufacturer','product_tbl.brand = manufacturer.m_id');
		$this->db->where('pro_id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	
	function viewdetail($tab,$where)
	{
		$query=$this->db->get_where($tab,$where);
		return $query->result_array();
	}

	function getprodata($tbl,$udata)
	{
		$query=$this->db->get_where($tbl,$udata);
		return $query->result_array();
	}
	
	function getpromdata($tbl)
	{
		$query=$this->db->get($tbl);
		return $query->result_array();
	}
	function getproductdata($tbl,$pdata)
	{
		$query=$this->db->get_where($tbl,$pdata);
		return $query->result_array();
	}
	function proupdate($tbl,$udata,$where)
	{
		$query=$this->db->update($tbl,$udata,$where);
		
	
	}
	function updateproductdetail($tbl,$data,$where)
	{
		$query=$this->db->update($tbl,$data,$where);
		
		
	}
	
	function deletpro($tab,$where)
	{
		$query=$this->db->delete($tab,$where);
		

	}
	function priceinsert($tab,$data)
	{
		$this->db->insert($tab,$data);
	}
	function changeproimage($tab,$udata,$where)
	{
		$query=$this->db->update($tab,$udata,$where);
		
	}
	function deletepro($tab,$where)
	{
		$query=$this->db->delete($tab,$where);
		

	}
	public function prochk($pro) {
        $qry = "SELECT count(*) as cnt from product_tbl where pro_name= ?";
        $res = $this->db->query($qry,array($pro))->result();
        if ($res[0]->cnt > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }
	function keyword($tab,$where)
	{
		$this->db->order_by('date','desc');
		$query=$this->db->get_where($tab,$where);
		return $query->result_array();
	}
	
}
?>