<?php

class order_model extends CI_Model {
	
	 public function __construct()
	{
			parent::__construct();
			// Your own constructor code
	}
    function insert_order_details($post_array){
		$this->db->insert('order_detail', $post_array); 	
	}
	
	function uddate_order_details($post_array, $primary_key){
		$this->db->where('ord_det_id',$primary_key);
		$this->db->update('order_detail', $post_array);
	}
	
	function uddate_order($post_array, $primary_key){
		$this->db->where('ord_id',$primary_key);
		$this->db->update('order_tbl', $post_array);
	}
	
}
?>