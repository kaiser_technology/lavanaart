<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($this->input->post('uemail'))
		{
				
				$this->form_validation->set_rules('uemail', 'Email', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				 if($this->form_validation->run() == FALSE)
				{
					
					$data['msg']="Try Again";
					$this->load->templateTwo('login',$data);
				}else{
					
					$email=$this->input->post("uemail");	
					$password=$this->input->post("password");
					$udata['table'] ="login_credential"; 
					$udata['where'] =array('email'=>$email,'password'=>sha1($password),
					//'store_key' => STORE_KEY,
					'status'=>"1","role_id"=>"4");
					
					$custdata = $this->CorModel->getData('customListing',$udata);
					if(count($custdata->detail) == 1){
						
						$sess_array = array();
						 foreach($custdata->detail as $row)
						 {
						 
						   $sess_array = array(
							 'userDetail'=>$row
						   );
						   $this->session->set_userdata($sess_array);
						 }
						if($this->uri->segment(3)){
							
						redirect($this->uri->segment(3));
						}else{
							redirect('home');							
						}		
					}
					else{
						
						$data['msg']="Enter Valid Credential";
						$this->load->templateTwo('login',$data);
					}
						
				}
		}else{		
		
		$this->load->templateTwo('login');
		}
	}
	public function forgotpass()
	{
		$data = array();
		if($this->input->post('uemail'))
		{
			$email=$this->input->post("uemail");	
			
			$password = $this->pro->generateRandomString('6');
			$udata['table'] ="login_credential"; 
			$udata['data'] =array('password'=>sha1($password)); 
			$udata['where'] =array('email'=>$email,"role_id"=>"4"); 
			$custdata = $this->CorModel->update($udata);
			
			if($custdata){
			$subject="Reset your lavanaart password";
		

						$msg='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

						<tbody>
						<tr bgcolor="#404245"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
						<tr> 

						<td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="" title="Visit www.lavanaart.com" target="_blank"><img class="CToWUd" alt="lavanaart" src="http://demo.lavanaart.com/admin/assets/images//store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
						<td style="text-align:right;padding:0px 20px"> 
						<table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
						<tbody>
						
						<tr> 
						<td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif">Reset Password</span> </td> 
						</tr> 
						<tr> 
						<td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> </span> </td> 
						</tr> 
						</tbody>
						</table> </td> 
						</tr> 

						<tr> 
						<td colspan="2" style="padding:0 0px;width:640px"> 
						<table style="border-top:3px solid #2d3741;width:100%" cellpadding="0" cellspacing="0"> 
						<tbody>
						<tr> 
						<td  style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;margin:15px 20px 0 20px">Dear User,</p> <p style="margin:4px 20px 18px 20px;width:640px"> We have auto generate a new password after login please reset your password. </p> </td> 
						</tr> 
						<tr> 
						
						<td style="font:14px Arial,san-serif;padding:11px 18px 14px 18px;width:280px;background-color:#efefef" valign="top"><span style="color:#666">Your new password: <strong>'.$password.'</strong></span><br> 
							
						</tr> 
						<tr> 
						<td colspan="2" style="font-size:10px;color:#666;padding:0 10px 20px 10px;line-height:16px;width:640px"> <p style="margin:10px 0 0 0;font:11px/16px Arial,sans-serif;color:#333"> If have issues with your password, please contact helpdesk or email to info@lavanaart.com

						</p> </td> 
						</tr> 
						
						</tbody>
						</table> </td> 
						</tr> ';
						
						$msg.='
						<tr bgcolor="#404245"> 
						<td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.lavanaart.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
						</tr> 

						</tbody>
						</table>';
	


			$this->pro->sendMail($msg,$email,$subject);
			
		
			
			$data['message'] = "Your New password has been sent to your email. <a href='".ADMIN_URL."login'>Click Here for Login page</a>.";
		
			}
			else{
				$data['message'] = "Sorry, We have not found your email in our database. Please insert registered email id.";
			}
		}//else{
		
		//print_r($data);
		//exit;
			
		$this->load->templateTwo('forgotpass',$data);
		//}
	}
}
