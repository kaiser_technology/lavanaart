<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html 
	 */
	public function index()
	{
		$param['store_key'] =STORE_KEY;
		$param['where'] =array(
			"store_key" => STORE_KEY,
			"pro_status" => "Active",
			"displayOnHome" => "Yes"
		);
		$productList = $this->CorModel->getData('productList',$param);
		
		foreach($productList->detail as $pval)
		{
			$productListArray[$pval->pro_cat][] = $pval;
		}
		
		$data['productList']= $productListArray;
		$banner['where'] =array(
			"store_key" => STORE_KEY,
			"status" => "Active"
		);		
		$data['bannerList'] = $this->CorModel->getData('bannerList',$banner);
		
		
		
		$this->load->library("pagination");
		$paramAll['where'] =array(
			"store_key" => STORE_KEY,
			"pro_status" => "Active"
		);
		//$data['allProductList'] = 
		$data['storeinfo'] = $this->CorModel->getData('storeInfo',$param);
		$this->load->template('home',$data);
	}
}
