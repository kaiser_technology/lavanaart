<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{			
		$this->load->template('ContactDetail');
	}

	public function contactusSubmit()
	{
				
				$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean');
				
				$this->form_validation->set_rules('mobile', 'Contact number', 'trim|required|numeric');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
				
				if($this->form_validation->run() == FALSE)
				{
					//$data['msg'] = "opps!! something went wrong.";
					$this->session->set_flashdata('msg','opps!! something went wrong.');
					redirect('Contactus/index/');
					//$this->load->template('ContactDetail', $data);
				}
				else
				{ 
					$user_firstName=$this->input->post('name');
					$user_contact=$this->input->post("mobile");
					$user_email=$this->input->post("email");
					$message=$this->input->post("message");
					
				
					
					$userRequestType = 'Enquiry';
					
					
					$this->db->from('enqtable');
					$this->db->select('enqid');
					$this->db->order_by('id', 'DESC');
					$this->db->limit(1);
		 			$query=$this->db->get();
					$result = $query->result_array();
					
					if(empty($result)){
						$enqid = "10001";
					}else{
						 $no = str_replace("LVART","",$result[0]['enqid']);
						
						$enqid = ($no+1);

					}

					$insprm['table'] = "enqtable";
					$insprm['data'] = array("enqid"=>"LVART".$enqid,"name"=>$user_firstName,"email"=>$user_email,"mobile"=>$user_contact,"msg"=>$message,"enqtype"=>"Contact-us-enquiry","url"=>$_SERVER['HOST']);
					$this->CorModel->insert($insprm);
					$this->db->from('enqtable');
					$this->db->select('enqid');
					$this->db->order_by('id', 'DESC');
					$this->db->limit(1);
		 			$query=$this->db->get();
					$result = $query->result_array();
					$enid = $result[0]['enqid'];	
					//$this->sendMail($dd);
					$this->sendVerifyMail($userRequestType, $user_firstName, $user_contact, $user_email, $message,$enid);
				//	$data['msg'] = " Query submitted!, Thank you for enquiring with Sayonara Luggage. Your enquiry reference number is ".$enid.". Our team shall get in touch with you shortly.";
					$this->session->set_flashdata('msg','Query submitted!, <br/>Thank you for enquiring with Sayonara Luggage. <br/>Your enquiry reference number is <b>'.$enid.'</b>. <br/>Our team shall get in touch with you shortly.');
				//	$this->load->template('ContactDetail', $data);		
					redirect('Contactus/index/');
				}			
				
				
	}
	public function productenq()
	{			 $pid = $this->uri->segment(3);

		
			
			$this->db->from('product_tbl');
			$this->db->where('pro_id', $pid);
			 $query=$this->db->get();
			$result = $query->result_array();

			$data['product'] = $result[0];
		$this->load->templateTwo('enquiry',$data);
	}
		public function enqsubmit()
	{
				
		$this->form_validation->set_rules('name', 'Full Name', 'trim|required|xss_clean');
				
		$this->form_validation->set_rules('mobile', 'Contact number', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		
		if($this->form_validation->run() == FALSE)
		{
			//$data['msg'] = "opps!! something went wrong.";
			$this->session->set_flashdata('msg','opps!! something went wrong.');
			redirect('Contactus/index/');
			//$this->load->template('ContactDetail', $data);
		}
		else
		{ 
			$user_firstName=$this->input->post('name');
			$user_contact=$this->input->post("mobile");
			$user_email=$this->input->post("email");
			$message=$this->input->post("message");
			$proname=$this->input->post("productname");
			$proid=$this->input->post("productid");
			
			$userRequestType = 'Enquiry';
			
			
			$this->db->from('enqtable');
			$this->db->select('enqid');
			$this->db->order_by('id', 'DESC');
			$this->db->limit(1);
			 $query=$this->db->get();
			$result = $query->result_array();
			
			if(empty($result)){
				$enqid = "10001";
			}else{
				 $no = str_replace("LVART","",$result[0]['enqid']);
				
				$enqid = ($no+1);

			}

			$insprm['table'] = "enqtable";
			$insprm['data'] = array("enqid"=>"LVART".$enqid,"name"=>$user_firstName,"email"=>$user_email,"mobile"=>$user_contact,"productname"=>$proname,"productid"=>$proid,"msg"=>$message,"enqtype"=>"Contact-us-enquiry","url"=>$_SERVER['HOST']);
			$this->CorModel->insert($insprm);
			$this->db->from('enqtable');
			$this->db->select('enqid');
			$this->db->order_by('id', 'DESC');
			$this->db->limit(1);
			 $query=$this->db->get();
			$result = $query->result_array();
			$enid = $result[0]['enqid'];	
			//$this->sendMail($dd);
			$this->enqMail($userRequestType, $user_firstName, $user_contact, $user_email, $message,$enid,$proname,$proid);
		//	$data['msg'] = " Query submitted!, Thank you for enquiring with Sayonara Luggage. Your enquiry reference number is ".$enid.". Our team shall get in touch with you shortly.";
			$this->session->set_flashdata('msg','Query submitted!, <br/>Thank you for enquiring with Sayonara Luggage. <br/>Your enquiry reference number is <b>'.$enid.'</b>. <br/>Our team shall get in touch with you shortly.');
		//	$this->load->template('ContactDetail', $data);		
			redirect('Contactus/productenq/'.$proid);
		}
				
				
	}

	
	function sendVerifyMail($userRequestType, $user_firstName, $user_contact, $user_email, $message,$enid)
	{
		$msg='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

		<tbody>
		<tr bgcolor="#272262"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
		<tr> 
  
		<td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="http://www.lavanaart.com" title="Visit www.lavanaart.com" target="_blank"><img class="CToWUd" alt="sayonara" src="http://demo.lavanaart.com/admin/assets/images//store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
		<td style="text-align:right;padding:0px 20px"> 
		<table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
		<tbody>
		
		<tr> 
		<td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif"> Contectus Enquiry Detail</span> </td> 
		</tr> 
		<tr> 
		<td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> </span> </td> 
		</tr> 
		</tbody>
		</table> </td> 
		</tr> 
  
		<tr> 
		<td colspan="2" style="padding:0 0px;width:640px"> 
		<table style="border-top:3px solid #2d3741;width:100%" cellpadding="0" cellspacing="0"> 
		<tbody>
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Enquiry ID 
								  
								  </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$enid.'
								  </td>
							  </tr>	
								<td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Name
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$user_firstName.'
								  </td>
							  </tr>
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Contact Number 
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$user_contact.'
								  </td>
							  </tr>
							  <tr style="background-color: #eee;">
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Email Id
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$user_email.'
								  </td>
							  </tr>
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Message 
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$message.'
								  </td>
							  </tr>
		
		<tr> 
		<td colspan="2" style="font-size:10px;color:#666;padding:0 10px 20px 10px;line-height:16px;width:640px"> <p style="margin:10px 0 0 0;font:11px/16px Arial,sans-serif;color:#333"> If have issues with your password, please contact helpdesk or email to info@sayonaraluggage.com
  
		</p> </td> 
		</tr> 
		
		</tbody>
		</table> </td> 
		</tr> ';
		
		$msg.='
		<tr bgcolor="#272262"> 
		<td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.sayonaraluggage.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
		</tr> 
  
		</tbody>
		</table>';	
		
$this->pro->sendMail($msg,'connect@lavanaart.com',"Site Contactus Enquery");
    
    
	} 
	function enqMail($userRequestType, $user_firstName, $user_contact, $user_email, $message,$enid,$proname,$proid)
	{
		$msg='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

		<tbody>
		<tr bgcolor="#272262"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
		<tr> 
  
		<td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="http://www.lavanaart.com" title="Visit www.lavanaart.com" target="_blank"><img class="CToWUd" alt="sayonara" src="http://demo.lavanaart.com/admin/assets/images//store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
		<td style="text-align:right;padding:0px 20px"> 
		<table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
		<tbody>
		
		<tr> 
		<td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif"> Contectus Enquiry Detail</span> </td> 
		</tr> 
		<tr> 
		<td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> </span> </td> 
		</tr> 
		</tbody>
		</table> </td> 
		</tr> 
  
		<tr> 
		<td colspan="2" style="padding:0 0px;width:640px"> 
		<table style="border-top:3px solid #2d3741;width:100%" cellpadding="0" cellspacing="0"> 
		<tbody>
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Enquiry ID 
								  
								  </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$enid.'
								  </td>
							  </tr>
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Product Name 
								  
								  </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$proname.'
								  </td>
							  </tr>	
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Product ID 
								  
								  </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$proid.'
								  </td>
							  </tr>		
								<td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Name
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$user_firstName.'
								  </td>
							  </tr>
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Contact Number 
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$user_contact.'
								  </td>
							  </tr>
							  <tr style="background-color: #eee;">
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Email Id
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$user_email.'
								  </td>
							  </tr>
							  <tr>
								  <td style="padding: 10px 0 10px 10px; font-family: Avenir, sans-serif; font-size: 16px;width: 50%;">
									  Message 
								  
								   </td><td style="padding: 10px 0 10px 0; font-family: Avenir, sans-serif; font-size: 16px;">
									  '.$message.'
								  </td>
							  </tr>
		
		<tr> 
		<td colspan="2" style="font-size:10px;color:#666;padding:0 10px 20px 10px;line-height:16px;width:640px"> <p style="margin:10px 0 0 0;font:11px/16px Arial,sans-serif;color:#333"> If have issues with your password, please contact helpdesk or email to info@sayonaraluggage.com
  
		</p> </td> 
		</tr> 
		
		</tbody>
		</table> </td> 
		</tr> ';
		
		$msg.='
		<tr bgcolor="#272262"> 
		<td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.sayonaraluggage.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
		</tr> 
  
		</tbody>
		</table>';	

$this->pro->sendMail($msg,'connect@lavanaart.com',"Site Contactus Enquery");
    
    
	} 
}
