<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		if($this->input->post('email'))
		{
				$this->form_validation->set_rules('fname', 'First name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
				 if($this->form_validation->run() == FALSE)
				{
					$data['msg']="Try Again";
					$this->load->template('register',$data);
				}else{
							$title = $this->input->post('prefix');
							$fname = $this->input->post('fname');
							$lname = $this->input->post('lname');
							$address = $this->input->post('address');
							$city = $this->input->post('city');
							$pincode = $this->input->post('pincode');
							$email = $this->input->post('email');
							$phone = $this->input->post('phone');
							$pass = $this->input->post('cpass');
							$udata['table'] ="user_tbl"; 
							$udata['where'] =array('user_email'=>$email,'store_key' => STORE_KEY); 
							$custdata = $this->CorModel->getData('customListing',$udata);
							
							if(count($custdata->detail) <1){
							$param['udata'] = array('user_prefix'=>$title,
													'user_firstName'=>$fname,
													'user_lastName'=>$lname,
													'user_address'=>$address,
													'user_postalCode'=>$pincode,
													'city' => $city,
													'user_contact' =>$phone,
													'user_email'=>$email,
													'user_status'=>'Active',
													'store_key' => STORE_KEY,
													);
							$param['ldata'] = array('user_name'=>$fname.' '.$lname,
													'email'=>$email,
													'password' => sha1($pass),
													'role_id' =>	'4',
													'status' =>	'1',
													'store_key' => STORE_KEY,
													);						
						
							$data['userid'] = $this->CorModel->getData('userRagistration',$param);
							$data['msg']="Registration Successful. Thank you for registration.";		
							
							$msg='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

							<tbody>
							<tr bgcolor="#404245"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
							<tr> 
	
							<td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="" title="Visit www.lavanaart.com" target="_blank"><img class="CToWUd" alt="lavanaart" src="http://demo.lavanaart.com/admin/assets/images/store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
							<td style="text-align:right;padding:0px 20px"> 
							<table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
							<tbody>
							
							<tr> 
							<td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif">Registration</span> </td> 
							</tr> 
							<tr> 
							<td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> </span> </td> 
							</tr> 
							</tbody>
							</table> </td> 
							</tr> 
	
							<tr> 
							<td colspan="2" style="padding:0 0px;width:640px"> 
							<table style="border-top:3px solid #2d3741;width:100%" cellpadding="0" cellspacing="0"> 
							<tbody>
							<tr> 
							<td  style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;margin:15px 20px 0 20px">Dear User,</p> <p style="margin:4px 20px 18px 20px;width:640px"> Welcome to the Lavanaart. You have successfully registered to www.lavanaart.com, Now you can start buying geniune products from our portal. </p> </td> 
							</tr> 
							
							<tr> 
							<td colspan="2" style="font-size:10px;color:#666;padding:0 10px 20px 10px;line-height:16px;width:640px"> <p style="margin:10px 0 0 0;font:11px/16px Arial,sans-serif;color:#333"> If have issues with your password, please contact helpdesk or email to info@lavanaart.com
	
							</p> </td> 
							</tr> 
							
							</tbody>
							</table> </td> 
							</tr> ';
							
							$msg.='
							<tr bgcolor="#404245"> 
							<td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.lavanaart.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
							</tr> 
	
							</tbody>
							</table>';
				$this->pro->sendMail($msg,$email,"Lavanaart Registration");
				
				// Auto login after register
					$udata['table'] ="login_credential"; 
					$udata['where'] =array('email'=>$email,'password'=>sha1($pass),
					//'store_key' => STORE_KEY,
					'status'=>"1","role_id"=>"4"); 
					$custdata = $this->CorModel->getData('customListing',$udata);
					if(count($custdata->detail) == 1){
						
						$sess_array = array();
						 foreach($custdata->detail as $row)
						 {
						 
						   $sess_array = array(
							 'userDetail'=>$row
						   );
						   $this->session->set_userdata($sess_array);
						 }
							redirect('home');							
								
					}
				}else{
					$data['msg']="The email address you have entered is already registered.";

				}	
				
			}
			$this->load->templateTwo('register',$data);
		}
		else{
			$this->load->templateTwo('register');
		}
	}
}
