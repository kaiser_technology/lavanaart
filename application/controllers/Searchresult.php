<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Searchresult extends CI_Controller {
	
	function index()
	{
		 $category =$this->input->post('category_id');
		 $keyword = $this->input->post('search');
		 $param['table'] = "product_tbl";
		 $param['where'] = array("store_key" => STORE_KEY);
		 $param['search'] = $keyword;
		 $param['category'] = $category;
		 $this->load->model("productmodel");
		 $result['data'] = $this->productmodel->productdetail($category,$keyword,STORE_KEY);
		
		$this->load->template("searchresult",$result);
	}
}
?>