<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class category extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($cat_id = 0)
	{
		$this->load->library("pagination");
		$param['where'] =array(
			"store_key" => STORE_KEY,
			"pro_status" =>"Active"
		);
		$param['cid'] = $cat_id;
		$prolist = $this->CorModel->getData('productList',$param);
		$config = array();
		$config["base_url"] = base_url() . "category/index/".$this->uri->segment(3);
		$config["total_rows"] = COUNT($prolist->detail);
		$config["per_page"] = 16;
		$config["uri_segment"] = 4;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close']="</ul>";
		$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><span class="current page-number">';
		$config['cur_tag_close'] = '</span></li>';	
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$displayRecord = $config["per_page"];
		$start = $page;
		//$prolist = $this->CorModel->getData('productList',$param);
		//print_r($prolist);exit;
		$data['productList'] = array_slice($prolist->detail, $start, $displayRecord);
		$data["links"] = $this->pagination->create_links();
		// For Pagination 			
		
		$data['storeinfo'] = $this->CorModel->getData('storeInfo',$param);
			
		//$this->load->template('category',$data);
		$this->load->templateTwo('category_filter',$data);
	}
}
