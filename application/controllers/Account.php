<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct()
	{
		parent::__construct();
			
		$this->session_data=$this->load->isSignedIn();	
		//print_r($this->session_data);exit;
	}
	 
	public function index()
	{
	
		try{
			$this->load->templateTwo('account');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}				
	}
	public function profile()
	{
	
		try{
			$param['table'] ="user_tbl"; 
			$param['where'] =array(
				"store_key" => STORE_KEY,
				"user_email" => $this->session->userdata('userDetail')->email
			);
			
			$userDetail = $this->CorModel->getData('customListing',$param);
			//print_r($userDetail);exit;
			$data ['userinfo']=$userDetail->detail[0]; 
			if($this->input->post('update')){
				$title = $this->input->post('prefix');
							$fname = $this->input->post('fname');
							$lname = $this->input->post('lname');
							$address = $this->input->post('address');
							$city = $this->input->post('city');
							$pincode = $this->input->post('pincode');
							$email = $this->input->post('email');
							$phone = $this->input->post('phone');
							$pass = $this->input->post('cpass');
				$udata['table'] ="user_tbl"; 
				$udata['where'] =array('user_email'=>$email,'store_key' => STORE_KEY);
				$udata['urec'] = array('user_prefix'=>$title,
													'user_firstName'=>$fname,
													'user_lastName'=>$lname,
													'user_address'=>$address,
													'user_postalCode'=>$pincode,
													'city' => $city,
													'user_contact' =>$phone,
													'user_email'=>$email
													);
					//$param['table']="user_tbl";								
					$custdata = $this->CorModel->getDocument('document',$udata);	
//print_r($custdata);exit;					
				if($custdata->response == "true"){
					$data['msg'] = MSG_UPDATE;
				
				}else{
					$data['msg'] = MSG_UPDATE_ERR;
		
				}
				$userDetail = $this->CorModel->getData('customListing',$param);
			//print_r($userDetail);exit;
			$data ['userinfo']=$userDetail->detail[0]; 
				$this->load->templateTwo('profile',$data);
			}
			else
			{
			
			$this->load->templateTwo('profile',$data);
			}

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}				
	}
	public function changePassword()
	{
		try{
			if($this->input->post('update')){
					$o = $this->input->post('opass');
					$n = $this->input->post('npass');
					$c = $this->input->post('cpass');
					//echo sha1($o);
					$email = strtolower($this->input->post('email'));
				$udata['table'] ="login_credential"; 
				$udata['where'] =array('password'=>sha1($o),'email'=>$email,
				//'store_key' => STORE_KEY
				);
				$userDetail = $this->CorModel->getData('customListing',$udata);
				//print_r($udata);exit;
				if($userDetail->response == "true"){
				
					$udata['data'] = array('password'=>sha1($n));
						//$param['table']="user_tbl";								
						$custdata = $this->CorModel->getData('CustomListings',$udata);
						
					if($custdata->response == "true"){
						$data['msg'] = MSG_UPDATE;
					
					}else{
						$data['msg'] = MSG_UPDATE_ERR;
			
					}
				}
				else{
					
					$data['msg'] = "Current Password is not match";
				}
				$this->load->templateTwo('change-password',$data);
			}else{		
			$this->load->templateTwo('change-password');
			}
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function logout()
	{
			$this->session->unset_userdata('userDetail');
			 redirect(base_url().'login');
			
	}
	public function orders()
	{
			
			$uid = $this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY);
			$where=array('user_id'=>$uid);
			//$this->load->model("userloginmodel");
			$udata['table'] ="order_tbl"; 
			$udata['where'] =array('user_id'=>$uid,'store_key' => STORE_KEY);
			$data['order'] = $this->CorModel->getData('customListing',$udata);
					//print_r($data);exit;
			$this->load->template("orderhistory",$data);
			
	}
	function order_hist_detail()
	{
		
			$oid = $this->uri->segment(3);
			$udata['table'] ="order_tbl"; 
			$udata['where'] =array('ord_id'=>$oid);
			$data['order'] = $this->CorModel->getData('customListing',$udata);
			
			$sid = $this->uri->segment(5);
			$udata['table'] ="shipping_tbl"; 
			$udata['where'] =array('ship_id'=>$sid);
			$data['shipadd'] = $this->CorModel->getData('customListing',$udata);
			
			$uid = $this->uri->segment(4);
			$udata['table'] ="user_tbl"; 
			$udata['where'] =array('user_id'=>$uid);
			$data['billadd'] = $this->CorModel->getData('customListing',$udata);
						
			$oid = $this->uri->segment(3);
			$udata['table'] ="order_detail"; 
			$udata['where'] =array('ord_id'=>$oid);
			$data['product'] = $this->CorModel->getData('customListing',$udata);
			
			$this->load->templateTwo("orderhistorydetail",$data); 
		
	}
	function cancel_order()
	{
		$uid = $this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY);
		$id = $this->uri->segment(3);
		$udata['table'] ="order_tbl"; 
			$udata['where'] =array('ord_id'=>$id);
			$data['order'] = $this->CorModel->getData('customListing',$udata);
		$ordcode = $data['order']->detail[0]->ord_code;
		
		$message='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

   <tbody>
   <tr bgcolor="#5E8939"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
    <tr> 
     
     <td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="" title="Visit www.sayonara.com" target="_blank"><img class="CToWUd" alt="abassistance" src="https://www.asmitaorganicfarm.com/admin/assets/images//store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
     <td style="text-align:right;padding:0px 20px"> 
      <table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
       <tbody>
        
        <tr> 
         <td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif">Order cancellation </span> </td> 
        </tr> 
        <tr> 
         <td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> Order #'. $ordcode .' </span> </td> 
        </tr> 
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;margin:15px 20px 0 20px">Dear User,</p> <p style="margin:4px 20px 18px 20px;width:640px"> your order number ".$ordcode." is send for cancellation and admin will confirm for the same if possible. </p> </td> 
    </tr> 
  
    <tr bgcolor="#2C381E"> 
     <td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.sayonara.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
    </tr> 
   
   </tbody>
  </table>';
		
			$adminmessage='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

   <tbody>
   <tr bgcolor="#5E8939"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
    <tr> 
     
     <td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="" title="Visit www.sayonara.com" target="_blank"><img class="CToWUd" alt="abassistance" src="https://www.asmitaorganicfarm.com/admin/assets/images//store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
     <td style="text-align:right;padding:0px 20px"> 
      <table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
       <tbody>
        
        <tr> 
         <td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif">Order cancellation </span> </td> 
        </tr> 
        <tr> 
         <td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> Order #'. $ordcode .' </span> </td> 
        </tr> 
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;margin:15px 20px 0 20px">Dear Admin,</p> <p style="margin:4px 20px 18px 20px;width:640px"> you recived cancellation request for order number '.$ordcode.'. </p> </td> 
    </tr> 
  
    <tr bgcolor="#2C381E"> 
     <td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.sayonara.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
    </tr> 
   
   </tbody>
  </table>';
			
					$this->pro->sendMail($message,$this->session->userdata('userDetail')->email,"Sayonara: Order cancellation");
					$this->pro->sendMail($adminmessage,'amit.s@jackfruittech.com',"Sayonara: Order cancellation");
		redirect('/account/orders/success');
	}
}