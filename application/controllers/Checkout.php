<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Checkout extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
	{
		parent::__construct();
		$page = "checkout";	
		$this->session_data=$this->load->isSignedIn($page);	
		//print_r($this->session_data);exit;
	}
	public function index()
	{		
		$param['table'] ="shipping_tbl"; 
		$param['order'] ="desc"; 
		$param['where'] =array(
			"store_key" => STORE_KEY,
			"user_id" => $this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY)
		);		
		//print_r($param);exit;
		$shippingAddress['shippingDetails'] = $this->CorModel->getData('customListing',$param);
		
		if($this->input->post('s_first_name'))
		{
			
			
			$this->form_validation->set_rules('s_first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('s_email_address', 'Email ID', 'trim|required');
			$this->form_validation->set_rules('s_address', 'Address', 'trim|required');
			$this->form_validation->set_rules('s_city', 'City', 'trim|required');
			$this->form_validation->set_rules('s_postal_code', 'Zip', 'trim|required');
			$this->form_validation->set_rules('s_telephone', 'Contact No', 'trim|required');
			if($this->form_validation->run() == FALSE)
			{
				$this->load->template('checkout',$shippingAddress);
			}else{
				$paramData['table'] = 'shipping_tbl';
			
				$paramData['shipping'] = array('user_id'=>$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY),
										'store_key'=>STORE_KEY,
										'address_type'=>'shipping', 
										'fname'=>$this->input->post('s_first_name'),
										'lname' => $this->input->post('s_last_name'),
										'email'=>$this->input->post('s_email_address'),
										'ship_straddress'=>$this->input->post('s_address'),
										'city_name' => $this->input->post('s_city'),
										'state_name' => $this->input->post('s_state'),
										'ship_postalCode' => $this->input->post('s_postal_code'),
										'country_name' => $this->input->post('country_name'),
										'ship_contact' => $this->input->post('s_telephone'),
										'shipping' => $this->input->post('toship'),
										);	
					
				$data['shippingid'] = $this->CorModel->getData('instDocument',$paramData);	
			
				$insid = $data['shippingid']->detail;
				$param['table'] ="shipping_tbl"; 
				$param['order'] ="desc"; 
				$param['where'] =array(
					"store_key" => STORE_KEY,
					"user_id" => $this->session->userdata('userDetail')->id
				);	
				
				$param['order'] ="desc"; 				
				$shippingAddress['shippingDetails'] = $this->CorModel->getData('customListing',$param);
				redirect("order/index/".$insid);		
			}
		}		
		$this->load->template('checkout',$shippingAddress);
	} 
}
