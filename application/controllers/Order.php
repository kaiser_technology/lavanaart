 <?php 
 class Order extends CI_Controller 
{
	
	function index(){
	
//	$this->pro->printlable('NVORD-100199'); exit;
		$ord_code=$this->pro->ordercode();
		$oc=explode('-',$ord_code);
		$this->load->helper('string');
		$a= random_string('alnum', 5);
		$orduncode=$a."".$oc[1];
		$user_id=$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY);
		$user_ip=$this->input->ip_address();
		$ord_date=date("Y-m-d");
		$total_amount=$this->cart->total();
		$total_items=$this->cart->total_items();
		$ship_charge="";
		$ord_status="not_confirm";
		$ord_dtadded=date("Y-m-d h:i:s");
		$ship_id = $this->uri->segment(3);
		$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"));
		$dtime= date("M").' '.date("d",$tomorrow).', '.date("Y");
		$cou_id = "";
		$disc = "";
		$ordtype = $this->input->post('payment-method');
		$store_key = STORE_KEY;
		$data = array("ord_code"=>$ord_code,'ordrndcode'=>$orduncode,'user_id'=>$user_id,'user_ip'=>$user_ip,'ord_date'=>$ord_date,'total_amount'=>$total_amount,'total_items'=>$total_items,'ship_charge'=>$ship_charge,'ord_status'=>$ord_status,'OderType'=>$ordtype,'ship_id'=>$ship_id,'del_date'=>$dtime,'cou_id'=>$cou_id,'discount'=>$disc,'store_key'=>$store_key,'ord_dtadded'=>$ord_dtadded);
		$param['table'] = "order_tbl";
		$param['data'] = $data;
		
		$oid=$this->CorModel->insert($param);
		$param['table'] = "shipping_tbl";
		$param['data'] = array("order_id"=>$oid);
		$param['where'] = array("ship_id" => $ship_id);
		$this->CorModel->update($param);
		
		foreach ($this->cart->contents() as $items)
			{	
			
			 $op=$this->cart->product_options($items['rowid']);
			
			$ord_id=$oid;
			$cart_id=$items['rowid'];
			$pro_id=$op['proid'];
			$pro_price_id=$op['pro_price_id'];
			$pro_price=$items['price'];
			$pro_qty=$items['qty'];
			$total_pro_amount=$items['subtotal'];
			$ord_det_dtadded=date("Y-m-d h:i:s");
			
			$wrname = $this->pro->wr_name($pro_id);
						
			$data = array("ord_id"=>$ord_id,'cart_id'=>$cart_id,'pro_id'=>$pro_id,'pro_price'=>$pro_price,'pro_price_id'=>$pro_price_id,'ord_pro_qty'=>$pro_qty,'wrhouse'=>$wrname,'total_pro_amount'=>$total_pro_amount,'ord_det_dtadded'=>$ord_det_dtadded);
			
			$odparam['table'] = "order_detail";
			$odparam['data'] = $data;
			$this->CorModel->insert($odparam);
			
			 $ordqtyupd['where']=array('pro_price_id'=>$pro_price_id);
			 $ordqtyupd['table']="product_price_tbl";
			 $query = $this->CorModel->getRecords($ordqtyupd);
			 $proqty= $query[0]['pro_qty'];
			 $stockpro=$proqty-$pro_qty;
			
			$pdata['where']=array('pro_price_id'=>$pro_price_id);
			$pdata['data'] = array('pro_qty'=>$stockpro);
			$pdata['table']="product_price_tbl";
			$this->CorModel->update($pdata);
		}
		
		$data = array("OrderId"=>$ord_id,'user_id'=>$user_id,'paymentAmount'=>$total_amount,'paymentStatus'=>"not_started",'date'=>date('Y-m-d'));
			
			$odparam['table'] = "paymentinfo";
			$odparam['data'] = $data;
			$this->CorModel->insert($odparam);
		
		redirect('order/paymentProcess/'.$oid.'/'.$ordtype);
		
	}
	function paymentProcess()
	{
		
		$this->session->unset_userdata('buynw');
		$oid= $this->uri->segment(3);
		$ordType= $this->uri->segment(4);
			// Merchant key here as provided by Payu
			$MERCHANT_KEY = MERCHANT_KEY;
			// Merchant Salt as provided by Payu
			$SALT = SALT;
			// End point - change to https://secure.payu.in for LIVE mode
			$PAYU_BASE_URL = PAYU_BASE_URL;
			$action = '';
			$posted = array();
			$param['table'] ="user_tbl"; 
			$param['where'] =array(
				"store_key" => STORE_KEY,
				"user_email" => $this->session->userdata('userDetail')->email
			);
				$formError = 0;
 
				
				  // Generate random transaction id
				   $txnid="";
				   $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
				  
			$pdata['where']=array('user_id'=>$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY),'OrderId'=>$oid,'date'=>date('Y-m-d'));
			$pdata['data'] = array('paymentTransactionId'=>$txnid);
			$pdata['table']="paymentinfo";
			$this->CorModel->update($pdata);
			
			$userDetail = $this->CorModel->getData('customListing',$param);
			 //  $pinarray=array();// remove comment 
			 $param['table'] ="shipping_tbl"; 
			$param['where'] =array(
				"store_key" => STORE_KEY,
				"email" => $this->session->userdata('userDetail')->email
			);
			 	$shipdetail = $this->CorModel->getData('customListing',$param);
			  $cnt = count($shipdetail->detail);
                 $cpin =     $shipdetail->detail[$cnt-1]->ship_postalCode;                  
                 $pinarray=array('400050','400051');// commnet this line 
                if(in_array($cpin,$pinarray)){
                   $finalamout =  ($this->cart->total()-(($this->cart->total() * 10)/100));
                }
                else{
                    $finalamout =  $this->cart->total();
                }
        
			$posted = array("key" =>$MERCHANT_KEY,"txnid"=>$txnid,"amount"=> $finalamout,"firstname"=>$userDetail->detail[0]->user_firstName,"lastname"=>$userDetail->detail[0]->user_lastName,"email"=>$userDetail->detail[0]->user_email,"phone"=>$userDetail->detail[0]->user_contact,"address1"=>$userDetail->detail[0]->user_address,"surl"=>SURL,"furl"=>RURL,"productinfo"=>"Lavanaart order","service_provider"=>"payu_paisa");
			
			
			$hash = '';
			// Hash Sequence
			$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

			  if(empty($posted['key'])
					  || empty($posted['txnid'])
					  || empty($posted['amount'])
					  || empty($posted['firstname'])
					  || empty($posted['email'])
					  || empty($posted['phone'])
					  || empty($posted['productinfo'])
					  || empty($posted['surl'])
					  || empty($posted['furl'])
			   || empty($posted['service_provider'])
			  ) {
				  //echo $posted['key']."|".$posted['amount']."|".$posted['firstname']."|".$posted['email'].'|'.$posted['phone'];
				 //exit;
				$formError = 1;
			  } else {
				//$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
			 $hashVarsSeq = explode('|', $hashSequence);
				$hash_string = ''; 
			 foreach($hashVarsSeq as $hash_var) {
				  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
				  $hash_string .= '|';
				}
			 
				$hash_string .= $SALT;
			 
			 
				$hash = strtolower(hash('sha512', $hash_string));
				$action = $PAYU_BASE_URL . '/_payment';
			  }
				$data['posted'] = $posted;
				$data['hash'] = $hash;
				$data['action'] = $action;
				$data['orderid'] = $oid;
				$data['ordType'] = $ordType;
				
			$this->load->templateTwo('orderprocess',$data);
			
	}
	function response_old()
	{
		$id = $this->uri->segment(3);
		if($id == 0){
			$data['status']="Successfully";
			$data['message']="Thank you for shopping with us. Your card has been charged and your transaction is successful. We will be shipping your order to you soon";
			$this->load->templateTwo('ordercomplate');
		}
		else{
			$data['status']="Fail";
			$data['message']="Thank you for shopping with us.However,the transaction has been declined.";
		$this->load->templateTwo('ordercomplate');
		}
	}
	function response()
	{
		$id = $this->uri->segment(3);
		//print_r($_POST);exit;
		if(!empty($_POST["status"])){
		$status=$_POST["status"];
		}else{
			$status = "bypass";
		}
	 	$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$salt="yIEkykqEH3";
//echo "<br>";

$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
 $hashVarsSeq = explode('|', $hashSequence);
				$hash_string = ''; 
			 foreach($hashVarsSeq as $hash_var) {
				  $hash_string .= isset($_POST[$hash_var]) ? $_POST[$hash_var] : '';
				  $hash_string .= '|';
				}
				$hash_string .= $SALT;
			 
			 
				$hash = strtolower(hash('sha512', $hash_string)); 
		
				
				
			   if ($id != 1) {
				   $paramtran['table'] ="paymentinfo"; 
						$paramtran['where'] =array(
							"paymentTransactionId" => $txnid
						);

								
						$TranDetail = $this->CorModel->getData('customListing',$paramtran);
				   
				   	//$result = $this->pro->shiping($param);
					$paramordr['table'] ="order_tbl"; 
					$paramordr['data'] =array(
							"ord_status" => $status
						);
					$paramordr['where'] =array(
							"ord_id" => $TranDetail->detail[0]->OrderId
						);			
						$OrderDetail = $this->CorModel->updateData($paramordr);
						
					$pdata['where']=array('paymentTransactionId'=>$txnid,'date'=>date('Y-m-d'));
					$pdata['data'] = array('paymentresponse'=>base64_encode(serialize($_POST)),'paymentStatus'=>"Fail");
					$pdata['table']="paymentinfo";
					$this->CorModel->update($pdata);		
					//return product
					$paramordr['table'] ="order_detail"; 
						$paramordr['where'] =array(
							"ord_id" => $TranDetail->detail[0]->OrderId
						);			
						$OrderDetaillist = $this->CorModel->getData('customListing',$paramordr);	
					
					foreach($OrderDetaillist->detail as $del)
					{
						$ordqtyupd['where']=array('pro_price_id'=>$del->pro_price_id);
						 $ordqtyupd['table']="product_price_tbl";
						 $query = $this->CorModel->getRecords($ordqtyupd);
						 $proqty= $query[0]['pro_qty'];
						 $stockpro=$proqty+$del->ord_pro_qty;
						
						$pdata['where']=array('pro_price_id'=>$del->pro_price_id);
						$pdata['data'] = array('pro_qty'=>$stockpro);
						$pdata['table']="product_price_tbl";
						$this->CorModel->update($pdata);
					}
						
			
			
					
					$data['status']=$status;
					$data['message']="Invalid Transaction. Please try again";
				 
				   }
			   else {	

					if(isset($this->session->userdata('userDetail')->email))
					{
						$email = $this->session->userdata('userDetail')->email;
						if($email == ""){
							$email = $this->session->userdata('userDetail')['email'];
						}
					}else{
						$email = $this->session->userdata('userDetail')['email'];
					}
			  // echo $status;exit;
					  //$data['status']=$status;
					  $data['status']="placed successfully";
					  $data['amount']=$amount;
					  $data['txnid']=$txnid;
					  $data['firstname']=$firstname;
					  $data['address1']=$add1;
					 
					$data['message']="
					<table style='width:700px'>
					<tbody>
					    
					    <tr>
					        <th>
					            <img src='http://demo.lavanaart.com/admin/assets/images//store/NVDS1105.jpg'/>
					        </th>
					   </tr>
					   <tr>
					        <th>
					            <h2>Congratulations! Your order has been placed successfully.</h2>
					        </th>
					   </tr>
					   <tr>
					        <td>
					            <h4>Hi ".$firstname.",<h4>
    					        <h4>Thank you for shopping with us! your order has been placed successfully and We will be shipping your order to you soon.</h4>
    					        <h4>Here are your order details: <br/>Your Transaction ID for this transaction is ".$txnid.".<br/> We have received payment of Rs. " . $amount . "/- Your order will soon be shipped.</h4>
					            
					        </td>
					    </tr>
					</tbody>
					</table>";
				$q = $this->db->query("select * from shipping_tbl where order_id='" . $TranDetail->detail[0]->OrderId . "'");
		//Emal Code//
		$r = $q->result();	
					
					$message='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

   <tbody>
   <tr bgcolor="#404245"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
    <tr> 
     
     <td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="" title="Visit www.lavanaart.com" target="_blank"><img class="CToWUd" alt="abassistance" src="http://demo.lavanaart.com/admin/assets/images//store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
     <td style="text-align:right;padding:0px 20px"> 
      <table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
       <tbody>
        
        <tr> 
         <td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif">Shipping Confirmation</span> </td> 
        </tr> 
        <tr> 
         <td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> Order #'. $TranDetail->detail[0]->OrderId .' </span> </td> 
        </tr> 
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;margin:15px 20px 0 20px">Hello '. $r[0]->fname .',</p> <p style="margin:4px 20px 18px 20px;width:640px"> We thought you&rsquo;d like to know that we&rsquo;ve dispatched your item(s). Your order is on the way. If you need to return an item from this shipment or manage other orders on www.lavanaart.com. </p> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="padding:0 0px;width:640px"> 
      <table style="border-top:3px solid #2d3741;width:100%" cellpadding="0" cellspacing="0"> 
       <tbody>
       ';
        
		if($r[0]->shipping == "Yes"){
		
		$message='<tr> 
        
         <td style="font:14px Arial,san-serif;padding:11px 18px 14px 18px;width:280px;background-color:#efefef" valign="top"><span style="color:#666">Your package was sent to:</span><br> <p style="margin:2px 0"> <strong>'. $r[0]->fname .' <br>'. $r[0]->ship_straddress .', <br> 
            <u></u>
             '.$r[0]->city_name.','. $r[0]->state_name .'-'. $r[0]->ship_postalCode .'
            <u></u> <br> India <br>(M):-'.$r[0]->ship_contact.'</strong> </p> </td> 
        </tr>'; 
		}else{
			$message='<tr> 
        
         <td style="font:14px Arial,san-serif;padding:11px 18px 14px 18px;width:280px;background-color:#efefef" valign="top"><span style="color:#666">Your package pickup to:</span><br> <p style="margin:2px 0"> <strong>Lavanaart, <br> 513A, Arun Building, 6th floor,<br>opposite VJTI college main gate,</br>H. R. Mahajani Road,Matunga<br>
            <u></u>
            
			Mumbai, Maharastra - 400019
            <u></u> <br> India <br>(O):-022 2600 6185</strong> </p> </td> 
        </tr>'; 
		}
		
      $message=' 
        <tr> 
         <td colspan="2" style="font-size:10px;color:#666;padding:0 10px 20px 10px;line-height:16px;width:640px"> <p style="margin:10px 0 0 0;font:11px/16px Arial,sans-serif;color:#333">  </p> </td> 
        </tr> 
        
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;border-bottom:1px solid #ccc;margin:0 20px 3px 20px;padding:0 0 3px 0"> Shipment Details </p> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="padding:16px 40px;width:640px"> 
      <table cellpadding="0" cellspacing="0" width="100%"> 
	  <thead >
	  <tr >
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Products</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Item Description</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Quantity</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">price</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Total</td>
		
	  </tr>
	  </thead>
       <tbody>';
	   
	   foreach ($this->cart->contents() as $items)
			{
				$op=$this->cart->product_options($items['rowid']);
				CDN_IMG_URL.'product/'.$items['options']['proimage'];
        $message.='<tr> 
         <td style="width:150px;height:115px" align="center" valign="top"> <a href="" style="margin:0 20px 0 0" title="ItemImage" target="_blank"> <img class="CToWUd" src="'.CDN_IMG_URL.'product/'.$items['options']['proimage'].'" style="margin:0 0 14px 0" border="0" width="100" height="100"> </a> </td> 
         <td style="color:#666;padding:10px 10px 0 10px;" valign="top"> <p  style="font:14px Arial,sans-serif;text-decoration:none;color:#006699" target="_blank">'.$this->pro->pro_name($op['proid']).'-'.$this->pro->pro_unit($op['pro_price_id']).'  </p>  </td> 
         <td style="color:#666;padding:10px 10px 0 10px;" valign="top"> <p  style="font:14px Arial,sans-serif;" target="_blank">'.$items['qty'].'  </p>  </td> 
         
		 <td style="font:14px Arial,san-serif;padding:10px 0 0 0;" align="left" valign="top"> <strong> Rs.'.$items['price'].'</strong> </td> 
		 <td style="font:14px Arial,san-serif;padding:10px 0 0 0;" align="right" valign="top"> <strong> Rs.'.number_format($items['price'] * $items['qty'],'2','.','').'</strong> </td>
        </tr>'; 
			}
			
$message.='       
	   <tr> 
         <td colspan="5" style="border-top:1px solid rgb(234,234,234);padding:0pt 0pt 16px;width:560px"> &nbsp; </td> 
        </tr> 
       <tr> 
         <td colspan="3" style="font:12px/18px Arial,sans-serif;padding:0 10px 0 0;color:#333;width:480px" align="right" valign="top"> Item Subtotal: </td> 
         <td style="font:12px/18px Arial,sans-serif;color:#333;width:85px" align="right" valign="top"> '.$this->cart->total().' </td> 
        </tr>  ';
		
		$message.='</tr>
       
       </tbody>
      </table> </td> 
    </tr> 
   
    <tr bgcolor="#404245"> 
     <td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.lavanaart.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
    </tr> 
   
   </tbody>
  </table>';
					
	//echo $message;exit;				
					
					$pdata['where']=array('paymentTransactionId'=>$txnid,'date'=>date('Y-m-d'),'paymentAmount'=>round($amount));
					$pdata['data'] = array('paymentresponse'=>base64_encode(serialize($_POST)),'paymentStatus'=>"Successful");
					$pdata['table']="paymentinfo";
					$this->CorModel->update($pdata);					
					
						$paramUser['table'] ="user_tbl"; 
						$paramUser['where'] =array(
							"store_key" => STORE_KEY,
							"user_email" => $email
						);	
//print_r()						
						$userDetail = $this->CorModel->getData('customListing',$paramUser);
												
						$paramtran['table'] ="paymentinfo"; 
						$paramtran['where'] =array(
							"paymentTransactionId" => $txnid
						);

								
						$TranDetail = $this->CorModel->getData('customListing',$paramtran);
					
			
			
						$paramordr['table'] ="order_tbl"; 
						$paramordr['where'] =array(
							"ord_id" => $TranDetail->detail[0]->OrderId
						);			
						$OrderDetail = $this->CorModel->getData('customListing',$paramordr);
					
						///for view
						$data['oid'] = $TranDetail->detail[0]->OrderId;
						$data['orderid'] = $OrderDetail->detail[0]->ord_code;
						$data['name'] = $userDetail->detail[0]->user_firstName. " ". $userDetail->detail[0]->user_lastName;
						$data['address'] = $userDetail->detail[0]->user_address;
						$data['pinscode'] =  $userDetail->detail[0]->user_postalCode;
						$data['number'] =  $userDetail->detail[0]->user_contact;
						$data['city'] =  $userDetail->detail[0]->city;
						$data['user_email'] =  $userDetail->detail[0]->user_email;
						//for view 
						//order detail	
						
						$data['order'] = $OrderDetail->detail[0];
						$paramUser['table'] ="order_detail"; 
						$paramUser['where'] =array(
							"ord_id" => $TranDetail->detail[0]->OrderId
						);			
						$orderDetails = $this->CorModel->getData('customListing',$paramUser);
						
						$data['orddetail'] = $orderDetails->detail;
						//order detail	
						
						$param['ref1'] = $userDetail->detail[0]->user_firstName. " ". $userDetail->detail[0]->user_lastName;
						$param['ref2'] = $userDetail->detail[0]->user_address;
						$param['ref3'] = $userDetail->detail[0]->user_postalCode;
						$param['ordnum'] = $OrderDetail->detail[0]->ord_code;
						$param['detail'] = $data;
					$toship = $this->pro->toship($TranDetail->detail[0]->OrderId);
					
						if($toship == "Yes"){
							$result = $this->pro->shipingnew($param);
							$this->pro->printlable($TranDetail->detail[0]->OrderId); 
							//print_r($result);exit;
							
							/* vendor email */
							 
							
							/* vendor email */
							foreach($result['vendetail'] as $v)
							{
							
							 $data['vendor_message']="Hi, you have received new order. Please login to your admin panel to get more details about this order. You need to print shipping bill and stick on your parcel.<br>
							<h4>Your Transaction ID ".$v['awbid'].".</h4>
							<h4>Order Amount " . $v['amount'] . ".</h4>"; 
												   
							
							$this->pro->sendMail($data['vendor_message'],$v['email'],"Lavanaart: Order Details",'nikunj9721@gmail.com');
							}	
						}	
						$this->pro->sendMail($message,$result['email'],"Lavanaart: Order Details",'nikunj9721@gmail.com ');
						
				   }  
				 
				   if($status != "failure")
				   {
					   $this->cart->destroy();
				   }
				  //$this->pro->printlable($param['detail']['orderid']);
				   
					$this->load->templateTwo('ordercomplate',$data);
		
	}
	function responseOrder()
	{
		$id = $this->uri->segment(3);
		
			$status = $_POST["status"];
		
	 	$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$orderid=$_POST["orderid"];

		
			  	

					if(isset($this->session->userdata('userDetail')->email))
					{
						$email = $this->session->userdata('userDetail')->email;
						if($email == ""){
							$email = $this->session->userdata('userDetail')['email'];
						}
					}else{
						$email = $this->session->userdata('userDetail')['email'];
					}
			  // echo $status;exit;
					  //$data['status']=$status;
					  $data['status']="placed successfully";
					  $data['amount']=$amount;
					  $data['txnid']=$txnid;
					  $data['firstname']=$firstname;
					  $data['address1']=$add1;
					 
					$data['message']="
					<table style='width:700px'>
					<tbody>
					    
					    <tr>
					        <th>
					            <img src='http://demo.lavanaart.com/admin/assets/images//store/NVDS1105.jpg'/>
					        </th>
					   </tr>
					   <tr>
					        <th>
					            <h2>Congratulations! Your order has been placed successfully.</h2>
					        </th>
					   </tr>
					   <tr>
					        <td>
					            <h4>Hi ".$firstname.",<h4>
    					        <h4>Thank you for shopping with us! your order has been placed successfully and We will be shipping your order to you soon.</h4>
    					        <h4>Here are your order details: <br/>Your Transaction ID for this transaction is ".$txnid.".<br/> We have received payment of Rs. " . $amount . "/- Your order will soon be shipped.</h4>
					            
					        </td>
					    </tr>
					</tbody>
					</table>";
				$q = $this->db->query("select * from shipping_tbl where order_id='" . $orderid . "'");
		//Emal Code//
		$r = $q->result();	
					 
					$message='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

   <tbody>
   <tr bgcolor="#404245"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
    <tr> 
     
     <td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="" title="Visit www.lavanaart.com" target="_blank"><img class="CToWUd" alt="abassistance" src="http://demo.lavanaart.com/admin/assets/images//store/NVDS1105.jpg" border="0" style="width:100px;"></a> </td> 
     <td style="text-align:right;padding:0px 20px"> 
      <table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
       <tbody>
        
        <tr> 
         <td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif">Shipping Confirmation</span> </td> 
        </tr> 
        <tr> 
         <td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> Order #'. $orderid .' </span> </td> 
        </tr> 
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;margin:15px 20px 0 20px">Hello '. $r[0]->fname .',</p> <p style="margin:4px 20px 18px 20px;width:640px"> We thought you&rsquo;d like to know that we&rsquo;ve dispatched your item(s). Your order is on the way. If you need to return an item from this shipment or manage other orders on www.lavanaart.com. </p> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="padding:0 0px;width:640px"> 
      <table style="border-top:3px solid #2d3741;width:100%" cellpadding="0" cellspacing="0"> 
       <tbody>';
        
		if($r[0]->shipping == "Yes"){
		
		$message='<tr> 
        
         <td style="font:14px Arial,san-serif;padding:11px 18px 14px 18px;width:280px;background-color:#efefef" valign="top"><span style="color:#666">Your package was sent to:</span><br> <p style="margin:2px 0"> <strong>'. $r[0]->fname .' <br>'. $r[0]->ship_straddress .', <br> 
            <u></u>
             '.$r[0]->city_name.','. $r[0]->state_name .'-'. $r[0]->ship_postalCode .'
            <u></u> <br> India <br>(M):-'.$r[0]->ship_contact.'</strong> </p> </td> 
        </tr>'; 
		}else{
			$message='<tr> 
        
         <td style="font:14px Arial,san-serif;padding:11px 18px 14px 18px;width:280px;background-color:#efefef" valign="top"><span style="color:#666">Your package pickup to:</span><br> <p style="margin:2px 0"> <strong>Lavanaart, <br> 513A, Arun Building, 6th floor,<br>opposite VJTI college main gate,</br>H. R. Mahajani Road,Matunga<br>
            <u></u>
            
			Mumbai, Maharastra - 400019
            <u></u> <br> India <br>(O):-022 2600 6185</strong> </p> </td> 
        </tr>'; 
		}
		
      $message=' <tr> 
         <td colspan="2" style="font-size:10px;color:#666;padding:0 10px 20px 10px;line-height:16px;width:640px"> <p style="margin:10px 0 0 0;font:11px/16px Arial,sans-serif;color:#333">  </p> </td> 
        </tr> 
        
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;border-bottom:1px solid #ccc;margin:0 20px 3px 20px;padding:0 0 3px 0"> Shipment Details </p> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="padding:16px 40px;width:640px"> 
      <table cellpadding="0" cellspacing="0" width="100%"> 
	  <thead >
	  <tr >
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Products</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Item Description</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Quantity</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">price</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Total</td>
		
	  </tr>
	  </thead>
       <tbody>';
	   
	   foreach ($this->cart->contents() as $items)
			{
				$op=$this->cart->product_options($items['rowid']);
				CDN_IMG_URL.'product/'.$items['options']['proimage'];
        $message.='<tr> 
         <td style="width:150px;height:115px" align="center" valign="top"> <a href="" style="margin:0 20px 0 0" title="ItemImage" target="_blank"> <img class="CToWUd" src="'.CDN_IMG_URL.'product/'.$items['options']['proimage'].'" style="margin:0 0 14px 0" border="0" width="100" height="100"> </a> </td> 
         <td style="color:#666;padding:10px 10px 0 10px;" valign="top"> <p  style="font:14px Arial,sans-serif;text-decoration:none;color:#006699" target="_blank">'.$this->pro->pro_name($op['proid']).'-'.$this->pro->pro_unit($op['pro_price_id']).'  </p>  </td> 
         <td style="color:#666;padding:10px 10px 0 10px;" valign="top"> <p  style="font:14px Arial,sans-serif;" target="_blank">'.$items['qty'].'  </p>  </td> 
         
		 <td style="font:14px Arial,san-serif;padding:10px 0 0 0;" align="left" valign="top"> <strong> Rs.'.$items['price'].'</strong> </td> 
		 <td style="font:14px Arial,san-serif;padding:10px 0 0 0;" align="right" valign="top"> <strong> Rs.'.number_format($items['price'] * $items['qty'],'2','.','').'</strong> </td>
        </tr>'; 
			}
			
$message.='       
	   <tr> 
         <td colspan="5" style="border-top:1px solid rgb(234,234,234);padding:0pt 0pt 16px;width:560px"> &nbsp; </td> 
        </tr> 
       <tr> 
         <td colspan="3" style="font:12px/18px Arial,sans-serif;padding:0 10px 0 0;color:#333;width:480px" align="right" valign="top"> Item Subtotal: </td> 
         <td style="font:12px/18px Arial,sans-serif;color:#333;width:85px" align="right" valign="top"> '.$this->cart->total().' </td> 
        </tr>  ';
		
		$message.='</tr>
       
       </tbody>
      </table> </td> 
    </tr> 
   
    <tr bgcolor="#404245"> 
     <td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>www.lavanaart.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
    </tr> 
   
   </tbody>
  </table>';
					
	//echo $message;exit;				
					$paramordr['table'] ="order_tbl"; 
					$paramordr['data'] =array(
							"ord_status" => $status,
							"OderType" => "COD"
						);
					$paramordr['where'] =array(
							"ord_id" => $orderid
						);			
						$OrderDetail = $this->CorModel->updateData($paramordr);
							
										
					
						$paramUser['table'] ="user_tbl"; 
						$paramUser['where'] =array(
							"store_key" => STORE_KEY,
							"user_email" => $email
						);	
//print_r()						
						$userDetail = $this->CorModel->getData('customListing',$paramUser);
												
			
						$paramordr['table'] ="order_tbl"; 
						$paramordr['where'] =array(
							"ord_id" => $orderid
						);
						$OrderDetail = $this->CorModel->getData('customListing',$paramordr);
					
						///for view
						$data['oid'] = $orderid;
						$data['orderid'] = $OrderDetail->detail[0]->ord_code;
						$data['name'] = $userDetail->detail[0]->user_firstName. " ". $userDetail->detail[0]->user_lastName;
						$data['address'] = $userDetail->detail[0]->user_address;
						$data['pinscode'] =  $userDetail->detail[0]->user_postalCode;
						$data['number'] =  $userDetail->detail[0]->user_contact;
						$data['city'] =  $userDetail->detail[0]->city;
						$data['user_email'] =  $userDetail->detail[0]->user_email;
						//for view 
						//order detail	
						
						$data['order'] = $OrderDetail->detail[0];
						$paramUser['table'] ="order_detail"; 
						$paramUser['where'] =array(
							"ord_id" => $orderid
						);			
						$orderDetails = $this->CorModel->getData('customListing',$paramUser);
						
						$data['orddetail'] = $orderDetails->detail;
						//order detail	
						
						$param['ref1'] = $userDetail->detail[0]->user_firstName. " ". $userDetail->detail[0]->user_lastName;
						$param['ref2'] = $userDetail->detail[0]->user_address;
						$param['ref3'] = $userDetail->detail[0]->user_postalCode;
						$param['ordnum'] = $OrderDetail->detail[0]->ord_code;
						$param['detail'] = $data;
						$param['orderType'] = "cod";
					 $toship = $this->pro->toship($orderid);
					
						if($toship == "Yes"){
						$result = $this->pro->shipingnew($param);
						
										
						$this->pro->printlable($TranDetail->detail[0]->OrderId); 
						
						/* vendor email */
						foreach($result['vendetail'] as $v)
						{
						
                         $data['vendor_message']="Hi, you have received new order. Please login to your admin panel to get more details about this order. You need to print shipping bill and stick on your parcel.<br>
						<h4>Your Transaction ID ".$v['awbid'].".</h4>
						<h4>Order Amount " . $v['amount'] . ".</h4>"; 
						                       
						
						$this->pro->sendMail($data['vendor_message'],$v['email'],"Lavanaart: Order Details",'nikunj9721@gmail.com');
						}	
						}
						
						$this->pro->sendMail($message,$result['email'],"Lavanaart: Order Details",'nikunj9721@gmail.com');
					
				   if($status != "failure")
				   {
					   $this->cart->destroy();
				   }
				  //$this->pro->printlable($param['detail']['orderid']);
				   
					$this->load->templateTwo('ordercomplate',$data);
		
	}
	function applyPromocode(){
		
		 $amount = $this->cart->total();
		
		$code = $this->uri->segment(3);
		
		$discount = $this->pro->promoCode($amount,$code);

		echo  json_encode($discount);
	}
}
 ?>