<?php
class ProductCart extends CI_Controller 
{
	function index(){
		echo "asdf";
	}
	function addtocart()
	{
		
		$name = $this->pro->slugify($this->input->post('name'));
$this->load->library('cart');
		//$this->cart->destroy();
		$data=array( 'id'  => $this->input->post('ppid'),						
                       'qty'  => $this->input->post('qty'),
                       'price'=> $this->input->post('price'),
                       'urlname' => $name,
                       'name' => $this->input->post('name'),
					    'options' => array('procode' => $this->input->post('pcode'),'proimage' => $this->input->post('pimg'),'proid'=> $this->input->post('id'),'pro_price_id'=> $this->input->post('ppid'),'catid'=>$this->input->post('catid'))
					   );

		$this->cart->insert($data);
		
		 $item=$this->cart->total_items();
		
		header('Content-Type: application/json');
		echo  json_encode($item); 
	}

	function productdetailaddcart()
	{
		
		$id=$this->uri->segment(3);
		$this->load->model("cartmodel");
		$where=array('pro_id'=>$id);
		$data=$this->cartmodel->addcart('product_tbl',$where);
				
		$proid=$data[0]['pro_id'];
		$pqty=$this->input->post('quantity');
		$pid=$this->input->post('unit');
		
		$pwhere=array('pro_price_id'=>$pid);
		$pdata=$this->cartmodel->addcart('product_price_tbl',$pwhere);
		
		$ppid=$pdata[0]['pro_price_id'];
		
	
		$this->load->library('cart');
		$cdata=$this->cart->contents();
		$cdat=array("0"=>"");		
		
		foreach ($this->cart->contents() as $items)
		{
			$cdat[]=$items['id'];
			
		}
		
		if(in_array($pdata[0]['pro_price_id'],$cdat))
			{
			
				
				foreach ($this->cart->contents() as $items)
					{
					
						 if($items['id'] == $pid)
						 {
							
							
							$items['qty'] +=$pqty ;
								
							$this->cart->update($items);
						}
						
					}
							
			}
			else
			{
				$mydata=array( 'id'  => $pdata[0]['pro_price_id'],						
                       'qty'  => $pqty,
                       'price'=> $pdata[0]['pro_seal_price'],
                       'name' => $data[0]['pro_name']." ".$pdata[0]['pro_no_unit']." ".$pdata[0]['pro_unit'],
					    'options' => array('procode' => $data[0]['pro_code'],'proimage' => $data[0]['pro_image'],'proid'=>$data[0]['pro_id'],'pro_price_id'=>$pdata[0]['pro_price_id'])
					   );
				$this->cart->insert($mydata);
				
			}
			$item=$this->cart->total_items();
			
	header('Content-Type: application/json');
			echo  json_encode($item); 

/*
			if($this->uri->segment(5))
			{
			redirect('productdetail/index/'.$id."/".$pid."/".$this->uri->segment(5));
			}
			else
			{
				redirect('productdetail/index/'.$id."/".$pid);
			}*/
	}




	function mycart()
	{    
		//$this->load->library('cart');
		$where=array("menu"=>1,'parent_id'=>0);
		$this->load->model("sermoremodel");
		$data['menu']=$this->sermoremodel->menu('category_tbl',$where);
		$data['bg']=$this->sermoremodel->bg('bgimg');
		$mwhere=array('parent_id'=>0,"menu"=>0);
		$data['more']=$this->sermoremodel->menu('category_tbl',$mwhere);
		
		
		
		/*$id=$this->uri->segment(3);
		$where=array('pro_id'=>$id);
		$this->load->model("cartmodel");
		$data['pro']=$this->cartmodel->addcart('product_tbl',$where);*/
		
		$this->load->library('cart');
		
		  if($this->cart->total_items()== 0)
	  		{
	  		redirect('home/index');
	 		 }
		
		$cn=$this->cart->total_items();
		$data['count']=array("cn"=>$cn);
		$message = $this->session->flashdata('message');
		
		$this->load->view('cart',$data);
	}
	public function updatecart()
	{ 
		if($this->input->post('update_cart_action')){
			$data = $this->input->post();
			
			unset($data['update_cart_action']);		
			
			$this->cart->update($data);
			
		}else if($this->input->post('clear_cart_action')){
			$data = $this->input->post();
			unset($data['clear_cart_action']);
			$clear_cart = array();
			foreach($data as $d){
				$clear_cart[] = array(
					'rowid'=> $d['rowid'],
					'qty'=> 0
				);
			}	
			$this->cart->update($clear_cart);
		}
		
		redirect('/CartDetails');
	}
	function removecart()
	{
		$data=array(
        'rowid'=>$this->input->post('rowid'),
        'qty'=>0
    	);
		
    	$this->cart->update($data);  
			$item=$this->cart->total_items();
			$dd=array("item"=>$item,"total"=>$this->cart->total());
		header('Content-Type: application/json');
			echo  json_encode($dd); 
		
	}
	function item()
	{
		$this->load->library('cart');
		
	}
	
	function shippingaddress()
	{
		if($this->session->userdata('id')=="" && $this->session->userdata('guestid')=="" )
		{
			redirect('login/index/shippingaddress/');
		}
		else if($this->session->userdata('sppingaddid')!="")
		{
			redirect('cart/orderconform/'.$this->session->userdata('sppingaddid'));
		}		
		else
		{
		$where=array("menu"=>1,'parent_id'=>0);
		$this->load->model("sermoremodel");
		$data['menu']=$this->sermoremodel->menu('category_tbl',$where);
		$data['bg']=$this->sermoremodel->bg('bgimg');
		$this->load->library('cart');
		$cn=$this->cart->total_items();
		$data['count']=array("cn"=>$cn);		
		
		
		$uid=$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY);
		$where=array("user_id"=>$uid);
		$this->load->model("cartmodel");
		$data['udata']=$this->cartmodel->userdetail('user_tbl',$where);
		
		$data['country']=$this->sermoremodel->view("country_tbl");
							
		$where=array("country_id"=>105);
		$data['state']=$this->sermoremodel->viewwhere("state_tbl",$where);
		
		$where=array("state_id"=>4);
		$data['city']=$this->sermoremodel->viewwhere("city_tbl",$where);
		
		$gid=$this->session->userdata('guestid');
		$where=array("guest_id"=>$gid);
		$this->load->model("cartmodel");
		$data['guest']=$this->cartmodel->userdetail('guest_user',$where);
		
		$this->load->view('shipping',$data);
		}
	}
	
	function insertshipping()
	{
		$uid=$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY);
		
		$guest_id=$this->session->userdata('guestid');
		
		
		date_default_timezone_set('Asia/Calcutta');
							
		$user_id=$uid;
		$user_firstName=$this->input->post('firstName');
		$user_lastName=$this->input->post('lastName');
		$ship_name=$user_firstName." ".$user_lastName;
		$ship_straddress=$this->input->post("address1");
		$ship_address=$this->input->post("address2");
		$city_id=$this->input->post("city");
		$ship_postalCode=$this->input->post("pincode");
		$state_id="4";
		$country_id="105";
		
		$ship_contact=$this->input->post('mobilePhone');
		$ship_email=$this->input->post('email');
		$ship_status="1";
		$ship_dtadded=date("Y-m-d h:i:s");						
		
		if($user_id == "")
		{
		$idata=array("guest_id"=>$guest_id,"ship_name"=>$ship_name,"ship_straddress"=>$ship_straddress,"ship_address"=>$ship_address,"city_id"=>$city_id,"ship_postalCode"=>$ship_postalCode,"state_id"=>$state_id,"country_id"=>$country_id,"ship_contact"=>$ship_contact,"ship_email"=>$ship_email,"ship_status"=>$ship_status,"ship_dtadded"=>$ship_dtadded);
		}
		else
		{																
		$idata=array("user_id"=>$user_id,"ship_name"=>$ship_name,"ship_straddress"=>$ship_straddress,"ship_address"=>$ship_address,"city_id"=>$city_id,"ship_postalCode"=>$ship_postalCode,"state_id"=>$state_id,"country_id"=>$country_id,"ship_contact"=>$ship_contact,"ship_email"=>$ship_email,"ship_status"=>$ship_status,"ship_dtadded"=>$ship_dtadded);
		}
		$this->load->model("cartmodel");
		$insert_id=$this->cartmodel->addship('shipping_tbl',$idata);
		
			$this->session->set_userdata("sppingaddid",$insert_id);
		redirect('cart/orderconform/'.$insert_id);
	}
	
	function orderconform()
	{
		
		
		$where=array("menu"=>1,'parent_id'=>0);
		$this->load->model("sermoremodel");

		$data['menu']=$this->sermoremodel->menu('category_tbl',$where);
		$mwhere=array('parent_id'=>0,"menu"=>0);
		$data['more']=$this->sermoremodel->menu('category_tbl',$mwhere);
		$data['bg']=$this->sermoremodel->bg('bgimg');
		$this->load->library('cart');
		
		
	  if($this->cart->total_items()== 0)
	  {
	  		redirect('home/index');
	  }
	  
	 
		
		$cn=$this->cart->total_items();
		$data['count']=array("cn"=>$cn);		
		
		$uid=$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY);
		$where=array("user_id"=>$uid);
		$this->load->model("cartmodel");
		$data['udata']=$this->cartmodel->userdetail('user_tbl',$where);
		
		$insert_id=$this->uri->segment(3);
		$where=array('ship_id'=>$insert_id);
		$this->load->model("cartmodel");
		$data['ship']=$this->cartmodel->view('shipping_tbl',$where);
		
				
		$this->load->view('orderconform',$data);
		
	}
	function couponfun()
	{
		
		
			$cup=$this->input->post('cup');
			$amt=$this->input->post('amt');
			$am=number_format($amt,2,'.',',');
			//$cup=45;	
			//$amt=350;
			$this->session->set_userdata('cou_id',$cup);
			$cd=$this->pro->coupon_price($cup,$amt);
		
			if($cd == "no")
			{
				
				$this->session->set_userdata('cou_id',"No");
				$this->session->set_userdata('discount','0');
				$cdta=array('msg'=>'Wrong Coupon Code','dis'=>'0','totamt'=>$am);
				header('Content-Type: application/json');
			echo  json_encode($cdta); 
				
			}
			else
			{
					$this->session->set_userdata('discount',$cd['dis']);
				header('Content-Type: application/json');
			echo  json_encode($cd); 
			}
			
		
	}
	function updateshippingaddress()
	{
		$where=array("menu"=>1,'parent_id'=>0);
		$this->load->model("sermoremodel");
		$data['menu']=$this->sermoremodel->menu('category_tbl',$where);
		$mwhere=array('parent_id'=>0,"menu"=>0);
		$data['more']=$this->sermoremodel->menu('category_tbl',$mwhere);
		$data['bg']=$this->sermoremodel->bg('bgimg');
		$this->load->library('cart');
		$cn=$this->cart->total_items();
		$data['count']=array("cn"=>$cn);
		
		$sid=$this->uri->segment(3);
		$where=array('ship_id'=>$sid);
		$this->load->model("cartmodel");
		$data['sdata']=$this->cartmodel->view('shipping_tbl',$where);
		
		$data['country']=$this->sermoremodel->view("country_tbl");
							
		$where=array("country_id"=>105);
		$data['state']=$this->sermoremodel->viewwhere("state_tbl",$where);
		
		$where=array("state_id"=>4);
		$data['city']=$this->sermoremodel->viewwhere("city_tbl",$where);
		
		$this->load->view('updateshipping',$data);
	}
	function updateorderconform()
	{
		$where=array("menu"=>1,'parent_id'=>0);
		$this->load->model("sermoremodel");
		$data['menu']=$this->sermoremodel->menu('category_tbl',$where);
		$data['bg']=$this->sermoremodel->bg('bgimg');
		$this->load->library('cart');
		$cn=$this->cart->total_items();
		$data['count']=array("cn"=>$cn);		
		
		date_default_timezone_set('Asia/Calcutta');
		$guest_id=$this->session->userdata('guestid');
		
		$ship_id=$this->input->post('sid');				
		$user_id=$this->input->post('uid');
		$user_firstName=$this->input->post('firstName');
		$user_lastName=$this->input->post('lastName');
		$ship_name=$user_firstName." ".$user_lastName;
		$ship_straddress=$this->input->post("address1");
		$ship_address=$this->input->post("address2");
		$city_id=$this->input->post("city");
		$ship_postalCode=$this->input->post("pincode");
		$state_id="4";
		$country_id="105";
		
		$ship_contact=$this->input->post('mobilePhone');
		$ship_email=$this->input->post('email');
		$ship_status="1";
		$ship_dtadded=date("Y-m-d h:i:s");						
		
		if($user_id == "")
		{																
		$idata=array("guest_id"=>$guest_id,"ship_name"=>$ship_name,"ship_straddress"=>$ship_straddress,"ship_address"=>$ship_address,"city_id"=>$city_id,"ship_postalCode"=>$ship_postalCode,"state_id"=>$state_id,"country_id"=>$country_id,"ship_contact"=>$ship_contact,"ship_email"=>$ship_email,"ship_status"=>$ship_status,"ship_dtadded"=>$ship_dtadded);
		}
		else
		{
		$idata=array("user_id"=>$user_id,"ship_name"=>$ship_name,"ship_straddress"=>$ship_straddress,"ship_address"=>$ship_address,"city_id"=>$city_id,"ship_postalCode"=>$ship_postalCode,"state_id"=>$state_id,"country_id"=>$country_id,"ship_contact"=>$ship_contact,"ship_email"=>$ship_email,"ship_status"=>$ship_status,"ship_dtadded"=>$ship_dtadded);
		}
		
		$where=array('ship_id'=>$ship_id);
		$this->load->model("cartmodel");
		$this->cartmodel->updateship('shipping_tbl',$idata,$where);
		
		//$where=array('ship_id'=>$insert_id);
		//$data['ship']=$this->cartmodel->view('shipping_tbl',$where);
		
		redirect('cart/orderconform/'.$this->uri->segment(3));
		
	}
	
	
	public function checkout()
	{	
			if(date('D')=='Sat')
				{
					$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"));
					$dtime= date("M").' '.date("d",$tomorrow).', '.date("Y");
				}
				else
				{
					$this->load->helper('date');
					date_default_timezone_set("Asia/Kolkata");
					$now = date("H:i:s");
										
					if($now < "20:00:00")
					{
						$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
						$dtime= date("M").' '.date("d",$tomorrow).', '.date("Y");
					}
					else
					{
						$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"));
						$dtime= date("M").' '.date("d",$tomorrow).', '.date("Y");
					}
					
                }
		
			$this->load->model("cartmodel");
			date_default_timezone_set('Asia/Calcutta');
		
			$this->load->library('cart');
		
			$guest_id=$this->session->userdata('guestid');
			$ship_id = $this->uri->segment(3);
			$ord_code=$this->pro->ordercode();
			
			$oc=explode('-',$ord_code);
			//date_default_timezone_set('GMT'); 
		$this->load->helper('string');
		$a= random_string('alnum', 5);
			$orduncode=$a."".$oc[1];
			
			$user_id=$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY);
			$user_ip=$this->input->ip_address();
			$ord_date=date("Y-m-d");
			$total_amount=$this->cart->total();
			$total_items=$this->cart->total_items();
			$ship_charge=$this->pro->shipcharge($this->cart->total());
			
			$cou_id=$this->session->userdata('cou_id');
			
			
			$disc=$this->session->userdata('discount');
			
			foreach ($this->cart->contents() as $items)
			{	
		
				$op=$this->cart->product_options($items['rowid']);
				$pro_id=$op['proid'];
				 $this->pro->sellerid($pro_id);
				
				$user[]=$this->pro->sellerid($pro_id);
			}
			
			$sid=array_unique($user);
			$seller_id=implode(",",$sid);
			
			$ord_status="not_confirm";
			$ord_dtadded=date("Y-m-d h:i:s");
			
			if($user_id == "")
			{			
			$data = array("ord_code"=>$ord_code,'ordrndcode'=>$orduncode,'guest_id'=>$guest_id,'user_ip'=>$user_ip,'ord_date'=>$ord_date,'total_amount'=>$total_amount,'total_items'=>$total_items,'ship_charge'=>$ship_charge,'ord_status'=>$ord_status,'ship_id'=>$ship_id,'del_date'=>$dtime,'cou_id'=>$cou_id,'discount'=>$disc,'seller_id'=>$seller_id,'ord_dtadded'=>$ord_dtadded);
		
			if($cou_id!=0)
			{
				$this->db->query("UPDATE `coupon_tbl` SET `cou_status`='0',cou_useddate='".$ord_dtadded."',ship_id='".$ship_id."' where cou_code='".$cou_id."'");
				
			}
			
			}
			else
			{
			$data = array("ord_code"=>$ord_code,'ordrndcode'=>$orduncode,'user_id'=>$user_id,'user_ip'=>$user_ip,'ord_date'=>$ord_date,'total_amount'=>$total_amount,'total_items'=>$total_items,'ship_charge'=>$ship_charge,'ord_status'=>$ord_status,'ship_id'=>$ship_id,'del_date'=>$dtime,'cou_id'=>$cou_id,'discount'=>$disc,'seller_id'=>$seller_id,'ord_dtadded'=>$ord_dtadded);
			if($cou_id!=0)
			{
				$this->db->query("UPDATE `coupon_tbl` SET `cou_status`='0',cou_useddate='".$ord_dtadded."',ship_id='".$ship_id."' where cou_code='".$cou_id."'");
				
			}
			
			}
			$oid=$this->cartmodel->addorder('order_tbl',$data);
			$this->db->query("insert into order_status_detail(ord_id,ord_code,ord_date)value('".$oid."','".$orduncode."','".$ord_dtadded."')");
			
			$this->session->unset_userdata('cou_id');
			$this->session->unset_userdata('discount'); 
			foreach ($this->cart->contents() as $items)
			{	
			 
			 $op=$this->cart->product_options($items['rowid']);
			
			$ord_id=$oid;
			$cart_id=$items['rowid'];
			$pro_id=$op['proid'];
			$pro_price_id=$op['pro_price_id'];
			$pro_price=$items['price'];
			$pro_qty=$items['qty'];
			$total_pro_amount=$items['subtotal'];
			$ord_det_dtadded=date("Y-m-d h:i:s");
			
			
						
			$data = array("ord_id"=>$ord_id,'cart_id'=>$cart_id,'pro_id'=>$pro_id,'pro_price'=>$pro_price,'pro_price_id'=>$pro_price_id,'ord_pro_qty'=>$pro_qty,'total_pro_amount'=>$total_pro_amount,'ord_det_dtadded'=>$ord_det_dtadded);
			
			
			$this->cartmodel->addorder('order_detail',$data);
			$where=array('pro_price_id'=>$pro_price_id);
			 $query = $this->cartmodel->getprice('product_price_tbl',$where);
			 $proqty= $query[0]['pro_qty'];
			 $stockpro=$proqty-$pro_qty;
			
			$pdata = array('pro_qty'=>$stockpro);
			$this->cartmodel->updateprice('product_price_tbl',$pdata,$where);
		}
		
		$q = $this->db->query("select * from shipping_tbl where ship_id='" . $ship_id . "'");
		//Emal Code//
		$r = $q->result();
				
				if(date('D')=='Sat')
				{
					$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"));
					$dtime= date("M").' '.date("d",$tomorrow).', '.date("Y");
				}
				else
				{
					$this->load->helper('date');
					date_default_timezone_set("Asia/Kolkata");
					$now = date("H:i:s");
										
					if($now < "20:00:00")
					{
						$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
						$dtime= date("M").' '.date("d",$tomorrow).', '.date("Y");
					}
					else
					{
						$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"));
						$dtime= date("M").' '.date("d",$tomorrow).', '.date("Y");
					}
					
                }
				
		
		$message='<table style="font:12px/16px Arial,sans-serif;color:#333;background-color:#fff;margin:0 auto; border:1px solid #ccc;" cellpadding="0" cellspacing="0" width="640" > 

   <tbody>
   <tr bgcolor="#e11827"><td colspan="2"><p style="padding:5px 0px 5px 20px; color:#FFFFFF;"></p></td></tr>
    <tr> 
     
     <td style="padding:14px 0px 10px 20px;width:100px;border-collapse:collapse" valign="top"> <a href="https://www.asmitaorganicfarm.com" title="Visit asmitaorganicfarm.com" target="_blank"><img class="CToWUd" alt="asmitaorganicfarm" src="https://www.asmitaorganicfarm.com/admin/assets/images//store/NVDS1105.jpg" border="0" ></a> </td> 
     <td style="text-align:right;padding:0px 20px"> 
      <table style="font:12px/16px Arial,sans-serif;color:#333;margin:0 auto;border-collapse:collapse" cellpadding="0" cellspacing="0"> 
       <tbody>
        
        <tr> 
         <td style="text-align:right;padding:7px 0px 0px 20px;width:490px"> <span style="font:20px Arial,san-serif">Shipping Confirmation</span> </td> 
        </tr> 
        <tr> 
         <td style="text-align:right;padding:0px 0px 5px 20px;width:490px"> <span style="font-size:12px"> Order #<a href="" style="text-decoration:none;color:#006699" target="_blank">'. $orduncode .'</a> </span> </td> 
        </tr> 
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;margin:15px 20px 0 20px">Hello '. $r[0]->ship_name .',</p> <p style="margin:4px 20px 18px 20px;width:640px"> We thought you&rsquo;d like to know that we&rsquo;ve dispatched your item(s). Your order is on the way. If you need to return an item from this shipment or manage other orders, please visit <a href="" style="color:#006699;text-decoration:none" target="_blank">Orders Hestory</a> on asmitaorganicfarm.com. </p> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="padding:0 0px;width:640px"> 
      <table style="border-top:3px solid #2d3741;width:100%" cellpadding="0" cellspacing="0"> 
       <tbody>
        <tr> 
         <td style="font:14px Arial,san-serif;padding:11px 0 14px 18px;width:280px;background-color:#efefef" valign="top"> <span style="color:#666">Your estimated delivery date is: <strong>'.$dtime.'</strong> </span> </td> 
         <td style="font:14px Arial,san-serif;padding:11px 18px 14px 18px;width:280px;background-color:#efefef" valign="top"><span style="color:#666">Your package was sent to:</span><br> <p style="margin:2px 0"> <strong>'. $r[0]->ship_name .' <br>'. $r[0]->ship_straddress .' <br>'. $r[0]->ship_address .', <br> 
            <u></u>
             '. $this->pro->cityname($r[0]->city_id) .','. $this->pro->statename($r[0]->state_id) .'-'. $r[0]->ship_postalCode .'
            <u></u> <br> India <br>(M):-'.$r[0]->ship_contact.'</strong> </p> </td> 
        </tr> 
        <tr> 
         <td colspan="2" style="font-size:10px;color:#666;padding:0 10px 20px 10px;line-height:16px;width:640px"> <p style="margin:10px 0 0 0;font:11px/16px Arial,sans-serif;color:#333">  </p> </td> 
        </tr> 
        
       </tbody>
      </table> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="width:640px"> <p style="font:18px Arial,sans-serif;color:#cc6600;border-bottom:1px solid #ccc;margin:0 20px 3px 20px;padding:0 0 3px 0"> Shipment Details </p> </td> 
    </tr> 
    <tr> 
     <td colspan="2" style="padding:16px 40px;width:640px"> 
      <table cellpadding="0" cellspacing="0" width="100%"> 
	  <thead >
	  <tr >
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Products</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Item Description</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Quantity</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">price</td>
		<td style="background-color:#fcfbfa;border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;padding:10px 10px;text-transform:uppercase; font-size:13px; font-family:helvetica_condensedbold;">Total</td>
		
	  </tr>
	  </thead>
       <tbody>';
	   
	   foreach ($this->cart->contents() as $items)
			{
				$op=$this->cart->product_options($items['rowid']);
				
        $message.='<tr> 
         <td style="width:150px;height:115px" align="center" valign="top"> <a href="" style="margin:0 20px 0 0" title="ItemImage" target="_blank"> <img class="CToWUd" src="'.$this->pro->view_img_path().'productimg/'.$this->pro->pro_img($op['proid']).'" style="margin:0 0 14px 0" border="0" width="100" height="100"> </a> </td> 
         <td style="color:#666;padding:10px 10px 0 10px;" valign="top"> <p  style="font:14px Arial,sans-serif;text-decoration:none;color:#006699" target="_blank">'.$this->pro->pro_name($op['proid']).'-'.$this->pro->pro_unit($op['pro_price_id']).'  </p> <br> <span style="font:12px Arial,sans-serif"> Product-code: '.$this->pro->pro_code($op['proid']).' </span><br>
		 <span style="font:12px Arial,sans-serif"> Sold by asmitaorganicfarm </span> </td> 
         <td style="color:#666;padding:10px 10px 0 10px;" valign="top"> <p  style="font:14px Arial,sans-serif;" target="_blank">'.$items['qty'].'  </p>  </td> 
         
		 <td style="font:14px Arial,san-serif;padding:10px 0 0 0;" align="right" valign="top"> <strong> Rs.'.$items['price'].'</strong> </td> 
		 <td style="font:14px Arial,san-serif;padding:10px 0 0 0;" align="right" valign="top"> <strong> Rs.'.number_format($items['price'] * $items['qty'],'2','.','').'</strong> </td>
        </tr>'; 
			}
			
$message.='       
	   <tr> 
         <td colspan="5" style="border-top:1px solid rgb(234,234,234);padding:0pt 0pt 16px;width:560px"> &nbsp; </td> 
        </tr> 
       <tr> 
         <td colspan="3" style="font:12px/18px Arial,sans-serif;padding:0 10px 0 0;color:#333;width:480px" align="right" valign="top"> Item Subtotal: </td> 
         <td style="font:12px/18px Arial,sans-serif;color:#333;width:85px" align="right" valign="top"> '.$this->cart->total().' </td> 
        </tr>        
       <tr>
	   	  <td colspan="3" style="font:12px/18px Arial,sans-serif;padding:0 10px 0 0;color:#333;width:480px" align="right" valign="top"> Discount: </td>
		  <td style="font:12px/18px Arial,sans-serif;color:#333;width:85px" align="right" valign="top"> '.number_format($disc,2,'.','').' </td> 		 
        <tr>
		 <tr> 
         <td colspan="3" style="font:12px/18px Arial,sans-serif;padding:0 10px 0 0;color:#333;width:480px" align="right" valign="top"> Shipping &amp; Handling: </td> 
         <td style="font:12px/18px Arial,sans-serif;color:#333;width:85px" align="right" valign="top"> '.$this->pro->shipcharge($this->cart->total()).' </td> 
        </tr>  
         <td colspan="3" style="font:14px Arial,sans-serif;padding:10px 10px 10px 0;color:#333;width:480px" align="right" valign="top"> Shipment Total: </td> ';
		 if($this->cart->total()>1000)
		 {	 
        	$message.='<td style="color:#333;font:14px Arial,sans-serif;padding:10px 0 5px 0;color:#333;width:85px" align="right" valign="top"> <strong> '.number_format($this->cart->total()-$disc, 2, '.', '').' </strong> </td>'; 
		 }
		 else
		 {
			 $message.='<td style="color:#333;font:14px Arial,sans-serif;padding:10px 0 5px 0;color:#333;width:85px" align="right" valign="top"> <strong>'.number_format($this->cart->total()-$disc+100, 2, '.', '').'</strong> </td> ';
		
		 }
		$message.='</tr>
       
       </tbody>
      </table> </td> 
    </tr> 
    <tr bgcolor="#43474A"> 
     <td colspan="2" style="padding:0 20px;line-height:22px;width:640px"> <p style="color:#ccc;padding:20px 0 0 0"> If you need further assistance with your order, please visit <a href="" style="color:#e11827;text-decoration:none" target="_blank">Customer Service</a>. </p> </td> 
    </tr> 
    <tr bgcolor="#43474A"> 
     <td colspan="2" style="padding:0 20px 0 20px;line-height:22px;width:640px"> <p style="margin:10px 0;padding:0 0 20px 0;color:#eaeaea">We hope to see you again soon!<br> <span style="font:14px Arial,san-serif"> <strong>asmitaorganicfarm.com</strong> </span> </p> <p style="font-size:10px;color:#fff;padding:0 20px 20px 20px;line-height:16px;width:640px"><strong>Note: </strong>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</p></td> 
    </tr> 
   
   </tbody>
  </table>';
		
		$this->sendMail($message,$ship_id);
		
/*****************Mobile sms ******************/
	$mno=$this->pro->mobile($ship_id);
	
	$this->load->library('mobile');
	$oldcnt= $this->mobile->smsinfo(); 
	$txtmsg="Dear ".$this->pro->shipfullname($ship_id).", Thanks for placing order no ".$orduncode.". EST DELIVERY DATE ".$dtime.". Check mail for brief details. Regards www.abassistance.in. ";
	$rs= $this->mobile->sms($mno,$txtmsg);

	$rsi= $this->mobile->smsinfo();

$rr=$rsi[0];
$d=explode(" ",$rr); 
$rr2=$oldcnt[0];
$d2=explode(" ",$rr2);

	
	if($d2!=$d)
	{
		
		$smsdata=$this->db->query("insert into smsdetail (ordcode,ship_id,mobileno,status,smscont)value('".$orduncode."','".$ship_id."','".$mno."','1','".$txtmsg."')");
		$smsdetail=$this->db->query("update smsinfo set remainsms='".$d[0]."'");
	}
	else
	{
		$smsdata=$this->db->query("insert into smsdetail (ordcode,ship_id,mobileno,status,smscont)value('".$orduncode."','".$ship_id."','".$mno."','0','".$txtmsg."')");
		$smsdetail=$this->db->query("update smsinfo set remainsms='".$d[0]."'");
	}
	
/*****************Mobile sms ******************/			
		$this->cart->destroy();
		if($this->session->userdata('guestid'))
		{
			$this->session->sess_destroy();
		}
		
		
		
		
		redirect('cart/thanksorder/'.$ord_id);
		
	}
	function sendMail($message,$ship_id)
{
	
$email=$this->pro->shipemail($ship_id);

		$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'mail.asmitaorganicfarm.com',
				'smtp_port' => 587,
				'smtp_user' => 'emailacc@asmitaorganicfarm.com', // change it to yours
				'smtp_pass' => 'Emailacc@123', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);
			ini_set('SMTP','mail.asmitaorganicfarm.com');
			ini_set('smtp_port',587);
			
			
			
        $this->load->library('email', $config);
      $this->email->set_newline("\r\n");
      $this->email->to($email); // change it to yours
          $this->email->from('noreply@asmitaorganicfarm.com', 'Asmita Organic Farm');// change it to yours
      $this->email->subject('New Order From Asmita Organic Farm');
       $this->email->message($message);
	  $this->email->send();
    

}
function thanksorder()
{
		$oid=$this->uri->segment(3);
		$where=array("menu"=>1,'parent_id'=>0);
		$this->load->model("sermoremodel");
		$data['menu']=$this->sermoremodel->menu('category_tbl',$where);
		$mwhere=array('parent_id'=>0,"menu"=>0);
		$data['more']=$this->sermoremodel->menu('category_tbl',$mwhere);
		$data['bg']=$this->sermoremodel->bg('bgimg');
		$this->load->library('cart');
		$cn=$this->cart->total_items();
		$data['count']=array("cn"=>$cn);

			$this->load->model("cartmodel");
			$data['detail']=$this->cartmodel->order_detail($oid);
			$data['product']=$this->cartmodel->product_detail($oid);
			
			$this->load->view("thanks_for_ordder",$data);
}
}?>