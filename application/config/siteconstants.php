<?php

defined('BASEPATH') OR exit('No direct script access allowed');
define('PANEL_NAME','adminpanel');
define('MSG_LOGIN_EXPIRED', "Your login session is expired, please re-login");
define('MSG_RIGHTS','You are not authorised to view this page');
define('MSG_INVALID_USER','Incorrect E-mail or Password...Please Try Again');
define('MSG_LOGOUT','Logged Out from Admin Area');
define('MSG_STATUS','Record(s) Status Updated Successfully');
define('MSG_STATUS_ERR','Cannot Update Status, Please Try Again');
define('MSG_ADD','Record added successfully.');
define('MSG_ADD_ERR','Cannot Add, Please Try Again');
define('MSG_UPDATE','Record updated successfully.');
define('MSG_UPDATE_ERR','Cannot Update, Please Try Again');
define('MSG_DELETE','Record(s) deleted successfully.');
define('MSG_DELETE_ERR','Cannot Delete, Please Try Again');
define('MSG_EMAIL_FOUND','E-mail address already exists.');
define('MSG_DATE_COMP','End date should be greater then start date.');
define('MSG_ALREADY_EXISTS','Record Already Exists.');
define('MSG_TOKEN_EXPIRED','Token was expired or not match.');
define('MSG_SUCCESS_EXPORT','Records Exported Successfully!');
define('MSG_SUCCESS_EXPORT_ERR','Error : Records Not Exported');
define('MSG_DELETE_IMAGE','Image deleted successfully.');
define('MSG_DELETE_IMAGE_ERR','Cannot Delete Image, Please Try Again.');
define('ADMIN_MAIL','nikunj.qualdev@gmail.com');

// CATEGORIES	
	

	define('CATEGORY_LARGE_IMG_WIDTH','500');
	define('CATEGORY_LARGE_IMG_HEIGHT','250');
	define('CATEGORY_THUMB_IMG_WIDTH','250');
	define('CATEGORY_THUMB_IMG_HEIGHT','250');

?>
