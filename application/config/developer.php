<?php
defined('BASEPATH') OR exit('No direct script access allowed');

echo site_url();exit;
// PATH
	define('SITE_URL',$site_url);  
	define('SECURE_URL',$secure_url);  
	define('INSECURE_URL',$insecure_url);  
	
	// DIR
	define('DIR_LOGS', DIR_ROOT . 'logs/'); 
	define('DIR_LIB', DIR_ROOT. 'lib/'); 	
	define('DIR_LIB_URL', SITE_URL. 'lib/'); 	
	define('DIR_CONFIGS', DIR_ROOT. 'configs/'); 	
	define('DIR_CLASS', DIR_ROOT . "classes/");
	define('DIR_FUNCTION', DIR_ROOT . "functions/");
	define('DIR_SMARTY', DIR_ROOT . "smarty/");
	define('DIR_SCRIPT',DIR_ROOT . "script/"); 
	define('DIR_TEMPLATE',DIR_ROOT . "templates/");
	define('DIR_CACHE',DIR_ROOT . "cache/");
	


?>
