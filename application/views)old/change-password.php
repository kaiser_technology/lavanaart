 <section class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <div class="col-main col-sm-9 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>Change Password</h2>
            </div>
				<div class="account-login">
          <div class="box-authentication new-customer-box Account Page ">
          
          <form method="post" id="regform">
                <div class="row">
                  <div class="col-xs-12">
                   <?php if(isset($msg)){ echo '<label id="email-error" class="error" for="email">'.$msg.'</lebel>'; }?>
                  </div>
				  <div class="row">
		         <div class="col-sm-12">
                <label>Old Password:</label>
                <div class="input-text">
			
                  <input type="password" name="opass" id = "opass" value="" class="form-control">
                </div>
              </div>
              <div class="col-sm-12">
                <label>New Password:</label>
                <div class="input-text">
			
                  <input type="password" name="npass" id = "npass" value="" class="form-control">
                </div>
              </div>
              <div class="col-sm-12">
                <label>Confirm Password:</label>
                <div class="input-text">
                  <input type="password" name="cpass" id="cpass" value="" class="form-control">
                </div>
              </div>
				  </div>
				 
				  
                  <div class="col-xs-12">
                    <!--<div class="billing-checkbox">
                    <label class="inline" for="rememberme">
                      <input type="checkbox" value="forever" id="rememberme" name="rememberme">
                      Sign up for our newsletter! </label></div>-->
                    <div class="submit-text">
						<input type="hidden" name="email" value="<?php echo ucwords($this->session->userdata('userDetail')->email);?>" />
						<input type="hidden" name="update" value="update" />
                      <button class="button"><i class="fa fa-user"></i>&nbsp; <span>Update</span></button>
                    </div>
                  </div>
                </div>
              </form>
              
         
       
          </div>
        </div>
    
           </div>
        </div>
        <aside class="right sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              <ul>
                <li><a href="<?php echo ADMIN_URL."account" ;?>">Account Dashboard</a></li>
                <li ><a href="<?php echo ADMIN_URL."account/profile" ;?>">Account Information</a></li>
                <li><a href="<?php echo ADMIN_URL."account/orders" ;?>">My Orders</a></li>
                <li class="current"><a href="javascript:void(0)">Change Password</a></li>
              </ul>
            </div>
          </div>
          
        </aside>
      </div>
    </div>
  </section> <script src="http://navavyapar.com/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#regform").validate({
		rules: {
			opass: {
				required: true,
				
			},
			npass: {
				required: true,
				
			},
			cpass: {
				required: true,
				equalTo : npass
			}
		},
		messages: {
			
			opass: {
				required: "Field Required",
				
			},
			npass: {
				required: "Field Required",
				
			},
			cpass: {
				required: "Field Required",
				equalTo : "Please Enter Same Password as above"
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>