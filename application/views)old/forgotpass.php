<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
           
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="page-content">
	  
        <div class="account-login">
		
          <div class="box-authentication">
		  <?php 
			
			  if(isset($message) && $message !=""){
			  echo '<label id="email-error" class="error" for="email">'.$message.'</label>';
			  }else{
			  ?>
		  <form method="post" id="passform" >
            <h4>Forgot Password </h4>
            <p class="before-login-text">Welcome back! Sign in to your account</p>
            <label for="emmail_login"> Please enter your registered email id. <span class="required">*</span></label>
            <input id="uemail" name="uemail" type="text" class="form-control">
			<?php echo form_error('uemail', '<label id="email-error" class="error" for="email">', '</label>'); ?>
            <br>
            <button class="button" type="submit"><i class="fa fa-lock"></i>&nbsp; <span>Process</span></button>
			
            <!--<label class="inline" for="rememberme">
              <input type="checkbox" value="forever" id="rememberme" name="rememberme">
              Remember me </label>-->
			  </form>
			  <?php }?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
    <script src="http://navavyapar.com/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#passform").validate({
		rules: {
			
			uemail: {
				required: true,
				email:true
			}
		},
		messages: {
			
			
			uemail: {
				required: "Field Required",
				email : "Enter Valid Email"
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>