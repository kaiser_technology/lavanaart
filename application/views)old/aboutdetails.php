  <!-- Main Container -->
	<section class="main-container col1-layout">
		<div class="main container">
			<div class="row">
				<section class="col-main col-sm-12">
                    <div id="contact" class="page-content page-contact">
						
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">
								<!--h3 class="page-subheading">100% Organic produce harvested using Natural Farming Methods</h3-->
                                <!--p>“Organic agriculture is a production system that sustains the health of soils, ecosystems and people. It relies on ecological processes, biodiversity and cycles adapted to local conditions, rather than the use of inputs with adverse effects. Organic agriculture combines tradition, innovation and science to benefit the shared environment and promote fair relationships and a good quality of life for all involved…”</br></br>Asmita Group is committed to nature, clean environment and health of the people and added one innovative steps by opting ‘Organic farming technique’ in its multi locational farms in the state of Maharashtra under the name ‘Asmita Organic Farm’ having vast range of agro produces through Organic farming technique, it involves cultivation of plants and rearing of animals in natural ways. This process involves the use of biological materials, avoiding synthetic substances to maintain soil fertility and ecological balance thereby minimizing pollution and wastage.   </br></br>Fresh Fruits, Vegetables, Staple Foods and pulses, cultivated/produces at ‘Asmita Organic Farm’ fields are loaded with nutrients such as vitamins, enzymes, minerals and other micro-nutrients compared to those from conventional farms. It relies on ecologically balanced agricultural principles like crop rotation, green manure, organic waste, biological pest control, mineral and rock additives.</p-->
								<p>Asmita Farms has been growing organic products for 1 year, after successfully getting our organic certification from Biocert and PGS – India Green. We grow food grains, pulses and spices and produce Dairy products.</p>
								
								<p>We cultivate and grow food grains, pulses and spices as per the 100% Organic ZBNF (Zero Budget Natural Farming) methods. </p>
								<p>Asmita Group is committed to nature, clean environment and health of the people and has added one innovative steps by opting ‘Organic Farming Technique’ in its multi-locational farms in the state of Maharashtra under the name ‘Asmita Organic Farms’ having vast range of agro produces through the Organic Farming Technique, it involves cultivation of plants and rearing of animals in natural ways. This process involves the use of biological materials, avoiding synthetic substances to maintain soil fertility and ecological balance thereby minimizing pollution and wastage. </p>
								
								<h3 class="page-subheading">Product List</h3>
								<ol>
									<li>Rice Flour: Rice flour is a form of flour made from finely milled rice. It is distinct from rice starch; Rice flour is also used as a thickening agent in recipes that are refrigerated or frozen since it inhibits liquid separation.</li>
									<li>Kolam Rice: Kolam Rice is used for preparing various dishes in the kitchen. It is also used as the main ingredient with dishes like fish, poultry, meat, etc. This rice is tested on different stages of quality parameters by our quality controllers to assure its quality and long shelf life.</li>
									<li>Kolam Rice (Sugandhi):</li>
									<li>Kabuli Chana:	White Chickpeas or Kabuli Chana has a lovely nutty flavor. It is used to make curries, added to salads and even in dips. Kabuli Chana is especially popular in North India. It must be soaked for several hours before cooking as it is a "very hard" bean.</li>
									<li>Brown Chana:	It is a smaller version of the regular pale yellow chickpea. Although it has a brown skin, it remains yellow underneath and has a nutty flavor and starchy texture when cooked. It’s a very versatile legume</li>
									<li>Desi Brown Chana: Desi means 'Country' or 'Local', its other names include Kala Chana or Chholaa bootre. Desi chana can be black, green or speckled. This variety is hulled and split to make Chana Dal.</li>
									<li>Flax Seeds: Flax seed is a rich source of healthy fat, antioxidants, and fiber. The seeds contain protein, lignans, and the essential fatty acid alpha-linoleic acid, also known as ALA or Omega-3. The nutrients in flax seed may help lower the risk of diabetes, cancer, and heart disease. For this reason, it is sometimes thought of as a functional food, a food that can be consumed to achieve health purposes.</li>
									<li>Pure Desi Gir Cow Ghee: The ghee from the indigenous breed of Gir cows has curative properties. It helps rid the body of toxins by lubricating the colon and thus leads to tissue rejuvenation. A daily supplement of Gir cow ghee helps to keep joints supple and takes care of minor common ailments such as insomnia.</li>
								</ol>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">
								<img src="<?php echo base_url('assets/images/about_bottom.JPG');?>" alt="about us" / >
							</div>
						</div>
						
					</div>
				</section>
			</div>
		</div>
	</section>
  <!-- Main Container End -->	