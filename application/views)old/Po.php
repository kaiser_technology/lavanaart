<!-- Main Container -->
	<section class="main-container col1-layout">
		<div class="main container">
			<div class="row">
				<section class="col-main col-sm-12">
                    <div id="contact" class="page-content page-contact">
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">								
								<h3 class="page-subheading">PAYMENT OPTIONS</h3>
								<p>
								Payment Policy:</br>
								Payment can be done via</br>
								i.	Credit Cards,</br>
								ii.	Debit Cards</br>
								iii. Net Banking. </br></br>
								 
								1.	We accept COD (Cash on Delivery) only for Mumbai, Thane and Navi Mumbai. </br>
								2.	Our payment system online is totally safe and hack proof. </br>
								3.	None of the card details are shared with any third party. </br>
								4.	We don’t charge any additional transaction fee on card payments. </br>
								5.	No EMI facility is available. </br></br>
								 
								Credit Card/Debit Card Details</br>
								The Credit Card/Debit Card details provided to Asmita Organic Farm by the customer will not be shared with any third party. In case of fraudulent matters, the company holds the right to share the details with the concerned body for verification. The customer shall not under any circumstance use a credit/debit card that is not lawfully owned by him. The customer is hence requested to provide only valid and accurate card details. </br></br>
								 
								Fraudulent Transactions</br>
								If any fraud transactions are carried by any of the customers on the website, Asmita Organic Farm reserves the right to recover all the losses and expenses incurred like lawyer fees, collection charges, etc from the concerned party. The company also holds the right to take legal action against such person. </br></br>
								 
								Offers & Discounts: </br>
								Discounts/Coupon/Voucher Codes</br>
								As a privilege, we provide coupon codes to our customers at regular intervals. Here are a few things to keep in mind while enjoying the benefits of coupon codes: </br>
								1.	Coupon codes cannot be clubbed with any other on-going offers in the store or on the website</br>
								2.	Coupon codes will be valid only till the date mentioned in the email</br>
								3.	One coupon code can be used only once. </br>
								4.	Coupon codes cannot be used by only one customer i.e., the same code will not be valid to your family or friends</br>
								5.	Asmita Organic Farm can change the terms and conditions related to any available coupon codes on the website without any prior intimation</br>
								6.	Shipping rules will be applicable to every product purchased using coupon codes</br></br>
								 
								NEFT Details: </br>
								1.	Account Name: Asmita Supermarkets Pvt. Ltd. </br>
								2.	Account No: 03582320000028</br>
								3.	IFSC: HDFC0000358 MICR :400240047</br>
								4.	Bank Name: HDFC</br>
								5.	Account Branch: Mira Road</br>
								</p>
								<br/>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
  <!-- Main Container End -->	
								
	