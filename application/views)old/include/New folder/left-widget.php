<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="index.html">
                <img src="images/profile.jpg" class="img-circle m-b" alt="logo">
            </a>

            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase">Chaitanya Mehta</span>

                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <small class="text-muted">Director <b class="caret"></b></small>
                    </a>
                    <!--<ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="analytics.html">Analytics</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>-->
                </div>


                <div id="sparkline1" class="small-chart m-t-sm"></div>
                <div>
                    <h4 class="font-extra-bold m-b-xs">
                        $260 104,200
                    </h4>
                    <small class="text-muted">Your income from the last year in sales product X.</small>
                </div>
            </div>
        </div>
		<!-- #menu -->
		   <ul class="nav" id="side-menu">
		   <?php 
		   if(isset($menu)){
			   foreach($menu as $cmenu)
				{
					$class="";
					if($this->uri->segment(1) == $cmenu['act_name'])
					{
						$class="active";
					}
				   // print_r($cmenu);exit;
				   $rol = explode(',',$cmenu['role_id']);
					if(in_array($session_data['rolid'],$rol))
					{
			   ?>
			  <li class="<?php echo $class;?>" >
				 <a href="<?php echo ADMIN_URL.$cmenu['menu_link'];?> ">
				 <i class="fa <?php echo $cmenu['icon']; ?>"></i>
				 <span class="link-title">&nbsp;<?php echo $cmenu['menu_name']; ?></span>
				 </a>
					<?php
					  $query1 = $this->db->query("select * from nv_menu_tbl where parent_menu_id='".$cmenu['id']."'");
					  if($query1->num_rows() > 0)
					  {
							$submenu = $query1->result_array();
							echo '<ul class="nav nav-second-level">';

							foreach($submenu as $sub)	
							{

					 ?>
						<li  ><a href="<?php echo ADMIN_URL.$sub['menu_link'];?>"><i class="fa <?php echo $sub['icon'];?>"></i><span class="nav-label"> <?php echo $sub['menu_name'];?></span></a></li>
					 <?php
							 }
					 echo '</ul>';
					 }
					 ?>
			
			  </li> <?php 
					}
				}
		   }?>
		   </ul>
    </div>
</aside>