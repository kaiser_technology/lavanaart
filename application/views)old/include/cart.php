<script type="text/javascript">
	function addToCart(pro_id,pro_seal_price,pro_name,pro_price_id,pro_code,pro_image,qty) {			
		$.ajax({
			type : "POST", 
			url : "<?php echo base_url();?>cart/addtocart",
			data: {
				'id':pro_id,
				'price': pro_seal_price,
				'name': pro_name,
				'ppid': pro_price_id,
				'pcode':pro_code,
				'pimg':pro_image,
				'qty' : qty
			},
			success:function(state){
				
				$('.cart-num').text(state);
				alert("One product added to cart");				
			}
		});

	}
	function buynow(pro_id,pro_seal_price,pro_name,pro_price_id,pro_code,pro_image,qty) {			
		$.ajax({
			type : "POST", 
			url : "<?php echo base_url();?>buynow/index",
			data: {
				'id':pro_id,
				'price': pro_seal_price,
				'name': pro_name,
				'ppid': pro_price_id,
				'pcode':pro_code,
				'pimg':pro_image,
				'qty' : qty
			},
			success:function(state){
				
				//$('.cart-num').text(state);
				window.location.href ="<?php echo base_url();?>"+state.url;
							
			}
		});

	}
	function removeCart(rowid) {			
		$.ajax({
			type : "POST", 
			url : "<?php echo base_url();?>cart/removecart",
			data: {
				'rowid':rowid
			},
			success:function(state){
				
				$('#'+rowid).hide();
				$('.cart-number').text(state.item);	
				$('.cart-num').text(state.item);
				$('.cart-total-amt').text(state.total);	
					
			}
		});

	}

</script>
