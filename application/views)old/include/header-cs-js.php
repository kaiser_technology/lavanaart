	<link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/dl-icon.css">
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/plugins.css">
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/revoulation.css">
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/main.css">

	<!-- jquery js --> 
	<script src="<?php echo base_url('assets/js/vendor/modernizr-2.8.3.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/vendor/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/plugins.js');?>"></script>
	<script src="<?php echo base_url('assets/js/ajax-mail.js');?>"></script>
	<script src="<?php echo base_url('assets/js/main.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/jquery.themepunch.tools.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/jquery.themepunch.revolution.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.actions.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.carousel.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.kenburn.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.layeranimation.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.migration.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.navigation.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.parallax.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.slideanims.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation/extensions/revolution.extension.video.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/revoulation.js');?>"></script>
	
