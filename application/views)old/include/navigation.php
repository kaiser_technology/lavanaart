<!-- Header Area Start -->
        <header class="header header-fullwidth header-style-3">
            <div class="header-inner">
                <div class="header-top">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <span class="header-text">Welcome Amit</span>
                            </div>
                            <div class="col-6 text-right">
                                <div class="header-component">
                                    <div class="header-component__item header-component__language">
                                        <?php 
										if(isset($this->session->userdata('userDetail')->user_name)){?>
										<ul class="header-component__menu">
                                            <li><a href="<?php echo ADMIN_URL."account" ;?>">Welcome <?php echo ucwords($this->session->userdata('userDetail')->user_name);?> </a></li>
											<li><a href="<?php echo ADMIN_URL."account/logout";?>">Log Out</a></li>						
                                        </ul>
										<?php }else {?>
                                    </div>
                                    <div class="header-component__item header-component__currency">
                                        <ul class="header-component__menu">
                                            <li><a href="<?php echo ADMIN_URL."register" ;?>">Sign Up</a></li>
											<li><a href="<?php echo ADMIN_URL."login";?>">Log In</a></li>
                                        </ul>
										<?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-middle">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-4 col-lg-5 d-lg-block d-none">
                                <!-- Social Icons Start Here -->
                                <ul class="social social-medium mb--20">
                                    <li class="social__item">
                                        <a href="https://www.facebook.com/" class="social__link">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="https://www.twitter.com/" class="social__link">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>

                                    <li class="social__item">
                                        <a href="https://www.pinterest.com/" class="social__link">
                                            <i class="fa fa-pinterest-p"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="https://www.linkedin.com/" class="social__link">
                                            <i class="fa fa-linkedin-square"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- Social Icons End Here -->

                                <!-- Contact Info Start Here -->
                                <div class="header-contact-info">
                                    <div class="header-contact-info__item">
                                        <span>24/7 HOTLINE</span>
                                        <span>(+91) 022 2600 6185</span>
                                    </div>
                                </div>
                                <!-- Contact Info End Here -->
                            </div>
                            <div class="col-xl-4 col-lg-2 col-4 text-lg-center">
                                <a href="index.html" class="logo-box">
                                    <figure class="logo--normal">
                                        <img style="width:100px" href="<?php echo ADMIN_URL; ?>" src="<?php echo CDN_IMG_URL."/store/".STORE_KEY.".jpg";?>" alt="Sayonara">
                                    </figure>
                                </a>
                            </div>
                            <div class="col-xl-4 col-lg-5 col-8">
                                <div class="header-middle-right">
                                    <div class="searchform-wrapper d-none d-lg-block">
                                        <form action="<?php echo base_url("searchresult/index");?>" method = "post" id="searcform" class="searchform searchform-2">
                                            <input type="text" class="searchform__input" id="search2" name="search" placeholder="Search Here...">
                                            <button type="submit" class="searchform__submit">
                                                <i class="dl-icon-search1"></i>
                                            </button>
                                        </form>	
                                    </div>
									
                                    <ul class="header-toolbar text-right">                                        
                                        <li class="header-toolbar__item">
                                            <a href="<?php echo ADMIN_URL."Cartdetails/index/" ;?>" class="mini-cart-btn toolbar-btn">
                                                <i class="dl-icon-cart4"></i>
                                                <sup class="mini-cart-count"><?php if( $this->session->userdata('buynw') != "yes"){ echo $this->cart->total_items(); } else{ echo "0"; }?></sup>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bottom fixed-header">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-lg-2">
                                <!-- Logo Start Here -->
                                <a href="index.html" class="sticky-logo">
                                    <figure class="logo--normal"> 
                                        <img src="assets/img/logo/m01-logo.png" alt="Logo">   
                                    </figure>
                                </a>
                                <!-- Logo End Here -->
                            </div>
                            <div class="col-lg-12">
                                <!-- Main Navigation Start Here -->
                                <nav class="main-navigation">
                                    <ul class="mainmenu mainmenu--2 mainmenu--centered">
										<li class="mainmenu__item">
                                            <a href="index.html" class="mainmenu__link">
                                                <span class="mm-text">Home</span>
                                            </a>
                                        </li>
										<li class="mainmenu__item">
                                            <a href="about-us.html" class="mainmenu__link">
                                                <span class="mm-text">About</span>
                                            </a>
                                        </li>
										<?php foreach($main_category[0] as $mkey => $menu){							
										?>
										<li class="mainmenu__item menu-item-has-children">
                                            <a href="shop-sidebar.html" class="mainmenu__link">
                                                <span class="mm-text"><?php  echo $menu->cat_name;?></span>
												<!--<span class="tip">Hot</span>-->
                                            </a>
											
                                            <ul class="megamenu four-column">											
                                                <?php if (array_key_exists('sub', $menu)) { 
													foreach($menu->sub as $level1){
												?>
												<li>
                                                    <a class="megamenu-title" href="<?php echo base_url();?>category/index/<?php echo $level1->cat_id;?>">
                                                        <span class="mm-text"><?php echo $level1->cat_name;?></span>
                                                    </a>
													
                                                    <ul>
														<?php if (array_key_exists('sub', $level1)) { 
														foreach($level1->sub as $level2){
														?>
                                                        <li>
                                                            <a href="<?php echo base_url();?>category/index/<?php echo $level2->cat_id;?>">
                                                                <span class="mm-text"><?php echo $level2->cat_name;?></span>
                                                            </a>
                                                        </li> 
														<?php
																}
															}
														?>														
                                                    </ul>													
                                                </li>
												<?php
														}
													}
												?>                                                
                                            </ul>											
                                        </li>
										<?php } ?>
                                        <li class="mainmenu__item">
                                            <a href="contact-us.html" class="mainmenu__link">
                                                <span class="mm-text">Contact</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                <!-- Main Navigation End Here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Area End -->


