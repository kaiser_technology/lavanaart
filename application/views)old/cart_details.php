<!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-main">
        <div class="cart">
          
          <div class="page-content page-order"><div class="page-title">
            <h2>Shopping Cart</h2>
          </div>
            
            
            <div class="order-detail-content">
              <div class="table-responsive">
			  
				<form name="form1" method="post" action="<?php echo ADMIN_URL."cart/updatecart";?>">
                <table class="table table-bordered cart_summary">
                  <thead>
                    <tr>
                      <th class="cart_product">Product</th>
                      <th>Description</th>
                      <!--<th class="text-center">Availability</th>-->
                      <th class="text-center">Unit price</th>
                      <th class="text-center">Qty</th>
                      <th class="text-center">Total</th> 
                      <th class="action"><i class="fa fa-trash-o"></i></th>
                    </tr>
                  </thead>
                  <tbody>
				  <?php $i = 1; ?>
				  <?php foreach ($this->cart->contents() as $items): ?>
				  
				  <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                    <tr id="<?php echo $items['rowid'];?>">
                      <td class="cart_product">
					  <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
					  <a href="#"><img src="<?php echo CDN_IMG_URL.'product/'.$items['options']['proimage'];?>" alt="Product"></a>
					  <?php endif; ?>
					  </td>
                      <td class="cart_description"><p class="product-name"><a href="#"><?php echo $items['name']; ?> </a></p>
                        <!--<small><a href="#">Color : Red</a></small><br>
                        <small><a href="#">Size : M</a></small></td>-->
                      <!--<td class="availability in-stock"><span class="label">In stock</span></td>-->
                      <td class="price"><span><?php echo $this->cart->format_number($items['price']); ?></span></td>
                      <td class="qty" style="width:170px">
					  
					
					  
						
						<div class="col-lg-2">
							<div class="input-group">
								<span class="input-group-btn"><div class="btn btn-default value-control" data-action="minus" data-target="cart-size-<?php echo $i;?>"><span class="glyphicon glyphicon-minus"></span></div></span>
								<input type="text" name="<?php echo $i.'[qty]';?>" value="<?php echo $items['qty']; ?>" class="form-control" id="cart-size-<?php echo $i;?>" style="width:50px">
								<span class="input-group-btn"><div class="btn btn-default value-control" data-action="plus" data-target="cart-size-<?php echo $i;?>"><span class="glyphicon glyphicon-plus"></span></div></span>
							</div>
						</div>
					  </td>
                      <td class="price"><span><?php echo $this->cart->format_number($items['subtotal']); ?></span></td>
                      <td class="action"><a href="#" onclick="removeCart('<?php echo $items['rowid']; ?>')"><i class="icon-close"></i></a></td>
                    </tr> 
				<?php $i++; ?>		
				<?php endforeach; ?> 
                  </tbody>
             <tfoot>
                    <tr class="first last">
                      <td colspan="50" class="a-right last"><button type="button" title="Continue Shopping" class="button btn-continue" onclick="location.href = '<?php echo ADMIN_URL;?>';"><span>Continue Shopping</span></button>
                        <button type="submit" name="update_cart_action" value="update_qty" title="Update Cart" class="button btn-update"><span>Update Cart</span></button>
                        <button type="submit" name="clear_cart_action" value="empty_cart" title="Clear Cart" class="button btn-empty" id="empty_cart_button"><span>Clear Cart</span></button></td>
                    </tr>
                  </tfoot>
                </table>
				</form>
              </div>
              
            </div>
            
            <div class="cart-collaterals row">
            <div class="col-sm-4">
                </div>
            <div class="col-sm-4">
              <div class="discount">
                
              </div>
            </div>
            <div class="totals col-sm-4">
              <h3>Shopping Cart Total</h3>
              <div class="inner">
                <table id="shopping-cart-totals-table" class="table shopping-cart-table-total">
                  <colgroup>
                  <col>
                  <col width="1">
                  </colgroup>
                  <tfoot>
                    <tr>
                      <td style="" class="a-left" colspan="1"><strong>Grand Total</strong></td>
                      <td style="" class="a-right"><strong><span class="price cart-total-amt"><?php echo $this->cart->format_number($this->cart->total()); ?></span></strong></td>
                    </tr>
                  </tfoot>
                  <!--<tbody>
                    <tr>
                      <td style="" class="a-left" colspan="1"> Subtotal </td>
                      <td style="" class="a-right"><span class="price">$249.98</span></td>
                    </tr>
                  </tbody>-->
                </table>
                <ul class="checkout">
                  <li>
                    <button type="button" title="Proceed to Checkout" onclick="location.href='<?php echo base_url();?>Checkout/index'" class="button btn-proceed-checkout"><span>Proceed to Checkout</span></button>
                  </li>
                  <br>
                  
                  <br>
                </ul>
              </div>
              <!--inner--> 
              
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script>
$(document).on('click','.value-control',function(){
    var action = $(this).attr('data-action')
    var target = $(this).attr('data-target')
    var value  = parseFloat($('[id="'+target+'"]').val());
    if ( action == "plus" ) {
      value++;
    }
    if ( action == "minus" ) {
      value--;
    }
    $('[id="'+target+'"]').val(value)
	$('.btn-update').click();
})
  </script>