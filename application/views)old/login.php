<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li><strong>My Account</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="page-content">
	  
        <div class="account-login">
		
          <div class="box-authentication">
		  <?php 
			  if(isset($msg)){
			  echo '<label id="email-error" class="error" for="email">'.$msg.'</label>';
			  }
			  ?>
		  <form method="post" id="loginform">
            <h4>Log in <span class="right"><a href="<?php echo ADMIN_URL."register" ;?>"> Register</a></span></h4>
            <p class="before-login-text">Welcome back! Sign in to your account</p>
            <label for="emmail_login"> Your email or username <span class="required">*</span></label>
            <input id="email" name="uemail" type="text" class="form-control">
			<?php echo form_error('uemail', '<label id="email-error" class="error" for="email">', '</label>'); ?>
            <label for="password_login">Your password <span class="required">*</span></label>
            <input id="password" name="password" type="password" class="form-control">
			<?php echo form_error('password', '<label id="password-error" class="error" for="password">', '</label>'); ?>
            <p class="forgot-pass"><a href="<?php echo base_url('login/forgotpass');?>">Forgot password?</a></p>
            <button class="button" type="submit"><i class="fa fa-lock"></i>&nbsp; <span>Login</span></button>
			
            <!--<label class="inline" for="rememberme">
              <input type="checkbox" value="forever" id="rememberme" name="rememberme">
              Remember me </label>-->
			  </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
<script src="<?php echo ADMIN_URL;?>assets/js/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#loginform").validate({
		rules: {
			
			email: {
				required: true,
				email:true
			},
			password: {
				required: true,
				
			}
		},
		messages: {
			
			
			email: {
				required: "Field Required",
				email : "Enter Valid Email"
			},
			password: {
				required: "Field Required",
				
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>