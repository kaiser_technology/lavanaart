								
  <!-- Main Container -->
	<section class="main-container col1-layout">
		<div class="main container">
			<div class="row">
				<section class="col-main col-sm-12">
                    <div id="contact" class="page-content page-contact">
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">								
								<h3 class="page-subheading">PRIVACY POLICY</h3>
								<p>
								Asmita Organic Farm thinks your privacy is important. Please read this section of our website to learn more about Asmita Organic Farm’s philosophy and privacy policy. We understand that the protection of privacy is a major concern to many of our customers. Asmita Organic Farm has worked hard to gain your trust by selling only the highest quality foods and products, and we want to do everything reasonably possible to keep your trust. To build on our relationship, Asmita Organic Farm wants you to understand a little bit about how our website interacts with visitors and how we will use any information gathered when you visit our website. By using our services and viewing this Site, you are consenting to the information collection, use and disclosure practices described in this privacy policy.</br></br>
								 
								Types of Information Gathered</br>
								 
								Asmita Organic Farm may gather the following types of information: </br>
								1.	Personal information given voluntarily while using a feature of this website, for example, during a registration process or purchase, including name, address, phone number, e-mail address, birth date; financial information, such as credit card number; </br>
								2.	Information gathered as a result of voluntary participation in a survey or poll, or when communicating with our webmaster or customer service Team Members. </br>
								3.	Information gathered automatically when visitors visit our website. </br>
								We may collect information about your computer and your visits to this website such as your IP address, geographical location, browser type, referral source, length of visit and number of page views. We may use this information in the administration of this website, to improve the website’s usability, and for marketing purposes. We use cookies on this website. This enables the web server to identify and track the web browser. We may send a cookie which may be stored on by your browser on your computer’s hard drive. We may use the information we obtain from the cookie in the administration of this website, to improve the website’s usability and for marketing purposes. We may also use that information to recognize your computer when you visit our website, and to personalize our website for you. Our advertisers may also send you cookies. Most browsers allow you to refuse to accept cookies. This will, however, have a negative impact upon the usability of many websites, including this one. </br>
								Asmita Organic Farm will not sell or otherwise share personally identifying information with other people or non-affiliated companies except to provide services to you at your request, or as required by law. Asmita Organic Farm will share this information with certain business partners to provide some promotional & marketing services to you (eg. Regular offer intimations, emailers, phone calls etc) or requested service that we do not provide directly. Asmita Organic Farm will only share personally identifying information with these business partners if they agree not to disclose the information to other parties and agree not to use this information to solicit. </br>
								Asmita Organic Farm may share aggregate data collected through its website that does not personally identify any party with our research partner, and may from time to time share this type data with other business partners. </br>
								Asmita Organic Farm may disclose specific information upon governmental request, in response to a court order, when required by law, or to protect our or others' rights, property, or safety. We do not provide information to these agencies or companies for marketing or commercial purposes. </br>
								Also, if you participate in any blog or other online forum on our Site, any personal information that you post on the website may be shared with other participants in the forum. In any of these situations, the recipient of the personal information may be located in India or another jurisdiction that may not provide an equivalent level of data protection to the laws in your home country. </br>
								Except as provided in this privacy policy, we will not provide your information to third parties. </br></br>
								 
								Registration</br>
								In order for you to receive certain benefits of our website you may first be asked to supply certain information such as your email id, mobile number, pin code etc. Asmita Organic Farm wants to supply you with information that benefits you and the more relevant information we have about you the better we are able to do this. </br>
								</p>
								
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
  <!-- Main Container End -->
