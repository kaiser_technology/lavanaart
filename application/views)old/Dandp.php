<section class="main-container col1-layout">
		<div class="main container">
			<div class="row">
				<section class="col-main col-sm-12">
                    <div id="contact" class="page-content page-contact">
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">								
								<h3 class="page-subheading">DELIVERY & PAYMENT</h3>
								<p>
								Note: Please Note that our shipping policies have changed and now all shipping charges are automatically calculated as per your pin code of the delivery location. </br>
								We have divided our shipping location pin code wise into five areas as per logistics arrangements </br>
								I.	Rest Of India (Only Groceries) </br>
								II.	Local (Mumbai, Navi Mumbai, Thane) </br>
								1.	Shipping is free for orders on & above Rs.599 (For Mumbai, Navi Mumbai & Thane). </br>
								2.	Shipping charges depend upon the weight of your order and the same is applicable for cities. </br>
								3.	Mumbai, Thane, Navi Mumbai-Orders placed between Tue-Sat are delivered to the customer on the next day and orders placed on Sun & Mon are sent for delivery on Tuesday. </br>
								4.	For Rest of India-All orders handling time is 48 hrs from the day of order. And shipping time is 64 hrs. </br>
								5.	Shipping is done through our Asmita Organic Farm delivery services for Mumbai, Navi Mumbai & Thane and through our shipping partner Amazon Delivery Services for rest of India. </br>
								6.	For Customers outside Mumbai/Navi Mumbai & Thane, depending on the customer’s shipping address; the consignment will be delivered within 2 to 7 days from the date of registering the order. (Considering Groceries and Others are dry items and has a shelf life of 6 months to 1 year) </br>
								7.	For Customers from Mumbai/Navi Mumbai & Thane, the order shall be delivered within 1-2 days (Considering Fruits & Vegetables have a high perishability and has a shelf life of 3-6 days) </br>
								8.	We deliver across India to all major cities; please select your location before proceeding. </br> </br>
								Payments Policy: </br>
								Payment can be done via </br>
								i.	Credit Cards, </br>
								ii.	Debit Cards and </br>
								iii.	Net Banking. </br>
								1.	We accept COD (Cash on Delivery), Card on Delivery only for Mumbai, Thane and Navi Mumbai & some part of nearby areas. </br>
								2.	Our payment system online is totally safe and hack proof. </br>
								3.	None of the card details are shared with any third party. </br>
								4.	We don’t charge any additional transaction fee on card payments. </br>
								5.	No EMI facility is available. </br> </br>
								Credit Card/Debit Card Details </br>
								The Credit Card/Debit Card details provided to Asmita Organic Farm by the customer will not be shared with any third party. In case of fraudulent matters, the company holds the right to share the details with the concerned body for verification. The customer shall not under any circumstance use a credit/debit card that is not lawfully owned by him. The customer is hence requested to provide only valid and accurate card details. </br> </br>
								Fraudulent Transactions </br>
								If any fraud transactions are carried by any of the customers on the website, Asmita Organic Farm reserves the right to recover all the losses and expenses incurred like lawyer fees, collection charges, etc from the concerned party. The company also holds the right to take legal action against such person. </br>
								</p>
								<br/>	
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
  <!-- Main Container End -->