 <div class="main-container col1-layout">
    <div class="container">
      <div class="row">
        <div class="col-main col-sm-12 col-xs-12">
          <div class="shop-inner">
            <div class="page-title">
              <h2> Search Result</h2>
            </div>
           
            <div class="product-grid-area">
              <ul class="products-grid">
			  			  
			  <?php 
			//  print_r($data);exit;
			  if(isset($data) && !empty($data)){
				$i = 1;
					foreach($data as $product){
						//print_r($product);exit;
					?>
                <li class="item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <div class="box-hover">
                      <div class="btn-quickview"> <a href="#" data-toggle="modal" data-target="#modal-quickview<?php echo $i;?>"><i class="fa fa-search-plus" aria-hidden="true"></i> Quick View</a> </div>
                    
                    </div>
                        <a href="#" class="product-item-photo"> <img class="product-image-photo" src="<?php echo CDN_IMG_URL.'product/'.$product['pro_image'];?>" alt="" height="271px"></a> </div>
                      <div class="pro-box-info">
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <h4><a title="<?php echo $product['pro_name'];?>" href="#"><?php echo $product['pro_name'];?> </a></h4> </div>
                            <div class="item-content">
                              <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                              <div class="item-price">
                                <div class="price-box">
                                  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> Rs.<?php echo $product['pro_sale_price'];?></span> </p>
                                  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> Rs.<?php echo $product['pro_purchase_price'];?> </span> </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="box-hover">
                          <div class="product-item-actions">
                            <div class="pro-actions">
                               <button onclick="addToCart('<?php echo $product['pro_id'];?>','<?php echo $product['pro_sale_price'];?>','<?php echo $product['pro_name'];?>','<?php echo $product['pro_price_id'];?>','<?php echo $product['pro_code'];?>','<?php echo $product['pro_image'];?>','1');" class="action add-to-cart" type="button" title="Add to Cart"> <span>Add to Cart</span> </button>
                            </div>
                            <div class="pro-actions">
								 <button  onclick="buynow('<?php echo $product->pro_id;?>','<?php echo $product->pro_sale_price;?>','<?php echo $product->pro_name;?>','<?php echo $product->pro_price_id;?>','<?php echo $product->pro_code;?>','<?php echo $product->pro_image;?>','1');" class="action buynow" type="button" title="Buy Now"> <span>Buy Now</span> </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
				  
				   <!--model-->
					
					  <div id="modal-quickview<?php echo $i;?>" class="modal fade" role="dialog">
						<div class="modal-dialog">
						  <div class="modal-body">
							<button type="button" class="close myclose" data-dismiss="modal">�</button>
							<div class="product-view-area">
							  <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
								<!--<div class="icon-sale-label sale-left">Sale</div>-->
								<div class="slider-items-products">
								  <div id="previews-list-slider" class="product-flexslider hidden-buttons">
									<div class="slider-items slider-width-col6"> <a href="<?php echo CDN_IMG_URL.'product/'.$product['pro_image'];?>" class="cloud-zoom-gallery" id="zoom1"> <img class="zoom-img" src="<?php echo CDN_IMG_URL.'product/'.$product['pro_image'];?>" alt="products"> </a> </div>
								  </div>
								</div>
								
								<!-- end: more-images --> 
								
							  </div>
							  <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">
								<div class="product-name">
								  <h2><?php echo $product['pro_name'];?></h2>
								</div>
								<div class="price-box">
								  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo $product['pro_sale_price'];?> </span> </p>
								  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> <?php echo $product['pro_mrp_price'];?> </span> </p>
								</div>
								<div class="ratings">
								  <div class="rating"> <!--<i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
								  <p class="rating-links"> <a href="#">Add Your Review</a> </p>-->
								  <p class="availability in-stock pull-right">Availability: 
								  <?php if($product['pro_qty'] >2){$stk = "In Stock";}else{$stk = "Out Of Stock";}?>
								  <span><?php echo $stk;?></span></p>
								</div>
								<div class="short-description">
								  <h3>Quick Overview</h3>
								  <p><?php  echo $product['pro_detail'];?></p>
								</div>
								<!--<div class="product-color-size-area">
								  
								  <div class="size-area">
									<h2 class="saider-bar-title">Size</h2>
									<div class="size">
									  <ul>
										<li><a href="#">S</a></li>
										<li><a href="#">L</a></li>
										<li><a href="#">M</a></li>
										<li><a href="#">XL</a></li>
										<li><a href="#">XXL</a></li>
									  </ul>
									</div>
								  </div>
								</div>-->
								<div class="product-variation">
								  <form action="#" method="post">
									<div class="cart-plus-minus">
									  <label for="qty">Quantity:</label>
									  <div class="numbers-row">
										<div onClick="var result = document.getElementById('qty<?php echo $i;?>'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="dec qtybutton"><i class="fa fa-minus">&nbsp;</i></div>
										<input type="text" class="qty" title="Qty" value="1" maxlength="12" id="qty<?php echo $i;?>" name="qty">
										<div onClick="var result = document.getElementById('qty<?php echo $i;?>'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="inc qtybutton"><i class="fa fa-plus">&nbsp;</i></div>
									  </div>
									</div></br></br></br>
									  <div class="product-item-actions">
                                        <div class="pro-actions">
									        <button onclick="addToCart('<?php echo $product->pro_id;?>','<?php echo $product->pro_sale_price;?>','<?php echo $product->pro_name;?>','<?php echo $product->pro_price_id;?>','<?php echo $product->pro_code;?>','<?php echo $product->pro_image;?>',document.getElementById('qty<?php echo $i;?>').value);" class="button pro-add-to-cart" title="Add to Cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
    									</div>
                                        <div class="pro-actions">   
									        <button  onclick="buynow('<?php echo $product->pro_id;?>','<?php echo $product->pro_sale_price;?>','<?php echo $product->pro_name;?>','<?php echo $product->pro_price_id;?>','<?php echo $product->pro_code;?>','<?php echo $product->pro_image;?>','1');" class="button buynow" type="button" title="Buy Now"> <span><i class="fa fa-shopping-cart"></i> Buy Now</span> </button>
                                        </div>
                                    </div>
								  </form>
								</div>
								<div class="product-cart-option">
								  <!--<ul>
									<li><a href="wishlist.html"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
									<li><a href="#"><i class="fa fa-retweet"></i><span>Add to Compare</span></a></li>
									<li><a href="#"><i class="fa fa-envelope"></i><span>Email to a Friend</span></a></li>
								  </ul>-->
								</div>
							  </div>
							</div>
						  </div>
						  <div class="modal-footer"> <a href="#" class="btn-services-shop-now" data-dismiss="modal">Close</a> </div>
						</div>
					  </div>
					</div>

				  <!--model-->
                </li>
				<?php $i++ ; }
				}
				?>
                
              </ul>
            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>