
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a><span>&raquo;</span></li>
            <li><strong>New Customer</strong></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Breadcrumbs End --> 
  
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="page-content">
	  <?php
			   if(isset($msg))
			   {?>
					<div class="alert alert-info alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<?php echo $msg;?>
					</div>
			<?php
				}
			 ?>   
        <div class="account-login">
          <div class="box-authentication new-customer-box Account Page ">
          
          <form method="post" id="regform">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="check-title">
                      <h4>New Customer</h4>
                    </div>
                  </div>
				   <div class="col-sm-4">
                    <label>Title:</label>
                    <div class="input-text">
                     <select name="prefix" id = "prefix" style="width: 100%;">
						<option value="">Select</option>
						<option>Mr.</option>
						<option>Mrs.</option>
						<option>Miss</option>
					 </select>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <label>First Name:</label>
                    <div class="input-text">
                      <input type="text" name="fname" id = "fname" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <label>Last Name:</label>
                    <div class="input-text">
                      <input type="text" name="lname" id="lname" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <label>Address:</label>
                    <div class="input-text">
                      <input type="text" name="address" id="address" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <label>City/Town:</label>
                    <div class="input-text">
                      <input type="text" name="city" id="city" class="form-control">
                    </div>
                  </div>
				  <div class="col-sm-6">
                    <label>Pincode:</label>
                    <div class="input-text">
                      <input type="text" name="pincode" id="pincode" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <label>Email:</label>
                    <div class="input-text">
                      <input type="text" name="email" id="email" class="form-control">
					   <?php echo form_error('email', '<label id="email-error" class="error" for="email">', '</label>'); ?>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <label>Phone:</label>
                    <div class="input-text">
                      <input type="text" name="phone" id = "phone" class="form-control">
                    </div>
                  </div>
				  <div class="col-sm-6">
                    <label>Password:</label>
                    <div class="input-text">
                      <input type="password" name="pass" id = "pass" class="form-control">
                    </div>
                  </div>
				  <div class="col-sm-6">
                    <label>Confirm Password:</label>
                    <div class="input-text">
                      <input type="password" name="cpass" id = "cpass" class="form-control">
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <!--<div class="billing-checkbox">
                    <label class="inline" for="rememberme">
                      <input type="checkbox" value="forever" id="rememberme" name="rememberme">
                      Sign up for our newsletter! </label></div>-->
                    <div class="submit-text">
						
                      <button class="button"><i class="fa fa-user"></i>&nbsp; <span>Register</span></button>
                    </div>
                  </div>
                </div>
              </form>
              
         
       
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
  <script src="https://www.asmitaorganicfarm.com/assets/js/jquery-validation/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#regform").validate({
		rules: {
			prefix: {
				required: true,
				
			},
			fname: {
				required: true,
				
			},
			lname: {
				required: true,
				
			},
			address: {
				required: true,
				
			},
			city: {
				required: true,
				
			},
			pincode: {
				required: true,
				
			},
			email: {
				required: true,
				email:true
			},
			phone: {
				required: true,
				number : true
			},
			pass: {
				required: true,
				
			},
			cpass: {
				required: true,
				equalTo : pass
			}
		},
		messages: {
			
			prefix: {
				required: "Field Required",
				
			},
			fname: {
				required: "Field Required",
				
			},
			lname: {
				required: "Field Required",
				
			},
			address: {
				required: "Field Required",
				
			},
			city: {
				required: "Field Required",
				
			},
			pincode: {
				required: "Field Required",
				
			},
			email: {
				required: "Field Required",
				email : "Enter Valid Email"
			},
			phone: {
				required: "Field Required",
				
			},
			pass: {
				required: "Field Required",
				
			},
			cpass: {
				required: "Field Required",
				equalTo : "Please Enter Same Password as above"
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>