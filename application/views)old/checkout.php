<div class="main container">
    <div class="row">
	<div class="page-title">
          <h2>Checkout</h2>
          <h3 style="color:#ff0000;text-align: center;margin: 15px 0 25px;"><b>Today's Offer</b> Bandra Residence will get <b>10%</b> off</h3>
        </div>
      <div class="col-main col-sm-6 col-xs-12">    
	   <?php 
	  if(isset($msg)){
	  echo '<label id="email-error" class="error" for="email">'.$msg.'</label>';
	  }
	  ?>
		<form name="frm1" id="frm1" method="post">
		<?php krsort($shippingDetails->detail);
		$shippingDetails->detail = array_values($shippingDetails->detail); 
		
		?>
        <div class="page-content checkout-page">      
		
          <h4 class="checkout-sep">Shipping Information</h4>
            <div class="box-border">
                <ul>
                                    
                    <li class="row">
                        
                        <div class="col-sm-6">
                           
                            <label for="first_name_1" class="required"><span class="required">*</span> First Name</label>
                            <input class="input form-control" type="text" name="s_first_name" value="<?php echo  !empty($shippingDetails->detail[0]->fname) ? $shippingDetails->detail[0]->fname : ""; ?>" id="s_first_name">
							<?php echo form_error('s_first_name', '<label id="s_first_name-error" class="error" for="first_name_1">', '</label>'); ?>
                        </div><!--/ [col] -->

                        <div class="col-sm-6">
                            
                            <label for="last_name_1" class="required"><span class="required">*</span> Last Name</label>
                            <input class="input form-control" type="text" value="<?php echo  !empty($shippingDetails->detail[0]->lname) ? $shippingDetails->detail[0]->lname : ""; ?>" name="s_last_name" id="s_last_name">
							<?php echo form_error('s_last_name', '<label id="last_name_1-error" class="error" for="last_name_1">', '</label>'); ?>
                        </div><!--/ [col] -->

                    </li><!--/ .row -->

                    <li class="row">
                        
                       <div class="col-sm-6">

                            <label for="telephone_1" class="required"><span class="required">*</span> Contact No</label>
                            <input class="input form-control" type="text" value="<?php echo  !empty($shippingDetails->detail[0]->ship_contact) ? $shippingDetails->detail[0]->ship_contact : ""; ?>"  name="s_telephone" id="s_telephone">
							<?php echo form_error('s_telephone', '<label id="telephone_1-error" class="error" for="telephone_1">', '</label>'); ?>
                        </div><!--/ [col] -->

                        <div class="col-sm-6">
                            
                            <label for="email_address_1" class="required"><span class="required">*</span> Email Address</label>
                            <input class="input form-control" type="text" name="s_email_address" value="<?php echo  !empty($shippingDetails->detail[0]->email) ? $shippingDetails->detail[0]->email : ""; ?>" id="s_email_address">
							<?php echo form_error('s_email_address', '<label id="email_address_1-error" class="error" for="email_address_1">', '</label>'); ?>
                        </div><!--/ [col] -->

                    </li><!--/ .row -->

                    <li class="row">

                        <div class="col-xs-12">

                            <label for="address_1" class="required"><span class="required">*</span> Address</label>
                            <input class="input form-control" type="text" name="s_address" value="<?php echo  !empty($shippingDetails->detail[0]->ship_straddress) ? $shippingDetails->detail[0]->ship_straddress : ""; ?>" id="s_address">
							<?php echo form_error('s_address', '<label id="address_1-error" class="error" for="address_1">', '</label>'); ?>
                        </div><!--/ [col] -->

                    </li><!--/ .row -->

                    <li class="row">

                        <div class="col-sm-6">
                            
                            <label for="city_1" class="required">City</label>
                            <input class="input form-control" type="text" name="s_city" value="<?php echo  !empty($shippingDetails->detail[0]->city_name) ? $shippingDetails->detail[0]->city_name : ""; ?>" id="s_city">
							<?php echo form_error('s_city', '<label id="city_1-error" class="error" for="city_1">', '</label>'); ?>
                        </div><!--/ [col] -->

                        <div class="col-sm-6">

                            <label class="required"><span class="required">*</span> State/Province</label>

                            <div class="custom_select">
								<input type="text" class="input form-control" value="<?php echo  !empty($shippingDetails->detail[0]->state_name) ? $shippingDetails->detail[0]->state_name : ""; ?>" name="s_state" id="s_state" >
                               

                            </div>

                        </div><!--/ [col] -->

                    </li><!--/ .row -->

                    <li class="row">

                        <div class="col-sm-6">

                            <label for="postal_code_1" class="required"><span class="required">*</span> Zip/Postal Code</label>
                            <input class="input form-control" type="text" name="s_postal_code" value="<?php echo  !empty($shippingDetails->detail[0]->ship_postalCode) ? $shippingDetails->detail[0]->ship_postalCode : ""; ?>"  id="s_postal_code">
							<?php echo form_error('s_postal_code', '<label id="s_postal_code-error" class="error" for="postal_code_1">', '</label>'); ?>
                        </div><!--/ [col] -->

                        <div class="col-sm-6">

                            <label class="required"> Country</label>

                            <div class="custom_select">

                               <input type="text" class="input form-control" value="<?php echo  !empty($shippingDetails->detail[0]->country_name) ? $shippingDetails->detail[0]->country_name : ""; ?>" name="country_name" id="cntName" >  

                            </div>

                        </div><!--/ [col] -->

                    </li><!--/ .row -->

                 

                </ul>
                <button class="button" type="submit"><i class="fa fa-angle-double-right"></i>&nbsp; <span>Save</span></button>
            </div>
                       
        </div>
		
      </div>
      <!--<aside class="right sidebar col-sm-6 col-xs-12">
        <div class="sidebar-checkout block">
         
                 <div class="block-content">
            <dl>
              <div class="col-sm-6">
			  <dt class="complete"> Billing Address <span class="separator">|</span> <a href="#">Change</a> </dt>
              <dd class="complete">
                <address>
                Deo Jone<br>
                Company Name<br>
                7064 Lorem <br>
                Ipsum <br>
                Vestibulum 0 666/13<br>
                United States<br>
                T: 12345678 <br>
                F: 987654
                </address>
              </dd>
			  </div>
			  <div class="col-sm-6">
              <dt class="complete"> Shipping Address <span class="separator">|</span> <a href="#">Change</a> </dt>
              <dd class="complete">
                 <address>
                Deo Jone<br>
                Company Name<br>
                7064 Lorem <br>
                Ipsum <br>
                Vestibulum 0 666/13<br>
                United States<br>
                T: 12345678 <br>
                F: 987654
                </address>
              </dd>
			  </div>
              
            </dl>
          </div>
        </div>
        </aside>-->
		<div>
			<h4 class="checkout-sep last"> Order Review</h4>
        <div class="box-border">
            <div class="table-responsive">
                <table class="table table-bordered cart_summary">
                    <thead>
                        <tr>
                            <th class="cart_product">Product</th>
                            <th>Description</th>
                            <th>Unit price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th class="action"><i class="fa fa-trash-o"></i></th>
                        </tr>
                    </thead>
                    <tbody>
					<?php $i = 1; ?> 
					<?php foreach ($this->cart->contents() as $items): ?>
                        <tr id="<?php echo $items['rowid'];?>">
						
                            <td class="cart_product">
							 <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                <a href="#"><img src="<?php echo CDN_IMG_URL.'product/'.$items['options']['proimage'];?>" alt="Product"></a>
								<?php endif; ?>
                            </td>
                            <td class="cart_description">
                                <p class="product-name"><a href="#"><?php echo $items['name']; ?></a></p>
                            </td>                            
                            <td class="price"><span><?php echo $this->cart->format_number($items['price']); ?> </span></td>
                            <td class="qty">
                                <?php echo $items['qty']; ?>
                              
                            </td>
                            <td class="price">
                                <span><?php echo $this->cart->format_number($items['subtotal']); ?></span>
                            </td>
                            <td class="action">
                                <a href="#" onclick="removeCart('<?php echo $items['rowid']; ?>')"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <?php $i++; ?>		
					<?php endforeach; ?> 

                    </tbody>
                    <tfoot>
                        
						<?php  if(sizeof($this->cart->contents()) > 0){?>
                        <tr>						
						<td></td>
						<td></td>
                            <td colspan="2"><strong>Total</strong></td>
                            <td colspan="2"><strong class="cart-total-amt"><?php echo $this->cart->format_number($this->cart->total()); ?> </strong></td>                        
						</tr>
						<?php  } ?>
                    </tfoot>    
                </table>
				</div>
                
				 <button class="button" type="submit"><i class="fa fa-angle-double-right"></i>&nbsp; <span>Place by PayUmoney</span></button>
        </div>		
		</div></form>
		
		
		
    </div>
  </div>
  
  <script src="http://navavyapar.com/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#frm1").validate({
		rules: {
			
			b_first_name: {
				required: true
			},
			b_last_name: {
				required: true
			},
			b_company_name: {
				required: true
			},
			b_email_address: {
				required: true
			},
			b_address: {
				required: true
			},
			b_city: {
				required: true
			},
			b_state: {
				required: true
			},
			b_postal_code: {
				required: true
			},
			b_telephone: {
				required: true
			},
			s_first_name: {
				required: true
			},
			s_last_name: {
				required: true
			},
			s_company_name: {
				required: true
			},
			s_email_address: {
				required: true
			},
			s_address: {
				required: true
			},
			s_city: {
				required: true
			},
			s_state: {
				required: true
			},
			s_postal_code: {
				required: true
			},
			s_telephone: {
				required: true
			}
		},
		messages: {
			
			
			b_first_name: {
				required: "Field Required"
			},
			b_last_name: {
				required: "Field Required"
			},
			b_company_name: {
				required: "Field Required"
			},
			b_email_address: {
				required: "Field Required"
			},
			b_address: {
				required: "Field Required"
			},
			b_city: {
				required: "Field Required"
			},
			b_state: {
				required: "Field Required"
			},
			b_postal_code: {
				required: "Field Required"
			},
			b_telephone: {
				required: "Field Required"
			},
			s_first_name: {
				required: "Field Required"
			},
			s_last_name: {
				required: "Field Required"
			},
			s_company_name: {
				required: "Field Required"
			},
			s_email_address: {
				required: "Field Required"
			},
			s_address: {
				required: "Field Required"
			},
			s_city: {
				required: "Field Required"
			},
			s_state: {
				required: "Field Required"
			},
			s_postal_code: {
				required: "Field Required"
			},
			s_telephone: {
				required: "Field Required"
			}			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>