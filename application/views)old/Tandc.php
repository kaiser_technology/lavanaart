  <!-- Main Container -->
	<section class="main-container col1-layout">
		<div class="main container">
			<div class="row">
				<section class="col-main col-sm-12">
                    <div id="contact" class="page-content page-contact">
						<div class="page-title">
							<h2>Terms & Conditions and Other Company Policies</h2>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">
								<h3 class="page-subheading">TERMS & CONDITIONS</h3>
								<p>
								1.	Information</br>
								•	Asmita Organic Farm is your one stop solution to all your healthy food needs. </br>
								•	We aim at producing and delivering organically grown fruits and vegetables in Mumbai and organic groceries in PAN India. Our quality control team ensures that only fresh stock reaches you when you place an order via a phone call or do online organic grocery shopping. We come across number of customers who are satisfied with the quality that we provide yet we strive to provide exceptionally well healthy food. </br>
								•	Our team has been dedicate backing up the company to improvise and innovate the food habits of our consumers, letting the company build a brand image that is trusted, transparent and most importantly reliable. </br>
								•	All orders for Goods shall be deemed to be an offer by the Buyer to purchase Goods pursuant to these Terms and Conditions and are subject to acceptance by the Seller. The Seller may choose not to accept or cancel an order without assigning any reason. </br>
								•	Asmita Organic Farm may change the terms and conditions and disclaimer set out above from time to time. By browsing this web site you are accepting that you are bound by the current terms and conditions and disclaimer and so you should check these each time you revisit the web site. </br>
								•	Asmita Organic Farm may change the format and content of this web site at any time. </br>
								•	Asmita Organic Farm may suspend the operation of this web site for support or maintenance work, in order to update the content or for any other reason. </br>
								•	Personal details provided to Asmita Organic Farm through this web site will only be used in accordance with our privacy policy. Please read this carefully before going on. By providing your personal details to us you are consenting to its use in accordance with our privacy policy. </br>
								•	If you have a query or complaint, please contact us at info@asmitaorganicfarm.com</br>
								•	Asmita Organic Farm reserves all other rights</br></br>
								2.	Pricing</br>
								•	The Company ensures that prices for all products offered for sale are correct. Prices can change at the time of billing as per there weights as there is no exact weight for fruits and vegetable the final amount of the bill is for reference and amount may fluctuate. Ex. If the cost of 1kg watermelon is 60/- and the weight while billing 1kg 50g then you will be charged for 1kg and 50g. </br></br>


								3.	Other Terms and Conditions</br>
								•	Delivery for fruits and vegetables is in Mumbai, Navi Mumbai and Thane. We don’t deliver fruits and vegetables Pan India. As it takes 3 to 4 days to deliver Pan India. Vegetables and fruits may get spoiled. </br>
								•	To get the next day delivery the minimum bill amount should be above 399 rupees as we do not process the order below 399 rupees. </br>
								•	A customer is eligible for free shipping only if the bill amount is on & above 599. </br>
								•	Customer support team is available from 9:00 AM to 7:00 PM. (Tuesday to Sunday) regarding any complaints and delivery issue contact customer support team landline number 022-28113596 </br>
								•	After receiving the delivery if you find any defect in the products please share photos within 24 hours on WhatsApp number 9820166952 to get replacement in the next delivery. If a customer is unable to share, the company is not responsible for his/her replacement. </br>
								•	After confirmation call by customer support team orders would be processed. If the call is been unanswered the order cannot be processed. Order before 7.00PM to get next day delivery. </br>
								•	After placing an order on website the bill amount you get is for reference. Products are billed by their weights and price may vary. As there is no exact size for fruits and vegetables. </br>
								•	We do deliveries all over Mumbai, Navi Mumbai and Thane. The boundary line for western zone is Mira Road for central Zone is Thane. On Sunday’s deliveries above boundary lines. Customers beyond the boundary lines can order on Saturday to get next day delivery on Sunday’s. </br>
								•	For outstation orders delivery charges are mandatory. For outstation orders WhatsApp your orders on 9820166952. Our team will send you the bill amount along with delivery charges once NEFT is done. Order will be dispatched from our side. </br>
								•	For Delivery status customer can contact Customer Support Team 022-28113596. They will give you an approx. timing. There is no exact timing for the deliveries. </br></br>
								Cancellations and Refund</br>
								•	Products once ordered can be cancelled till the time of delivery. </br>
								•	You can cancel your orders at the time of delivery. </br>
								•	After accepting the order if there is any defect in the product a customer can return the product there itself. </br>
								•	While accepting the order please check all the products. Once accepted the Order cannot be cancelled. </br>
								</p>
								<br/>	
								<h3 class="page-subheading">DELIVERY & PAYMENT</h3>
								<p>
								Note: Please Note that our shipping policies have changed and now all shipping charges are automatically calculated as per your pin code of the delivery location. </br>
								We have divided our shipping location pin code wise into five areas as per logistics arrangements </br>
								I.	Rest Of India (Only Groceries) </br>
								II.	Local (Mumbai, Navi Mumbai, Thane) </br>
								1.	Shipping is free for orders on & above Rs.599 (For Mumbai, Navi Mumbai & Thane). </br>
								2.	Shipping charges depend upon the weight of your order and the same is applicable for cities. </br>
								3.	Mumbai, Thane, Navi Mumbai-Orders placed between Tue-Sat are delivered to the customer on the next day and orders placed on Sun & Mon are sent for delivery on Tuesday. </br>
								4.	For Rest of India-All orders handling time is 48 hrs from the day of order. And shipping time is 64 hrs. </br>
								5.	Shipping is done through our Asmita Organic Farm delivery services for Mumbai, Navi Mumbai & Thane and through our shipping partner Amazon Delivery Services for rest of India. </br>
								6.	For Customers outside Mumbai/Navi Mumbai & Thane, depending on the customer’s shipping address; the consignment will be delivered within 2 to 7 days from the date of registering the order. (Considering Groceries and Others are dry items and has a shelf life of 6 months to 1 year) </br>
								7.	For Customers from Mumbai/Navi Mumbai & Thane, the order shall be delivered within 1-2 days (Considering Fruits & Vegetables have a high perishability and has a shelf life of 3-6 days) </br>
								8.	We deliver across India to all major cities; please select your location before proceeding. </br> </br>
								Payments Policy: </br>
								Payment can be done via </br>
								i.	Credit Cards, </br>
								ii.	Debit Cards and </br>
								iii.	Net Banking. </br>
								1.	We accept COD (Cash on Delivery), Card on Delivery only for Mumbai, Thane and Navi Mumbai & some part of nearby areas. </br>
								2.	Our payment system online is totally safe and hack proof. </br>
								3.	None of the card details are shared with any third party. </br>
								4.	We don’t charge any additional transaction fee on card payments. </br>
								5.	No EMI facility is available. </br> </br>
								Credit Card/Debit Card Details </br>
								The Credit Card/Debit Card details provided to Asmita Organic Farm by the customer will not be shared with any third party. In case of fraudulent matters, the company holds the right to share the details with the concerned body for verification. The customer shall not under any circumstance use a credit/debit card that is not lawfully owned by him. The customer is hence requested to provide only valid and accurate card details. </br> </br>
								Fraudulent Transactions </br>
								If any fraud transactions are carried by any of the customers on the website, Asmita Organic Farm reserves the right to recover all the losses and expenses incurred like lawyer fees, collection charges, etc from the concerned party. The company also holds the right to take legal action against such person. </br>
								</p>
								<br/>	
								<h3 class="page-subheading">PAYMENT OPTIONS</h3>
								<p>
								Payment Policy:</br>
								Payment can be done via</br>
								i.	Credit Cards,</br>
								ii.	Debit Cards</br>
								iii. Net Banking. </br></br>
								 
								1.	We accept COD (Cash on Delivery) only for Mumbai, Thane and Navi Mumbai. </br>
								2.	Our payment system online is totally safe and hack proof. </br>
								3.	None of the card details are shared with any third party. </br>
								4.	We don’t charge any additional transaction fee on card payments. </br>
								5.	No EMI facility is available. </br></br>
								 
								Credit Card/Debit Card Details</br>
								The Credit Card/Debit Card details provided to Asmita Organic Farm by the customer will not be shared with any third party. In case of fraudulent matters, the company holds the right to share the details with the concerned body for verification. The customer shall not under any circumstance use a credit/debit card that is not lawfully owned by him. The customer is hence requested to provide only valid and accurate card details. </br></br>
								 
								Fraudulent Transactions</br>
								If any fraud transactions are carried by any of the customers on the website, Asmita Organic Farm reserves the right to recover all the losses and expenses incurred like lawyer fees, collection charges, etc from the concerned party. The company also holds the right to take legal action against such person. </br></br>
								 
								Offers & Discounts: </br>
								Discounts/Coupon/Voucher Codes</br>
								As a privilege, we provide coupon codes to our customers at regular intervals. Here are a few things to keep in mind while enjoying the benefits of coupon codes: </br>
								1.	Coupon codes cannot be clubbed with any other on-going offers in the store or on the website</br>
								2.	Coupon codes will be valid only till the date mentioned in the email</br>
								3.	One coupon code can be used only once. </br>
								4.	Coupon codes cannot be used by only one customer i.e., the same code will not be valid to your family or friends</br>
								5.	Asmita Organic Farm can change the terms and conditions related to any available coupon codes on the website without any prior intimation</br>
								6.	Shipping rules will be applicable to every product purchased using coupon codes</br></br>
								 
								NEFT Details: </br>
								1.	Account Name: Asmita Supermarkets Pvt. Ltd. </br>
								2.	Account No: 03582320000028</br>
								3.	IFSC: HDFC0000358 MICR :400240047</br>
								4.	Bank Name: HDFC</br>
								5.	Account Branch: Mira Road</br>
								</p>
								<br/>	
								<h3 class="page-subheading">PRIVACY POLICY</h3>
								<p>
								Asmita Organic Farm thinks your privacy is important. Please read this section of our website to learn more about Asmita Organic Farm’s philosophy and privacy policy. We understand that the protection of privacy is a major concern to many of our customers. Asmita Organic Farm has worked hard to gain your trust by selling only the highest quality foods and products, and we want to do everything reasonably possible to keep your trust. To build on our relationship, Asmita Organic Farm wants you to understand a little bit about how our website interacts with visitors and how we will use any information gathered when you visit our website. By using our services and viewing this Site, you are consenting to the information collection, use and disclosure practices described in this privacy policy.</br></br>
								 
								Types of Information Gathered</br>
								 
								Asmita Organic Farm may gather the following types of information: </br>
								1.	Personal information given voluntarily while using a feature of this website, for example, during a registration process or purchase, including name, address, phone number, e-mail address, birth date; financial information, such as credit card number; </br>
								2.	Information gathered as a result of voluntary participation in a survey or poll, or when communicating with our webmaster or customer service Team Members. </br>
								3.	Information gathered automatically when visitors visit our website. </br>
								We may collect information about your computer and your visits to this website such as your IP address, geographical location, browser type, referral source, length of visit and number of page views. We may use this information in the administration of this website, to improve the website’s usability, and for marketing purposes. We use cookies on this website. This enables the web server to identify and track the web browser. We may send a cookie which may be stored on by your browser on your computer’s hard drive. We may use the information we obtain from the cookie in the administration of this website, to improve the website’s usability and for marketing purposes. We may also use that information to recognize your computer when you visit our website, and to personalize our website for you. Our advertisers may also send you cookies. Most browsers allow you to refuse to accept cookies. This will, however, have a negative impact upon the usability of many websites, including this one. </br>
								Asmita Organic Farm will not sell or otherwise share personally identifying information with other people or non-affiliated companies except to provide services to you at your request, or as required by law. Asmita Organic Farm will share this information with certain business partners to provide some promotional & marketing services to you (eg. Regular offer intimations, emailers, phone calls etc) or requested service that we do not provide directly. Asmita Organic Farm will only share personally identifying information with these business partners if they agree not to disclose the information to other parties and agree not to use this information to solicit. </br>
								Asmita Organic Farm may share aggregate data collected through its website that does not personally identify any party with our research partner, and may from time to time share this type data with other business partners. </br>
								Asmita Organic Farm may disclose specific information upon governmental request, in response to a court order, when required by law, or to protect our or others' rights, property, or safety. We do not provide information to these agencies or companies for marketing or commercial purposes. </br>
								Also, if you participate in any blog or other online forum on our Site, any personal information that you post on the website may be shared with other participants in the forum. In any of these situations, the recipient of the personal information may be located in India or another jurisdiction that may not provide an equivalent level of data protection to the laws in your home country. </br>
								Except as provided in this privacy policy, we will not provide your information to third parties. </br></br>
								 
								Registration</br>
								In order for you to receive certain benefits of our website you may first be asked to supply certain information such as your email id, mobile number, pin code etc. Asmita Organic Farm wants to supply you with information that benefits you and the more relevant information we have about you the better we are able to do this. </br>
								</p>
								<br/>	
								<h3 class="page-subheading">RETURN & CANCELLATION</h3>
								<p>
								Returns & Exchange Policy: </br>
								1.	Our products are thoroughly checked for the quality before delivering.</br>
								2.	We insist that the customer has thoroughly checked the quality of fresh produce and grocery at the time of delivery. </br>
								3.	In case of fresh produce (fruits, vegetable, and dairy products), it is mandatory for the customer to check the quality of the product at the time of delivery itself. Applicable only for Mumbai, Navi Mumbai & Thane</br>
								4.	If any of the fresh produce is not up to the customer’s requirement, they are requested to return the whole quantity of that product to the delivery boy. Applicable only for Mumbai, Navi Mumbai & Thane</br>
								a.	The delivery boy will deduct the amount of the returned product from the total bill, and the customer will make the payment accordingly. </br>
								b.	No quality issues related to the fresh produce will be entertained by the Call Centre executive thereafter. </br>
								c.	However in case of grocery, if the customer is unable to check the quality at the time of delivery, he/she should inform the same to the Call Centre (022-28113596 ) within 72 hrs (3 days) of delivery. Applicable only for Mumbai, Navi Mumbai & Thane</br>
								5.	The customer could get an exchange of the grocery pack only under following conditions: </br>
								a.	Packets that have live or dead insects. </br>
								b.	Packets that have signs of rotting. </br>
								c.	Packets that are torn, not sealed properly at the time of delivery</br>
								d.	Products that have expired. </br>
								e.	Return accepted within 2 days from the date customer purchase the products. </br>
								f.	Other than above no other issues related to products will be addressed</br>
								6.	The customer shall not get an exchange of grocery product under following conditions: </br>
								a.	When the packet is opened and a product is used. </br>
								b.	Customers can’t claim an exchange when they have cooked or consumed the item. </br>
								c.	The item is packed and returned with any other packing material. </br>
								7.	The customer could exchange the product with a new pack of the same product, or any other product of the same or higher value. </br>
								8.	For customers outside Mumbai, Navi Mumbai & Thane can get the money back only as per our money back policy. </br></br>
								 
								Cancellation Policy: </br>
								1.	Request to cancel the order will be accepted only if the product has not been shipped by us. If accepted, the customer is entitled to get a refund of the entire amount. </br>
								2.	If we receive a cancellation request from the customer after the product/s has been shipped by us, cancellation request will not be accepted. </br>
								3.	The Asmita Organic Farm reserves the right to cancel or refuse to accept any order placed due to various reasons like no availability of stock, pricing errors, factual errors or problems identified with the personal/financial details provided by the customer. </br></br>
								 
								Money Back in case of cancellation or return: </br>
								In general case of exchange/replacement/return due to above-mentioned quality issues and a written proof from the customer following refund policies are applicable: </br>
								1.	If a customer doesn’t wish to avail the replacement, we issue a voucher of the same value to the customer to be availed within a specified time period mentioned on that. </br>
								2.	In case of order cancellation/return through card/Net banking we credit the money back to your credit/debit card or Net Banking which takes up to 10 to 15 working days to reflect in your statement. </br>
								3.	In any of the above case delivery charges are non-refundable. </br>
								Note: Under no circumstances, the customer would be given a cash refund</br>


								</p>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
  <!-- Main Container End -->

