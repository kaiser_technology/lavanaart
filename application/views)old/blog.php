  <section class="blog_post">
    <div class="container"> 
      
      <!-- row -->
      <div class="row"> 
        
        <!-- Center colunm-->
        <div class="center_column col-xs-12 col-sm-12" id="center_column">
          <div class="page-title">
            <h2>Blog</h2>
          </div>
          <ul class="blog-posts">
		  <?php 
		
		  foreach($bloglist as $val){ ?>
            <li class="post-item">
              <article class="entry">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="entry-thumb"> <a href="<?php echo ADMIN_URL."blog/detail/".$val['id'] ;?>">
                      <figure><img src="<?php echo CDN_IMG_URL.'blog/'.$val['mainImage'];?>" alt="<?php echo $val['blogName'];?>"></figure>
                      </a> </div>
                  </div>
                  <div class="col-sm-8">
                    <h3 class="entry-title"><a href="<?php echo ADMIN_URL."blog/detail/".$val['id'] ;?>"><?php echo $val['blogName'];?></a></h3>
                    <div class="entry-excerpt"><?php echo $val['sortDesc'];?></div>
                    <div class="entry-more"> <a href="<?php echo ADMIN_URL."blog/detail/".$val['id'] ;?>" class="button"> Continus reading &nbsp; <i class="fa fa-angle-double-right"></i></a> </div>
                  </div>
                </div>
              </article>
            </li>
		  <?php } ?>
          
            
          </ul>
        </div>
        <!-- ./ Center colunm --> 
      </div>
      <!-- ./row--> 
    </div>
  </section>