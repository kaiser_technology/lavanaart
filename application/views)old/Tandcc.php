  <!-- Main Container -->
	<section class="main-container col1-layout">
		<div class="main container">
			<div class="row">
				<section class="col-main col-sm-12">
                    <div id="contact" class="page-content page-contact">
						
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">
								<h3 class="page-subheading">TERMS & CONDITIONS</h3>
								<p>
								1.	Information</br>
								•	Asmita Organic Farm is your one stop solution to all your healthy food needs. </br>
								•	We aim at producing and delivering organically grown fruits and vegetables in Mumbai and organic groceries in PAN India. Our quality control team ensures that only fresh stock reaches you when you place an order via a phone call or do online organic grocery shopping. We come across number of customers who are satisfied with the quality that we provide yet we strive to provide exceptionally well healthy food. </br>
								•	Our team has been dedicate backing up the company to improvise and innovate the food habits of our consumers, letting the company build a brand image that is trusted, transparent and most importantly reliable. </br>
								•	All orders for Goods shall be deemed to be an offer by the Buyer to purchase Goods pursuant to these Terms and Conditions and are subject to acceptance by the Seller. The Seller may choose not to accept or cancel an order without assigning any reason. </br>
								•	Asmita Organic Farm may change the terms and conditions and disclaimer set out above from time to time. By browsing this web site you are accepting that you are bound by the current terms and conditions and disclaimer and so you should check these each time you revisit the web site. </br>
								•	Asmita Organic Farm may change the format and content of this web site at any time. </br>
								•	Asmita Organic Farm may suspend the operation of this web site for support or maintenance work, in order to update the content or for any other reason. </br>
								•	Personal details provided to Asmita Organic Farm through this web site will only be used in accordance with our privacy policy. Please read this carefully before going on. By providing your personal details to us you are consenting to its use in accordance with our privacy policy. </br>
								•	If you have a query or complaint, please contact us at info@asmitaorganicfarm.com</br>
								•	Asmita Organic Farm reserves all other rights</br></br>
								2.	Pricing</br>
								•	The Company ensures that prices for all products offered for sale are correct. Prices can change at the time of billing as per there weights as there is no exact weight for fruits and vegetable the final amount of the bill is for reference and amount may fluctuate. Ex. If the cost of 1kg watermelon is 60/- and the weight while billing 1kg 50g then you will be charged for 1kg and 50g. </br></br>


								3.	Other Terms and Conditions</br>
								•	Delivery for fruits and vegetables is in Mumbai, Navi Mumbai and Thane. We don’t deliver fruits and vegetables Pan India. As it takes 3 to 4 days to deliver Pan India. Vegetables and fruits may get spoiled. </br>
								•	To get the next day delivery the minimum bill amount should be above 399 rupees as we do not process the order below 399 rupees. </br>
								•	A customer is eligible for free shipping only if the bill amount is on & above 599. </br>
								•	Customer support team is available from 9:00 AM to 7:00 PM. (Tuesday to Sunday) regarding any complaints and delivery issue contact customer support team landline number 022-28113596 </br>
								•	After receiving the delivery if you find any defect in the products please share photos within 24 hours on WhatsApp number 9820166952 to get replacement in the next delivery. If a customer is unable to share, the company is not responsible for his/her replacement. </br>
								•	After confirmation call by customer support team orders would be processed. If the call is been unanswered the order cannot be processed. Order before 7.00PM to get next day delivery. </br>
								•	After placing an order on website the bill amount you get is for reference. Products are billed by their weights and price may vary. As there is no exact size for fruits and vegetables. </br>
								•	We do deliveries all over Mumbai, Navi Mumbai and Thane. The boundary line for western zone is Mira Road for central Zone is Thane. On Sunday’s deliveries above boundary lines. Customers beyond the boundary lines can order on Saturday to get next day delivery on Sunday’s. </br>
								•	For outstation orders delivery charges are mandatory. For outstation orders WhatsApp your orders on 9820166952. Our team will send you the bill amount along with delivery charges once NEFT is done. Order will be dispatched from our side. </br>
								•	For Delivery status customer can contact Customer Support Team 022-28113596. They will give you an approx. timing. There is no exact timing for the deliveries. </br></br>
								Cancellations and Refund</br>
								•	Products once ordered can be cancelled till the time of delivery. </br>
								•	You can cancel your orders at the time of delivery. </br>
								•	After accepting the order if there is any defect in the product a customer can return the product there itself. </br>
								•	While accepting the order please check all the products. Once accepted the Order cannot be cancelled. </br>
								</p>
								<br/>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
  <!-- Main Container End -->	