  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="row">
        <section class="col-main col-sm-12">
          
          <div id="contact" class="page-content page-contact">
          <div class="page-title">
            <h2>Contact Us</h2>
          </div>
            <div id="message-box-conact">We're here to help !</div>
            <div class="row">
              <div class="col-xs-12 col-sm-6" id="contact_form_map">
                <h3 class="page-subheading">100% Organic produce harvested using Natural Farming Methods</h3>
                <p>“Organic agriculture is a production system that sustains the health of soils, ecosystems and people. It relies on ecological processes, biodiversity and cycles adapted to local conditions, rather than the use of inputs with adverse effects. Organic agriculture combines tradition, innovation and science to benefit the shared environment and promote fair relationships and a good quality of life for all involved…”</br></br>Asmita Group is committed to nature, clean environment and health of the people and added one innovative steps by opting ‘Organic farming technique’ in its multi locational farms in the state of Maharashtra under the name ‘Asmita Organic Farm’ having vast range of agro produces through Organic farming technique, it involves cultivation of plants and rearing of animals in natural ways. This process involves the use of biological materials, avoiding synthetic substances to maintain soil fertility and ecological balance thereby minimizing pollution and wastage.   </br></br>Fresh Fruits, Vegetables, Staple Foods and pulses, cultivated/produces at ‘Asmita Organic Farm’ fields are loaded with nutrients such as vitamins, enzymes, minerals and other micro-nutrients compared to those from conventional farms. It relies on ecologically balanced agricultural principles like crop rotation, green manure, organic waste, biological pest control, mineral and rock additives.</p>
                
                <br/>
                
                <ul class="store_info">
                  <li><i class="fa fa-home"></i>
                      Asmita Supermarkets Pvt. Ltd.<br>
                      <i class="fa fa-homes"></i>Opp. HDFC Bank, Naya Nagar, Mumbai – 401107, Dist. Thane, Maharashtra, India<br>
                  <li><i class="fa fa-phone"></i><span>98193 12333</span></li>
                  <!--<li><i class="fa fa-fax"></i><span>+91 9870072870</span></li>-->
                  <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:info@asmitaorganicfarm.com">info@asmitaorganicfarm.com</a></span></li>
                  <li><i class="fa fa-facebook"></i> <span><a target="_blank" href="http://www.facebook.com/asmitaorganicfarm">www.facebook.com/asmitaorganicfarm</a></span></li>
                   <li><i class="fa fa-twitter"></i> <span><a target="_blank" href="http://www.twitter.com/asmitaorganic">www.twitter.com/asmitaorganic</a></span></li>
                </ul>
                </br>
                <h3>Nagpur Farm Location</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3201.0373514870053!2d79.24557581451302!3d21.445344085774114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd4d6e5b53d32a9%3A0x7c12b92e5938b355!2sAsmita+Organic+Farm+-+Nagpur!5e1!3m2!1sen!2sin!4v1513171057858" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                
                </br>
                </br>
                <h3>Dahanu Farm Location</h3>
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2913.7579748632556!2d72.82745671491558!3d19.994674086567564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be724498db387cf%3A0xc595c04b2d4ebd2e!2sDahanu+-+Asmita+Organic+Farm!5e1!3m2!1sen!2sin!4v1542815484876" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
              <div class="col-sm-6">
			   <?php
				if(isset($msg) && !empty($msg))
				{?>

				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong><?php echo $msg; ?></strong>
				</div>
				<?php } ?>
                <h3 class="page-subheading">Make an enquiry</h3>
				  <form class="form align-center" method="post" action="<?php echo ADMIN_URL."Contactus/ragistration" ;?>" id="regform" >
                <div class="contact-form-box">
                  <div class="form-selector">
                    <label>Full Name</label>
                    <input type="text" name="name" class="form-control input-sm" id="name" />
					<?php echo form_error('name', '<small data-bv-result="INVALID" data-bv-for="name" data-bv-validator="notEmpty" class="help-block error" style="">', '</small>'); ?>
                  </div>
                  <div class="form-selector">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control input-sm" id="email" />
					<?php echo form_error('email', '<small data-bv-result="INVALID" data-bv-for="email" data-bv-validator="notEmpty" class="help-block error" style="">', '</small>'); ?>
                  </div>
                  <div class="form-selector">
                    <label>Contact number</label>
                    <input type="text" name="mobile" class="form-control input-sm" id="mobile" />
					<?php echo form_error('mobile', '<small data-bv-result="INVALID" data-bv-for="mobile" data-bv-validator="notEmpty" class="help-block error" style="">', '</small>'); ?>
                  </div>			
                  <div class="form-selector">
                    <label>Add Your Queries Here</label>
                     <textarea class="form-control input-sm" rows="10" id="message" name="message"></textarea>
					<?php echo form_error('message', '<small data-bv-result="INVALID" data-bv-for="message" data-bv-validator="notEmpty" class="help-block error" style="">', '</small>'); ?>
                  </div>
                  <div class="form-selector">
                    <button class="button"><i class="fa fa-send"></i>&nbsp; <span>Send Message</span></button>
                    &nbsp; <a href="#" class="button">Clear</a> </div>
                </div></form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
  <!-- Main Container End -->
<script src="<?php echo base_url('assets/js/jquery-validation/jquery.validate.min.js');?>"></script>
<script>
   $(function(){

	$("#regform").validate({
		rules: {
			
			name: {
				required: true,
				
			},			
			email: {
				required: true,
				
			},
			mobile: {
				required: true
			}
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});

</script>
