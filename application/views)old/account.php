 <section class="main-container col2-right-layout">
    <div class="main container">
		<div class="row">
			<div class="myaccount pb20">
		<div class="myaccount">
      <div class="well">
      <?php $uid = $this->session->userdata('id'); ?>
        <div class="row"><div class="col-sm-2 tac"><img src="<?php echo base_url('assets/images/spacer.gif');?>" class="myaccount-sp sworld-edit" alt="" /></div>
        <div class="col-sm-10 text-center-xs">
          <h4><a href="<?php echo base_url('account/profile/'.$uid);?>">Edit Profile</a></h4>
          <p>Here you can change the information given to us when you first became a member. You can update your name, address, etc.</p>
        </div></div>
      </div>
      <div class="well">
        <div class="row"><div class="col-sm-2 tac"><img src="<?php echo base_url('assets/images/spacer.gif');?>" class="myaccount-sp sworld-changepw" alt="" /></div>
        <div class="col-sm-10 text-center-xs">
          <h4><a href="<?php echo base_url('account/changePassword/'.$uid);?>">Change Password</a></h4>
          <p>Here you can change your password for login to Asmita Organic Farm</p>
        </div></div>
      </div>
     <!-- <div class="well">
        <div class="row"><div class="col-sm-2 tac"><img src="<?php echo base_url('assets/images/spacer.gif');?>" class="myaccount-sp sworld-addbook" alt="" /></div>
        <div class="col-sm-10 text-center-xs">
          <h4><a href="<?php echo base_url('account/address_book/'.$uid);?>">Address book</a></h4>
          <p>Here you are able to store and edit your shipping and billing information.</p>
        </div></div>
      </div>-->
      <div class="well">
        <div class="row"><div class="col-sm-2 tac"><img src="<?php echo base_url('assets/images/spacer.gif');?>" class="myaccount-sp sworld-order" alt="" /></div>
        <div class="col-sm-10 text-center-xs">
          <h4><a href="<?php echo base_url('account/orders/');?>">Order History</a></h4>
          <p>Here you can view all previous orders placed with Asmita Organic Farm.</p>
        </div></div>
      </div>
     <!-- <div class="well" >
        <div class="row"><div class="col-sm-2 tac"><img src="<?php echo base_url('assets/images/spacer.gif');?>" class="myaccount-sp sworld-wishlist" alt="" /></div>
        <div class="col-sm-10 text-center-xs">
          <h4><a href="<?php echo base_url('account/wallet/'.$uid);?>">Wallet</a></h4>
          <p>Here You can recharge you wallet!</p>
        </div></div>
      </div>
	   <div class="well" style="margin-bottom:0px;">
        <div class="row"><div class="col-sm-2 tac"><img src="<?php echo base_url('assets/images/spacer.gif');?>" class="myaccount-sp sworld-order" alt="" /></div>
        <div class="col-sm-10 text-center-xs">
          <h4><a href="<?php echo base_url('account/subscrib/'.$uid);?>">Subscribe</a></h4>
          <p>Here you can view all previous orders placed with Kuchdifferent . This can be helpful if an item you purchased sold.</p>
        </div></div>
      </div>-->
    </div>
    </div>
		</div>		
</div>
  </section>