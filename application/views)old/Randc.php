								
  <!-- Main Container -->
	<section class="main-container col1-layout">
		<div class="main container">
			<div class="row">
				<section class="col-main col-sm-12">
                    <div id="contact" class="page-content page-contact">
						<div class="row">
							<div class="col-xs-12 col-sm-12" id="contact_form_map">								
								<h3 class="page-subheading">RETURN & CANCELLATION</h3>
								<p>
								Returns & Exchange Policy: </br>
								1.	Our products are thoroughly checked for the quality before delivering. </br>
								2.	We insist that the customer has thoroughly checked the quality of fresh produce and grocery at the time of delivery. </br>
								3.	In case of fresh produce (fruits, vegetable, and dairy products), it is mandatory for the customer to check the quality of the product at the time of delivery itself. Applicable only for Mumbai, Navi Mumbai & Thane </br>
								4.	If any of the fresh produce is not up to the customer’s requirement, they are requested to return the whole quantity of that product to the delivery boy. Applicable only for Mumbai, Navi Mumbai & Thane </br>
								a.	The delivery boy will deduct the amount of the returned product from the total bill, and the customer will make the payment accordingly. </br>
								b.	No quality issues related to the fresh produce will be entertained by the Call Centre executive thereafter. </br>
								c.	However in case of grocery, if the customer is unable to check the quality at the time of delivery, he/she should inform the same to the Call Centre (022-28113596 ) within 72 hrs (3 days) of delivery. Applicable only for Mumbai, Navi Mumbai & Thane </br>
								5.	The customer could get an exchange of the grocery pack only under following conditions: </br>
								a.	Packets that have live or dead insects. </br>
								b.	Packets that have signs of rotting. </br>
								c.	Packets that are torn, not sealed properly at the time of delivery </br>
								d.	Products that have expired. </br>
								e.	Return accepted within 2 days from the date customer purchase the products. </br>
								f.	Other than above no other issues related to products will be addressed </br>
								6.	The customer shall not get an exchange of grocery product under following conditions: </br>
								a.	When the packet is opened and a product is used. </br>
								b.	Customers can’t claim an exchange when they have cooked or consumed the item. </br>
								c.	The item is packed and returned with any other packing material. </br>
								7.	The customer could exchange the product with a new pack of the same product, or any other product of the same or higher value. </br>
								8.	For customers outside Mumbai, Navi Mumbai & Thane can get the money back only as per our money back policy. </br> </br>
								 
								Cancellation Policy: </br>
								1.	Request to cancel the order will be accepted only if the product has not been shipped by us. If accepted, the customer is entitled to get a refund of the entire amount. </br>
								2.	If we receive a cancellation request from the customer after the product/s has been shipped by us, cancellation request will not be accepted. </br>
								3.	The Asmita Organic Farm reserves the right to cancel or refuse to accept any order placed due to various reasons like no availability of stock, pricing errors, factual errors or problems identified with the personal/financial details provided by the customer. </br> </br>
								 
								Money Back in case of cancellation or return: </br>
								In general case of exchange/replacement/return due to above-mentioned quality issues and a written proof from the customer following refund policies are applicable: </br>
								1.	If a customer doesn’t wish to avail the replacement, we issue a voucher of the same value to the customer to be availed within a specified time period mentioned on that. </br>
								2.	In case of order cancellation/return through card/Net banking we credit the money back to your credit/debit card or Net Banking which takes up to 10 to 15 working days to reflect in your statement. </br>
								3.	In any of the above case delivery charges are non-refundable. </br>
								Note: Under no circumstances, the customer would be given a cash refund </br>


								</p>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
  <!-- Main Container End -->

