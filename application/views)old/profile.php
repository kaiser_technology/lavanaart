 <section class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <div class="col-main col-sm-9 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>User Profile</h2>
            </div>
				<div class="account-login">
          <div class="box-authentication new-customer-box Account Page ">
          
          <form method="post" id="regform">
                <div class="row">
                  <div class="col-xs-12">
                   <?php if(isset($msg)){ echo '<label id="email-error" class="error" for="email">'.$msg.'</lebel>'; }?>
                  </div>
				  <div class="row">
				   <div class="col-sm-4">
                    <label>Title:</label>
                    <div class="input-text">
                     <select name="prefix" id = "prefix" style="width: 100%;">
						<option value="">Select</option>
						<option <?php if($userinfo->user_prefix == "Mr."){ echo "selected";} ?>>Mr.</option>
						<option <?php if($userinfo->user_prefix == "Mrs."){ echo "selected";} ?>>Mrs.</option>
						<option <?php if($userinfo->user_prefix == "Miss"){ echo "selected";} ?>>Miss</option>
					 </select>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <label>First Name:</label>
                    <div class="input-text">
					
                      <input type="text" name="fname" id = "fname" value="<?php echo $userinfo->user_firstName;?>" class="form-control">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <label>Last Name:</label>
                    <div class="input-text">
                      <input type="text" name="lname" id="lname" value="<?php echo $userinfo->user_lastName;?>" class="form-control">
                    </div>
                  </div>
				  </div>
				  <div class="row">
                  <div class="col-xs-12">
                    <label>Address:</label>
                    <div class="input-text">
                      <input type="text" name="address" id="address" value="<?php echo $userinfo->user_address;?>" class="form-control">
                    </div>
                  </div>
				  </div><div class="row">
                  <div class="col-xs-6">
                    <label>City/Town:</label>
                    <div class="input-text">
                      <input type="text" name="city" id="city" value="<?php echo $userinfo->city;?>" class="form-control">
                    </div>
                  </div>
				  <div class="col-xs-6">
                    <label>Pincode:</label>
                    <div class="input-text">
                      <input type="text" name="pincode" id="pincode" value="<?php echo $userinfo->user_postalCode;?>" class="form-control">
                    </div>
                  </div></div><div class="row">
                  <div class="col-sm-6">
                    <label>Email:</label>
                    <div class="input-text">
                      <input type="text" name="email" id="email" value="<?php echo $userinfo->user_email;?>"  class="form-control">
					   <?php echo form_error('email', '<label id="email-error" class="error" for="email">', '</label>'); ?>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <label>Phone:</label>
                    <div class="input-text">
                      <input type="text" name="phone" id = "phone" value="<?php echo $userinfo->user_contact;?>" class="form-control">
                    </div>
                  </div>
                  </div>
				  
                  <div class="col-xs-12">
                    <!--<div class="billing-checkbox">
                    <label class="inline" for="rememberme">
                      <input type="checkbox" value="forever" id="rememberme" name="rememberme">
                      Sign up for our newsletter! </label></div>-->
                    <div class="submit-text">
						<input type="hidden" name="update" value="update" />
                      <button class="button"><i class="fa fa-user"></i>&nbsp; <span>Update</span></button>
                    </div>
                  </div>
                </div>
              </form>
              
         
       
          </div>
        </div>
    
           </div>
        </div>
        <aside class="right sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              <ul>
                <li><a href="<?php echo ADMIN_URL."account" ;?>">Account Dashboard</a></li>
                <li class="current"><a href="javascript:void(0)">Account Information</a></li>
                <li><a href="<?php echo ADMIN_URL."account/orders" ;?>">My Orders</a></li>
                <li><a href="<?php echo ADMIN_URL."account/changePassword" ;?>">Change Password</a></li>
              </ul>
            </div>
          </div>
                  </aside>
      </div>
    </div>
  </section> <script src="http://navavyapar.com/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#regform").validate({
		rules: {
			prefix: {
				required: true,
				
			},
			fname: {
				required: true,
				
			},
			lname: {
				required: true,
				
			},
			address: {
				required: true,
				
			},
			city: {
				required: true,
				
			},
			pincode: {
				required: true,
				
			},
			email: {
				required: true,
				email:true
			},
			phone: {
				required: true,
				number : true
			}
		},
		messages: {
			
			prefix: {
				required: "Field Required",
				
			},
			fname: {
				required: "Field Required",
				
			},
			lname: {
				required: "Field Required",
				
			},
			address: {
				required: "Field Required",
				
			},
			city: {
				required: "Field Required",
				
			},
			pincode: {
				required: "Field Required",
				
			},
			email: {
				required: "Field Required",
				email : "Enter Valid Email"
			},
			phone: {
				required: "Field Required",
				
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>