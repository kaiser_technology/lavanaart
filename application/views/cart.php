 <!-- Breadcrumb area Start -->

        <div class="breadcrumb-area bg--white-6 breadcrumb-bg-1 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title">Cart</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Cart</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--80 pb--80 pt-md--45 pt-sm--25 pb-md--60 pb-sm--40">
                        <div class="col-lg-8 mb-md--30">
                            <form class="cart-form" method="post" action="<?php echo ADMIN_URL."ProductCart/updatecart";?>">
                                <div class="row no-gutters">
                                    <div class="col-12">
                                        <div class="table-content table-responsive">
                                            <table class="table text-center">
                                                <thead>
                                                    <tr>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>
                                                        <th class="text-left">Product</th>
                                                        <th>price</th>
                                                        <th>quantity</th>
                                                        <th>total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
												 <?php $i = 1; ?>
												<?php foreach ($this->cart->contents() as $items){ ?>
												<?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                                                    <tr id="<?php echo $items['rowid'];?>">
                                                        <td class="product-remove text-left"><a href="#" onclick="removeCart('<?php echo $items['rowid']; ?>')"><i class="dl-icon-close"></i></a></td>
                                                        <td class="product-thumbnail text-left">
                                                            <img src="<?php echo CDN_IMG_URL.'product/'.$items['options']['proimage'];?>" alt="<?php echo $items['name']; ?>">
                                                        </td>
                                                        <td class="product-name text-left wide-column">
                                                            <h3>
                                                                <a href="<?php echo base_url()."product/detail/".$this->pro->slugify($this->pro->cat_name($items['options']['catid'])).'/'.$items['urlname'].'/'.$items['options']['proid'].'/'.$items['options']['catid'];?>"><?php echo $items['name']; ?></a>
                                                            </h3>
                                                        </td>
                                                        <td class="product-price">
                                                            <span class="product-price-wrapper">
                                                                <span class="money"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo ($this->pro->convertRate($items['price'])); ?></span>
                                                            </span>
                                                        </td>
                                                        <td class="product-quantity">
                                                            <div class="quantity">
                                                                <input type="number" class="quantity-input" name="<?php echo $i.'[qty]';?>" id="cart-size-<?php echo $i;?>" value="<?php echo $items['qty']; ?>" min="1">
                                                            </div>
                                                        </td>
                                                        <td class="product-total-price">
                                                            <span class="product-price-wrapper">
                                                                <span class="money"><strong><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo ($this->pro->convertRate($items['subtotal'])); ?></strong></span>
                                                            </span>
                                                        </td>
                                                    </tr>
												<?php }?>
                                                  </tbody>
                                            </table>
                                        </div>  
                                    </div>
                                </div>
                                <div class="row no-gutters border-top pt--20 mt--20">
                                    <div class="col-sm-6">
                                        <!--div class="coupon">
                                            <input type="text" id="coupon" name="coupon" class="cart-form__input" placeholder="Coupon Code">
                                            <button type="submit" class="cart-form__btn">Apply Coupon</button>
                                        </div-->
                                    </div>
                                    <div class="col-sm-6 text-sm-right">
									<?php if($this->cart->total() > 0){?>
                                        <button type="submit" class="cart-form__btn" name="clear_cart_action" value="empty_cart" id="empty_cart_button">Clear Cart</button>
                                        <button type="submit" name="update_cart_action" value="update_qty" title="Update Cart"  class="cart-form__btn btn-update">Update Cart</button>
									<?php }?>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-4">
                            <div class="cart-collaterals">
                                <div class="cart-totals">
                                    <h5 class="mb--15">Cart totals</h5>
                                    <div class="table-content table-responsive">
                                        <table class="table order-table">
                                            <tbody>
                                                <tr>
                                                    <th>Subtotal</th>
                                                    <td><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo ($this->pro->convertRate($this->cart->total())); ?></td>  
                                                </tr>
                                                <tr>
                                                    <th>Shipping</th>
                                                    <td>
                                                        <span><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> 00.00</span>
                                                        </td>  
                                                </tr>
                                                <tr class="order-total">
                                                    <th>Total</th>
                                                    <td>
                                                        <span class="product-price-wrapper">
                                                            <span class="money"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo ($this->pro->convertRate($this->cart->total())); ?></span>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
								<?php if($this->cart->total() > 0){?>
                                <a href="<?php echo base_url();?>checkout/index" class="btn btn-fullwidth btn-style-1">
                                    Proceed To Checkout
                                </a>
								<?php }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->
		 