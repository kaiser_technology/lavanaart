 <!-- Breadcrumb area Start -->

        <div class="breadcrumb-area bg--white-6 breadcrumb-bg-1 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title">Register</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Register</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--75 pt-md--55 pt-sm--35 pb--80 pb-md--60 pb-sm--40">
                        <div class="col-md-8 offset-md-2 mb-sm--30">
                            <div class="login-box">
                                <h4 class="mb--35 mb-sm--20">Register</h4>
								<?php 
			  if(isset($msg)){
			  echo '<label id="email-error" class="error" for="email">'.$msg.'</label>';
			  }
			  ?>
                                <form class="form form--login" id="regform" method="post">
								<div class="row">
									<div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="title">Title <span class="required">*</span></label>
                                       
										<select name="prefix" class="form__input form__input--3" id = "prefix" style="width: 100%;">
										<option value="">Select</option>
										<option>Mr.</option>
										<option>Mrs.</option>
										<option>Miss</option>
									</select>
										
                                    </div>
									
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="First Name">First Name <span class="required">*</span></label>
                                        <input type="text" class="form__input form__input--3" id="fname" name="fname">
										<?php echo form_error('fname', '<label id="email-error" class="error" for="email">', '</label>'); ?>
                                    </div>
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="Last Name">Last Name <span class="required">*</span></label>
                                        <input type="text" class="form__input form__input--3" id="lname" name="lname">
										<?php echo form_error('lname', '<label id="email-error" class="error" for="email">', '</label>'); ?>
                                    </div>
									</div>
									 <div class="form__group mb--20">
                                        <label class="form__label form__label--2" for="address">Address <span class="required">*</span></label>
                                        <input type="text" class="form__input form__input--3" id="address" name="address">
										<?php echo form_error('address', '<label id="email-error" class="error" for="address">', '</label>'); ?>
                                    </div>
									<div class="row">
									 <div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="City/Town">City/Town <span class="required">*</span></label>
                                        <input type="text" class="form__input form__input--3" id="city" name="city">
										<?php echo form_error('city', '<label id="email-error" class="error" for="City/Town">', '</label>'); ?>
                                    </div>
									 <div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Pincode">Pincode <span class="required">*</span></label>
                                        <input type="text" class="form__input form__input--3" id="pincode" name="pincode">
										<?php echo form_error('pincode', '<label id="email-error" class="error" for="Pincode">', '</label>'); ?>
                                    </div>
                                 </div>
								  <div class="row">
									<div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Email">Email <span class="required">*</span></label>
                                        <input type="text" class="form__input form__input--3" id="email" name="email">
										<?php echo form_error('email', '<label id="email-error" class="email" for="Email">', '</label>'); ?>
                                    </div>
									<div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Phone">Mobile Number <span class="required">*</span></label>
                                        <input type="text" class="form__input form__input--3" id="phone" name="phone">
										<?php echo form_error('phone', '<label id="email-error" class="error" for="Phone">', '</label>'); ?>
                                    </div>
									
									 </div>
								  <div class="row">
									<div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Password">Password <span class="required">*</span></label>
                                        <input type="password" class="form__input form__input--3" id="pass" name="pass">
										<?php echo form_error('pass', '<label id="email-error" class="error" for="Password">', '</label>'); ?>
                                    </div>
									<div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Confirm Password">Confirm Password <span class="required">*</span></label>
                                        <input type="password" class="form__input form__input--3" id="cpass" name="cpass">
										<?php echo form_error('cpass', '<label id="email-error" class="error" for="Confirm Password">', '</label>'); ?>
                                    </div>
                                    </div>
                                    <div class="d-flex align-items-center mb--20">
                                        <div class="form__group">
                                            <input type="submit" value="Register" class="btn btn-submit btn-style-1">
                                        </div>
                                      
                                    </div>
                                   
                                </form>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->

<script src="<?php echo ADMIN_URL;?>assets/js/jquery.validate.min.js"></script>
<script>

  $(function(){

	$("#regform").validate({
		rules: {
			prefix: {
				required: true,
				
			},
			fname: {
				required: true,
				
			},
			lname: {
				required: true,
				
			},
			address: {
				required: true,
				
			},
			city: {
				required: true,
				
			},
			pincode: {
				required: true,
				
			},
			email: {
				required: true,
				email:true
			},
			phone: {
				required: true,
				number : true
			},
			pass: {
				required: true,
				
			},
			cpass: {
				required: true,
				equalTo : pass
			}
		},
		messages: {
			
			prefix: {
				required: "Field Required",
				
			},
			fname: {
				required: "Field Required",
				
			},
			lname: {
				required: "Field Required",
				
			},
			address: {
				required: "Field Required",
				
			},
			city: {
				required: "Field Required",
				
			},
			pincode: {
				required: "Field Required",
				
			},
			email: {
				required: "Field Required",
				email : "Enter Valid Email"
			},
			phone: {
				required: "Field Required",
				
			},
			pass: {
				required: "Field Required",
				
			},
			cpass: {
				required: "Field Required",
				equalTo : "Please Enter Same Password as above"
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>