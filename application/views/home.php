 <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
		<?php if(isset($bannerList->detail) && !empty($bannerList->detail)){ ?>
  
			 <?php 
				$offerlist = array();
				$p = 1;
				foreach($bannerList->detail as $val){
						if($val->sale == "external"){
						$offerlist[] = $val;
					}
				}
		
				?>
            <div class="homepage-slider">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div id="rev_slider_10_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="home-15" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                            <!-- START REVOLUTION SLIDER 5.4.7 auto mode -->
                                <div id="rev_slider_10_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7">
                                    <ul>    <!-- SLIDE  -->
										<?php
											$p = 1;
											foreach($bannerList->detail as $val){
											if($val->bannerType != "Main Banner")	{ continue; }	
											?>
									
                                        <li data-index="rs-22-<?php echo $p;?>" data-transition="random" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                            <!-- MAIN IMAGE -->
                                            <img src="<?php echo CDN_IMG_URL.'/banner/'.$val->img;?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                                            <!-- LAYERS -->
                                        </li>
                                        <!-- SLIDE  -->
								<?php 
								}
				
								?>	
										
                                        
                                    </ul>
                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> 
                                </div>
                            </div><!-- END REVOLUTION SLIDER -->      
                        </div>
                    </div>
                </div>
            </div>
		 <?php 
		  }
		  ?>
            <!-- Banner Area Start -->

            <div class="banner-area pt--30">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12">
                            <div class="airi-element-carousel banner-carousel"
                                data-slick-options='{
                                    "spaceBetween": 30,
                                    "slidesToShow": 3,
                                    "slidesToScroll": 3
                                }'
                                data-slick-responsive='[
                                    {"breakpoint":1200, "settings": {
                                        "slidesToShow": 2,
                                        "slidesToScroll": 2
                                    } },
                                    {"breakpoint":576, "settings": {
                                        "slidesToShow": 1,
                                        "slidesToScroll": 1
                                    } }
                                ]'
                            >
							<?php
											$p = 1;
											foreach($bannerList->detail as $val){
											if($val->bannerType == "Main Banner")	{ continue; }	
											?>
                                <div class="item">
                                    <div class="banner-box banner-type-10 banner-hover-3">
                                        <div class="banner-inner">
                                            <div class="banner-image">
                                                <img src="<?php echo CDN_IMG_URL.'/banner/'.$val->img;?>" alt="Banner">
                                            </div>
                                            <div class="banner-info">
                                                <div class="banner-info--inner">
													<!--<p class="banner-title-1">Mid-Century Collections</p>
                                                    <p class="banner-title-7 font-bold text-uppercase">Sale 25%</p>-->
                                                    <a href="<?php echo $val->url;?>" class="btn btn-2 btn-medium btn-style-1">Shop Now</a>
                                                </div>
                                            </div>
                                            
                                           
                                        </div>
                                    </div>
                                </div>
											<?php }?>
                               </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Banner Area End -->

            <!-- Product Tab Area Start Here -->

            <section class="product-tab-area pt--70 pt-md--50 pb--40 pb-md--30">
                <div class="container-fluid">
                    <div class="row mb--40 mb-md--30">
                        <div class="col-12 text-center">
                            <h2 class="heading-secondary font-bold text-uppercase">New Arrival</h2>
                            <hr class="separator separator-color-2 center mt--25 mt-md--15">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="product-tab tab-style-3">
                                <div class="nav nav-tabs product-tab__head justify-content-center align-items-center flex-md-row flex-column mb--40 mb-md--30" id="product-tab" role="tablist">
    								<a class="product-tab__link nav-link active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-selected="true"> 
                                            <span>All</span>
                                        </a>
    								<?php 
    								$p=0;
    								foreach($productList as $key=>$val){
    									?>	
                                        <a class="product-tab__link nav-link " id="nav-all-tab" data-toggle="tab" href="#nav-<?php echo $this->pro->slugify($this->pro->cat_name($key));?>" role="tab" aria-selected="true"> 
                                            <span><?php echo $this->pro->cat_name($key);?></span>
                                        </a>
    								<?php 
    								$p++;
    								if($p == 6) { break;}
    								}?>	
                                   
                                </div>
                                <div class="tab-content product-tab__content fadeInUp" id="product-tabContent">
                                    <div class="tab-pane show active" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
                                        <div class="row">
										<?php 
											$p=0;
											$i=0;
											foreach($productList as $key=>$pval){
											if($p == 6) { break; }
											$p++;	
												foreach($pval as $product) {
								                $detailUrl= base_url()."product/detail/". $this->pro->slugify($this->pro->cat_name($product->pro_cat)).'/'.$this->pro->slugify($product->pro_name).'/'.$product->pro_id .'/'.$product->pro_cat;
											
											?>	
                                            <div class="col-xl-3 col-md-4 col-sm-6 mb--40 mb-md--30">
                                                <div class="airi-product">
                                                    <div class="product-inner">
                                                        <figure class="product-image">
                                                            <div class="product-image--holder">
                                                                <a href="<?php echo $detailUrl; ?>">
                                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image;?>" alt="<?php echo $product->pro_name;?>" class="primary-image">
                                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image;?>" alt="<?php echo $product->pro_name;?>" class="secondary-image">
                                                                </a>
                                                            </div>
                                                            <div class="airi-product-action">
                                                                <div class="product-action">
                                                                    <a href="<?php echo $detailUrl; ?>" class="quickview-btn action-btn" data-toggle="tooltip" data-placement="top" title="More Details">
                                                                        <!--<span data-toggle="modal" data-target="#productModal<?php echo $i;?>">-->
                                                                            <i class="dl-icon-view"></i>
                                                                        <!--</span>-->
                                                                    </a>
                                                                    <a class="add_to_cart_btn action-btn" onclick="addToCart('<?php echo $product->pro_id;?>','<?php echo $product->pro_mrp_price;?>','<?php echo $product->pro_name;?>','<?php echo $product->pro_price_id;?>','<?php echo $product->pro_code;?>','<?php echo $product->pro_image;?>','1','<?php echo $product->pro_cat;?>');" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Add to Cart">
                                                                        <i class="dl-icon-cart29"></i>
                                                                    </a>
                                                                    
                                                                </div>
                                                            </div>
                                                        </figure>
                                                        <div class="product-info text-center">
                                                            <h3 class="product-title">
                                                                <a href="product-details.html"><?php echo $product->pro_name;?></a>
                                                            </h3>
                                                            <span class="product-price-wrapper">
                                                                <span class="money"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo $this->pro->convertRate($product->pro_mrp_price);?></span>
                                                            </span>
                                                        </div>
                                                        <div class="product-overlay"></div>
                                                    </div>
													<?php //include('include/productModel.php');?>
                                                </div>
                                            </div>
												<?php 
												
												$i++;
												}
											}?>												
                                           </div>
                                    </div>
									
									<?php 
									
											$p=0;
											$i=50;
											foreach($productList as $key=>$pval){
											if($p == 6) { break; }
											$p++;
												
											?>	
                                    <div class="tab-pane" id="nav-<?php echo $this->pro->slugify($this->pro->cat_name($key));?>" role="tabpanel" aria-labelledby="nav-chair-tab">
                                        <div class="row">
										<?php foreach($pval as $product) { ?>
                                             <div class="col-xl-3 col-md-4 col-sm-6 mb--40 mb-md--30">
                                                <div class="airi-product">
                                                    <div class="product-inner">
                                                        <figure class="product-image">
                                                            <div class="product-image--holder">
                                                                <a href="product-details.html">
                                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image;?>" style="width: 500px;height: 500px;" alt="<?php echo $product->pro_name;?>" class="primary-image">
                                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image1;?>" style="width: 500px;height: 500px;" alt="<?php echo $product->pro_name;?>" class="secondary-image">
                                                                </a>
                                                            </div>
                                                            <div class="airi-product-action">
                                                                <div class="product-action">
                                                                    <a class="quickview-btn action-btn" data-toggle="tooltip" data-placement="top" title="Quick Shop">
                                                                        <span data-toggle="modal" data-target="#productModal<?php echo $i;?>">
                                                                            <i class="dl-icon-view"></i>
                                                                        </span>
                                                                    </a>
                                                                    <a class="add_to_cart_btn action-btn" onclick="addToCart('<?php echo $product->pro_id;?>','<?php echo $product->pro_mrp_price;?>','<?php echo $product->pro_name;?>','<?php echo $product->pro_price_id;?>','<?php echo $product->pro_code;?>','<?php echo $product->pro_image;?>','1','<?php echo $product->pro_cat;?>');" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Add to Cart">
                                                                        <i class="dl-icon-cart29"></i>
                                                                    </a>
                                                                    
                                                                </div>
                                                            </div>
                                                        </figure>
                                                        <div class="product-info text-center">
                                                            <h3 class="product-title">
                                                                <a href="product-details.html"><?php echo $product->pro_name;?></a>
                                                            </h3>
                                                            <span class="product-price-wrapper">
                                                                <span class="money"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i><?php echo $this->pro->convertRate($product->pro_mrp_price);?></span>
                                                            </span>
                                                        </div>
                                                        <div class="product-overlay"></div>
                                                    </div>
                                                </div>
                                            </div>
										<?php 
										//include('include/productModel.php');
												$i++;
										}?>   
											</div>
										 </div>	
										<?php }?>   	
                                   
                                    </div>
                            </div>
                        </div>
                    </div>
                    <!--div class="row">
                        <div class="col-12 text-center">
                            <a class="view-all" href="shop-sidebar.html">View All</a>
                        </div>
                    </div-->
                </div>
            </section>

            <!-- Product Tab Area End Here -->

            <!-- Banner Area Start -->

            <div class="banner-area pt--40 pt-md--30">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12">
                            <div class="airi-element-carousel banner-carousel"
                                data-slick-options='{
                                    "spaceBetween": 30,
                                    "slidesToShow": 3,
                                    "slidesToScroll": 3
                                }'
                                data-slick-responsive='[
                                    {"breakpoint":1200, "settings": {
                                        "slidesToShow": 2,
                                        "slidesToScroll": 2
                                    } },
                                    {"breakpoint":576, "settings": {
                                        "slidesToShow": 1,
                                        "slidesToScroll": 1
                                    } }
                                ]'
                            >
							
							<?php
											$p = 1;
											foreach($bannerList->detail as $val){
											if($val->bannerType == "Main Banner")	{ continue; }
											if( $p < 4)
											{
												$p++;
												continue;
											}
											$p++;
											?>
                                <div class="item">
                                    <div class="banner-box banner-type-10 banner-hover-3">
                                        <div class="banner-inner">
                                            <div class="banner-image">
                                                <img src="<?php echo CDN_IMG_URL.'/banner/'.$val->img;?>" alt="Banner">
                                            </div>
                                            <div class="banner-info">
                                                <div class="banner-info--inner">
													<p class="banner-title-1"><?php echo $val->sort_title ?></p>
                                                    <p class="banner-title-7 font-bold text-uppercase">Sale <?php echo $val->sale ?></p>
													
                                                    <a href="<?php echo $val->url;?>" class="btn btn-2 btn-medium btn-style-1">Shop Now</a>
                                                </div>
                                            </div>
                                            
                                           
                                        </div>
                                    </div>
                                </div>
											<?php }?>
							
                                 </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Banner Area End -->

            <!-- Method area Start Here -->

            <section class="method-area pt--40 pt-md--30 pb--35 pb-md--25">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 mb-md--30">
                            <div class="method-box method-box-2 text-center">
                                <img src="assets/img/icons/icon-5.png" alt="Icon">
                                <h4 class="font-weight-400 mt--20">FREESHIPPING WORLD WIDE</h4>
                                <p>Freeship over oder $100</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mb-md--30">
                            <div class="method-box method-box-2 text-center">
                                <img src="assets/img/icons/icon-6.png" alt="Icon">
                                <h4 class="font-weight-400 mt--20">30 DAYS MONEY Returns</h4>
                                <p>Derabitur eget vehicula</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 mb-sm--30">
                            <div class="method-box method-box-2 text-center">
                                <img src="assets/img/icons/icon-7.png" alt="Icon">
                                <h4 class="font-weight-400 mt--20">SUPPORT 24/7</h4>
                                <p>Dedicated Support</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="method-box method-box-2 text-center">
                                <img src="assets/img/icons/icon-8.png" alt="Icon">
                                <h4 class="font-weight-400 mt--20">100% SECURE CHECKOUT</h4>
                                <p>Protect buyer &amp; Security</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Method area End Here -->
        </div>
        <!-- Main Content Wrapper Start -->
