<!-- Breadcrumb area Start -->

        <div class="breadcrumb-area bg--white-6 breadcrumb-bg-1 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title">Checkout</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Checkout</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--80 pt-md--60 pt-sm--40">
                        <div class="col-12">
                           <!-- User Action Start -->
                           <div class="user-actions user-actions__coupon" id="cpnsection">
                                <div class="message-box mb--30 mb-sm--20">
                                    <p><i class="fa fa-exclamation-circle"></i> Have A Coupon? <a class="expand-btn" href="#coupon_info">Click Here To Enter Your Code.</a></p>
                                </div>
                                <div id="coupon_info" class="user-actions__form hide-in-default">
                                   
                                        <p>If you have a coupon code, please apply it below.</p>
                                        <div class="form__group d-sm-flex">
                                            <input type="text" name="coupon" id="coupon"  class="form__input form__input--2 mr--20 mr-xs--0" placeholder="Coupon Code">
                                            <button type="submit" class="btn btn-medium btn-style-1" onClick="applycode()">Apply Coupon</button>
                                        </div>
                                        <div class="mb--30 mb-sm--20" id="errormsg">
                                            
                                        </div>
                                </div>
                            </div>
                            <div class="user-actions user-actions__coupon" id="cpnmsg" style="display:none">
                                <div class="message-box mb--30 mb-sm--20" id="msgcontent">
                                   
                                </div>
                                
                            </div>
                            <!-- User Action End -->
                        </div>
                    </div> 
                    <div class="row pb--80 pb-md--60 pb-sm--40">
                        <!-- Checkout Area Start -->  
                        <div class="col-lg-6">
						<form class="form form--checkout" name="frm1" id="frm1" method="post">
							<div class="form-row mb--30">
                                        <div class="form__group col-md-6 mb-sm--30">
										<label>
										<input type="radio" name="toship" value="Yes" onClick="$('#shiptitle').show()" checked />Ship To Address </label>
										</div>
										<div class="form__group col-md-6 mb-sm--30">
										<label>
										<input type="radio" name="toship" value="No" onClick="$('#shiptitle').hide()" /> Hand Pick from Studio3 in Mumbai</label>
										</div>
							</div>			
						
                            <div class="checkout-title mt--10">
                                <h2>Billing Details <span id="shiptitle">/ Shipping Detial</span></h2>
                            </div>
                            <div class="checkout-form">
							<?php 
							  if(isset($msg)){
							  echo '<label id="email-error" class="error" for="email">'.$msg.'</label>';
							  }
							  ?>
								
								<?php krsort($shippingDetails->detail);
								$shippingDetails->detail = array_values($shippingDetails->detail); 
								
								?>
                                
                                    <div class="form-row mb--30">
                                        <div class="form__group col-md-6 mb-sm--30">
                                            <label for="billing_fname" class="form__label form__label--2">First Name  <span class="required">*</span></label>
											<input class="form__input form__input--2" type="text" name="s_first_name" value="<?php echo  !empty($shippingDetails->detail[0]->fname) ? $shippingDetails->detail[0]->fname : ""; ?>" id="s_first_name">
											<?php echo form_error('s_first_name', '<label id="s_first_name-error" class="error" for="first_name_1">', '</label>'); ?>
											
											
                                        </div>
                                        <div class="form__group col-md-6">
                                            <label for="billing_lname" class="form__label form__label--2">Last Name  <span class="required">*</span></label>
                                            <input class="form__input form__input--2" type="text" value="<?php echo  !empty($shippingDetails->detail[0]->lname) ? $shippingDetails->detail[0]->lname : ""; ?>" name="s_last_name" id="s_last_name">
											<?php echo form_error('s_last_name', '<label id="last_name_1-error" class="error" for="last_name_1">', '</label>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_company" class="form__label form__label--2">Company Name (Optional)</label>
                                            <input type="text" name="billing_company" id="billing_company" class="form__input form__input--2">
                                        </div>
                                    </div>
                                    
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_streetAddress" class="form__label form__label--2">Street Address <span class="required">*</span></label>

											<input class="form__input form__input--2 mb--30" type="text" name="s_address" value="<?php echo  !empty($shippingDetails->detail[0]->ship_straddress) ? $shippingDetails->detail[0]->ship_straddress : ""; ?>" id="s_address">
											<?php echo form_error('s_address', '<label id="address_1-error" class="error" for="address_1">', '</label>'); ?>
                                            
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_city" class="form__label form__label--2">Town / City <span class="required">*</span></label>
											<input class="form__input form__input--2" type="text" name="s_city" value="<?php echo  !empty($shippingDetails->detail[0]->city_name) ? $shippingDetails->detail[0]->city_name : ""; ?>" id="s_city">
											<?php echo form_error('s_city', '<label id="city_1-error" class="error" for="city_1">', '</label>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_state" class="form__label form__label--2">State / County <span class="required">*</span></label>
                                            
											<input type="text" class="form__input form__input--2" value="<?php echo  !empty($shippingDetails->detail[0]->state_name) ? $shippingDetails->detail[0]->state_name : ""; ?>" name="s_state" id="s_state" >
											
                                        </div>
                                    </div>
									<div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="postal Code" class="form__label form__label--2">Postal Code <span class="required">*</span></label>
                                            
											<input class="form__input form__input--2" type="text" name="s_postal_code" value="<?php echo  !empty($shippingDetails->detail[0]->ship_postalCode) ? $shippingDetails->detail[0]->ship_postalCode : ""; ?>"  id="s_postal_code">
											<?php echo form_error('s_postal_code', '<label id="s_postal_code-error" class="error" for="postal_code_1">', '</label>'); ?>
											
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_phone" class="form__label form__label--2">Phone <span class="required">*</span></label>
                                           
											<input class="form__input form__input--2" type="text" value="<?php echo  !empty($shippingDetails->detail[0]->ship_contact) ? $shippingDetails->detail[0]->ship_contact : ""; ?>"  name="s_telephone" id="s_telephone">
											<?php echo form_error('s_telephone', '<label id="telephone_1-error" class="error" for="telephone_1">', '</label>'); ?>
											
                                        </div>
                                    </div>
                                    <div class="form-row mb--30">
                                        <div class="form__group col-12">
                                            <label for="billing_email" class="form__label form__label--2">Email Address  <span class="required">*</span></label>
                                           <input class="form__input form__input--2" type="text" name="s_email_address" value="<?php echo  !empty($shippingDetails->detail[0]->email) ? $shippingDetails->detail[0]->email : ""; ?>" id="s_email_address">
											<?php echo form_error('s_email_address', '<label id="email_address_1-error" class="error" for="email_address_1">', '</label>'); ?>
											
                                        </div>
                                    </div>
                                   <div class="form-row">
                                        <div class="form__group col-12">
                                            <label for="orderNotes" class="form__label form__label--2">Order Notes</label>
                                            <textarea class="form__input form__input--2 form__input--textarea" id="orderNotes" name="orderNotes" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                        </div>
                                    </div>
                               
                            </div>
                        </div>
                        <div class="col-xl-5 offset-xl-1 col-lg-6 mt-md--40">
                            <div class="order-details">
                                <div class="checkout-title mt--10">
                                    <h2>Your Order</h2>
                                </div>
                                <div class="table-content table-responsive mb--30">
                                    <table class="table order-table order-table-2">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th class="text-right">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php $i = 1; ?> 
											<?php foreach ($this->cart->contents() as $items){ ?>
                                            <tr>
                                                <th><?php echo $items['name']; ?>
                                                    <strong><span>&#10005;</span> <?php echo $items['qty']; ?></strong>
                                                </th>
                                                <td class="text-right"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo $this->cart->format_number($items['price']); ?></td>
                                            </tr>
											 <?php $i++; ?>		
											<?php } ?> 
                                            </tbody>
                                        <tfoot>
                                            <tr class="cart-subtotal">
                                                <th>Subtotal</th>
                                                <td class="text-right"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo $this->cart->format_number($this->cart->total()); ?></td>
                                            </tr>
                                            <tr class="shipping">
                                                <th>Shipping</th>
                                                <td class="text-right">
                                                    <span><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> 00.00</span>
                                                </td>
                                            </tr>
                                            <tr class="shipping">
                                                <th>Discount</th>
                                                <td class="text-right">
                                                    <span id="disamount"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> 00.00</span>
                                                    <input type="hidden" id ="disountamt" name="disountamt" value="0" />
                                                    <input type="hidden" id ="promocode" name="promocode" value="0" />
                                                </td>
                                            </tr>
                                            <tr class="order-total">
                                                <th>Order Total</th>
                                                <td class="text-right"><span id="totamount" class="order-total-ammount"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo $this->pro->convertRate($this->cart->total()); ?></span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="checkout-payment">
                                      
                                        <!--div class="payment-group mb--10">
                                            <div class="payment-radio">
                                                <input type="radio" value="Online" name="payment-method" id="cheque">
                                                <label class="payment-label" for="cheque">
                                                    PayUmoney
                                                </label>
                                            </div>
                                            <div class="payment-info cheque hide-in-default" data-method="cheque">
                                                <p>Pay your payment with card</p>
                                            </div>
                                        </div>
                                        <div class="payment-group mb--10">
                                            <div class="payment-radio">
                                                <input type="radio" value="COD" name="payment-method" id="cash">
                                                <label class="payment-label" for="cash">
                                                    CASH ON DELIVERY
                                                </label>
                                            </div>
                                            <div class="payment-info cash hide-in-default" data-method="cash">
                                                <p>Pay with cash upon delivery.</p>
                                            </div>
                                        </div-->
                                        <div class="payment-group mt--20">
                                          <label>  <input type="checkbox" onchange="document.getElementById('chickoutbtn').disabled = !this.checked;"  required name="tnc" value="accept" id="tncacpt" /> accept <a href="javascript:void(0)" style=" color: #ff0000;text-decoration: underline;" data-toggle="modal" data-target="#myModal">terms and conditions</a></label>
                                            <button type="submit" id="chickoutbtn" disabled class="btn btn-fullwidth btn-style-1">Place Order</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Checkout Area End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="max-width: 75%;">
    
      <!-- Modal content-->
      <div class="modal-content" style="max-height: 750px;overflow: scroll;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" style="font-size: 3rem;">&times;</button>
          
        </div>
        <div class="modal-body">
           <!-- Single Blog Start Here -->
                                <article class="single-post-details"  style="align:justify">
                                    
                                    <div class="entry-content">
                                        <h2 style="text-align: center;">Terms & Conditions</h2>
                                        <h3>A. GENERAL</h3>
                                        <p>1. Thank you for visiting www.lavanaart.com. By accessing or using this website or any other media ("Website"), whether automated or otherwise, you, a registered or guest user in terms of the eligibility criteria set out herein you (“User” or “you” or “Buyer”) agree to be bound by these Terms and Conditions (“Terms”). </p>
                                        <p>2. By impliedly or expressly accepting these Terms, you also accept and agree to be bound by our policies (including but not limited to privacy policy), as amended from time to time. If you do not want to be bound by the Terms, you must not subscribe to or use our services. We encourage our users to read these Terms carefully while using the Website and prior to making a purchase on the Website. </p>
                                        <p>3. In these Terms, references to "you", "User" and/ or “Buyer” shall mean the end user/customer accessing the Website, its contents and using the services offered through the Websiteand references to the “Website”, "Lavana Art", “lavaanaart.com”, "we", "us" and "our" shall mean the website. “Artwork” shall mean and include; a work of drawing, painting, sculpture, engraving or lithography; a photographic work; a work of applied art; an illustration, sketch or handicraft. </p>
                                        <p>4. The contents set out herein form an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and as amended from time to time. As such, this document does not require any physical or digital signatures and forms a valid and binding agreement between the Website and the User. These Terms are made available to the User pursuant to and in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries Guidelines) Rules, 2011 that require publishing the rules, regulations, privacy policy and Terms for access or usage of the Website. </p>
                                        <p>5. The headings and subheadings herein are included for convenience and identification only and are not intended to describe, interpret, define or limit the scope, extent or intent of the Terms or the right to use the Website by you contained herein or any other section or pages of the Website or any linked sites in any manner whatsoever. </p>
                                        <p>6. The Terms herein shall apply equally to both the singular and plural form of the terms defined. Whenever the context may require, any pronoun shall include the corresponding masculine and feminine. The words "include", "includes" and "including" shall be deemed to be followed by the phrase "without limitation". Unless the context otherwise requires, the terms "herein", "hereof", "hereto", "hereunder" and words of similar import refer to the Terms as a whole. </p>
                                        <p>7. The Website is operated by Lavana Art, a sole proprietorship firm, having its place of business at Mumbai. All references to Website in these Terms shall deem to refer to the aforesaid entity in inclusion of the online portal. </p>
                                        <p>8. This Website may also contains links to other websites, which are not operated by the Website, and the Website has no control over the linked sites and accepts no responsibility for them or for any loss or damage that may arise from your use of them. Your use of the linked sites will be subject to the terms of use and service contained within each such site. </p>
                                        <p>9. We reserve the right to change these Terms at any time. Such changes will be effective when posted on the Website and may, at the sole discretion of the Website be notified to the Users from time to time. Notwithstanding the foregoing, by continuing to use the Website after we post any such changes, you accept the Terms as modified. </p>
                                        <p>10. You shall use the Website in good faith and for the essential purposes for which the Website is intended. Unless otherwise specified, the Website is for your personal and non-commercial use. You may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, software, Artworks or services obtained from the Website. </p>
                                        <p>11. As a condition of your use of the Website, you warrant to Lavana Art that you will not use the Website for any purpose that is unlawful or prohibited by these terms, conditions, and notices. You may not use the Website in any manner which could damage, disable, overburden, or impair the Website or interfere with any other party's use and enjoyment of the Website. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the Website. </p>
                                        <p>12. Software (if any) that is made available to download from the Website, excluding software that may be made available by end-users through a Communication Service, ("Software") is the copyrighted work of Lavana Art. Lavana Art hereby grants to you, the user, a personal, non-transferable license to use the Software for viewing and otherwise using the particular Site in accordance with these Terms and Conditions, and for no other purpose. Without limiting the foregoing, copying or reproduction of the software to any other server or location for further reproduction or redistribution is expressly prohibited. </p>
                                        <p>13. The information, software, products, and services included in or available through the Website may include inaccuracies or typographical errors. Changes are periodically added to the information herein. </p>
                                        <p>14. Lavana Art may make improvements and/or changes in the Website at any time. Advice received via the Website should not be relied upon for personal, medical, legal or financial decisions and you should consult an appropriate professional for specific advice tailored to your situation. </p>
                                        <h4>B. ELIGIBILITY TO USE AND REGISTRATION:</h4>
                                        <p>1. Use of the Website is available only to such persons who can legally contract under Indian Contract Act, 1872. Persons who are "incompetent to contract" within the meaning of the Indian Contract Act, 1872 including minors, un-discharged insolvents etc. shall not be eligible to use the Website. </p>
                                        <p>2. You warrant that all the information furnished at the time of registration is true and correct and any change in the information provided will be updated and intimated to Lavana Art at the time of purchase or earlier. Lavana Art is entitled to act on the basis of the information provided and seek such further information, clarifications or verifications as it may deem necessary for granting registration. </p>
                                        <p>3. Any minor desirous to use or transact on Website, is required to conduct such transaction through their legal guardian or parents. </p>
                                        <p>4. The Website reserves the right to terminate any membership and / or refuse to provide access to the Website if it is brought to the Website’s notice or if it is discovered that the person accessing/using the Website is under the age of 18 years. </p>
                                        <p>5. By accepting the Terms or using or transacting on the Website, the User irrevocably declares and undertakes that he/she is of legal age i.e. 18 years or older and capable of entering into a binding contract and such usage shall be deemed to form a contract between the Website and such User to the extent permissible under applicable laws. </p>
                                        <p>6. Any person may access the Website and the Artworks either by registering to the Website or using the Website as a guest. However, a guest user may not have access to all sections of the Website including certain benefits/promotional offerswhich shall be reserved only for the purpose of registered Users, and which may change from time to time at the sole discretion of the Website. </p>
                                        <p>7. If you wish to register yourself with the Website, you shall be required to create an account by registering through Facebook or your email account. You are responsible for any and all activities that occur under your password or account. You agree to (a) immediately notify the Website of any unauthorized use of your password or account or any other breach of security, and (b) ensure that you exit from your account at the end of each session. We cannot and will not be liable for any loss or damage arising from your failure to comply with this Section or for any losses occurring thereto. You, as the User, waive any claims against the Website for any loss and damage suffered by you on account of your failure to comply with the Terms and reasonably expected good practices in this regard. </p>
                                        <p>8. The Website may be inaccessible for such purposes as it may, at its sole discretions deem necessary, including but not limited to regular maintenance. However, under no circumstances will Lavana Art be held liable for any losses or claims arising out of such inaccessibility to the Users and the Users expressly waive any claims against Lavana Art in this regard. </p>
                                        <p>9. Users of the Website may be required to provide certain person information and expressly permit the Website from accessing and/or collecting and retaining such personal information of the Users. Such provision and/or collection, storage, retention, use and disclosure of the personal information of the Users shall be subject to the Website’s privacy policy. </p>
                                        <h4>C. PLACING AN ORDER:</h4>
                                        <p>1. Once you have placed an order on this Website, it would constitute an irrevocable acceptance of the purchase and is an enforceable contract of sale between You and Lavana Art. </p>
                                        <p>2. Lavana Art shall have the right to cancel any order placed on its Website, at any time prior to the delivery of the Artwork, without assigning any reason whatsoever. Lavana Art shall assume no responsibility for any errors or omissions that may occur in the description, pricing or other content related to the Artwork. In the event of such an error, Lavana Art reserves the right to cancel the order placed by the Buyer by informing the Buyer of the same. </p>
                                        <p>3. An Artwork made available for Sale on the Website may not or may not be in our physical possession and may be physically located anywhere in the world.Our role is confined to that of a platform for the display and sale of Artwork. </p>
                                        <p>4. All Artworks displayed on the Website are supported by an Authenticity Certificate from Artist. In case an Authenticity Certificate from the Artist is not available, Lavana Artwill make an endeavor to obtain the same from the artist and/or its estate. AnyAuthenticity Certificate from Lavana Art on behalf of the artist and/or its estatewill begiven in good faith. </p>
                                        <p>5. If you wish to cancel the order placed, please contact Lavana Art within 1 hour of placing the order. Lavana Art shall endeavor to cancel the order. </p>
                                        <h4>D. PAYMENT:</h4>
                                        <p>1. Prices for Artworks are specified with respective listings. Price mentioned with respective Artwork does not include applicable Taxes. The price mentioned also does not include Packing and Shipping Charges. Applicable Taxes and Packing and Shipping Charges will be separately identified and included in your invoice before you proceed to pay. </p>
                                        <p>2. You agree that you are liable and will make payment in respect of any duties and taxes that might be applicable on the purchase of Artworks and such charges shall be payable in addition to the price of the Artwork. </p>
                                        <p>3. Once an order is placed on the Website, you are required to make payment for the total price of the Artwork including duties, taxes as well as packing and shipping charges immediately on placing the order. In the event we do not receive payment, we reserve the right to cancel the order without prior notice to you. </p>
                                        <p>4. We prefer that the payment is made from your own bank account. However, in case you arrange for the payment to be made from another bank account, you have to provide a declaration carrying the signature of that account holder along with yours. We will provide you with the declaration format. </p>
                                        <p>5. Orders where payment is by cheque / demand draft is made will be processed only after clearance of the cheque / demand draft. </p>
                                        <p>6. Buyers with Indian billing addresses are invoiced in INR and buyers with non-Indian billing addresses are invoiced at the prevailing Indian Rupee equivalent amount at the time of billing, either in the currency of that country or in [USD], as may be convenient to us. </p>
                                        <p>7. Prices for Artworks are subject to change without prior notice, and at any time whatsoever, irrespective of whether an item has been earmarked/wish listed by a Buyer. The Website disclaims any and all claims and/or liabilities arising from such revision in prices. </p>
                                        <p>8. We may enter into agreements with third party payment gateway aggregators and financial institutions authorized by the Reserve Bank of India for collection, refund and remittance and to facilitate payment for purchase, sale and return of Artwork on the Website. The Website shall initiate the remittance of the payments made by the Buyer and the date of completion of transaction shall be after the Artwork are delivered to the User and such other additional time as may be agreed</p>
                                        <p>9. While availing any of the payment method/s available on the Website, the Website will not be responsible or assume any liability, whatsoever in respect of any loss or damage arising directly or indirectly to the User due to:<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) Lack of authorization for any transaction/s, or<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) Exceeding the preset limit mutually agreed by and between the User and relevant banks of the Buyer, or<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) Any payment issues arising out of the transaction, or<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d) Illegitimacy of the payment methods (credit/debit card frauds etc.) being used by a Buyer; <br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e) Decline of transaction for any other reason(s). </p>
                                        <p>10. The Website is merely a facilitator for providing the User with payment channels through automated online electronic payments (either itself or through Service Providers), , collection and remittance facility for the payment of Artworks purchased by the User on the Website using the existing authorized banking infrastructure and credit card payment gateway networks (of either the Website or Service Providers). </p>
                                        <h4>E. PACKING AND SHIPPING</h4>
                                        <p>1. The Artwork will be delivered/shipped to the Buyer once all necessary documentation relating to transportation, tax, etc. as may be reasonably requested for by us are completed and payment has been realized in full including any additional charges. Process of Shipping will be initiated within 15days thereafter. Orders where payment is by cheque / demand draft will be shipped only after clearance of the cheque / demand draft. Transit insurance costs will be included in such additional charges, unless specifically advised otherwise by the Buyer. </p>
                                        <p>2. In the event that the Buyer chooses to himself collect/pick up the Artwork from our office at Mumbai he/ she must do so within two weeks of us confirming the order and receipt of full payment. In the event that the Artwork is not collected within a period of two weeks, we shall at our discretion arrange for storage of the Artwork at the Buyer's expense and shall only release the Artwork after payment for storage has been made in full. Please note that in case the item is to be picked up from our office, we will be insuring the same for the time the Artwork resides at our office/ storage facility, accordingly insurance and any other storage charges will be levied on the Buyer. </p>
                                        <p>3. It is our endeavor that all Artwork purchased from the Website shall be delivered to the Buyer by standard courier services within approximately 14-21 days for domestic orders and 21-25 days for international orders. All deliveries where applicable shall be made on a best effort basis, and while the Website will endeavor to deliver the Artworks within the time frame, the Website disclaims any claims or liabilities arising from any delay in this regard. </p>
                                        <p>4. Lavana Art shall not be responsible for any delay in the delivery of the Artwork. Lavana Art shall not be liable for any damage to the Artwork in transit due to mishandling by the logistics partner/courier. </p>
                                        <p>5. While Lavana Art will take utmost care while packing the Artwork, Lavana Art mayuse third party service providers to pack the Artwork and accordingly does not guarantee the quality and condition of packing. </p>
                                        <p>6. In case the User is not reachable or does not accept delivery of Artworks, Lavana Art reserves the right to cancel the order(s) at its discretion. </p>
                                        <p>7. On placing your order, you will receive an email containing a summary of the order. Sometimes, delivery may take longer due to inter alia:<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) bad weather;<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) flight delays;<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) political disruptions; <br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d) transport strike/lockout; and<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e) other unforeseen circumstances. </p>
                                        <p>8. Where there is a likelihood of delay in delivery of the Artworks, the Buyer may be notified of the same from time to time. However, no refunds may be claimed by the User for any delay or failure in delivery of the Artworks, which was caused due to reasons beyond the reasonable control of Lavana Art. </p>
                                        <p>9. Please note that Lavana Art does not reframe the Artwork if it is already been framed. It is the discretion of the artist whether the Artwork is in a framed form or not. Please also note that Lavana Art does not frame Artworks which are not framed by the artist before shipping them.  In the event you wish that an Artwork should be framed please contact Lavana Art. </p>
                                        <p>10. In case a User purchases multiple Artworks in one transaction, we may combine the same and deliver them together. However, this may not always be possible and will be subject to shipping and packaging requirements and limitations. </p>
                                        <p>11. If a User wishes to get delivery to different addresses, then the Buyer shall be required to purchase the Artworks under separate transactions and provide separate delivery addresses for each transaction, as may be required. The User agrees that the delivery can be made to the person who is present at the shipping address provided by the Buyer. </p>
                                        <h4>F. RETURNS</h4>
                                        <p>1. We will not accept returns because the Buyer did not like the artwork that they have purchased. </p>
                                        <p>2. While great care is taken to reproduce the colors and form of paintings as depicted on the website, it is understood that since each painting is individually painted there may be variations in shade and color. No complaints or returns in this regard would be entertained. </p>
                                        <p>3. We are not obligated to provide any physical preview of the Artwork either before or after the sale. Weare in no way liable for the condition of the Artwork. The Buyer may only return an Artwork purchased by him / her / it, if there is a material difference between the actual size of the Artwork and that mentioned on the Website. </p>
                                        <p>4. We will accept returns of an Artwork and issue a refund to the Buyer only if the Artwork is delivered to the Buyer in a damaged condition and Lavana Art is accordingly informed by the Buyer within 24 hours of delivery of the Artwork along with a picture of the artwork “as is” (sent via email), and if weare unable to rectify / replace the Artwork. Refund will be processed through NEFT/ RTGS once the claim has been verified and processed by the Insurer, if it has been insured. When the work is insured, it will be the Buyers onus to furnish accurate information in respect of the damage to process the claim. In the event the required information is not furnished by the Buyer and consequently the insurance claim is rejected, this will be at the cost and consequences of the Buyer. Please note that the 24 hours’ time period would commence from the time the Artwork was delivered as per the confirmation received from our logistics team or courier partners. Lavana Art does not bear any responsibility for the loss to the purchaser in case the insurance claim is rejected and will not be refunding any amounts in case such insurance claim is rejected. In the event, the Artwork is not insured or if insured but the insurance claim is not paid in full, Lavana Art will not be making any additional payments over and above the insurance amount, i.e. in case the insurance claim is rejected or is not paid in full the loss will be to the Buyer’s account. </p>
                                        <p>5. It is our policy that in the event of a disagreement, Lavana Art holds the right to make the final call on all returns. Lavana Art will not process any returns if it is not reported within the stipulated time period. Refund shall be conditional and shall be with recourse available to Lavana Art in case of any misuse by Buyer. </p>
                                        <p>6. Buyers are responsible for all the packaging, shipping and insurance charges back to Lavana Art on returned items. We request you to retain all packaging materials and ship your item back to us in its original packaging. We strongly encourage you to return the item via a trackable method. The Artwork returned by the Buyer must be in no worse condition than it was received by the Buyer. </p>
                                        <p>7. Insurance, packing and shipping fees associated with the return of the returned piece of Artwork shall be the sole responsibility of the Buyer, and Lavana Art shall have no obligation to reimburse the Buyer for such amounts. </p>
                                        <h4>G. WARRANTY AND DAMAGES</h4>
                                        <p>1. Lavana Art makes no representations about the suitability, reliability, availability, timeliness, and accuracy of the information, software, artworks, services and related graphics contained on the Website for any purpose. All such information, software, artworks, services and related graphics are provided "as is" and “as available” without warranty of any kind. Lavana Art hereby disclaim all warranties and conditions with regard to this information, software, artworks, services and related graphics, including all implied warranties and conditions of merchantability, fitness for a particular purpose, title and non-infringement. </p>
                                        <p>2. Lavana Art hereby disclaims any guarantees of exactness as to the finish and appearance of the final Artwork as ordered by the Buyer. </p>
                                        <p>3. In no event shall Lavana Art be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of the Website, with the delay or inability to use the Website or related services, the provision of or failure to provide services, or for any information, software, artworks, services and related graphics obtained through the Website, or otherwise arising out of the use of the Website, whether based on contract, tort, negligence, strict liability or otherwise. If you are dissatisfied with any portion of the Website, or with any of these terms of use, your sole and exclusive remedy is to discontinue using the Website. In any event, the liability of Lavana Art for any losses caused to the Buyer as any breach of this contract by Lavana Art will be limited to the price of the respective artwork sold by Lavana Art to the Buyer. </p>
                                        <p>4. The Buyer hereby indemnifies, defends and holds harmless Lavana Art, its subsidiaries, affiliates, vendors, agents and their respective directors, officers, employees, contractors and agents (herein after individually and collectively referred to as <b>"indemnified parties"</b>) from and against any and all losses, liabilities, claims, suits, proceedings, penalties, interests, damages, demands, costs and expenses (including legal and other statutory fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by the indemnified parties that arise out of, result from, or in connection with :<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) The Buyer’s breach of these Terms and Conditions; or<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) Any claims made by any third party due to, or arising out of, or in connection with, the Buyer’s use of the Website; or<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) Any claim that any third-party intellectual property right, proprietary information, content or materials provided by the Buyer causes any damage to a third party;<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d) Violation of any rights of any third party by the User, including any intellectual property rights. <p>
                                        <h4>H. GOVERNING LAW:</h4>
                                        <p>These Terms and Conditions of Use are governed by the laws of India. You hereby consent to the exclusive jurisdiction and venue of courts in Mumbai, India, in all disputes arising out of or relating to the use of the Website. Use of the Website is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph. </p>
                                        <h4>I. MISCELLANEOUS: </h4>
                                        <p>1. You agree that no joint venture, partnership, employment, or agency relationship exists between you and Lavana Art as a result of this agreement or use of the Website. If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in effect. </p>
                                        <p>2. Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and Lavana Art with respect to the Website and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and Lavana Artwith respect to the Website. </p>
                                        <p>3. A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. </p>
                                        <p>4. All contents of the Website are copyright protected in favour of Lavana Art. All trademarks, names, brand names, etc. used on the Website are either trademarks (whether registered or not) of Lavana Artor its associated companies. Any rights not expressly granted herein are reserved. </p>
                                        <p>5. All artworks provided by the artist confirms and establishes the fact that they are the owners and original creators of the artwork(s) as provided to Lavana Art. Lavana Art does not hold any liability in case of any discrepancy that is/has been found as a result of misuse of image or copyright produced by the original owner. </p>
                                        <p>6. You agree to abide by all provisions prescribed in the order forms submitted by you for the purchase of any Artworks from the Website. </p>
                                        <p>7. Lavana Art also holds right to use the artwork as it deems fit and proper in any form for either marketing and/or sales in any documents/ materials/ third party website/ platform. </p>
                                    </div>
                                </article>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <script>
$('#promocode').on('submit',function(){

    var code = $('#coupon').val();
    $.ajax({
			type : "POST", 
			url : "http://demo.lavanaart.com/order/applyPromocode/"+code,
			data: {
			},
			success:function(state){
                   $('#totamount').html('<i class="fa fa-inr"></i>'+ state.totalamount); 
                    $('#disamount').html('<i class="fa fa-inr"></i>'+state.discount); 
			}
		});

});
function applycode(){
    var code = $('#coupon').val();
    $.ajax({
            type : "POST",
            dataType:"json", 
			url : "http://demo.lavanaart.com/order/applyPromocode/"+code,
			data: {
			},
			success:function(state){
              
                if( parseInt(state.discount) > 0 ){
                   $('#totamount').html('<i class="fa fa-inr"></i>'+ state.totalamount); 
                    $('#disamount').html('<i class="fa fa-inr"></i>'+state.discount); 
                    $('#promocode').val(code);
                    $('#disountamt').val(state.discount);
                    $('#cpnsection').hide();
                    $('#cpnmsg').show();
                    $('#msgcontent').html(' <p><i class="fa fa-exclamation-circle"></i><b>'+code+'</b> successfuly apply.</p>');
                }else{
                    $('#errormsg').html(' <p><i class="fa fa-times-circle" style="color: #ff0000;"></i><b>'+code+'</b> not a vaid code.</p>');
                }   
            }
		});
}
</script>
