<!-- Breadcrumb area Start -->

<div class="breadcrumb-area bg--white-6 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title"><span><?php echo $this->pro->cat_name($this->uri->segment(3)); ?></h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Profile</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                       <div class="row pt--75 pt-md--55 pt-sm--35 pb--80 pb-md--60 pb-sm--40">
                        <div class="col-md-9 mb-sm--30">
                            <div class="login-box">
                                <h4 class="mb--35 mb-sm--20">Profile</h4>
                                      <?php 
                              if(isset($msg)){
                              echo '<label id="email-error" class="error" for="email">'.$msg.'</label>';
                              }
                              ?>
                                    <form method="post" id="regform">
                                   <div class="row">   
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="title">Title <span class="required">*</span></label>
                                          <select name="prefix" class="form__input form__input--3" id = "prefix" style="width: 100%;">
                                            <option value="">Select</option>
                                            <option <?php if($userinfo->user_prefix == "Mr."){ echo "selected";} ?>>Mr.</option>
                                            <option <?php if($userinfo->user_prefix == "Mrs."){ echo "selected";} ?>>Mrs.</option>
                                            <option <?php if($userinfo->user_prefix == "Miss"){ echo "selected";} ?>>Miss</option>
                                          </select>
                                    </div>    
                                    <div class="form__group col-md-4">
                                        <label class="form__label form__label--2" for="First Name">First Name <span class="required">*</span></label>
                                        <input type="text" name="fname" id = "fname" value="<?php echo $userinfo->user_firstName;?>" class="form__input form__input--3">
                                    </div>
                                    <div class="form__group  col-md-4">
                                        <label class="form__label form__label--2" for="First Name">Last Name <span class="required">*</span></label>
                                        <input type="text" name="lname" id="lname" value="<?php echo $userinfo->user_lastName;?>" class="form__input form__input--3">
                                    </div>
                            </div><div class="row">   
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="Address">Address <span class="required">*</span></label>
                                        <input type="text" name="address" id="address" value="<?php echo $userinfo->user_address;?>" class="form__input form__input--3">
                                    </div>
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="First Name">City/Town <span class="required">*</span></label>
                                        <input type="text" name="city" id="city" value="<?php echo $userinfo->city;?>" class="form__input form__input--3">
                                    </div>
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="First Name">Pincode <span class="required">*</span></label>
                                       <input type="text" name="pincode" id="pincode" value="<?php echo $userinfo->user_postalCode;?>" class="form__input form__input--3">
                                    </div>
                            </div><div class="row">        
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="First Name">Email <span class="required">*</span></label>
                                       <input type="text" name="email" id="email" value="<?php echo $userinfo->user_email;?>"  class="form__input form__input--3">
                                    </div>
                                    <div class="form__group mb--20 col-md-4">
                                        <label class="form__label form__label--2" for="First Name">Phone <span class="required">*</span></label>
                                        <input type="text" name="phone" id = "phone" value="<?php echo $userinfo->user_contact;?>" class="form__input form__input--3">
                                    </div>
                                   

                            </div>
                                    <div class="d-flex align-items-center mb--20">
                                        <div class="form__group">
                                            <input type="submit" name="update" value="Update" class="btn btn-submit btn-style-1">
                                        </div>
                                      
                                    </div>
                                </form>
                            </div>   
                        </div>
                        <aside class="right sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              <ul>
                <li><a href="<?php echo ADMIN_URL."account" ;?>">Account Dashboard</a></li>
                <li class="current"><a href="javascript:void(0)">Account Information</a></li>
                <li><a  href="<?php echo ADMIN_URL."account/orders/" ;?>">My Orders</a></li>
                <li  ><a href="<?php echo ADMIN_URL."account/changePassword/" ;?>">Change Password</a></li>
              </ul>
            </div>
          </div>
                  </aside>
                    </div>
                </div>
            </div>
        </div>
 

















<script src="http://navavyapar.com/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#regform").validate({
		rules: {
			prefix: {
				required: true,
				
			},
			fname: {
				required: true,
				
			},
			lname: {
				required: true,
				
			},
			address: {
				required: true,
				
			},
			city: {
				required: true,
				
			},
			pincode: {
				required: true,
				
			},
			email: {
				required: true,
				email:true
			},
			phone: {
				required: true,
				number : true
			}
		},
		messages: {
			
			prefix: {
				required: "Field Required",
				
			},
			fname: {
				required: "Field Required",
				
			},
			lname: {
				required: "Field Required",
				
			},
			address: {
				required: "Field Required",
				
			},
			city: {
				required: "Field Required",
				
			},
			pincode: {
				required: "Field Required",
				
			},
			email: {
				required: "Field Required",
				email : "Enter Valid Email"
			},
			phone: {
				required: "Field Required",
				
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>