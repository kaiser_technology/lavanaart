<div class="breadcrumb-area bg--white-6 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title"><span>Order History</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Order History</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->
        <div id="content" class="main-content-wrapper">
<div class="page-content-inner">
   <div class="container">
      <div class="pb15" style="margin-bottom: 20px;">
         <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 pb5"><strong>Order Code :</strong></div>
            <div class="col-md-3 col-sm-3 col-xs-6 pb5"><?php echo $order->detail[0]->ordrndcode;?></div>
            <div class="col-md-3 col-sm-3 col-xs-6 pb5"><strong>Order Date :</strong></div>
            <div class="col-md-3 col-sm-3 col-xs-6 pb5"><?php $dat= explode("-",$order->detail[0]->ord_date);
               echo $dat[2]."-".$dat[1]."-".$dat[0];
                
                ?></div>
         </div>
         <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 pb5"><strong>Delivery Date :</strong></div>
            <div class="col-md-3 col-sm-3 col-xs-6 pb5"><?php echo $order->detail[0]->del_date;?></div>
            <div class="col-md-3 col-sm-3 col-xs-6 pb5"><strong>Payment Status :</strong></div>
            <div class="col-md-3 col-sm-3 col-xs-6 pb5">
               <?php if($order->detail[0]->ord_status == "complet")
                  {
                   echo "Paid"; 
                  }
                  else{
                   echo "pending";
                  }
                  ?>
            </div>
         </div>
         <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6"><strong>Order Status :</strong></div>
            <div class="col-md-3 col-sm-3 col-xs-6"><?php echo $order->detail[0]->ord_status;?></div>
         </div>
      </div>
      <div class="row page-content checkout-page">
         <div class="col-sm-6 pb15">
            <div class="spg-panel">
               <h3 class="checkout-sep">Billing Address</h3>
               <div class="p10">
                  <address>
                     <?php echo $billadd->detail[0]->user_firstName." ".$billadd->detail[0]->user_lastName; ?><br>
                     <?php //echo $billadd->detail[0]->user_straddress; ?><br>
                     <?php echo $billadd->detail[0]->user_address; ?><br>
                     <?php echo $billadd->detail[0]->city; ?>-<?php echo $billadd->detail[0]->user_postalCode; ?><br>
                     <?php //echo $billadd->detail[0]->state_id; ?>,<?php //echo $this->pro->countryname($billadd->detail[0]->country_id); ?>Maharastra<br>
                     <?php echo $billadd->detail[0]->user_contact; ?><br>
                     <?php echo $billadd->detail[0]->user_email; ?><br>
                  </address>
               </div>
            </div>
         </div>
         <div class="col-sm-6 pb15">
            <div class="spg-panel">
               <h3 class="checkout-sep">Shipping Address</h3>
               <div class="p10">
                  <address>
                     <?php echo $shipadd->detail[0]->fname." ".$shipadd->detail[0]->fname; ?><br />
                     <?php echo $shipadd->detail[0]->ship_straddress; ?><br>
                     <?php echo $shipadd->detail[0]->ship_address; ?><br>
                     <?php echo $shipadd->detail[0]->city_name; ?>-<?php echo $shipadd->detail[0]->ship_postalCode; ?><br>
                     <?php echo $shipadd->detail[0]->state_name; ?>,<?php echo $shipadd->detail[0]->country_name; ?><br>
                     <?php echo $shipadd->detail[0]->ship_contact; ?><br>
                     <?php echo $shipadd->detail[0]->email; ?><br>
                  </address>
               </div>
            </div>
         </div>
      </div>
      <div class="wishlist-item table-responsive">
         <table class="col-md-12">
            <thead>
               <tr class="tableheader">
                  <td align="center">Products</td>
                  <td align="left">Item Description</td>
                  <td align="center">Quantity</td>
                  <td align="center">Unit Price</td>
                  <td align="right">Total Price</td>
               </tr>
            </thead>
            <tbody class="lightbg" valign="top">
               <?php
                  foreach($product->detail as $item)
                  {
                  ?>
               <tr align="center">
                  <td valign="top"><img alt="" width="70" src="<?php echo  CDN_IMG_URL.'product/'.$this->pro->pro_img($item->pro_id); ?>" class="ordimg"></td>
                  <td align="left">
                     <div class="pb5"><?php echo $this->pro->pro_name($item->pro_id)."-".$this->pro->pro_unit($item->pro_price_id); ?></div>
                     <div class="pb5"><strong>Product Code :</strong><?php echo $this->pro->pro_code($item->pro_id); ?></div>
                  <td><?php echo $item->ord_pro_qty; ?></td>
                  <td><?php echo $item->pro_price; ?></td>
                  <td align="right"><span class="price"><?php echo $item->total_pro_amount; ?></span></td>
               </tr>
               <?php
                  }
                  ?>            
            </tbody>
         </table>
      </div>
      <div class="row">
         <div class="col-md-12">
   
            <div class="wishlist-item table-responsive">
               <table class="col-md-4" style="float: right;">
               <tbody class="darkbg" align="right">
                  <tr>
                     <td >
                        <strong> Sub Total:</strong>
                     </td>
                     <td>  </td>
                     <td align="right"><span class="price"><i class="fa fa-inr pr3"></i> <?php echo number_format($order->detail[0]->total_amount,2,'.','');?></span></td>
                  </tr>
                  <tr>
                     <td align="right">
                        <strong>Discount:</strong>
                     </td>
                     <td></td>
                     <td align="right"><span class="price">  <?php  if($order->detail[0]->discount >0){ echo '<i class="fa fa-inr pr3"></i>'.number_format($order->detail[0]->discount,2,'.','');} else{ echo "-"; } ?></span></td>
                  </tr>
                  <!--<tr><td align="right">
                     <strong>Shipping Charge:</strong></td><td> <i class="fa fa-inr pr3"></i></td><td align="right"><span class="price"> <?php //echo $this->pro->shipcharge(number_format($order->detail[0]->total_amount+$order->detail[0]->ship_charge-$order->detail[0]->discount, 2, '.', ''));?></span></td></tr>-->
                  <tr>
                     <td align="right" style="border:none;">
                        <strong>Total Amount:</strong>
                     </td>
                     <td style="border:none;"></td>
                     <td align="right" style="border:none;"> <span class="price"><i class="fa fa-inr pr3"></i><?php echo number_format( ($order->detail[0]->total_amount+$order->detail[0]->ship_charge) - $order->detail[0]->discount, 2, '.', '');?></span></td>
                  </tr>
                 </tbody> 
               </table>
            </div>
         </div>
         <div class="form-actions"><a class="ovalbutton1 btn1" href="<?php echo base_url('account/orders/');?>">Back</a></div>
      </div>
   </div>
</div>
</div>

