<!-- Main Content Wrapper Start -->

<div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--75 pt-md--50 pt-sm--30 pb--80 pb-md--60 pb-sm--35">
                        <div class="col-md-12 mb-sm--30">
                            <?php 
                            if( $this->session->flashdata('msg') )
                            {
                                echo '<label id="email-error" class="success" for="email">'.$this->session->flashdata('msg').'</label>'; 
                            } ?>

                            <h2 class="heading-secondary mb--50 mb-md--35 mb-sm--20">Enquiry for <?php echo $product['pro_name']; ?></h2>

                            <!-- Contact form Start Here -->
                            <form class="form" method="post" action="/contactus/enqsubmit" id="enqform">
                                <input type="hidden" name="productname" value="<?php echo $product['pro_name']; ?>" />
                                <input type="hidden" name="productid" value="<?php echo $product['pro_id']; ?>" />
                                <div class="form__group mb--20">
                                    <input type="text" id="contact_name" name="name" class="form__input form__input--2" placeholder="Your name*">
                                </div>
                                <div class="form__group mb--20">
                                    <input type="email" id="contact_email" name="email" class="form__input form__input--2" placeholder="Email Address*">
                                </div>
                                <div class="form__group mb--20">
                                    <input type="text" id="contact_phone" name="mobile" class="form__input form__input--2" placeholder="Your Phone*">
                                </div>
                                <div class="form__group mb--20">
                                    <textarea class="form__input form__input--textarea" id="contact_message" name="message" placeholder="Message*"></textarea>
                                </div>
                                <div class="form__group">
                                    <input type="submit" value="Send" class="btn btn-submit btn-style-1">
                                </div>
                                <div class="form__output"></div>
                            </form>
                            <!-- Contact form end Here -->

                        </div>
                       
                    </div>
                </div>
              
            </div>
        </div>
        <!-- Main Content Wrapper Start -->
        <script src="<?php echo ADMIN_URL;?>assets/js/jquery.validate.min.js"></script>
<script>

  $(function(){

	$("#enqform").validate({
		rules: {
			name: {
				required: true,
				
			},
			email: {
				required: true,
				
			},
			mobile: {
				required: true,
				
			},
			message: {
				required: true,
				
			}
		},
		messages: {
			
			name: {
				required: "Please enter your name",
				
			},
			email: {
                required: "Please enter your email",
                email:"Please enter valid email"
				
			},
			mobile: {
				required: "Please enter Mobile number",
				
			},
			message: {
				required: "Please enter your message",
				
			}
			
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>