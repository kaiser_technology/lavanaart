 <!-- Breadcrumb area Start -->

 <div class="breadcrumb-area bg--white-6 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title"><span>Change Password</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Change Password</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                       <div class="row pt--75 pt-md--55 pt-sm--35 pb--80 pb-md--60 pb-sm--40">
                        <div class="col-md-9 mb-sm--30">
                            <div class="login-box">
                                <h4 class="mb--35 mb-sm--20">Change Password</h4>
                                      <?php 
                              if(isset($msg)){
                              echo '<label id="email-error" class="error" for="email">'.$msg.'</label>';
                              }
                              ?>
                                    <form method="post" id="regform">
                                           
                                    <div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Old Password">Old Password <span class="required">*</span></label>
                                        <input type="password" name="opass" id = "opass" value="" class="form__input form__input--3">
									                    	<?php echo form_error('fname', '<label id="email-error" class="error" for="email">', '</label>'); ?>
                                    </div>
                                    <div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Old Password">New Password <span class="required">*</span></label>
                                        <input type="password" name="npass" id = "npass" value="" class="form__input form__input--3">
									                    	<?php echo form_error('fname', '<label id="email-error" class="error" for="email">', '</label>'); ?>
                                    </div>
                                    <div class="form__group mb--20 col-md-6">
                                        <label class="form__label form__label--2" for="Old Password">Confirm Password <span class="required">*</span></label>
                                        <input type="password" name="cpass" id = "cpass" value="" class="form__input form__input--3">
									                    	<?php echo form_error('fname', '<label id="email-error" class="error" for="email">', '</label>'); ?>
                                    </div>
                                    <input type="hidden" name="email" value="<?php echo ucwords($this->session->userdata('userDetail')->email);?>" />
                                     <input type="hidden" name="update" value="update" />
                                    <div class="d-flex align-items-center mb--20">
                                        <div class="form__group">
                                            <input type="submit" value="Update" class="btn btn-submit btn-style-1">
                                        </div>
                                      
                                    </div>
                                </form>
                            </div>   
                        </div> <aside class="right sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              <ul>
                <li><a href="<?php echo ADMIN_URL."account" ;?>">Account Dashboard</a></li>
                <li ><a href="<?php echo ADMIN_URL."account/profile" ;?>">Account Information</a></li>
                <li><a  href="<?php echo ADMIN_URL."account/orders/" ;?>">My Orders</a></li>
                <li  class="current"><a href="javascript:void(0)">Change Password</a></li>
              </ul>
            </div>
          </div>
                  </aside>
                    </div>
                </div>
            </div>
        </div>
 
 
 
 
 
 
 
 
 
 
 <script src="http://navavyapar.com/assets/vendor/jquery-validation/jquery.validate.min.js"></script>

<script>

   $(function(){

	$("#regform").validate({
		rules: {
			opass: {
				required: true,
				
			},
			npass: {
				required: true,
				
			},
			cpass: {
				required: true,
				equalTo : npass
			}
		},
		messages: {
			
			opass: {
				required: "Field Required",
				
			},
			npass: {
				required: "Field Required",
				
			},
			cpass: {
				required: "Field Required",
				equalTo : "Please Enter Same Password as above"
			}
			
		},
		submitHandler: function(form) {
			form.submit();
		}
	});



});
</script>