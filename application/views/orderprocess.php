 <!-- Breadcrumb area Start -->
<style>
#exTab3 .active h3 {
  color : white;
 
}
</style>

        <div class="breadcrumb-area bg--white-6 breadcrumb-bg-1 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title">Payment Process</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Payment Process</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
			
                <div class="container">
                    <div class="row pt--80 pb--80 pt-md--45 pt-sm--25 pb-md--60 pb-sm--40">
                        <div class="col-lg-8 mb-md--30">
						
						  <div class="checkout-payment">
                               <div class="payment-group mb--10">
								<div class="payment-radio" style="font-size: 25px;">
									<input type="radio" value="cheque" name="payment-method" id="cheque">
									<label class="payment-label" for="cheque">
										COD
									</label>
								</div>
								<div style="text-align: center;" class="payment-info cheque hide-in-default" data-method="cheque">
									<p><i class="fa fa-cart-arrow-down fa-5x" aria-hidden="true"></i></p>
			<h3 style="color:#000">Please pay <i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i><?php echo $posted['amount']; ?> to the delivery executive when your order is delivered</h3>
			<form action="<?php echo base_url('/order/responseOrder/2');?>" method="post" name="COD">
					  <input type="hidden" name="txnid" value="<?php echo $posted['txnid']; ?>" />
					  <input type="hidden" name="amount" value="<?php echo $posted['amount']; ?>" />
					  <input type="hidden" name="firstname" value="<?php echo $posted['firstname']; ?>" />
					  <input type="hidden" name="lastname" value="<?php echo $posted['lastname']; ?>" />
					  <input type="hidden" name="email" value="<?php echo $posted['email']; ?>" />
					  <input type="hidden" name="phone" value="<?php echo $posted['phone']; ?>" />
					  <input type="hidden" name="productinfo" value="<?php echo $posted['productinfo']; ?>" />
					  <input type="hidden" name="status" value="<?php echo "not_confirm"; ?>" />
					  <input type="hidden" name="ordType" value="<?php echo "COD"; ?>" />
					  <input type="hidden" name="orderid" value="<?php echo $orderid; ?>" />
					  <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
					  <input type="hidden" name="address1" value="<?php echo $posted['address1']; ?>" />
					  <input type="hidden" name="city" value="Mumbai"  />
					  <input type="hidden" name="state" value="Maharastra"  />
					  <input type="hidden" name="country" value="India" />
					   <button class="btn btn-fullwidth btn-style-1" type="submit"><i class="fa fa-angle-double-right"></i>&nbsp; <span>Place Order</span></button>
					</form>	
								</div>
							</div>
							<div class="payment-group mb--10">
								<div class="payment-radio" style="font-size: 25px;">
									<input type="radio" value="cash" name="payment-method" id="cash">
									<label class="payment-label" for="cash">
										PayUmoney
									</label>
								</div>
								<div style="text-align: center;" class="payment-info cash hide-in-default" data-method="cash">
									<p><img src="<?php echo base_url('assets/images/payumoney.png');?>" width="320"/></p>
<h3 style="color:#000">Please pay <i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i><?php echo $posted['amount']; ?> to the delivery executive when your order is delivered</h3>
								
								
								<form action="<?php echo $action; ?>" method="post" name="payuForm">
								  <input type="hidden" name="key" value="<?php echo MERCHANT_KEY ?>" />
								  <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
								  <input type="hidden" name="txnid" value="<?php echo $posted['txnid']; ?>" />
								  <input type="hidden" name="amount" value="<?php echo $posted['amount']; ?>" />
								  <input type="hidden" name="firstname" value="<?php echo $posted['firstname']; ?>" />
								  <input type="hidden" name="lastname" value="<?php echo $posted['lastname']; ?>" />
								  <input type="hidden" name="email" value="<?php echo $posted['email']; ?>" />
								  <input type="hidden" name="phone" value="<?php echo $posted['phone']; ?>" />
								  <input type="hidden" name="productinfo" value="<?php echo $posted['productinfo']; ?>" />
								  <input type="hidden" name="surl" value="<?php echo $posted['surl']; ?>" />
								  <input type="hidden" name="furl" value="<?php echo $posted['furl']; ?>" />
								  <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
								  <input type="hidden" name="address1" value="<?php echo $posted['address1']; ?>" />
								  <input type="hidden" name="city" value="Mumbai"  />
								  <input type="hidden" name="state" value="Maharastra"  />
								  <input type="hidden" name="country" value="India" />
								   <button class="btn btn-fullwidth btn-style-1" type="submit"><i class="fa fa-angle-double-right"></i>&nbsp; <span>Place by PayUmoney</span></button>
								</form>	
							</div>
                                        
                               
						
                          
		
	
	</div>
	
		
	 </div>
						</div>
                        </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->
		 