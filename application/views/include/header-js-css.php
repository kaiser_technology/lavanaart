<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/bootstrap.min.css">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/font-awesome.min.css">

    <!-- dl Icon CSS -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/dl-icon.css">

    <!-- All Plugins CSS -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/plugins.css">

    <!-- Revoulation Slider CSS  -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/revoulation.css">

    <!-- style CSS -->
    <link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/main.css">
<!-- style CSS -->
<link rel="stylesheet" href="<?php echo ADMIN_URL;?>assets/css/my-account.css">
    <!-- modernizr JS
    ============================================ -->
    <script src="<?php echo ADMIN_URL;?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	 <!-- jQuery JS -->
    <script src="<?php echo ADMIN_URL;?>assets/js/vendor/jquery.min.js"></script>