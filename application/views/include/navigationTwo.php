<!-- Header Area Start -->
        <header class="header header-fullwidth header-style-1">
            <div class="header-inner fixed-header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-2 col-md-3 col-4 text-lg-center">
                            <!-- Logo Start Here -->
                            <a href="/" class="logo-box">
                                <figure class="logo--normal"> 
                                    <img style="width:150px" href="<?php echo ADMIN_URL; ?>" src="<?php echo CDN_IMG_URL."/store/".STORE_KEY.".jpg";?>" alt="Sayonara">    
                                </figure>
                                <figure class="logo--transparency">
                                      <img style="width:150px" href="<?php echo ADMIN_URL; ?>" src="<?php echo CDN_IMG_URL."/store/".STORE_KEY.".jpg";?>" alt="Sayonara">    
                                </figure>
                            </a>
                            <!-- Logo End Here -->
                        </div>
                        
						<div class="col-xl-8 col-lg-6">
                            <!-- Main Navigation Start Here -->
                             <nav class="main-navigation">
                                    <ul class="mainmenu mainmenu--2 mainmenu--centered">
										<li class="mainmenu__item">
                                            <a href="<?php echo ADMIN_URL; ?>" class="mainmenu__link">
                                                <span class="mm-text">Home</span>
                                            </a>
                                        </li>
										<li class="mainmenu__item">
                                            <a href="<?php echo ADMIN_URL; ?>Aboutus" class="mainmenu__link">
                                                <span class="mm-text">About</span>
                                            </a>
                                        </li>
										
										<?php foreach($main_category[0] as $mkey => $menu){							
										?>
										<li class="mainmenu__item menu-item-has-children">
                                            <a href="javascript:void(0)" class="mainmenu__link">
                                                <span class="mm-text"><?php  echo $menu->cat_name;?></span>
												<!--<span class="tip">Hot</span>-->
                                            </a>
											
                                            <ul class="megamenu four-column">											
                                                <?php if (array_key_exists('sub', $menu)) { 
													foreach($menu->sub as $level1){
												?>
												<li>
                                                    <a class="megamenu-title" href="<?php echo base_url();?>category/index/<?php echo $level1->cat_id;?>">
                                                        <span class="mm-text"><?php echo $level1->cat_name;?></span>
                                                    </a>
													
                                                    <ul>
														<?php if (array_key_exists('sub', $level1)) { 
														foreach($level1->sub as $level2){
														?>
                                                        <li>
                                                            <a href="<?php echo base_url();?>category/index/<?php echo $level2->cat_id;?>">
                                                                <span class="mm-text"><?php echo $level2->cat_name;?></span>
                                                            </a>
                                                        </li> 
														<?php
																}
															}
														?>														
                                                    </ul>													
                                                </li>
												<?php
														}
													}
												?>                                                
                                            </ul>											
                                        </li>
										<?php } ?>
                                        <li class="mainmenu__item">
                                            <a href="<?php echo ADMIN_URL; ?>Contactus" class="mainmenu__link">
                                                <span class="mm-text">Contact</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            <!-- Main Navigation End Here -->
                        </div>
                        <div class="col-xl-2 col-lg-4 col-md-9 col-8">
                            <ul class="header-toolbar text-right">
                                       
										<?php 
										if(isset($this->session->userdata('userDetail')->user_name)){?>
								<span class="header-text">Welcome <?php echo ucwords($this->session->userdata('userDetail')->user_name);?></span>
										<?php }?>
                                        <li class="header-toolbar__item user-info-menu-btn">
                                            <a href="#">
                                                <i class="fa fa-user-circle-o"></i>
                                            </a>
                                             <?php 
										if(isset($this->session->userdata('userDetail')->user_name)){?>
										 <ul class="user-info-menu">
                                                <li>
                                                    <a href="<?php echo ADMIN_URL."account" ;?>">My Account</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Order tracking</a>
                                                </li>
                                                 <li>
                                                    <a href="<?php echo ADMIN_URL."account/logout";?>">Sign out</a>
                                                </li>
                                            </ul>
										<?php }else {?>
											<ul class="user-info-menu">
                                                <li><a href="<?php echo ADMIN_URL."register" ;?>">Sign Up</a></li>
											<li><a href="<?php echo ADMIN_URL."login";?>">Log In</a></li>
                                            </ul>
										<?php }?>
                                        </li>
                                       <li class="header-toolbar__item">
                                       <a href="/CartDetails" class="mini-cart-btn">
                                                <i class="dl-icon-cart4"></i>
                                                <sup class="mini-cart-count"><?php if( $this->session->userdata('buynw') != "yes"){ echo $this->cart->total_items(); } else{ echo "0"; }?></sup>
                                            </a>
                                        </li>
										<li>
											 <select id="cur" onchange='document.cookie ="currenty="+this.value; location.reload(); '>
										<option <?php if(CURRENCY=="INR"){ echo "selected"; }?>>INR</option>
										<option <?php if(CURRENCY=="USD"){ echo "selected"; }?>>USD</option>
									</select>
										</li>
                                    </ul>
                               
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Area End -->
