<!doctype html>
<html class="no-js" lang="zxx">


<head>
	<?php include('meta.php');?>
    <!-- ************************* CSS Files ************************* -->

    <?php include('header-js-css.php');?>
</head>
<body>


    <div class="ai-preloader active">
        <div class="ai-preloader-inner h-100 d-flex align-items-center justify-content-center">
            <div class="ai-child ai-bounce1"></div>
            <div class="ai-child ai-bounce2"></div>
            <div class="ai-child ai-bounce3"></div>
        </div>
    </div>
	<!-- Main Wrapper Start -->
    <div class="wrapper">
		<?php 
			$this->pro->addvisitor(STORE_KEY,$_SERVER['REMOTE_ADDR']);
			$main_category = array();
			$remove_key = array();
			if(isset($category->detail) && !empty($category->detail)){
				foreach($category->detail as $cat){									
					$main_category[$cat->parent_id][$cat->cat_id] = $cat; 	
				}						
				foreach($main_category as $k => $sub){							
					foreach($main_category[$k] as $pk  => $level){
						if(array_key_exists($level->cat_id, $main_category)){
							$main_category[$k][$pk]->sub = $main_category[$level->cat_id];									
							$remove_key[] = $level->cat_id;
						}	
					}							
				}
			}
			foreach($remove_key as $remove){
				unset($main_category[$remove]);
			}				  
		?>
		<?php include('navigationTwo.php');?>
		<?php include('mobnavigation.php');?>