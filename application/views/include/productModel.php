<!-- Modal Start -->
        <div class="modal fade product-modal" id="productModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="dl-icon-close"></i></span>
                </button>
                <div class="row">
                    <div class="col-md-6">
                        <div class="airi-element-carousel product-image-carousel nav-vertical-center nav-style-1"
                                data-slick-options='{
                                    "slidesToShow": 1,
                                    "slidesToScroll": 1,
                                    "arrows": true,
                                    "prevArrow": "dl-icon-left",
                                    "nextArrow": "dl-icon-right"
                                }'
                        >
							<?php if($product->pro_image != ""){ ?>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="product-details.html">
                                        <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image;?>" alt="Product Image" class="primary-image">
                                    </a>
                                </div>
                                <span class="product-badge sale">sale</span>
                            </div>
							<?php }?>
							<?php if($product->pro_image1 != ""){ ?>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="product-details.html">
                                        <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image1;?>" alt="Product Image" class="primary-image" >
                                    </a>
                                </div>
                                <span class="product-badge sale">sale</span>
                            </div>
							<?php }?>
							<?php if($product->pro_image2 != ""){ ?>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="product-details.html">
                                        <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image2;?>" alt="Product Image" class="primary-image">
                                    </a>
                                </div>
                                <span class="product-badge sale">sale</span>
                            </div>
							<?php }?>
							<?php if($product->pro_image3 != ""){ ?>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="product-details.html">
                                        <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image3;?>" alt="Product Image" class="primary-image">
                                    </a>
                                </div>
                                <span class="product-badge sale">sale</span>
                            </div>
							<?php }?>
							<?php if($product->pro_image4 != ""){ ?>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="product-details.html">
                                        <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image4;?>" alt="Product Image" class="primary-image">
                                    </a>
                                </div>
                                <span class="product-badge sale">sale</span>
                            </div>
							<?php }?>
                           </div>
                    </div>
                    <div class="col-md-6">
                        <div class="modal-box product-summary">
                           
                            <h3 class="product-title mb--15"><?php echo $product->pro_name;?></h3>
                            <span class="product-price-wrapper mb--20">
                                 <span class="money">₹ <?php echo $product->pro_sale_price;?></span>
                                <span class="product-price-old">
                                    <span class="money">₹ <?php echo $product->pro_mrp_price;?></span>
                                </span>
                            </span>
                            <p class="product-short-description mb--25 mb-md--20"><?php echo $product->pro_detail;?></p>
                            <div class="product-action d-flex flex-row align-items-center mb--30 mb-md--20">
                                <div class="quantity">
                                    <input type="number" class="quantity-input" name="qty" id="quick-qty" value="1" min="1">
                                </div>
								
                                <button type="button" class="btn btn-style-1 btn-semi-large add-to-cart" onclick="addToCart('<?php echo $product->pro_id;?>','<?php echo $product->pro_sale_price;?>','<?php echo $product->pro_name;?>','<?php echo $product->pro_price_id;?>','<?php echo $product->pro_code;?>','<?php echo $product->pro_image;?>','1');" href="javascript:void(0)" >
                                    Add To Cart
                                </button>
                              
                            </div>  
                           
                            <div class="product-summary-footer d-flex justify-content-between flex-sm-row flex-column flex-sm-row flex-column">
                                <div class="product-meta">
                                    <span class="sku_wrapper font-size-12">Product Code: <span class="sku"><?php echo $product->pro_code;?></span></span>
                                    <span class="posted_in font-size-12">Categories: <a href="<?php echo base_url()."category/index/".$product->pro_cat;?>" rel="tag"><?php echo $this->pro->cat_name($product->pro_cat); ?></a></span>
                                </div>
                                <div class="product-share-box">
                                    <span class="font-size-12">Share With</span>
                                    <!-- Social Icons Start Here -->
                                    <ul class="social social-small">
                                        <li class="social__item">
											<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $detailUrl; ?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no');return false;" class="social__link" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="social__item">
                                            <a href="https://twitter.com/intent/tweet?url=<?php echo $detailUrl; ?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no');return false;" class="social__link">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="social__item">
                                            <a href="https://plus.google.com/share?url={<?php echo $detailUrl; ?>}" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no');return false;" class="social__link">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                       
                                    </ul>
                                    <!-- Social Icons End Here -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>