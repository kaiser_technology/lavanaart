 <!-- Footer Start -->
        <footer class="footer footer-3 bg--white border-top">
            <div class="container">
                <div class="row pt--40 pt-md--30 mb--40 mb-sm--30">
                    <div class="col-12 text-md-center">
                        <div class="footer-widget">
                            <div class="textwidget">
                                <a href="index.html" class="footer-logo">
                                    <img style="width:100px" href="<?php echo ADMIN_URL; ?>" src="<?php echo CDN_IMG_URL."/store/".STORE_KEY.".jpg";?>" alt="Lavana Art">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb--15 mb-sm--20">
                    <div class="col-xl-3 col-md-6 mb-lg--45">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">USEFUL LINKS</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="<?php echo ADMIN_URL; ?>">Home</a></li>
                                <li><a href="<?php echo ADMIN_URL; ?>Aboutus">About Us</a></li>
                                <li><a href="<?php echo ADMIN_URL; ?>Contactus">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-lg--45">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">TERMS & POLICIES</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="<?php echo ADMIN_URL; ?>tandc">Terms & Conditions</a></li>
                                <li><a href="<?php echo ADMIN_URL; ?>tandc">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <!--<div class="col-xl-2 col-md-4 mb-lg--30">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2">SHOPPING</h3>
                            <ul class="widget-menu widget-menu--2">
                                <li><a href="shop-instagram.html">Look Book</a></li>
                                <li><a href="shop-sidebar.html">Shop Sidebar</a></li>
                                <li><a href="shop-fullwidth.html">Shop Fullwidth</a></li>
                                <li><a href="shop-no-gutter.html">Man & Woman</a></li>
                            </ul>
                        </div>
                    </div>-->
                    <div class="col-xl-5 offset-xl-1 col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                        <div class="footer-widget">
                            <h3 class="widget-title widget-title--2 widget-title--icon">Subscribe now and get 10% off new collection</h3>
                            <form action="https://company.us19.list-manage.com/subscribe/post?u=2f2631cacbe4767192d339ef2&amp;id=24db23e68a" class="newsletter-form newsletter-form--3 mc-form" method="post" target="_blank">
                                <input type="email" name="newsletter-email" id="newsletter-email" class="newsletter-form__input" placeholder="Enter Your Email Address..">
                                <button type="submit" class="newsletter-form__submit">
                                    <i class="dl-icon-right"></i>
                                </button>
                            </form>
                            <!-- mailchimp-alerts Start -->
                            <div class="mailchimp-alerts">
                                <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                            </div>
                            <!-- mailchimp-alerts end -->
                        </div>
                    </div>
                </div>
                <div class="row align-items-center pt--10 pb--30">
                    <div class="col-md-4">
                        <!-- Social Icons Start Here -->
                        <ul class="social social-small">
                            <li class="social__item">
                                <a href="twitter.com" class="social__link">
                                <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="plus.google.com" class="social__link">
                                <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="facebook.com" class="social__link">
                                <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="youtube.com" class="social__link">
                                <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="instagram.com" class="social__link">
                                <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- Social Icons End Here -->
                    </div>
                    <div class="col-md-4 text-md-center">
                        <p class="copyright-text">&copy;2019 Lavana Art. Designed by Jackfruit Technologies</p>
                    </div>
                    <div class="col-md-4 text-md-right">
                        <img src="assets/img/others/payments-2.png" alt="Payment">
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->
	<?php include_once('widgets.php');?>	
	<?php include_once('footer-js-css.php');?>
	 <script type="text/javascript">
	function addToCart(pro_id,pro_seal_price,pro_name,pro_price_id,pro_code,pro_image,qty,catid) {		
		$.ajax({
			type : "POST", 
			url : "<?php echo base_url();?>ProductCart/addtocart",
			data: {
				'id':pro_id,
				'price': pro_seal_price,
				'name': pro_name,
				'ppid': pro_price_id,
				'pcode':pro_code,
				'pimg':pro_image,
				'qty' : qty,
				'catid' : catid
			},
			success:function(state){
				
				$('.mini-cart-count').text(state);
				alert("One product added to cart");				
			}
		});

	}
function removeCart(rowid) {	
//alert('asdf');		
		$.ajax({
			type : "POST", 
			url : "<?php echo base_url();?>ProductCart/removecart",
			data: {
				'rowid':rowid
			},
			success:function(state){
				
				$('#'+rowid).hide();
				$('.mini-cart-count').text(state.item);
				$('.mini-cart__total .ammount').text("<i class='fa fa-<?php echo strtolower(CURRENCY);?>'></i> " +state.total+".00");	
					
			}
		});

	}
	
</script>
	