 </div>
    <!-- Main Wrapper End -->


    <!-- ************************* JS Files ************************* -->

   

    <!-- Bootstrap and Popper Bundle JS -->
    <script src="<?php echo ADMIN_URL;?>assets/js/bootstrap.bundle.min.js"></script>

    <!-- All Plugins Js -->
    <script src="<?php echo ADMIN_URL;?>assets/js/plugins.js"></script>

    <!-- Ajax Mail Js -->
    <script src="<?php echo ADMIN_URL;?>assets/js/ajax-mail.js"></script>

    <!-- Main JS -->
    <script src="<?php echo ADMIN_URL;?>assets/js/main.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/jquery.themepunch.revolution.min.js"></script>    

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.carousel.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.migration.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.parallax.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation/extensions/revolution.extension.video.min.js"></script>

    <!-- REVOLUTION ACTIVE JS FILES -->
    <script src="<?php echo ADMIN_URL;?>assets/js/revoulation.js"></script>

</body>


</html>
