<div class="breadcrumb-area bg--white-6 pt--60 pb--70 pt-lg--40 pb-lg--50 pt-md--30 pb-md--40">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title"><span>Order History</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="current"><span>Order History</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->



        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    <div class="row pt--75 pt-md--55 pt-sm--35 pb--80 pb-md--60 pb-sm--40">
          <div class="col-md-9" >
                    <div class="my-account">
            <div class="page-title">
              <h2>Order Histroy</h2>
            </div>
				<?php if($this->uri->segment('3')){
						echo ' <span class="text-success"><strong>Cancellation request send successfuly.</strong></span>';
				}?>
         <div class="wishlist-item table-responsive">
              <table class="col-md-12">
            <tr class="tableheader">
              <td>Order No.</td>
              <td>Order Code.</td>
              <td>Order Date</td>
              <td>Total Amt.</td>
             
              <td>Order Status</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody class="lightbg" valign="top">
          <?php
         	$i=1;
			foreach($order->detail as $orderdata)
			{
				
		  ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $orderdata->ordrndcode;?></td>
              <td><?php
					$date=$orderdata->ord_date;
					$d=explode('-',$date);
					
			  $od =  $d[2].'-'.$d[1].'-'.$d[0];
			$orddate=date("M d, Y", strtotime($date));
			$deldate= date("Y-m-d" ,strtotime($orderdata->del_date));
			 echo $orddate;
			  ?></td>
              <td><?php echo $orderdata->total_amount;?></td>
              
              <td>
			  <?php if($orderdata->ord_status == "complete"){
						$class = "text-success";						  
					  }else if($orderdata->ord_status == "pending"){ 
						$class = "text-warning";
					  }
					  else{
						  $class = "text-danger";
					  }
					  ?>
			  <span class="<?php echo $class;?>"><strong><?php echo $orderdata->ord_status;?></strong></span></td>
                          
              <td><a href="<?php echo base_url('account/order_hist_detail/'.$orderdata->ord_id."/".$this->pro->user_Id($this->session->userdata('userDetail')->email,STORE_KEY)."/".$orderdata->ship_id);?>">View</a> 
			  

			  <?php
//echo $deldate .'>'. date("Y-m-d"); exit;
			  if($date < $deldate &&  $deldate > date("Y-m-d") && $orderdata->ord_status != "Cancel"){?>| 
			 
			 <a onclick="conf(<?php echo $i;?>);">Cancel</a>
			  <a id="link<?php echo $i;?>" href="<?php echo base_url('account/cancel_order/'.$orderdata->ord_id);?>"></a>
			  <?php }?>
			  </td>
            </tr> 
            <?php
			$i++;
			}
			?>         
          </tbody>
        </table>
      </div>
    
           </div>
        </div>
        
        <aside class="right sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              <ul>
                <li><a href="<?php echo ADMIN_URL."account" ;?>">Account Dashboard</a></li>
                <li ><a href="<?php echo ADMIN_URL."account/profile" ;?>">Account Information</a></li>
                <li class="current"><a  href="javascript:void(0)">My Orders</a></li>
                <li><a href="<?php echo ADMIN_URL."account/changePassword" ;?>">Change Password</a></li>
              </ul>
            </div>
          </div>
                  </aside></div> 
      </div>
    </div>
  </section> 
  <script>
function conf(str)
{
	a= confirm("Are you sure to Cancel order?");
	if(a == true)
	{
		var comp = document.getElementById('link'+str);
		comp.click();
		return true;
		
	}
	else{
	return false;
	}
} 
</script>