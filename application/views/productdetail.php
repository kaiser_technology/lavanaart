  <!-- Breadcrumb area Start -->

        <div class="breadcrumb-area pt--70 pt-md--25">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li><a href="<?php echo base_url()."category/index/".$product->pro_cat?>"><?php echo $this->pro->cat_name($product->pro_cat); ?></a></li>
                            <li class="current"><span><?php echo $product->pro_name?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner enable-full-width">
                <div class="container-fluid">
                    <div class="row pt--40">
                        <div class="col-md-6 product-main-image">
                            <div class="product-image">
                                <div class="product-gallery vertical-slide-nav">
                                    <div class="product-gallery__thumb">
                                        <div class="product-gallery__thumb--image">
                                            <div class="nav-slider slick-vertical" 
                                            data-options='{
                                            "vertical": true, 
                                            "vertical_md": false, 
                                            "infinite_md": false, 
                                            "slideToShow_sm": 4,
                                            "slideToShow_xs": 3,
                                            "arrows": true,
                                            "arrowPrev": "fa fa-angle-up",
                                            "arrowNext": "fa fa-angle-down",
                                            "arrowPrev_md": "dl-icon-left",
                                            "arrowNext_md": "dl-icon-right"
                                            }'>
											<?php if($product->pro_image != ""){ ?>
                                                <figure class="product-gallery__thumb--single">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image;?>" alt="Products">
                                                </figure>
												<?php }?>
												
											<?php if($product->pro_image1 != ""){ ?>
                                                <figure class="product-gallery__thumb--single">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image1;?>" alt="Products">
                                                </figure>
												<?php }?>
														
												<?php if($product->pro_image2 != ""){ ?>
                                                <figure class="product-gallery__thumb--single">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image2;?>" alt="Products">
                                                </figure>
												<?php }?>
												<?php if($product->pro_image3 != ""){ ?>
                                                <figure class="product-gallery__thumb--single">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image3;?>" alt="Products">
                                                </figure>
												<?php }?>
												<?php if($product->pro_image4 != ""){ ?>
                                                <figure class="product-gallery__thumb--single">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image4;?>" alt="Products">
                                                </figure>
												<?php }?>												
												 </div>
                                        </div>
                                    </div>
                                    <div class="product-gallery__large-image">
                                        <div class="gallery-with-thumbs">
                                            <div class="product-gallery__wrapper">
                                                <div class="main-slider product-gallery__full-image image-popup">
													<?php
												if($product->pro_image != ""){ ?>
                                                <figure class="product-gallery__image zoom">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image;?>" alt="Products">
                                                </figure>
												<?php }?>
												<?php
												if($product->pro_image1 != ""){ ?>
                                                <figure class="product-gallery__image zoom">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image1;?>" alt="Products">
                                                </figure>
												<?php }?><?php
												if($product->pro_image2 != ""){ ?>
                                                <figure class="product-gallery__image zoom">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image2;?>" alt="Products">
                                                </figure>
												<?php }?><?php
												if($product->pro_image3 != ""){ ?>
                                                <figure class="product-gallery__image zoom">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image3;?>" alt="Products">
                                                </figure>
												<?php }?><?php
												if($product->pro_image4 != ""){ ?>
                                                <figure class="product-gallery__image zoom">
                                                    <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image4;?>" alt="Products">
                                                </figure>
												<?php }?>
												
                                                </div>
                                                <div class="product-gallery__actions">
                                                    <button class="action-btn btn-zoom-popup"><i class="dl-icon-zoom-in"></i></button>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="product-badge new">New</span>
                            </div>
                        </div>
                        <div class="col-md-6 product-main-details mt--40 mt-md--10 mt-sm--30">
                            <div class="product-summary">
                                
                                <div class="clearfix"></div>
                                <h3 class="product-title"><?php echo $product->pro_name;?></h3>
                                <span class="product-stock in-stock float-right">
                                    <i class="dl-icon-check-circle1"></i>
                                    in stock
                                </span>
                                <div class="product-price-wrapper mb--40 mb-md--10">
                                    <span class="money"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo $this->pro->convertRate($product->pro_sale_price);?></span>
                                <span class="product-price-old">
                                    <span class="money"><i class="fa fa-<?php echo strtolower(CURRENCY);?>"></i> <?php echo $this->pro->convertRate($product->pro_mrp_price);?></span>
                                </span>
                                </div>
                                <div class="clearfix"></div>
                                <p class="product-short-description mb--45 mb-sm--20"><?php echo $product->pro_detail;?></p>
                                <form action="#" class="form--action mb--30 mb-sm--20 fltleft">
                                    <div class="product-action flex-row align-items-center">
                                        <div class="quantity">
                                            <input type="number" class="quantity-input" name="qty" id="qty" value="1" min="1">
                                        </div>
                                        <button type="button" onclick="addToCart('<?php echo $product->pro_id;?>','<?php echo $product->pro_sale_price;?>','<?php echo $product->pro_name;?>','<?php echo $product->pro_price_id;?>','<?php echo $product->pro_code;?>','<?php echo $product->pro_image;?>','1');" class="btn btn-style-1 btn-large add-to-cart">
                                            Add To Cart
                                        </button>
                                        <!-- Trigger the modal with a button -->
                                    
                                    </div>  
                                </form>
                                <a href="/contactus/productenq/<?php echo $product->pro_id;?>" type="button" class="btn btn-style-1 btn-semi-large " >Request Callback</a>
                                <div class="product-summary-footer d-flex justify-content-between flex-sm-row flex-column fltclear">
                                    <div class="product-meta">
                                         <span class="sku_wrapper font-size-12">Product Code: <span class="sku"><?php echo $product->pro_code;?></span></span>
                                    <span class="posted_in font-size-12">Categories: <a href="<?php echo base_url()."category/index/".$product->pro_cat;?>" rel="tag"><?php echo $this->pro->cat_name($product->pro_cat); ?></a></span>
                                       
                                    </div>
                                    <div class="product-share-box">
                                    <span class="font-size-12">Share With</span>
                                    <!-- Social Icons Start Here -->
                                    <ul class="social social-small">
                                        <li class="social__item">
											<a href="https://www.facebook.com/sharer/sharer.php?u=http:<?php echo current_url();?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no');return false;" class="social__link" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="social__item">
                                            <a href="https://twitter.com/intent/tweet?url=http:<?php echo current_url();?>" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no');return false;" class="social__link">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="social__item">
                                            <a href="https://plus.google.com/share?url={http:<?php echo current_url();?>}" onclick="javascript:window.open(this.href,
					  '', 'menubar=no,toolbar=no');return false;" class="social__link">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                       
                                    </ul>
                                    <!-- Social Icons End Here -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center pt--45 pt-lg--50 pt-md--55 pt-sm--35">
                      <!--div class="row pt--35 pt-md--25 pt-sm--15 pb--75 pb-md--55 pb-sm--35">
                        <div class="col-12">
                            <div class="row mb--40 mb-md--30">
                                <div class="col-12 text-center">
                                    <h2 class="heading-secondary">Related Products</h2>
                                    <hr class="separator center mt--25 mt-md--15">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="airi-element-carousel product-carousel nav-vertical-center" 
                                    data-slick-options='{
                                    "spaceBetween": 30,
                                    "slidesToShow": 4,
                                    "slidesToScroll": 1,
                                    "arrows": true, 
                                    "prevArrow": "dl-icon-left", 
                                    "nextArrow": "dl-icon-right" 
                                    }'
                                    data-slick-responsive='[
                                        {"breakpoint":1200, "settings": {"slidesToShow": 3} },
                                        {"breakpoint":991, "settings": {"slidesToShow": 2} },
                                        {"breakpoint":450, "settings": {"slidesToShow": 1} }
                                    ]'
                                    >
									<?php 
									 
										$subCategory = array();
										$manufacturer = array();
										$total = 0; 
										if(isset($productList) && !empty($productList)){
											
											 $i = 1;
											foreach($productList as $product){
												$subCategory[$product->pro_cat][] = $product->pro_cat; 
												$manufacturer[$product->brand][] = $product->brand;
												$detailUrl= base_url()."product/detail/". $this->pro->slugify($this->pro->cat_name($product->pro_cat)).'/'.$this->pro->slugify($product->pro_name).'/'.$product->pro_id .'/'.$product->pro_cat;												
											?>
                                        <div class="airi-product">
                                            <div class="product-inner">
                                                <figure class="product-image">
                                                    <div class="product-image--holder">
                                                        <a href="<?php echo $detailUrl; ?>">
                                                            <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image;?>" alt="Product Image" style="width: 500px;height: 500px;" class="primary-image">
                                                            <img src="<?php echo CDN_IMG_URL.'product/'.$product->pro_image1;?>" style="width: 500px;height: 500px;" alt="Product Image" class="secondary-image">
                                                        </a>
                                                    </div>
                                                    <div class="airi-product-action">
                                                        <div class="product-action">
                                                            <a class="quickview-btn action-btn" data-toggle="tooltip" data-placement="top" title="Quick Shop">
                                                                <span data-toggle="modal" data-target="#productModal">
                                                                    <i class="dl-icon-view"></i>
                                                                </span>
                                                            </a>
                                                            <a class="add_to_cart_btn action-btn" href="cart.html" data-toggle="tooltip" data-placement="top" title="Add to Cart">
                                                                <i class="dl-icon-cart29"></i>
                                                            </a>
                                                            <a class="add_wishlist action-btn" href="wishlist.html" data-toggle="tooltip" data-placement="top" title="Add to Wishlist">
                                                                <i class="dl-icon-heart4"></i>
                                                            </a>
                                                            <a class="add_compare action-btn" href="compare.html" data-toggle="tooltip" data-placement="top" title="Add to Compare">
                                                                <i class="dl-icon-compare"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </figure>
                                                <div class="product-info text-center">
                                                    <h3 class="product-title">
                                                        <a href="product-details.html">Open sweatshirt</a>
                                                    </h3>
                                                    <span class="product-price-wrapper">
                                                        <span class="money">$49.00</span>
                                                        <span class="product-price-old">
                                                            <span class="money">$60.00</span>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                       <?php 
								include('include/productModel.php');
								$i++; }
								}
								?>		
									   </div>
                                </div>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->




    </div>
    <!-- Main Wrapper End -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Request Callback</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <form class="form" method="post" onsubmit="return sendrcb()"  id="rcb-form">
      <div class="modal-body">
        <!-- Contact form Start Here -->
                           <input type="hidden" name="productname" value="<?php echo $product->pro_name;?>" />
                                <div class="form__group mb--20">
                                    <input type="text" id="contact_name" name="name" class="form__input form__input--2" placeholder="Your name*">
                                </div>
                                <div class="form__group mb--20">
                                    <input type="email" id="contact_email" name="email" class="form__input form__input--2" placeholder="Email Address*">
                                </div>
                                <div class="form__group mb--20">
                                    <input type="text" id="contact_phone" name="mobile" class="form__input form__input--2" placeholder="Your Phone*">
                                </div>
                                <div class="form__group mb--20">
                                    <textarea class="form__input form__input--textarea" id="contact_message" name="message" placeholder="Message*"></textarea>
                                </div>
                               
                                <div class="form__output"></div>
                            
                            <!-- Contact form end Here -->
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" >Send</button>
      </div>
      </form>
    </div>
    <script> 
function sendrcb(){
    var form= $("#rcb-form");
    $.ajax({
        type: form.attr('method'),
        url: '<?php echo base_url('/product/rcbsend');?>',
        data: form.serialize(),
        success: function (data) {
    if(data=="success")
    {
            $('#showmsg').click();
    }
    }
    });
}

</script>
  </div>
</div>

<!-- Modal success message-->
<div id="msgModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Query submitted!</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
     
      <div class="modal-body">
        <!-- Contact form Start Here -->
      <p>  Thank you for enquiring with Cox & Kings.</p>
        <p>
        Your enquiry reference number is 3231338
        </p><p>
        Our expert will get in touch with you soon.
        </p>                    <!-- Contact form end Here -->
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" >Close</button>
      </div>
    </div>

  </div>
</div>
<button type="button" id="showmsg" style="display:none" class="btn btn-style-1 btn-large" data-toggle="modal" data-target="#msgModal"></button>



