<!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="page-content-inner">
                <div class="container">
                    
                    <div class="row pt--80 pt-md--60 pt-sm--35 pb--40 pb-md--30 pb-sm--15">
                        <div class="col-lg-7 col-md-6 mb-sm--30">
                            <div class="about-text">
                                <h3 class="heading-tertiary heading-color mb--15">Airi Beautiful eCommerce Online Store</h3>
                                <p class="color--light-3 mb--25 mb-md--20">Praesent sed ex vel mauris eleifend mollis. Vestibulum dictum sodales ante, ac pulvinar urna sollicitudin in. Suspendisse sodales dolor nec mattis convallis. Quisque ut nulla viverra, posuere lorem eget, ultrices metus. Nulla facilisi. Duis aliquet, eros in auctor aliquam, tortor justo laoreet nisi, nec pulvinar lectus diam nec libero. Nullam sit amet Cras porta posuere lectus, vitae consectetur dolor elementum</p>
                                <figure>
                                    <img src="assets/img/about/about-signature.png" alt="signature">
                                </figure>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <figure>
                                <img src="assets/img/about/about-bg2.jpg" alt="about">
                            </figure>
                        </div>
                    </div>
                    
                    <div class="row pt--30 pt-md--20 pt-sm--15 pb--40 pb-md--30 pb-sm--20">
                        <div class="col-12">
                            <div class="row justify-content-center mb--35 mb-md--25">
                                <div class="col-xl-6 text-center">
                                    <h3 class="heading-tertiary heading-color mb--15">Meet Our Team</h3>
                                    <p class="color--light-3">Praesent sed ex vel mauris eleifend mollis. Vestibulum dictum sodales ante, ac pulvinar urna sollicitudin in. Suspendisse sodales</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="airi-element-carousel team-carousel" 
                                        data-slick-options='{
                                            "spaceBetween": 30,
                                            "slidesToShow": 3,
                                            "slidesToScroll": 3
                                        }'
                                        data-slick-responsive='[
                                            {"breakpoint":991, "settings": {"slidesToShow": 2} },
                                            {"breakpoint":479, "settings": {"slidesToShow": 1} }
                                        ]'>

                                        <div class="airi-team">
                                            <div class="team-member">
                                                <div class="team-member__thumbnail">
                                                    <img src="assets/img/team/member-1.jpg" alt="Team Member">
                                                    <a href="team.html" class="link-overlay">Team member</a>
                                                    <div class="team-member__overlay">
                                                        <ul class="social social-round">
                                                            <li class="social__item">
                                                                <a href="https://www.facebook.com/" class="social__link">
                                                                    <i class="fa fa-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://twitter.com/" class="social__link">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://www.pinterest.com/" class="social__link">
                                                                    <i class="fa fa-pinterest-p"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="team-member__info">
                                                    <h2 class="team-member__name"><a href="team.html">Dollie Watts</a></h2>
                                                    <p class="team-member__designation">CEO Founder</p>
                                                    <p class="team-member__desc">Pellentesque dignissim at ante sed iaculis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sod</p>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="airi-team">
                                            <div class="team-member">
                                                <div class="team-member__thumbnail">
                                                    <img src="assets/img/team/member-2.jpg" alt="Team Member">
                                                    <a href="team.html" class="link-overlay">Team member</a>
                                                    <div class="team-member__overlay">
                                                        <ul class="social social-round">
                                                            <li class="social__item">
                                                                <a href="https://www.facebook.com/" class="social__link">
                                                                    <i class="fa fa-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://twitter.com/" class="social__link">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://www.pinterest.com/" class="social__link">
                                                                    <i class="fa fa-pinterest-p"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="team-member__info">
                                                    <h2 class="team-member__name"><a href="team.html">Mitchell Bates</a></h2>
                                                    <p class="team-member__designation">Art Director</p>
                                                    <p class="team-member__desc">Pellentesque dignissim at ante sed iaculis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sod</p>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="airi-team">
                                            <div class="team-member">
                                                <div class="team-member__thumbnail">
                                                    <img src="assets/img/team/member-3.jpg" alt="Team Member">
                                                    <a href="team.html" class="link-overlay">Team member</a>
                                                    <div class="team-member__overlay">
                                                        <ul class="social social-round">
                                                            <li class="social__item">
                                                                <a href="https://www.facebook.com/" class="social__link">
                                                                    <i class="fa fa-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://twitter.com/" class="social__link">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://www.pinterest.com/" class="social__link">
                                                                    <i class="fa fa-pinterest-p"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="team-member__info">
                                                    <h2 class="team-member__name"><a href="team.html">Leona Bowman</a></h2>
                                                    <p class="team-member__designation">Marketing Manager</p>
                                                    <p class="team-member__desc">Pellentesque dignissim at ante sed iaculis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sod</p>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="airi-team">
                                            <div class="team-member">
                                                <div class="team-member__thumbnail">
                                                    <img src="assets/img/team/member-4.jpg" alt="Team Member">
                                                    <a href="team.html" class="link-overlay">Team member</a>
                                                    <div class="team-member__overlay">
                                                        <ul class="social social-round">
                                                            <li class="social__item">
                                                                <a href="https://www.facebook.com/" class="social__link">
                                                                    <i class="fa fa-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://twitter.com/" class="social__link">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://www.pinterest.com/" class="social__link">
                                                                    <i class="fa fa-pinterest-p"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="team-member__info">
                                                    <h2 class="team-member__name"><a href="team.html">Amanda Gutierrez</a></h2>
                                                    <p class="team-member__designation">CEO Founder</p>
                                                    <p class="team-member__desc">Pellentesque dignissim at ante sed iaculis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sod</p>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="airi-team">
                                            <div class="team-member">
                                                <div class="team-member__thumbnail">
                                                    <img src="assets/img/team/member-5.jpg" alt="Team Member">
                                                    <a href="team.html" class="link-overlay">Team member</a>
                                                    <div class="team-member__overlay">
                                                        <ul class="social social-round">
                                                            <li class="social__item">
                                                                <a href="https://www.facebook.com/" class="social__link">
                                                                    <i class="fa fa-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://twitter.com/" class="social__link">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://www.pinterest.com/" class="social__link">
                                                                    <i class="fa fa-pinterest-p"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="team-member__info">
                                                    <h2 class="team-member__name"><a href="team.html">Marc Cook</a></h2>
                                                    <p class="team-member__designation">Art Director</p>
                                                    <p class="team-member__desc">Pellentesque dignissim at ante sed iaculis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sod</p>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="airi-team">
                                            <div class="team-member">
                                                <div class="team-member__thumbnail">
                                                    <img src="assets/img/team/member-6.jpg" alt="Team Member">
                                                    <a href="team.html" class="link-overlay">Team member</a>
                                                    <div class="team-member__overlay">
                                                        <ul class="social social-round">
                                                            <li class="social__item">
                                                                <a href="https://www.facebook.com/" class="social__link">
                                                                    <i class="fa fa-facebook"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://twitter.com/" class="social__link">
                                                                    <i class="fa fa-twitter"></i>
                                                                </a>
                                                            </li>
                                                            <li class="social__item">
                                                                <a href="https://www.pinterest.com/" class="social__link">
                                                                    <i class="fa fa-pinterest-p"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="team-member__info">
                                                    <h2 class="team-member__name"><a href="team.html">Rose Robinson</a></h2>
                                                    <p class="team-member__designation">Marketing Manager</p>
                                                    <p class="team-member__desc">Pellentesque dignissim at ante sed iaculis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sod</p>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt--30 pt-md--20 pt-sm--10 pb--75 pb-md--55 pb-sm--35">
                        <div class="col-12">
                            <div class="row mb--35 mb-md--25">
                                <div class="col-12 text-center">
                                    <h3 class="heading-tertiary heading-color">What Client Say ?</h3>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <div class="airi-element-carousel testimonial-carousel"    
                                    data-slick-options='{
                                        "slidesToShow": 1,
                                        "slidesToScroll": 1
                                    }'>
                                        <div class="testimonial testimonial-style-3">
                                            <div class="testimonial__inner">
                                                <img src="assets/img/others/happy-client-1.jpg" alt="Client" class="testimonial__author--img">
                                                <p class="testimonial__desc">"Maecenas eu accumsan libero. Fusce id imperdiet felis. Cras sed ex vel turpis ultricies blandit nec et massa. Pellentesque lectus turpis, vestibulum eu interdum vel.</p>
                                                <div class="testimonial__author">
                                                    <h3 class="testimonial__author--name">Lura Frazier</h3>
                                                    <p class="testimonial__author--designation">Happy Client</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="testimonial testimonial-style-3">
                                            <div class="testimonial__inner">
                                                <img src="assets/img/others/happy-client-2.jpg" alt="Client" class="testimonial__author--img">
                                                <p class="testimonial__desc">"Maecenas eu accumsan libero. Fusce id imperdiet felis. Cras sed ex vel turpis ultricies blandit nec et massa. Pellentesque lectus turpis, vestibulum eu interdum vel.</p>
                                                <div class="testimonial__author">
                                                    <h3 class="testimonial__author--name">Lura Frazier</h3>
                                                    <p class="testimonial__author--designation">Happy Client</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->