 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        <li><a href="<?php echo ADMIN_URL."product";?>">Product</a></li>
                        
                        <li class="active">
                            <span>Edit Category </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   Edit Category
                </h2>
                <small> </small>
            </div>
        </div>
    </div>
	<?php
		//dump($catData);
	?>
    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
					
                  <small style="color:red">*</small> field is Mendatory
                </div>
               <div class="panel-body">
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>product/editCat" enctype="multipart/form-data" id="prodcatForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
                                <label class="control-label" for="categoryname"><small style="color:red">*</small> Category Name</label>
                                <input type="text" placeholder="Category Name" title="Category Name"  value="<?=$catData->cat_name?>" name="categoryname" id="categoryname" class="form-control">
                                <?php echo form_error('categoryname', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 <!--<div class="form-group  col-lg-6">
                                <label class="control-label" for="mainCategory"><small style="color:red">*</small>Main Category</label>
											<select class="form-control m-b" name="mainCategory" id="mainCategory">
											<option Value = "">Select</option>
											<option <?=$catData->parent_id == 0 ? 'selected':''?> value="0">Main Category</option>
												<?php foreach($catlist as $list){?>
												<option <?=$catData->parent_id == $list['cat_id'] ? 'selected':''?> Value ="<?php echo $list['cat_id'];?>"><?php echo $list['cat_name'];?></option>
												<?php } ?>
											</select>
                                <?php echo form_error('mainCategory', '<span class="help-block small">', '</span>'); ?>
                            </div>
							<div class="form-group col-lg-6">
								<label>Category</label>
							   <select class="form-control m-b" name="Category" id="sub_cat">
									<option value=""> -- Select --</option>
								</select>
							</div>-->
							<div class="form-group">
                                <label class="control-label" for="img">Category Image</label>
                               <input type="file"  class="form-control" name="img" id="img" >
                               <input type="hidden" name="old_img" value="<?=$catData->cat_image?>">
                            </div>
							 <div class="form-group">
                                <label>Meta Tag Title</label>
											<input type="text" value="<?=$catData->meta_tag_title?>" id="" class="form-control" name="metatagTitle">
                                <?php echo form_error('storeaddress', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 <div class="form-group">
                                <label>Meta Tag Description</label>
											<textarea rows="3" class="form-control" name="metatagDescription" ><?=$catData->meta_tag_description?></textarea>
                                <?php echo form_error('storecity', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 <div class="form-group">
                                <label>Meta Tag Keywords</label>
											<textarea rows="3" class="form-control" name="metatagKeywords" ><?=$catData->meta_tag_keywords?></textarea>
                                <?php echo form_error('storezip', '<span class="help-block small">', '</span>'); ?>
                            </div>
							
                            <!--<div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div>-->
                            <button type="submit" class="btn btn-primary ">Submit</button>
                           <!-- <a class="btn btn-default btn-block" href="#">Register</a>-->
                           <input type="hidden" name="exist_cid" value="<?=$catData->cat_id?>">
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>

   