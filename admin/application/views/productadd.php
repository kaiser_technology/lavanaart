	 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>Product </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    User Role Information
                </h2>
                <small> </small>
            </div>
        </div>
    </div>
     <div class="content animate-panel">
      
		<div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
                    Add Product
                </div>
                <div class="panel-body">

                    <form name="simpleForm" role="form" enctype="multipart/form-data"  id="simpleForm" action="<?php echo $this->config->item('ADMIN_URL')?>product/insertproduct" method="post">

                        <div class="text-center m-b-md" id="wizardControl">

                            <a class="btn btn-primary" href="#step1" data-toggle="tab">Product Form </a>
                            

                        </div>

                        <div class="tab-content">
                        <div id="step1" class="p-m tab-pane active">

                            <div class="row">
                                
                                <div class="col-lg-12">
                                    <div class="row">
									  <?php
									  $digits = 4;
									  $cd = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
									if(isset($code) && !empty($code)){
									  $c=$code[0]['pro_code'];
								   $d=explode("-",$c);
										$cd=$d[1]+1;
									}
								   ?>
										<div class="row">	
											<div class="form-group col-lg-12">
												<label>Product Code</label>
												<input type="text" value="<?php echo "GR-".$cd; ?>" id="productCode" class="form-control" readonly  name="productCode" placeholder="Product Code">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-lg-4">
												<label>Main Category</label>
												<select class="form-control m-b" name="mainCategory" id="parent_cat" >
													 <option value="">-- Select main category --</option>
												 <?php
												$i=1;
												foreach($cat as $maincatvalue)
												{
												?>
												<option value="<?php echo $maincatvalue['cat_id'];?>"><?php echo $maincatvalue['cat_name'];?></option>
												<?php 
												}
												?>
												</select>											
											</div>
											<div class="form-group col-lg-4">
												<label>Category</label>
											   <select class="form-control m-b" name="Category" id="sub_cat">
													<option value=""> -- Select --</option>
												</select>
											</div>
											<div class="form-group col-lg-4">
												<label>Category</label>
											   <select class="form-control m-b" name="subcategory" id="subcat">
													<option value=""> -- Select --</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-lg-4">
												<label>Product Name</label>
												<input type="text" value="" id="productName" class="form-control" name="productName" placeholder="Product Name" >
											</div>
											
											<div class="form-group col-lg-4">
												<label>Brand</label>
												<input type="text" value="" id="" class="form-control" name="brandName"  placeholder="Brand">
											</div>
											<div class="form-group col-lg-4">
												<label>Display On Home</label>
												<select class="form-control m-b" name="home">
													<option value="No">No</option>
													<option value="Yes">Yes</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-lg-2">
												<label>Select Colors</label>
												<select class="form-control m-b" name="tax">
													<option value="">-- Select Color --</option>
													<option value="black" style="background:gray"></option>
													<option value="red" style="background:red"></option>
													<option value="Green" style="background:green"></option>
													<option value="Yellow" style="background:yellow"></option>
												</select>
											</div>
											<div class="form-group col-lg-4">
												<label>Select Warehouses</label>
												<select class="form-control m-b" name="wrhouse">
													<option value="">-- Select Logistics --</option>
													<option value="AsmitaOrganic">Shyplite Logistics</option>
													<option value="inhouse">In House Logistics</option>
												</select>
											</div>
											<div class="form-group col-lg-3">
												<label>Select GST Rate</label>
												<select class="form-control m-b" name="gstrate">
													<option value="">-- GST PERCENT --</option>
													<option value="5">5%</option>
													<option value="10">10%</option>
													<option value="12">12%</option>
													<option value="18">18%</option>
													<option value="28">28%</option>
												</select>
											</div>
											<div class="form-group col-lg-3">
												<label>Date Available</label>
												<div class="form-group">
													<div class="input-group date" id="datetimepicker1">
															<span class="input-group-addon">
																<span class="fa fa-calendar"></span>
															</span>
														<input type="text" name="dispdate" id="dispdate" class="form-control"/>
													</div>
												</div>
											</div>
										</div>
										<div class="row">	
											<div class="form-group col-lg-12">
												<label>Product image </label>
												<input type="file" id="pimg" class="form-control" name="pimg" required>
											</div>
										</div>	
											<div class="row">	
											<div class="form-group col-lg-12">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg1" class="form-control" name="pimg1" >
											</div>
										</div>
										
										<div class="row">	
											<div class="form-group col-lg-12">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg2" class="form-control" name="pimg2" >
											</div>
										</div>
										
										<div class="row">	
											<div class="form-group col-lg-12">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg3" class="form-control" name="pimg3" >
											</div>
										</div>
										
										<div class="row">	
											<div class="form-group col-lg-12">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg4" class="form-control" name="pimg4" >
											</div>
										</div>
                                    </div>
                                </div>
                            </div>

                            <!--<div class="text-right m-t-xs">
                                <a class="btn btn-default prev" href="#">Previous</a>
                                <a class="btn btn-default next" href="#">Next</a>
                            </div>-->
							<div class="row">
                                
                                <div class="col-lg-12">
                                    <div class="row">
									<div class="row">
                                        <div class="form-group col-lg-12">
                                            <label>Meta Tag Title</label>
                                            <input type="text" value="" id="metatagTitle" class="form-control" name="metatagTitle">
                                        </div>
                                    </div>
									<div class="row">	
                                        <div class="form-group col-lg-6">
                                            <label>Meta Tag Description</label>
                                            <textarea rows="3" class="form-control" name="metatagDescription" ></textarea>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Meta Tag Keywords</label>
                                            <textarea rows="3" class="form-control" name="metatagKeywords" ></textarea>
                                        </div>
									</div>
									<div class="row">	
                                        <div class="col-lg-12">
											<label>Product Description</label>
                                            <textarea rows="3" class="form-control" name="productDesc" ></textarea>
										</div>
										</div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
								<div class="form-group">
									<div class="col-lg-2 no-padding">
									<label>Select Unit</label>
										<select class="form-control m-b" name="unit[]">
											 <!--<option value="">--Select Unit--</option>
											 <option value="gm">gm</option>
											 <option value="kg">kg</option>
											 <option value="ltr">ltr</option>
											 <option value="ml">ml</option>-->
											 <option value="pcs">pcs</option>
										</select>
	                                </div>
									<div class="col-lg-2">
									<label>No. Of Unit</label>
										<input type="text" placeholder="No. of Unit" class="form-control" name="no_unit[]">
	                                </div>
									<div class="col-lg-2">
									<label>MRP Price</label>
										<input type="text" placeholder="Mrp Price" class="form-control" name="mrp[]">
	                                </div>
									<!--<div class="col-lg-2">
									    <label>Purchase Price</label>
										<input type="text" placeholder="Purchase Price" class="form-control" name="purchase[]">
	                                </div>-->
									<div class="col-lg-2">
									<label>Selling Price</label>
										<input type="text" placeholder="Sell Price" id="sellprice" class="form-control" name="seal[]">
	                                </div>
									<div class="col-lg-2">
									<label>Quantity</label>
										<input type="text" placeholder="Qty" id ="qty" class="form-control" name="qty[]">
	                                </div>
	                                <div class="col-xs-1">
										<button type="button" id="Masters" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
									</div>	
									<div style='clear:both;padding-top:5px;'></div>
								</div>
							</div>
							
                            <div class="text-right m-t-xs">

								<input type="submit" value="Submit" class="btn btn-success submitWizard" />
                            </div>
                        </div>

                        <div id="step2" class="p-m tab-pane">

                            
                            <!--<div class="text-right m-t-xs">
                                <a class="btn btn-default prev" href="#">Previous</a>
                                <a class="btn btn-default next" href="#">Next</a>
                            </div>-->

                        </div>
                        <div id="step3" class="tab-pane">
                            
                        </div>
                        </div>
                    </form>
                    <!-- The template for adding new field -->
							
					<div class="form-group hide Masters" >
						<div class="col-lg-2 no-padding">
							<select class="form-control m-b" name="unit[]">
                            	<option value="">--Select Unit--</option>
								 <option value="gm">gm</option>
								 <option value="kg">kg</option>
								 <option value="ltr">ltr</option>
								 <option value="ml">ml</option>
								 <option value="pcs">pcs</option>
							</select>
                        </div>
						<div class="col-lg-2">
				<input type="text" placeholder="No. of Unit" class="form-control" name="no_unit[]">
                        </div>
				<div class="col-lg-2">
				<input type="text" placeholder="Mrp Price" class="form-control" name="mrp[]">
                        </div>
                        	<div class="col-lg-2">
							<input type="text" placeholder="Purchase Price" class="form-control" name="purchase[]">
                        </div>
						<div class="col-lg-2">
							<input type="text" placeholder="Sell Price" id="sellprice" class="form-control" name="seal[]">
                        </div>
						<div class="col-lg-1">
							<input type="text" placeholder="Qty" id ="qty" class="form-control" name="qty[]">
                        </div>
						<div class="col-xs-1">
							<button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
						</div>
						<div style='clear:both;padding-top:5px;'></div>
					</div>
                </div>
            </div>
        </div>

    </div>
    </div>
	
