<div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>User Role </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   Store Listing
				   
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
    <div class="row">
            <div class="col-lg-12 ">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
					
                  <small style="color:red">*</small> field is Mendatory
                </div>
               <div class="panel-body">
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>product/addoffer" enctype="multipart/form-data" id="colorForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group col-lg-6">
                                <label class="control-label" for="m_name"><small style="color:red">*</small> Promo Code</label>
                                <input type="text" placeholder="Promo Code" title="Promo Code"  value="<?php echo set_value('pcode'); ?>" name="pcode" id="pcode" class="form-control">
                                <?php echo form_error('m_name', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 
							<div class="form-group col-lg-6">
                                <label class="control-label" for="img"><small style="color:red">*</small>Type</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="">Select</option>
                                    <option>Percentage</option>
                                    <option>Cash</option>
                                </select>
                                <?php echo form_error('type', '<span class="help-block small">', '</span>'); ?>
                            </div>	
                            <div class="form-group col-lg-4">
                                <label class="control-label" for="m_name"><small style="color:red">*</small> Value</label>
                                <input type="text" placeholder="Promo Code" title="Promo Code"  value="<?php echo set_value('amount'); ?>" name="amount" id="amount" class="form-control">
                                <?php echo form_error('amount', '<span class="help-block small">', '</span>'); ?>
                            </div>
                            <div class="form-group col-lg-4">
                            <label class="control-label" for="m_name"><small style="color:red">*</small> Start Date</label>
                                <div class="input-group date" id="datetimepicker1">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    <input type="text" name="sdate" id="sdate" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                            <label class="control-label" for="m_name"><small style="color:red">*</small> End Date</label>
                                <div class="input-group date" id="datetimepicker">
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    <input type="text" name="edate" id="edate" class="form-control"/>
                                </div>
                            </div>
                            <!--<div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div>-->
                            <button type="submit" class="btn btn-primary ">Submit</button>
                           <!-- <a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
            </div>
        </div>
		
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
				<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">×</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>	
			
                </div>
                
                <div class="panel-body">
				
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
							<tr>
								<th>Sr No</th>
								
								<th>Promo Code</th>
								<th>Promo Type</th>
								<th>Value </th>
								<th>Start Date </th>
                                <th>End Date </th>
                                <th >action</th>
							</tr>
                        </thead>
						<tbody>
							<?php
							if(isset($list) && !empty($list)){
								$i=1; foreach($list as $val){?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $val['promocode'];?></td>
								<td><?php echo $val['type'];?></td>
                                <td><?php echo $val['amount'];?></td>
                                <td><?php echo $val['startDate'];?></td>
                                <td><?php echo $val['endDate'];?></td>
								<td align="right">
									<!--<a href="<?php echo base_url('product/editProduct/'). $val['cat_id'];?>" class="btn btn-info btn-xs" type="button"><i class="fa fa-paste"></i></a>-->
									<button class="btn btn-danger2 btn-xs deletstore" data-val="<?php echo $val['id'];?>" type="button"><i class="fa fa-trash-o"></i></button>
									<?php if($val['status'] == "Inactive"){?>
									<button class="btn btn-danger btn-xs changestatus" data-val="<?php echo $val['id'].'|'.$val['status'];?>" type="button"><i class="fa fa-circle-o"></i></button>
									<?php }else{?>
									<button class="btn btn-success btn-xs  changestatus" data-val="<?php echo $val['id'].'|'.$val['status'];?>" type="button"><i class="fa fa-dot-circle-o"></i></button>
									<?php }?>
								</td>
							</tr>
							<?php $i++; } 
							}
							?>		
						</tbody>
						
                    </table>

                </div>
            </div>
            </div>
        </div>
    </div>

   