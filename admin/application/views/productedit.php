	 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>Product </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    User Role Information
                </h2>
                <small> </small>
            </div>
        </div>
    </div>
    <?php
    
    	//dump($editProData);
    	//dump($scat);
    	//dump($sscat);
    ?>
     <div class="content animate-panel">
      
		<div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
                    Edit Product
                </div>
                <div class="panel-body">

                    <form name="simpleForm" role="form" enctype="multipart/form-data"  id="simpleForm" action="<?php echo $this->config->item('ADMIN_URL')?>product/updateProduct" method="post">

                        <div class="text-center m-b-md" id="wizardControl">

                            <a class="btn btn-primary" href="#step1" data-toggle="tab">Product Form </a>
                            

                        </div>

                        <div class="tab-content">
                        <div id="step1" class="p-m tab-pane active">

                            <div class="row">
                                
                                <div class="col-lg-12">
                                    <div class="row">
									  <?php
									  $digits = 4;
									  $cd = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
									if(isset($code) && !empty($code)){
									  $c=$code[0]['pro_code'];
								   $d=explode("-",$c);
										$cd=$d[1]+1;
									}
								   ?>
										<div class="row">	
											<div class="form-group col-lg-12">
												<label>Product Code</label>
												<input type="text" value="<?=$editProData->pro_code?>" id="productCode" class="form-control" readonly  name="productCode" placeholder="Product Code">
											</div>
										</div>
										<div class="row">
										<div class="form-group col-lg-4">
											<label>Main Category</label>
											<select class="form-control m-b" name="mainCategory" id="parent_cat" >
												 <option value="">-- Select main category --</option>
											 <?php
											$i=1;
											foreach($cat as $maincatvalue):
											$selected = "";
											if($editProData->second_parent && $editProData->second_parent == $maincatvalue['cat_id'] || $editProData->first_parent && $editProData->first_parent == $maincatvalue['cat_id'])
												$selected = "selected";	
											?>
											<option <?=$selected?> value="<?=$maincatvalue['cat_id'];?>"><?= $maincatvalue['cat_name'];?></option>
											<?php 
											endforeach;
											?>
											</select>											
										</div>
											<div class="form-group col-lg-4">
												<label>Category</label>
											   <select class="form-control m-b" name="Category" id="sub_cat">
													<option value=""> -- Select --</option>
													<?php
														if($sscat)
														{
															foreach($sscat as $ssc_val):
																$selected = "";
																if($ssc_val['cat_id'] == $editProData->pro_cat || $ssc_val['cat_id'] == $editProData->first_parent)
																	$selected = "selected";
															
																echo "<option ".$selected." value='".$ssc_val['cat_id']."'>".$ssc_val['cat_name']."</option>";
															endforeach;
														}
													?>
												</select>
											</div>
											<div class="form-group col-lg-4">
												<label>Category</label>
											   <select class="form-control m-b" name="subcategory" id="subcat">
													<option value=""> -- Select --</option>
													
													<?php
														if($scat && $editProData->second_parent)
														{
															foreach($scat as $sc_val):
																$selected = "";
																if($sc_val['cat_id'] == $editProData->pro_cat)
																	$selected = "selected";
																echo "<option ".$selected." value='".$sc_val['cat_id']."'>".$sc_val['cat_name']."</option>";
															endforeach;
														}
													?>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-lg-4">
												<label>Product Name</label>
												<input type="text" value="<?=$editProData->pro_name?>" id="productName" class="form-control" name="productName" placeholder="Product Name" >
											</div>
											
											<div class="form-group col-lg-4">
												<label>Brand</label>
												<input type="text" value="<?=$editProData->brand?>" id="" class="form-control" name="brandName"  placeholder="Brand">
											</div>
											<div class="form-group col-lg-4">
												<label>Display On Home</label>
												<select class="form-control m-b" name="home">
													<option <?=$editProData->displayOnHome == "No" ? 'selected':''?> value="No">No</option>
													<option <?=$editProData->displayOnHome == "Yes" ? 'selected':''?> value="Yes">Yes</option>
												</select>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-lg-3">
												<label>Select Farm</label>
												<select class="form-control m-b" name="tax">
													<option value="None">None</option>
													<option <?=$editProData->pro_tax == "Nagpur" ? 'selected':''?> value="Nagpur">Nagpur Farm</option>
													<option <?=$editProData->pro_tax == "Dahanu " ? 'selected':''?> value="Dahanu">Dahanu Farm</option>
												</select>
											</div>
											<div class="form-group col-lg-3">
												<label>Select Warehouses</label>
												<select class="form-control m-b" name="wrhouse">
													<option value="">-- Select Warehouses --</option>
													<option value="AsmitaOrganic" <?=$editProData->wrhouse == "AsmitaOrganic" ? 'selected':''?>>AsmitaOrganic</option>
													
												</select>
											</div>
											<div class="form-group col-lg-3">
												<label>Select GST rate</label>
												<select class="form-control m-b" name="gstrate">
													<option value="None">None</option>
													<option <?=$editProData->related_pro == "5" ? 'selected':''?> value="5">5%</option>
													<option <?=$editProData->related_pro == "10" ? 'selected':''?> value="10">10%</option>
													<option <?=$editProData->related_pro == "12" ? 'selected':''?> value="12">12%</option>
													<option <?=$editProData->related_pro == "18" ? 'selected':''?> value="18">18%</option>
													<option <?=$editProData->related_pro == "28" ? 'selected':''?> value="28">28%</option>													<option <?=$editProData->pro_tax == "Dahanu " ? 'selected':''?> value="Dahanu">Dahanu Farm</option>
												</select>
											</div>
											<div class="form-group col-lg-3">
												<label>Date Available</label>
												<div class="form-group">
													<div class="input-group date" id="datetimepicker1">
															<span class="input-group-addon">
																<span class="fa fa-calendar"></span>
															</span>
														<input type="text" value='<?=$editProData->pro_display_date?>' name="dispdate" id="dispdate" class="form-control"/>
													</div>
												</div>
											</div>
										</div>
										<div class="row">	
											<div class="form-group col-lg-2">
												<img src="<?php echo base_url('assets/images/product/'.$editProData->pro_image);?>" width="70" height="70"/>
											</div>
											<div class="form-group col-lg-10">
												<label>Product image </label>
												<input type="file" id="pimg" class="form-control" name="pimg" />
												<input type="hidden" class="form-control" name="pimg_old" value="<?=$editProData->pro_image?>" />
											</div>
										</div>
										
										
										<div class="row">	
											<div class="form-group col-lg-2">
												<div id="img1">
													<?php if(!empty($editProData->pro_image1)){?><img src="<?php echo base_url('assets/images/product/'.$editProData->pro_image1);?>"  width="70" height="70"/><span class="btn btn-danger2 btn-xs m-l-xl" onclick="rmvimg('1')"><i class="fa fa-trash-o"></i></span><?php }?>
												</div>
											</div>
											<div class="form-group col-lg-10">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg1" class="form-control" name="pimg1" >
												<input type="hidden" class="form-control" name="pimg_old1" id="pimg_old1" value="<?=$editProData->pro_image1?>" />
											</div>
										</div>
										
										<div class="row">	
											<div class="form-group col-lg-2">
												<div id="img2">
													<?php if(!empty($editProData->pro_image2)){?><img src="<?php echo base_url('assets/images/product/'.$editProData->pro_image2);?>"  width="70" height="70"/> <span class="btn btn-danger2 btn-xs m-l-xl" onclick="rmvimg('2')"><i class="fa fa-trash-o"></i></span><?php }?>
												</div>
											</div>
											<div class="form-group col-lg-10">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg2" class="form-control" name="pimg2" >
												<input type="hidden" class="form-control" name="pimg_old2" id="pimg_old2" value="<?=$editProData->pro_image2?>" />
											</div>
										</div>
										
										<div class="row">	
											<div class="form-group col-lg-2">
												<div id="img3">
													<?php if(!empty($editProData->pro_image3)){?><img src="<?php echo base_url('assets/images/product/'.$editProData->pro_image3);?>"  width="70" height="70"/><span class="btn btn-danger2 btn-xs m-l-xl" onclick="rmvimg('3')"><i class="fa fa-trash-o"></i></span><?php }?>
												</div>
											</div>
											<div class="form-group col-lg-10">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg3" class="form-control" name="pimg3" >
												<input type="hidden" class="form-control" name="pimg_old3" id="pimg_old3" value="<?=$editProData->pro_image3?>" />
											</div>
										</div>
										
										<div class="row">	
											<div class="form-group col-lg-2">
												<div id="img4">
													<?php if(!empty($editProData->pro_image4)){?><img src="<?php echo base_url('assets/images/product/'.$editProData->pro_image4);?>"  width="70" height="70"/><span class="btn btn-danger2 btn-xs m-l-xl" onclick="rmvimg('4')"><i class="fa fa-trash-o"></i></span><?php }?>
												</div>
											</div>
											<div class="form-group col-lg-10">
												<label>Product image (Optional)</label>
												<input type="file" id="pimg4" class="form-control" name="pimg4" >
												<input type="hidden" class="form-control" name="pimg_old4"  id="pimg_old4" value="<?=$editProData->pro_image4?>" />
											</div>
										</div>					
										
                                    </div>
                                </div>
                            </div>
<script>
function rmvimg(str){
	
	$('#pimg_old'+str).val("");
	$('#img'+str).hide();
}
</script>
                            <!--<div class="text-right m-t-xs">
                                <a class="btn btn-default prev" href="#">Previous</a>
                                <a class="btn btn-default next" href="#">Next</a>
                            </div>-->
							<div class="row">
                                
                                <div class="col-lg-12">
                                    <div class="row">
									<div class="row">
                                        <div class="form-group col-lg-12">
                                            <label>Meta Tag Title</label>
                                            <input type="text" value="<?=$editProData->m_title?>" id="metatagTitle" class="form-control" name="metatagTitle">
                                        </div>
                                    </div>
									<div class="row">	
                                        <div class="form-group col-lg-6">
                                            <label>Meta Tag Description</label>
                                            <textarea rows="3" class="form-control" name="metatagDescription" ><?=$editProData->m_desc?></textarea>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Meta Tag Keywords</label>
                                            <textarea rows="3" class="form-control" name="metatagKeywords" ><?=$editProData->m_keyword?></textarea>
                                        </div>
									</div>
									<div class="row">	
                                        <div class="col-lg-12">
											<label>Product Description</label>
                                            <textarea rows="3" class="form-control" name="productDesc" ><?=$editProData->pro_detail?></textarea>
										</div>
										</div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                            	<?php
                            		$price_tbl = getRowDataMulti("select * from product_price_tbl where pro_id =".$editProData->pro_id);
                            		$i = 0;
                            		$count = count($price_tbl);
                            		if(is_array($price_tbl))
                            		{
                            		foreach($price_tbl as $pt_val):
                            			$i++;
                            	?>
								<div class="form-group">
									<div class="col-lg-2 no-padding">
										<label>Select Unit</label>
										<select class="form-control m-b" name="unit_old[]">
											<option value="">--Select Unit--</option>
											 <option <?=$pt_val['pro_unit'] == "gm" ? 'selected':''?> value="gm">gm</option>
											 <option <?=$pt_val['pro_unit'] == "kg" ? 'selected':''?> value="kg">kg</option>
											 <option <?=$pt_val['pro_unit'] == "ltr" ? 'selected':''?> value="ltr">ltr</option>
											 <option <?=$pt_val['pro_unit'] == "ml" ? 'selected':''?> value="ml">ml</option>
											 <option <?=$pt_val['pro_unit'] == "pcs" ? 'selected':''?> value="pcs">pcs</option>
										</select>
	                                </div>
									<div class="col-lg-2">
									<label>No. Of Unit</label>
										<input type="text" value="<?=$pt_val['pro_no_unit'];?>" placeholder="No. of Unit" class="form-control" name="no_unit_old[]">
	                                </div>
									<div class="col-lg-2">
									<label>MRP Price</label>
										<input type="text" value="<?=$pt_val['pro_mrp_price'];?>"  placeholder="Mrp Price" class="form-control" name="mrp_old[]">
	                                </div>
									<div class="col-lg-2">
									<label>Purchase Price</label>
										<input type="text" value="<?=$pt_val['pro_purchase_price'];?>" placeholder="Purchase Price" class="form-control" name="purchase_old[]">
	                                </div>
									<div class="col-lg-2">
									<label>Selling Price</label>
										<input type="text" value="<?=$pt_val['pro_sale_price'];?>" placeholder="Sell Price" id="sellprice" class="form-control" name="seal_old[]">
	                                </div>
									<div class="col-lg-1">
									<label>Quantity</label>
										<input type="text" value="<?=$pt_val['pro_qty'];?>" placeholder="Qty" id ="qty" class="form-control" name="qty_old[]">
	                                </div>
	                                <input type="hidden" name="pro_price_id[]" value="<?=$pt_val['pro_price_id']?>" />
	                                <?php
	                                	if($i == $count)
	                                	{
	                                ?>
	                                <div class="col-xs-1">
										<button type="button" id="Masters" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
									</div>
									<?php
										}
										else
										{
											?>
										<div class="col-xs-1">
							<button type="button" onclick="remPriceData(<?=$pt_val['pro_price_id']?>,this);" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
						</div>	
											<?php
										}
									?>	
									<div style='clear:both;padding-top:5px;'></div>
								</div>
								<?php
									endforeach;
									}
									else
									{
									?>
								<div class="form-group">
									<div class="col-lg-2 no-padding">
										<select class="form-control m-b" name="unit[]">
											<option value="">--Select Unit--</option>
											 <option value="gm">gm</option>
											 <option value="kg">kg</option>
											 <option value="ltr">ltr</option>
											 <option value="ml">ml</option>
											 <option value="pcs">pcs</option>
										</select>
	                                </div>
									<div class="col-lg-2">
										<input type="text" placeholder="No. of Unit" class="form-control" name="no_unit[]">
	                                </div>
									<div class="col-lg-2">
										<input type="text" placeholder="Mrp Price" class="form-control" name="mrp[]">
	                                </div>
									<div class="col-lg-2">
										<input type="text" placeholder="Purchase Price" class="form-control" name="purchase[]">
	                                </div>
									<div class="col-lg-2">
										<input type="text" placeholder="Sell Price" id="sellprice" class="form-control" name="seal[]">
	                                </div>
									<div class="col-lg-1">
										<input type="text" placeholder="Qty" id ="qty" class="form-control" name="qty[]">
	                                </div>
	                               	<div class="col-xs-1">
										<button type="button" id="Masters" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
									</div>
									<div style='clear:both;padding-top:5px;'></div>
								</div>	
									<?php
									}
								?>
							</div>
                            <div class="text-right m-t-xs">

								<input type="submit" value="Submit" class="btn btn-success submitWizard" />
                            </div>
                        </div>

                        <div id="step2" class="p-m tab-pane">

                            
                            <!--<div class="text-right m-t-xs">
                                <a class="btn btn-default prev" href="#">Previous</a>
                                <a class="btn btn-default next" href="#">Next</a>
                            </div>-->

                        </div>
                        <div id="step3" class="tab-pane">
                            
                        </div>
                        </div>
                        <input type="hidden" name="exist_pro_id" value="<?=$editProData->pro_id?>">
                    </form>
                    <!-- The template for adding new field -->
							
					<div class="form-group hide Masters" >
						<div class="col-lg-2 no-padding">
							<select class="form-control m-b" name="unit[]">
                            	<option value="">--Select Unit--</option>
								 <option value="gm">gm</option>
								 <option value="kg">kg</option>
								 <option value="ltr">ltr</option>
								 <option value="ml">ml</option>
								 <option value="pcs">pcs</option>
							</select>
                        </div>
						<div class="col-lg-2">
							<input type="text" placeholder="No. of Unit" class="form-control" name="no_unit[]">
                        </div>
						<div class="col-lg-2">
							<input type="text" placeholder="Mrp Price" class="form-control" name="mrp[]">
                        </div>
						<div class="col-lg-2">
							<input type="text" placeholder="Purchase Price" class="form-control" name="purchase[]">
                        </div>
						<div class="col-lg-2">
							<input type="text" placeholder="Sell Price" id="sellprice" class="form-control" name="seal[]">
                        </div>
						<div class="col-lg-1">
							<input type="text" placeholder="Qty" id ="qty" class="form-control" name="qty[]">
                        </div>
						<div class="col-xs-1">
							<button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
						</div>
						<div style='clear:both;padding-top:5px;'></div>
					</div>
                </div>
            </div>
        </div>

    </div>
    </div>
	
