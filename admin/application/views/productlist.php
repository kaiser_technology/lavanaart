 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>User Role </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   Store Listing
				   
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
				<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">×</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>	
				  <a class="btn btn-primary " href="<?php echo base_url('product/productAdd');?>"><i class="fa fa-plus"></i> New Product</a>
					
                </div>
                <div class="panel-body">
				
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
							<tr>
								<th>Sr No</th>
								<th>Product Image</th>
								<th>Product Code</th>
								<th>Farm</th>
								<th>Product Name</th>
								<th>Status</th>
								<th>Action </th>
								
							</tr>
                        </thead>
						<tbody>
							<?php $i=1; foreach($list as $val){?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $val['pro_image'];?></td>
								<td><?php echo $val['pro_code'];?></td>
								<td><?php echo $val['pro_tax'];?></td>
								<td><?php echo $val['pro_name'];?></td>
								<td><?php echo $val['pro_status'];?></td>
								<td align="right">
									<a href="<?php echo base_url('product/editProduct/'). $val['pro_id'];?>" class="btn btn-info btn-xs" type="button"><i class="fa fa-paste"></i></a>
									<button class="btn btn-danger2 btn-xs deletstore" data-val="<?php echo $val['pro_id'];?>" type="button"><i class="fa fa-trash-o"></i></button>
									<?php if($val['pro_status'] != "Active"){?>
									<button class="btn btn-danger btn-xs changestatus" data-val="<?php echo $val['pro_id'].'|'.$val['pro_status'];?>" type="button"><i class="fa fa-circle-o"></i></button>
									<?php }else{?>
									<button class="btn btn-success btn-xs  changestatus" data-val="<?php echo $val['pro_id'].'|'.$val['pro_status'];?>" type="button"><i class="fa fa-dot-circle-o"></i></button>
									<?php }?>
								</td>
							</tr>
							<?php $i++; }?>		
						</tbody>
						
                    </table>
<div class="pull-right"><?php echo $links; ?></div>
                </div>
            </div>
            </div>
        </div>
    </div>

   