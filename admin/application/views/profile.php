 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>Profile </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    Profile
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Profile information and statistics
                    </div>
					<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">×</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>
                    <div class="panel-body">
                        <div class="row">
						<?php
							$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
									);?>
                        <form action="" method="post" role="form" id="profileForm">
						<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                        <div class="form-group"><label>Name</label> <input type="text" placeholder="Enter Name" Name="name" id="name" class="form-control" value="<?php echo $result[0]['contact_person'];?>" ></div>
						<div class="form-group"><label>Email</label> <input type="text" placeholder="Enter Email" name="email" id="email" readonly class="form-control" value="<?php echo $result[0]['contact_email'];?>" ></div>
						<div class="form-group"><label>Contact Number</label> <input type="text" placeholder="Enter Number" id="number" name="number" class="form-control" value="<?php echo $result[0]['contact_number'];?>" required></div>
                    
                        <div>
						<input type="submit" class="btn btn-sm btn-primary m-t-n-xs" name = "update" value="Update"/>
                           
                        </div>
                    </form>
                              
                        </div>
                    </div>
                    <div class="panel-footer">
                    <span class="pull-right">
                          {elapsed_time}
                    </span>
					Page rendered in
                       <!-- Last update: 21.05.2015-->
                    </div>
                </div>
            </div>
        </div>
    </div>

   