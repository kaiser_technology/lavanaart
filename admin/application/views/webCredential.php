 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li ><a href="<?php echo ADMIN_URL."store/storeList";?>">Store</a></li>
                             
						<li class="active">
                            <span> Web Credential </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   Web Credential
				   
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
	  	<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
				<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">×</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>	
				<h4> Add  Web Credential</h4>
					
                </div>
                <div class="panel-body">
				
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>store/addwebCredential" enctype="multipart/form-data" id="apiForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
                                <label class="control-label" for="category_name"><small style="color:red">*</small> Select Store</label>
								<select name="storekey" id="storekey" class="form-control m-b">
								<option value="">Select</option>
									<?php foreach($storelist as $str){?>
									<option value="<?php echo $str['store_key']; ?>"><?php echo $str['store_name'];?></option>
									<?php }?>
								</select>
                                <?php echo form_error('storekey', '<span class="help-block small">', '</span>'); ?>
                            </div>	
							 <button type="submit" class="btn btn-primary ">Submit</button>
						</form>
                </div>
            </div>
            </div>
        </div>
		
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
				<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">×</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>	
				<h4> Category List</h4>
					
                </div>
                <div class="panel-body">
				
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
							<tr>
								<th>Sr No</th>
								<th>Store Name</th>
								<th>Api Key</th>
								<th>Sicret Key</th>
								<th>Action </th>
							</tr>
                        </thead>
						<tbody>
							<?php 
							if(!empty($list)){
							$i=1; foreach($list as $val){?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $val['store_key'];?></td>
								<td><?php echo $val['api_key'];?></td>
								<td><?php echo $val['secret_key'];?></td>
								<td align="right">
									<a href="<?php echo base_url('store/StoreCategoryList/'). $val['id'];?>" class="btn btn-info btn-xs" type="button"><i class="fa fa-paste"></i></a>
									<button class="btn btn-danger2 btn-xs deletstore" data-val="<?php echo $val['id'];?>" type="button"><i class="fa fa-trash-o"></i></button>
									<?php if($val['status'] == "Inactive"){?>
									<button class="btn btn-danger btn-xs changestatus" data-val="<?php echo $val['id'].'|'.$val['status'];?>" type="button"><i class="fa fa-circle-o"></i></button>
									<?php }else{?>
									<button class="btn btn-success btn-xs  changestatus" data-val="<?php echo $val['id'].'|'.$val['status'];?>" type="button"><i class="fa fa-dot-circle-o"></i></button>
									<?php }?>
								</td>
							</tr>
							<?php $i++; }
							}?>		
						</tbody>
						
                    </table>
<div class="pull-right"><?php echo $links; ?></div>
                </div>
            </div>
            </div>
        </div>
    </div>

   