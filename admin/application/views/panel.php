    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12 text-center m-t-md">
                <h2>
                    Welcome to Lavana Art Admin Panel
                </h2>

                <p>
                    
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="hpanel">
                   <a href="<?php echo ADMIN_URL.'product/productlist';?>"> <div class="panel-body text-center h-200">
                        <i class="pe-7s-gift fa-4x"></i>

                        <h1 class="m-xs"><?php echo $this->pro->totalProduct($session_data['store_key']);?></h1>

                        <h3 class="font-extra-bold no-margins text-success">
                            Total Product(s)
                        </h3>
                        <small></small>
                    </div></a>
                    <div class="panel-footer">
                        products
                    </div>
                </div>
            </div>
			<div class="col-lg-4">
                <div class="hpanel">
                   
					<div class="panel-body text-center h-200">
                        <i class="pe-7s-menu fa-4x"></i>

                        <h1 class="m-xs"><?php echo $this->pro->totalVisitor($session_data['store_key']);?></h1>

                        <h3 class="font-extra-bold no-margins text-success">
                           Total Visitor(s)
                        </h3>
                        <small></small>
                    </div>
                    <div class="panel-footer">
                        Visitor
                    </div>
                </div>
            </div>
			<div class="col-lg-4">
                <div class="hpanel">
				<a href="<?php echo ADMIN_URL.'/SiteUsers';?>">
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-display2 fa-4x"></i>

                        <h1 class="m-xs"><?php echo $this->pro->totalUser($session_data['store_key']);?></h1>

                        <h3 class="font-extra-bold no-margins text-success">
                            Total User(s)
                        </h3>
                        <small></small>
                    </div>
                    <div class="panel-footer">
                        Users
                    </div>
                </div>
            </div>
			
        </div>
		 <div class="row">
            
			<div class="col-lg-4">
                <div class="hpanel">
                    <a href="#newOrder">
					<div class="panel-body text-center h-200">
                        <i class="pe-7s-menu fa-4x"></i>

                        <h1 class="m-xs"><?php echo $this->pro->neword($session_data['store_key']);?></h1>

                        <h3 class="font-extra-bold no-margins text-success">
                            New Order(s)
                        </h3>
                        <small></small>
                    </div></a>
                    <div class="panel-footer">
                        category
                    </div>
                </div>
            </div>
			<div class="col-lg-4">
                <div class="hpanel">
				<a href="<?php echo ADMIN_URL.'/order/pendingOrder/';?>">
                    <div class="panel-body text-center h-200">
                        <i class="pe-7s-display2 fa-4x"></i>

                        <h1 class="m-xs"><?php echo $this->pro->pendingord($session_data['store_key']);?></h1>

                        <h3 class="font-extra-bold no-margins text-success">
                            Total Pending Order(s)
                        </h3>
                        <small></small>
                    </div></a>
                    <div class="panel-footer">
                        Pending Order
                    </div>
                </div>
            </div>
			<div class="col-lg-4">
                <div class="hpanel">
                   <a href="<?php echo ADMIN_URL.'/order/complateOrder/';?>"> <div class="panel-body text-center h-200">
                        <i class="pe-7s-gift fa-4x"></i>

                        <h1 class="m-xs"><?php echo $this->pro->complateord($session_data['store_key']);?></h1>

                        <h3 class="font-extra-bold no-margins text-success">
                            Total complete order(s)
                        </h3>
                        <small></small>
                    </div></a>
                    <div class="panel-footer">
                        complete order 
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				 <div class="hpanel" id="newOrder">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        New Order
                    </div>
                    <div class="panel-body list">
                        
						<div class="table-responsive project-list">
				
                    <table id="example1" class="table table-striped" >
                        <thead>
							<tr>
								<th>Sr No</th>		
								<th>Order Code</th>
								<th>Quantity </th>
								<th>Total </th>
								<th>Order Date </th>
								<th>Action </th>
								
							</tr>
                        </thead>
						<tbody>
							<?php
							$ordlist = array();
							if(isset($list) && !empty($list)){
								$i=1; foreach($list as $val){
									
									if(in_array($val['ord_id'] , $ordlist))
									{
											continue;
									}
									else{
										$ordlist[] = $val['ord_id'];
									}
										$oid= urlencode(base64_encode(gzencode($val['ord_id'])));
									?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $val['ord_code'];?></td>
								<td><?php echo $val['ord_pro_qty'];?> </td>
								<td><?php echo $val['total_pro_amount'];?></td>
								<td><?php echo $val['ord_det_dtadded'];?></td>
								
								   <td align="right">
									<a href="<?php echo base_url('invoice/index/'). $oid;?>" class="btn btn-info btn-xs" type="button"><i class="fa fa-paste"></i></a>
									<button class="btn btn-warning btn-xs deletstore" title = "cancle order" data-val="<?php echo $val['ord_id'];?>" type="button"><i class="fa fa-trash-o"></i></button>
								
									<button class="btn btn-danger btn-xs changestatus" title = "Confirm"  data-val="<?php echo $val['ord_id'].'|'.$val['ord_status'];?>" type="button"><i class="fa fa-circle-o"></i></button>
									
								</td>
							  
							</tr>
							<?php $i++; } 
							}
							?>		
						</tbody>
						
                    </table>
<div class="pull-right"><?php echo $links; ?></div>
						</div>
					</div>
				</div>
			</div>
        </div>
		<!--<div class="row">
            <div class="col-lg-3">
                <div class="hpanel stats">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Last active
                    </div>
                    <div class="panel-body list">
                        <div class="stats-title pull-left">
                            <h4>Activity</h4>
                        </div>
                        <div class="stats-icon pull-right">
                            <i class="pe-7s-science fa-4x"></i>
                        </div>
                        <div class="m-t-xl">
                            <span class="font-bold no-margins">
                                Social users
                            </span>
                            <br/>
                            <small>
                                Lorem Ipsum is simply dummy text of the printing simply all dummy text. Lorem Ipsum is
                                simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.
                            </small>
                        </div>
                        <div class="row m-t-md">
                            <div class="col-lg-6">
                                <h3 class="no-margins font-extra-bold text-success">300,102</h3>

                                <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                            </div>
                            <div class="col-lg-6">
                                <h3 class="no-margins font-extra-bold text-success">280,200</h3>

                                <div class="font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                            </div>
                        </div>
                        <div class="row m-t-md">
                            <div class="col-lg-6">
                                <h3 class="no-margins font-extra-bold ">120,108</h3>

                                <div class="font-bold">38% <i class="fa fa-level-down"></i></div>
                            </div>
                            <div class="col-lg-6">
                                <h3 class="no-margins font-extra-bold ">450,600</h3>

                                <div class="font-bold">28% <i class="fa fa-level-down"></i></div>
                            </div>

                        </div>
                    </div>
                    <div class="panel-footer">
                        This is standard panel footer
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Recently active projects
                    </div>
                    <div class="panel-body list">
                        <div class="table-responsive project-list">
                            <table class="table table-striped">
                                <thead>
                                <tr>

                                    <th colspan="2">Project</th>
                                    <th>Completed</th>
                                    <th>Task</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input type="checkbox" class="i-checks" checked></td>
                                    <td>Contract with Zender Company
                                        <br/>
                                        <small><i class="fa fa-clock-o"></i> Created 14.08.2015</small>
                                    </td>
                                    <td>
                                        <span class="pie">1/5</span>
                                    </td>
                                    <td><strong>20%</strong></td>
                                    <td>Jul 14, 2013</td>
                                    <td><a href=""><i class="fa fa-check text-success"></i></a></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="i-checks"></td>
                                    <td>There are many variations of passages
                                        <br/>
                                        <small><i class="fa fa-clock-o"></i> Created 21.07.2015</small>
                                    </td>
                                    <td>
                                        <span class="pie">1/4</span>
                                    </td>
                                    <td><strong>40%</strong></td>
                                    <td>Jul 16, 2013</td>
                                    <td><a href=""><i class="fa fa-check text-navy"></i></a></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="i-checks" checked></td>
                                    <td>Contrary to popular belief
                                        <br/>
                                        <small><i class="fa fa-clock-o"></i> Created 12.06.2015</small>
                                    </td>
                                    <td>
                                        <span class="pie">0.52/1.561</span>
                                    </td>
                                    <td><strong>75%</strong></td>
                                    <td>Jul 18, 2013</td>
                                    <td><a href=""><i class="fa fa-check text-navy"></i></a></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="i-checks"></td>
                                    <td>Gamma project
                                        <br/>
                                        <small><i class="fa fa-clock-o"></i> Created 06.03.2015</small>
                                    </td>
                                    <td>
                                        <span class="pie">226/360</span>
                                    </td>
                                    <td><strong>16%</strong></td>
                                    <td>Jul 22, 2013</td>
                                    <td><a href=""><i class="fa fa-check text-navy"></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Activity
                    </div>
                    <div class="panel-body list">

                        <div class="pull-right">
                            <a href="#" class="btn btn-xs btn-default">Today</a>
                            <a href="#" class="btn btn-xs btn-default">Month</a>
                        </div>
                        <div class="panel-title">Last Activity</div>
                        <small class="fo">This is simple example</small>
                        <div class="list-item-container">
                            <div class="list-item">
                                <h3 class="no-margins font-extra-bold text-success">2,773</h3>
                                <small>Tota Messages Sent</small>
                                <div class="pull-right font-bold">98% <i class="fa fa-level-up text-success"></i></div>
                            </div>
                            <div class="list-item">
                                <h3 class="no-margins font-extra-bold text-color3">4,422</h3>
                                <small>Last activity</small>
                                <div class="pull-right font-bold">13% <i class="fa fa-level-down text-color3"></i></div>
                            </div>
                            <div class="list-item">
                                <h3 class="no-margins font-extra-bold text-color3">9,180</h3>
                                <small>Monthly income</small>
                                <div class="pull-right font-bold">22% <i class="fa fa-bolt text-color3"></i></div>
                            </div>
                            <div class="list-item">
                                <h3 class="no-margins font-extra-bold text-success">1,450</h3>
                                <small>Tota Messages Sent</small>
                                <div class="pull-right font-bold">44% <i class="fa fa-level-up text-success"></i></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>-->
    </div>
