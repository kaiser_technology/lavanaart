 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>User Role </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   SiteUser Listing
				   
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		<?php
			//dump($list,1);
		?>
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-body">
				
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
							<tr>
								<th>Sr No</th>
								<th>Name</th>
								<th>Email</th>
								<th>Contact</th>
								<th>Address</th>
								<?php
								if($this->session_data['rolid'] == 1)
								{
								?>
								<th>Store Name</th>
								<?php
								}
								?>
							</tr>
                        </thead>
						<tbody>
							<?php $i=1; foreach($list as $val){?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $val['user_prefix']." ".$val['user_firstName']." ".$val['user_lastName'];?></td>
								<td><?php echo $val['user_email'];?></td>
								<td><?php echo $val['user_contact'];?></td>
								<td><?php echo $val['user_address'].", ".$val['city'].", PinCode: ".$val['user_postalCode'];?></td>
								<?php
								if($this->session_data['rolid'] == 1)
								{
								?>
								<td><?php echo $val['store_name'];?></td>
								<?php
								}
								?>
							</tr>
							<?php $i++; }?>		
						</tbody>
						
                    </table>
<div class="pull-right"><?php echo $links; ?></div>
                </div>
            </div>
            </div>
        </div>
    </div>

   