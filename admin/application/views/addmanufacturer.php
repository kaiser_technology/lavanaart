 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        <li><a href="<?php echo ADMIN_URL."product";?>">Product</a></li>
                        
                        <li class="active">
                            <span>New Manufacturer </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   New Manufacturer
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
					
                  <small style="color:red">*</small> field is Mendatory
                </div>
               <div class="panel-body">
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>product/addManufacturer" enctype="multipart/form-data" id="prodcatForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
                                <label class="control-label" for="m_name"><small style="color:red">*</small> Manufacturer Name</label>
                                <input type="text" placeholder="Manufacturer Name" title="Manufacturer Name"  value="<?php echo set_value('m_name'); ?>" name="m_name" id="m_name" class="form-control">
                                <?php echo form_error('m_name', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 
							<div class="form-group">
                                <label class="control-label" for="img"><small style="color:red">*</small>Manufacturer Image</label>
                               <input type="file"  class="form-control" name="img" id="img" required >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
                            </div>							
                            <!--<div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div>-->
                            <button type="submit" class="btn btn-primary ">Submit</button>
                           <!-- <a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>

   