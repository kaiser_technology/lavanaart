 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>New Store </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   New Store
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
					
                  <small style="color:red">*</small> field is Mendatory
                </div>
               <div class="panel-body">
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>store/insertstore" enctype="multipart/form-data" id="storeForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
                                <label class="control-label" for="storename"><small style="color:red">*</small> Store Name</label>
                                <input type="text" placeholder="Store Name" title="Store Name"  value="<?php echo set_value('storename'); ?>" name="storename" id="storename" class="form-control">
                                <?php echo form_error('storename', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 <div class="form-group">
                                <label class="control-label" for="storelogo"><small style="color:red">*</small>Store Logo</label>
                                <input type="file" placeholder="Store Logo" title="Store Logo"  value="<?php echo set_value('storelogo'); ?>" name="storelogo" id="storelogo" class="form-control">
                                <?php echo form_error('storelogo', '<span class="help-block small">', '</span>'); ?>
                            </div>
							<div class="form-group">
                                <label class="control-label" for="storelogo"><small style="color:red">*</small>Store Category</label>
                                <select class="form-control m-b" name="storecategory" id="storecategory">
								<option value="">--Select Category --</option>
									<?php foreach($catlist as $cat) {?>
									<option value="<?php echo $cat['id'];?>"><?php echo $cat['category_name'];?></option>
									<?php };?>
								</select>
                            </div>
							 <div class="form-group">
                                <label class="control-label" for="storeaddress"><small style="color:red">*</small>Store Address</label>
                                <input type="text" placeholder="store Address" title="Store Address"  value="<?php echo set_value('storeaddress'); ?>" name="storeaddress" id="storeaddress" class="form-control">
                                <?php echo form_error('storeaddress', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 <div class="form-group">
                                <label class="control-label" for="storecity">Store City</label>
                                <input type="text" placeholder="Store City" title="Store City"  value="Mumbai" name="storecity" id="storecity" readonly="true" class="form-control">
                                <?php echo form_error('storecity', '<span class="help-block small">', '</span>'); ?>
                            </div>
							 <div class="form-group">
                                <label class="control-label" for="storezip"><small style="color:red">*</small>Store Zip</label>
                                <input type="text" placeholder="Store Zip" title="Store Zip"  value="<?php echo set_value('storezip'); ?>" name="storezip" id="storezip" class="form-control">
                                <?php echo form_error('storezip', '<span class="help-block small">', '</span>'); ?>
                            </div>
							<div class="form-group">
                                <label class="control-label" for="contactper"><small style="color:red">*</small>Contact Persion</label>
                                <input type="text" placeholder="Contact Persion" title="Contact Persion"  value="<?php echo set_value('contactper'); ?>" name="contactper" id="contactper" class="form-control">
                                <?php echo form_error('contactper', '<span class="help-block small">', '</span>'); ?>
                            </div>
							<div class="form-group">
                                <label class="control-label" for="contactemail"><small style="color:red">*</small>Contact Email</label>
                                <input type="text" placeholder="Contact Email" title="Contact Email"  value="<?php echo set_value('contactemail'); ?>" name="contactemail" id="contactemail" class="form-control">
                                <?php echo form_error('contactemail', '<span class="help-block small">', '</span>'); ?>
                            </div>
							<div class="form-group">
                                <label class="control-label" for="contactnum"><small style="color:red">*</small>Contact Number</label>
                                <input type="text" placeholder="Contact Number" title="Contact Number"  value="<?php echo set_value('contactnum'); ?>" name="contactnum" id="contactnum" class="form-control">
                                <?php echo form_error('contactnum', '<span class="help-block small">', '</span>'); ?>
                            </div>
                            <!--<div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div>-->
                            <button type="submit" class="btn btn-primary ">Submit</button>
                           <!-- <a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>

   