 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                      
                        <li class="active">
                            <span>New Blog </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   New Blog
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
					
                  <small style="color:red">*</small> field is Mendatory
                </div>
               <div class="panel-body">
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>blog/add" enctype="multipart/form-data" id="prodcatForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            
							<div class="form-group">
                                <label class="control-label" for="blog_name"><small style="color:red">*</small> Blog Name</label>
                                <input type="text" placeholder="Blog Name" title="Blog Name"  value="<?php echo set_value('blog_name'); ?>" name="blog_name" id="blog_name" class="form-control">
                            </div>
								<div class="form-group">
                                <label class="control-label" for="short_name"><small style="color:red">*</small> Short Name</label>
                                <input type="text" placeholder="Short Name" title="Short Name"  value="<?php echo set_value('short_name'); ?>" name="short_name" id="short_name" class="form-control">
                            </div>							
							<div class="form-group">
                                <label class="control-label" for="img"><small style="color:red">*</small>Blog Image</label>
                               <input type="file"  class="form-control" name="img" id="img" required >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}.jpg"</small>
                            </div>	
<div class="form-group">
                                <label class="control-label" for="img1"><small style="color:red">*</small>Blog Image1</label>
                               <input type="file"  class="form-control" name="img1" id="img1"  >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}1.jpg"</small>
                            </div>	
<div class="form-group">
                                <label class="control-label" for="img2"><small style="color:red">*</small>Blog Image2</label>
                               <input type="file"  class="form-control" name="img2" id="img2"  >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}2.jpg"</small>
                            </div>	
<div class="form-group">
                                <label class="control-label" for="img3"><small style="color:red">*</small>Blog Image3</label>
                               <input type="file"  class="form-control" name="img3" id="img3"  >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}3.jpg"</small>
                            </div>	
<div class="form-group">
                                <label class="control-label" for="img4"><small style="color:red">*</small>Blog Image4</label>
                               <input type="file"  class="form-control" name="img4" id="img4"  >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}4.jpg"</small>
                            </div>	
<div class="form-group">
                                <label class="control-label" for="img5"><small style="color:red">*</small>Blog Image5</label>
                               <input type="file"  class="form-control" name="img5" id="img5"  >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}5.jpg"</small>
                            </div>	
<div class="form-group">
                                <label class="control-label" for="img6"><small style="color:red">*</small>Blog Image6</label>
                               <input type="file"  class="form-control" name="img6" id="img6"  >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}6.jpg"</small>
                            </div>	
<div class="form-group">
                                <label class="control-label" for="img7"><small style="color:red">*</small>Blog Image7</label>
                               <input type="file"  class="form-control" name="img7" id="img7"  >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
								<small>Image Path: "https://www.asmitaorganicfarm.com/admin/assets/images/blog/{shortname}7.jpg"</small>
                            </div>								
                            <div class="form-group">
                                <label class="control-label" for="sortdesc"> Short Description</label>
				<textarea placeholder="Short Description"  name="sortdesc"  id="sortdesc" class="form-control"><?php echo set_value('sortdesc'); ?></textarea>
						

                            </div>
							<div class="form-group">
                                <label class="control-label" for="sortdesc"> Description</label>
								<textarea cols="80" id="editor1" name="editor1" rows="10"></textarea>

                            </div>
                            <button type="submit" class="btn btn-primary ">Submit</button>
                           <!-- <a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>
	<script src="https://cdn.ckeditor.com/4.11.1/standard-all/ckeditor.js"></script>

<script>
    CKEDITOR.replace('editor1', {
      language: 'es'
    });
  </script>