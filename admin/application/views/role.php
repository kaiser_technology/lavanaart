 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>User Role </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    User Role Information
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
				<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">×</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>	
				<h4> Add Role</h4>
					
                </div>
                <div class="panel-body">
				
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							

					?>


                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>role/addRole" enctype="multipart/form-data" id="storeForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
                                <label class="control-label" for="role_name"><small style="color:red">*</small> Role Name</label>
                                <input type="text" placeholder="Role Name" title="Role Name"  value="" name="role_name" id="role_name" class="form-control">
                                <?php echo form_error('role_name', '<span class="help-block small">', '</span>'); ?>
                            </div>	
							 <button type="submit" class="btn btn-primary ">Submit</button>
						</form>
                </div>
            </div>
            </div>
        </div>
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Role information and statistics
                    </div>
                    <div class="panel-body">
                        <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
							<tr>
								<th>Sr No</th>
								<th>Role Name</th>
							</tr>
                        </thead>
						<tbody>
							<?php $i=1; foreach($list as $val){?>
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $val['role_name'];?></td>
							</tr>
							<?php $i++; }?>		
						</tbody>
						
                    </table>
                    </div>
                    <div class="panel-footer">
                    <span class="pull-right">
                         Page rendered in 
                    </span>
                        {elapsed_time}
                    </div>
                </div>
            </div>
        </div>
    </div>

   