<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Sayonara</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

   <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo $this->config->item('VENDOR_JS_URL');?>fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo $this->config->item('VENDOR_JS_URL');?>metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo $this->config->item('VENDOR_JS_URL');?>animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo $this->config->item('VENDOR_JS_URL');?>bootstrap/dist/css/bootstrap.css" />
    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/pe-icon-7-stroke/css/helper.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/style.css');?>">

</head>
<body class="blank">

<!-- Simple splash screen-->
<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Lavana Art</h1><p>Admin Portal </p><div class="spinner"> <div class="rect1"></div><div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div>
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="color-line"></div>

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
				<img src="<?php echo ADMIN_IMAGES_URL.'store/NVDS1105';?>.jpg" alt="logo" width="200px">
                <!--<h3>Lavana Art</h3>-->
                <h3><small>Please Login To Admin Panel</small></h3>
            </div>
            <div class="hpanel">
                <div class="panel-body">
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>login" id="loginForm" method="post">
						<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input type="text" placeholder="example@domain.com" title="Please enter you username"  value="<?php echo set_value('username'); ?>" name="username" id="username" class="form-control">
                                <?php echo form_error('username', '<span class="help-block small">', '</span>'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******"  value="" name="password" id="password" class="form-control">
                                <?php echo form_error('password', '<span class="help-block small">', '</span>'); ?>
                            </div>
                            <!--<div class="checkbox">
                                <input type="checkbox" class="i-checks" checked>
                                     Remember login
                                <p class="help-block small">(if this is a private computer)</p>
                            </div>-->
                            <button type="submit" class="btn btn-success btn-block">Login</button>
                           <!-- <a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
        </div>
    </div>
    
</div>


<!-- Vendor scripts -->
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>jquery/dist/jquery.min.js"></script>
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>metisMenu/dist/metisMenu.min.js"></script>
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>iCheck/icheck.min.js"></script>
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>sparkline/index.js"></script>
<script src="<?php echo $this->config->item('VENDOR_JS_URL');?>jquery-validation/jquery.validate.min.js"></script>

<!-- App scripts -->
<script src="<?php echo base_url('assets/scripts/homer.js');?>"></script>
<script src="<?php echo $this->config->item('PAGE_JS_URL');?>Login.js"></script>
</body>
</html>