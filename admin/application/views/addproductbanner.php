 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        <li><a href="<?php echo ADMIN_URL."product";?>">Product</a></li>
                        
                        <li class="active">
                            <span>New Banner </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   New Banner
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
					
                  <small style="color:red">*</small> field is Mendatory
                </div>
               <div class="panel-body">
				<?php
					$csrf = array(
									'name' => $this->security->get_csrf_token_name(),
									'hash' => $this->security->get_csrf_hash()
							);	
							
						
					?>
                        <form role="form" action="<?php echo $this->config->item('ADMIN_URL')?>product/addProductBanner" enctype="multipart/form-data" id="prodcatForm" method="post">
							<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            
							<div class="form-group">
                                <label class="control-label" for="main_title"><small style="color:red">*</small> Main Title</label>
                                <input type="text" placeholder="Main Title" title="Main Title"  value="<?php echo set_value('main_title'); ?>" name="main_title" id="main_title" class="form-control">
                            </div> 
							<div class="form-group">
                                <label class="control-label" for="img"><small style="color:red">*</small>Banner Image</label>
                               <input type="file"  class="form-control" name="img" id="img" required >
							    <?php echo form_error('img', '<span class="help-block small">', '</span>'); ?>
                            </div>							
                            <div class="form-group">
                                <label class="control-label" for="bindex"> Banner Index</label>
                                <input type="text" placeholder="Banner Index" title="Banner Index"  value="<?php echo set_value('bindex'); ?>" name="bindex" id="bindex" class="form-control">
                            </div>
							<div class="form-group">
                                <label class="control-label" for="sort_title"> Sort Title</label>
                                <input type="text" placeholder="Sort Title" title="Sort Title"  value="<?php echo set_value('sort_title'); ?>" name="sort_title" id="sort_title" class="form-control">
                            </div>							
							<div class="form-group">
                                <label class="control-label" for="sale"> Sale</label>
                                <input type="text" placeholder="Sale" title="Sale"  value="<?php echo set_value('sale'); ?>" name="sale" id="sale" class="form-control">
                            </div>
							
                            <button type="submit" class="btn btn-primary ">Submit</button>
                           <!-- <a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>

   