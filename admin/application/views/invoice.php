 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>Invoice </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   Invoice 
				   
                </h2>
                <a><small onclick="printDiv('printableArea')">Print </small></a>
            </div>
        </div>
    </div>
<div class="content animate-panel">

    <div class="row" id="printableArea">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Invoice <small><?php echo $data[0]['ord_code'];?></small></h4>
                        </div>
                        <div class="col-md-6">
                           <!-- <div class="text-right">
                                <button class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit </button>
                                <button class="btn btn-default btn-sm"><i class="fa fa-check "></i> Save </button>
                                <button class="btn btn-primary btn-sm"><i class="fa fa-dollar"></i> Make A Payment</button>
                            </div>-->

                        </div>
                    </div>
                </div>
                <div class="panel-body p-xl">
                    <div class="row m-b-xl">
                        <div class="col-sm-6">
                            <h4><?php echo $data[0]['ord_code'];?></h4>
							<?php 
							$sky = $session_data['store_key'];
								$sid = $data[0]['ship_id'];
								$sdata = $this->pro->shipingAdr($sid);
								$cdata = $this->pro->storInfo($sky);
							?>
                            <address>
                                <strong><?php echo $sdata[0]['fname'].' '.$sdata[0]['lname'];?></strong><br>
                                <?php echo $sdata[0]['ship_straddress'];?><br>
                                <?php echo $sdata[0]['ship_address'];?><br>
                                <?php echo $sdata[0]['city_name'].', '.$sdata[0]['state_name'].'-'.$sdata[0]['ship_postalCode'];?><br>
                                <abbr title="Phone">P:</abbr> <?php echo $sdata[0]['ship_contact'];?>
                            </address>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span>To:</span>
                            <address>
                                <strong><?php echo $cdata[0]['store_name'];?></strong><br>
                                <?php echo $cdata[0]['store_address'];?><br>
                               <?php echo $cdata[0]['store_city'].'-'.$cdata[0]['store_zipcode'];?><br>
                                <abbr title="Phone">P:</abbr> <?php echo $cdata[0]['contact_number'];?>
                            </address>
                            <p>
                                <span><strong>Invoice Date:</strong><?php echo  date('F d Y', strtotime($data[0]['ord_date']));;?></span><br/>
                                
                            </p>
                        </div>
                    </div>

                    <div class="table-responsive m-t">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th style="width: 75%;">Item List</th>
                                <th >Quantity</th>
                                <th>Unit Price</th>
                                <th>Total Price</th>
                            </tr>
                            </thead>
                            <tbody>
							<?php foreach($product  as $val){
								setlocale(LC_MONETARY, 'en_IN');
 
								?>
                            <tr>
                                <td>
                                    <div><strong><?php echo $val['pro_name'];?></strong></div>
                                    <!--<small>
                                    </small>-->
                                </td>
                                <td><?php echo $val['ord_pro_qty'];?></td>
                                <td><?php echo $val['pro_price'];?></td>
                                
                                <td align="right"><i class="fa fa-inr"></i> <?php echo money_format('%!i', $val['ord_pro_qty'] * $val['pro_price']);?></td>
                            </tr>
							<?php }?>
                           

                            </tbody>
                        </table>
                    </div>
                    <div class="row m-t">
                        <div class="col-md-4 col-md-offset-8">
                            <table class="table table-striped text-right">
                                <tbody>
                                <tr>
                                    <td><strong>Sub Total :</strong></td>
                                    <td><i class="fa fa-inr"></i> <?php echo money_format('%!i', $data[0]['total_amount']);?></td>
                                </tr>
                                <tr>
                                    <td><strong>TAX :</strong></td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <td><strong>TOTAL :</strong></td>
                                    <td><i class="fa fa-inr"></i> <?php echo money_format('%!i', $data[0]['total_amount']);?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <!--<div class="m-t"><strong>Comments</strong>
                                It is a long established fact that a reader will be distracted by the readable content of a page
                                when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less
                            </div>-->
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


</div>

<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>