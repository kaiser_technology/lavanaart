 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>User Role </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   <?php if($this->uri->segment(2) == "complateOrder")
				   {
						echo "Complate Order";
				   }else{
					   echo "Pending Order";
				   }
				   ?>
				   
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
				<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">�</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>	
					<!--<a class="btn btn-primary " href="#"><i class="fa fa-key"></i> My Orders</a>-->
                </div>
                <div class="panel-body">
				
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
							<tr>
								<th>Sr No</th>								
								<th>Product Name</th>
								<th>Order Code</th>
								<th>Quantity </th>
								<th>Total </th>
								<th>Order Date </th>
								<th>Action </th>
								
							</tr>
                        </thead>
						<tbody>
							<?php
							$ordlist = array();
							if(isset($list) && !empty($list)){
								$i=1; foreach($list as $val){
									
									if(in_array($val['ord_id'] , $ordlist))
									{
											continue;
									}
									else{
										$ordlist[] = $val['ord_id'];
									}
										
									?>
							<tr>
								<td><?php echo $i;?></td>
								<th><?php echo $val['pro_name'];?></th>
								<th><?php echo $val['ord_code'];?></th>
								<th><?php echo $val['ord_pro_qty'];?> </th>
								<th><?php echo $val['total_pro_amount'];?></th>
								<th><?php echo $val['ord_det_dtadded'];?></th>
								<?php 
								$oid= urlencode(base64_encode(gzencode($val['ord_id'])));
								if($this->uri->segment(2) == "complateOrder")
							   {?>
								<td align="right">
									<a href="<?php echo base_url('invoice/index/'). $oid;?>" class="btn btn-info btn-xs" type="button"><i class="fa fa-paste"></i></a>
									
								</td>
							<?php   }else{ 
							
							?>
								   <td align="right">
									<a href="<?php echo base_url('invoice/index/'). $oid;?>" class="btn btn-info btn-xs" type="button"><i class="fa fa-paste"></i></a>
									<button class="btn btn-danger2 btn-xs deletstore" data-val="<?php echo $val['ord_id'];?>" type="button"><i class="fa fa-trash-o"></i></button>
									<?php if($val['ord_status'] == "pending"){?>
									<button class="btn btn-danger btn-xs changestatus" data-val="<?php echo $val['ord_id'].'|'.$val['ord_status'];?>" type="button"><i class="fa fa-circle-o"></i></button>
									<?php }else{?>
									<button class="btn btn-success btn-xs  changestatus" data-val="<?php echo $val['ord_id'].'|'.$val['ord_status'];?>" type="button"><i class="fa fa-dot-circle-o"></i></button>
									<?php }?>
								</td>
							   <?php }
							   ?>
								
							</tr>
							<?php $i++; } 
							}
							?>		
						</tbody>
						
                    </table>
<div class="pull-right"><?php echo $links; ?></div>
                </div>
            </div>
            </div>
        </div>
    </div>

   