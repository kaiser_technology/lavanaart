
	 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                        <li class="active">
                            <span>Product </span>
                        </li>
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                    User Role Information
                </h2>
                <small> </small>
            </div>
        </div>
    </div>
     <div class="content animate-panel">
      
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Role information and statistics
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?php echo $output; ?>                               
                        </div>
                    </div>
                    <div class="panel-footer">
                    <span class="pull-right">
                         Page rendered in 
                    </span>
                        {elapsed_time}
                    </div>
                </div>
            </div>
        </div>
    </div>