 <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/fontawesome/css/font-awesome.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/metisMenu/dist/metisMenu.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/animate.css/animate.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/bootstrap/dist/css/bootstrap.css');?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/vendor/sweetalert/lib/sweet-alert.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/toastr/build/toastr.min.css');?>" />
    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/pe-icon-7-stroke/css/helper.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/style.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/dev.css');?>">
	<script src="<?php echo base_url('assets/vendor/jquery/dist/jquery.min.js');?>"></script>

