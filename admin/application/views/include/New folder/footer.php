 <!-- Footer-->
    <footer class="footer">
        <span class="pull-right">
            Example text
        </span>
        Company 2015-2020
    </footer>

</div>
<!--wraper end-->
<!-- Vendor scripts -->
<script src="<?php echo base_url('assets/vendor/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery-ui/jquery-ui.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/slimScroll/jquery.slimscroll.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery-flot/jquery.flot.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery-flot/jquery.flot.resize.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery-flot/jquery.flot.pie.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/flot.curvedlines/curvedLines.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/jquery.flot.spline/index.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/metisMenu/dist/metisMenu.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/iCheck/icheck.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/peity/jquery.peity.min.js');?>"></script>
<script src="<?php echo base_url('assets/vendor/sparkline/index.js');?>"></script>

<!-- App scripts -->
<script src="<?php echo base_url('assets/scripts/homer.js');?>"></script>
<!--<script src="<?php echo base_url('assets/scripts/charts.js');?>"></script>-->

<script>
    // Initialize metsiMenu plugin to sidebar menu
	$('#side-menu').metisMenu();
	
	 // Initialize iCheck plugin
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    });
	
	 // Initialize animate panel function
    $('.animate-panel').animatePanel();

	
	// Sparkline bar chart data and options used under Profile image on left navigation panel
    $("#sparkline1").sparkline([5, 6, 7, 2, 0, 4, 2, 4, 5, 7, 2, 4, 12, 11, 4], {
        type: 'bar',
        barWidth: 7,
        height: '30px',
        barColor: '#62cb31',
        negBarColor: '#53ac2a'
    });
	
	// Initialize popover
    $("[data-toggle=popover]").popover();

	$( "#tabs" ).tabs();

</script>

<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<!--<script>

    $(function () {

        /**
         * Flot charts data and options
         */
        var data1 = [ [0, 55], [1, 48], [2, 40], [3, 36], [4, 40], [5, 60], [6, 50], [7, 51] ];
        var data2 = [ [0, 56], [1, 49], [2, 41], [3, 38], [4, 46], [5, 67], [6, 57], [7, 59] ];

        var chartUsersOptions = {
            series: {
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
            },
            grid: {
                tickColor: "#f0f0f0",
                borderWidth: 1,
                borderColor: 'f0f0f0',
                color: '#6a6c6f'
            },
            colors: [ "#62cb31", "#efefef"],
        };

        $.plot($("#flot-line-chart"), [data1, data2], chartUsersOptions);

        /**
         * Flot charts 2 data and options
         */
        var chartIncomeData = [
            {
                label: "line",
                data: [ [1, 10], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51] ]
            }
        ];

        var chartIncomeOptions = {
            series: {
                lines: {
                    show: true,
                    lineWidth: 0,
                    fill: true,
                    fillColor: "#64cc34"

                }
            },
            colors: ["#62cb31"],
            grid: {
                show: false
            },
            legend: {
                show: false
            }
        };

        $.plot($("#flot-income-chart"), chartIncomeData, chartIncomeOptions);



    });

</script>-->

</body>
</html>