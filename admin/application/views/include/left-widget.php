<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="<?php echo ADMIN_URL.'panel';?>">
                <img src="<?php echo ADMIN_IMAGES_URL.'store/'.$session_data['store_key'];?>.jpg" alt="logo" width="100px">
            </a>

            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase"><?php echo $session_data['name'];?></span>
				<?php 
				
					if($session_data['rolid'] == 1){ $post = "Super Admin";}
					if($session_data['rolid'] == 2){ $post = "Admin";}
					if($session_data['rolid'] == 3){ $post = "Vendor";}
				?>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <small class="text-muted"><?php echo $post;?> <b class="caret"></b></small>
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="<?php echo ADMIN_URL.'profile';?>">Profile</a></li>
                        <li><a href="<?php echo ADMIN_URL.'change-password';?>">Change Password</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo ADMIN_URL ?>logout">Logout</a></li>
                    </ul>
                </div>


                <!--<div id="sparkline1" class="small-chart m-t-sm"></div>
                <div>
                    <h4 class="font-extra-bold m-b-xs">
                        $260 104,200
                    </h4>
                    <small class="text-muted">Your income from the last year in sales product X.</small>
                </div>-->
            </div>
        </div>
		<!-- #menu -->
		   <ul class="nav" id="side-menu">
		   <?php if($session_data['rolid'] == 3){?>
		   <li><a href="<?php echo ADMIN_URL.'panel';?>">Panel</a></li>
		   <?php } ?>
		   <?php 
		   if(isset($menu)){
			   foreach($menu as $cmenu)
				{
					$class="";
					if($this->uri->segment(1) == $cmenu['act_name'])
					{
						$class="active";
					}
				   // print_r($cmenu);exit;
				   $rol = explode(',',$cmenu['role_id']);
					if(in_array($session_data['rolid'],$rol))
					{
			   ?>
			  <li class="<?php echo $class;?>" >
				 <a href="<?php echo ADMIN_URL.$cmenu['menu_link'];?> ">
				 <i class="fa <?php echo $cmenu['icon']; ?>"></i>
				 <span class="link-title">&nbsp;<?php echo $cmenu['menu_name']; ?></span>
				 <span class="fa arrow"></span></a>
					<?php
					  $query1 = $this->db->query("select * from nv_menu_tbl where parent_menu_id='".$cmenu['id']."'");
					  if($query1->num_rows() > 0)
					  {
							$submenu = $query1->result_array();
							echo '<ul class="nav nav-second-level">';

							foreach($submenu as $sub)	
							{

					 ?>
						<li  ><a href="<?php echo ADMIN_URL.$sub['menu_link'];?>"><i class="fa <?php echo $sub['icon'];?>"></i><span class="nav-label"> <?php echo $sub['menu_name'];?></span></a></li>
					 <?php
							 }
					 echo '</ul>';
					 }
					 ?>
			
			  </li> <?php 
					}
				}
		   }?>
		   </ul>
    </div>
</aside>