 <div class="small-header">
        <div class="hpanel">
            <div class="panel-body">
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb">
                        <li><a href="<?php echo ADMIN_URL."dashboard";?>">Dashboard</a></li>
                        
                      
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">
                   Blog Listing
				   
                </h2>
                <small> </small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">
      
		
		<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
				<?php
					if(isset($msg)){
						$cls =key($msg);
						$message = $msg[$cls];
						?>
					<div id="toast-container" class="toast-top-center" aria-live="polite" role="alert">
						<div class="animated fadeInDown <?php echo 'toast-'.$cls;?>" style="">
							<button type="button" class="toast-close-button" role="button">×</button>
							<div class="toast-message"><?php echo $message;?>
							</div>
						</div>
					</div>
					<?php }?>	
				  <a class="btn btn-primary " href="<?php echo base_url('blog/add');?>"><i class="fa fa-plus"></i> New blog</a>
					
                </div>
                <div class="panel-body">
				
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
							<tr>
								<th>Sr No</th>								
								<th>Blog Image</th>
								<th>Blog Name</th>
								<th>Sort Desc </th>
								<th>Action </th>
								
							</tr>
                        </thead>
						<tbody>
							<?php
							if(isset($list) && !empty($list)){
								$i=1; foreach($list as $val){?>
							<tr>
								<td><?php echo $i;?></td>
								<th><img src="<?php echo base_url('assets/images/blog/').$val['mainImage'];?>" width="100" height="100" /></th>
								<th><?php echo $val['blogName'];?></th>
								<th><?php echo $val['sortDesc'];?> </th>
								<
								<td align="right">
									
									<button class="btn btn-danger2 btn-xs deletstore" data-val="<?php echo $val['id'];?>" type="button"><i class="fa fa-trash-o"></i></button>
									<?php if($val['status'] == "Deactive"){?>
									<button class="btn btn-danger btn-xs changestatus" data-val="<?php echo $val['id'].'|'.$val['status'];?>" type="button"><i class="fa fa-circle-o"></i></button>
									<?php }else{?>
									<button class="btn btn-success btn-xs  changestatus" data-val="<?php echo $val['id'].'|'.$val['status'];?>" type="button"><i class="fa fa-dot-circle-o"></i></button>
									<?php }?>
								</td>
							</tr>
							<?php $i++; } 
							}
							?>		
						</tbody>
						
                    </table>
<div class="pull-right"><?php echo $links; ?></div>
                </div>
            </div>
            </div>
        </div>
    </div>

   