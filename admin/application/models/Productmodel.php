<?php
class productmodel extends CI_Model
{
	function productdetail($tab,$skey="")
	{
		$this->db->select('product_tbl.*,category_tbl.*');
		$this->db->from('product_tbl');
		$this->db->join('category_tbl','product_tbl.pro_cat = category_tbl.cat_id');
		$this->db->where('product_tbl.store_key',$skey);
		//$this->db->where("product_tbl.offer",'0');
		$this->db->where("product_tbl.pro_status !=",'Drop');
		$this->db->order_by("product_tbl.pro_id","desc");
		$query=$this->db->get($tab);
		 
		return $query->result_array();
	}
	function editProData($id)
	{
		$res = getRowDataSingle("select p.*, pdt.*, ct.parent_id as first_parent,(select parent_id from category_tbl where cat_id = first_parent) as second_parent
						 from product_tbl p 
						inner join prodcut_detail_tbl pdt on pdt.pro_id = p.pro_id
						inner join category_tbl ct on ct.cat_id = p.pro_cat
						where p.pro_id =".$id);
		return $res;
	}
	function paginationdata($params)
	{
		//print_r($params);
		$this->db->select('product_tbl.*,category_tbl.*');
		$this->db->from('product_tbl');
		$this->db->join('category_tbl','product_tbl.pro_cat = category_tbl.cat_id');
		 $this->db->limit($params['limit'], $params['start']);
		$this->db->where('product_tbl.store_key',$params['skey']);
		//$this->db->where("product_tbl.offer",'0');
		$this->db->where("product_tbl.pro_status !=",'Drop');
		$this->db->order_by("product_tbl.pro_id","desc");
		$query=$this->db->get();
		  //$sql = $this->db->last_query();
		  //echo $sql; exit;
		return $query->result_array();
	}
	function code($tab)
	{
		$this->db->order_by("pro_code","desc");	
		$query=$this->db->get($tab);
		return $query->result_array();
	}
	function insertpro($tab,$data)
	{
		$this->db->insert($tab,$data);
		$insert_id = $this->db->insert_id();
	return $insert_id;
	}
	function Updpro($tab,$data,$whr)
	{
		$this->db->update($tab,$data,$whr);
	}
	function addpro($tab,$data)
	{
		$this->db->insert($tab,$data);
	}
	function maincategory($tab,$where)
	{
		$query=$this->db->get_where($tab,$where);
		return $query->result_array();
	}
	function viewbrand($tab)
	{
		$query=$this->db->get($tab);
		return $query->result_array();
	}
	function get_sub_cat($tab,$data)
	{
		$query=$this->db->get_where($tab,$data);
						
		if($query->result())
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$options[$row->cat_id] = $row->cat_name;
			}   
			return $options;
		} 
	}
	function get_pro($tab,$data)
	{
		$query=$this->db->get_where($tab,$data);
							
		if($query->result())
		{
			$result = $query->result();
			$options[]='Select';
			foreach($result as $row)
			{
				$options[$row->pro_id] = $row->pro_name; 
			}   
			return $options;
		} 
	}
	
	function viewdetailpro($id)
	{
		$this->db->select('product_tbl.*,manufacturer.*');
		$this->db->from('product_tbl');
		$this->db->join('manufacturer','product_tbl.brand = manufacturer.m_id');
		$this->db->where('pro_id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}
	
	
	function viewdetail($tab,$where)
	{
		$query=$this->db->get_where($tab,$where);
		return $query->result_array();
	}

	function getprodata($tbl,$udata)
	{
		$query=$this->db->get_where($tbl,$udata);
		return $query->result_array();
	}
	
	function getpromdata($tbl)
	{
		$query=$this->db->get($tbl);
		return $query->result_array();
	}
	function getproductdata($tbl,$pdata)
	{
		$query=$this->db->get_where($tbl,$pdata);
		return $query->result_array();
	}
	function proupdate($tbl,$udata,$where)
	{
		$query=$this->db->update($tbl,$udata,$where);
		
	
	}
	function updateproductdetail($tbl,$data,$where)
	{
		$query=$this->db->update($tbl,$data,$where);
		
		
	}
	
	function deletpro($tab,$where)
	{
		$query=$this->db->delete($tab,$where);
		

	}
	function priceinsert($tab,$data)
	{
		$this->db->insert($tab,$data);
	}
	function changeproimage($tab,$udata,$where)
	{
		$query=$this->db->update($tab,$udata,$where);
		
	}
	function deletepro($tab,$where)
	{
		$query=$this->db->delete($tab,$where);
		

	}
	public function prochk($pro) {
        $qry = "SELECT count(*) as cnt from product_tbl where pro_name= ?";
        $res = $this->db->query($qry,array($pro))->result();
        if ($res[0]->cnt > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }
	function keyword($tab,$where)
	{
		$this->db->order_by('date','desc');
		$query=$this->db->get_where($tab,$where);
		return $query->result_array();
	}
	
}
?>