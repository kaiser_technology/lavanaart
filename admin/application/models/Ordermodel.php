<?php
class ordermodel extends CI_Model
{
	function getRecords($skey)
	{
		$this->db->select('order_tbl.*,order_detail.*,product_tbl.*');
		$this->db->from('order_tbl');
		$this->db->join('order_detail','order_tbl.ord_id = order_detail.ord_id');
		$this->db->join('product_tbl','order_detail.pro_id = product_tbl.pro_id');
		$this->db->where('product_tbl.store_key',$skey);
		$this->db->order_by("order_tbl.ord_id","desc");
		$query=$this->db->get();
		 //$sql = $this->db->last_query();
		 //echo $sql; exit;
		return $query->result_array();
	}
	function paginationdata($params)
	{
		$this->db->select('order_tbl.*,order_detail.*,product_tbl.*');
		$this->db->from('order_tbl');
		$this->db->join('order_detail','order_tbl.ord_id = order_detail.ord_id');
		$this->db->join('product_tbl','order_detail.pro_id = product_tbl.pro_id');
		$this->db->limit($params['limit'], $params['start']);
		$this->db->where('product_tbl.store_key',$params['skey']);
		$this->db->where('order_tbl.ord_status',$params['status']);
		$this->db->order_by("order_tbl.ord_id","desc");
		$query=$this->db->get();
		//echo $this->db->last_query();exit;
		return $query->result_array();
	}
	function viewOrder($skey)
	{
		$this->db->select('order_tbl.*,order_detail.*,product_tbl.*');
		$this->db->from('order_tbl');
		$this->db->join('order_detail','order_tbl.ord_id = order_detail.ord_id');
		$this->db->join('product_tbl','order_detail.pro_id = product_tbl.pro_id');
		$this->db->where('product_tbl.store_key',$skey);
		$this->db->order_by("order_tbl.ord_id","desc");
		$query=$this->db->get();
		 //$sql = $this->db->last_query();
		 //echo $sql; exit;
		return $query->result_array();
	}
	function getOrder($params)
	{
		$query=$this->db->get_where($params['table'],$params['where']);							
		return $query->result_array();
	}
	
	
	function update($params)
	{
		$query=$this->db->update($params['table'],$params['data'],$params['where']);
		
		
	}
	function order_detail($id)
	{	
		$this->db->select('*');
		$this->db->from('order_tbl');
		$this->db->join('user_tbl', 'order_tbl.user_id = user_tbl.user_id');
		
		$this->db->where("order_tbl.ord_id",$id);
		$query = $this->db->get();

		return $query->result_array();
			
		//$query=$this->db->get_where($tab,$where);
		//return $query->result_array();
	}
	function product_detail($id)
	{	
		$this->db->select('*');
		$this->db->from('order_detail');
		//$this->db->join('order_detail','order_tbl.ord_id = order_detail.ord_id');
		$this->db->join('product_tbl','product_tbl.pro_id = order_detail.pro_id');
		$this->db->join('product_price_tbl','product_price_tbl.pro_price_id = order_detail.pro_price_id');
		$this->db->where("order_detail.ord_id",$id);
		$query = $this->db->get();

		return $query->result_array();
			
		//$query=$this->db->get_where($tab,$where);
		//return $query->result_array();
	}
}
?>