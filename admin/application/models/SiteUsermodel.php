<?php
class SiteUsermodel extends CI_Model
{
	function userdetail($tab,$skey="")
	{
		$this->db->select('user_tbl.*,store_information.store_name');
		$this->db->from('user_tbl');
		$this->db->join('store_information','user_tbl.store_key	= store_information.store_key');
		if($this->session_data['rolid'] == 3)
		{
			$this->db->where('user_tbl.store_key',$skey);
		}
		//$this->db->where("product_tbl.offer",'0');
		$this->db->where("user_tbl.user_status =",'Active');
		$this->db->order_by("user_tbl.user_id","desc");
		$query=$this->db->get($tab);
		 
		return $query->result_array();
	}
	function paginationdata($params)
	{
		//print_r($params);
		$this->db->select('user_tbl.*,store_information.store_name');
		$this->db->from('user_tbl');
		$this->db->join('store_information','user_tbl.store_key = store_information.store_key');
		 $this->db->limit($params['limit'], $params['start']);
		 if($this->session_data['rolid'] == 3)
		{
			$this->db->where('user_tbl.store_key',$params['skey']);
		}
		//$this->db->where("product_tbl.offer",'0');
		$this->db->where("user_tbl.user_status =",'Active');
		$this->db->order_by("user_tbl.user_id","desc");
		$query=$this->db->get();
		  //$sql = $this->db->last_query();
		  //echo $sql; exit;
		return $query->result_array();
	}
	
}
?>