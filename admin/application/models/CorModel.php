<?PHP
Class CorModel extends CI_Model{
	
   public function insert($params){
	   
	  $query = $this->db->insert($params['table'],$params['data']);
		
	  if($query)
		   return $this->db->insert_id();
	  else
		  return false;
   }
   
   public function getRecords($params){
	   if(isset($params['select']))
	   {
			$this->db->select($params['select'],false);
	   }
	   if(isset($params['jointbl']))
	   {
			$cnt=count($params['jointbl']);
			
			for($i=0;$i<$cnt;$i++)
			{
					$this->db->join($params['jointbl'][$i],$params['joinfield']);
			}
	   
	   }
		
		if(isset($params['where']))
	   {
			$this->db->where($params['where']);
	   }
		$this->db->from($params['table']);
	 
		  $query=$this->db->get();
	 
	  
	  
	  return $query -> result_array();
   }
   	
   public function update($params){
	   
	  $query=$this->db->update($params['table'],$params['data'],$params['where']);
	  if($query)
		  return true;
	  else
		  return false;			
   }
   
   public function delete($params){
   
	  $this->db->where_in($params['field'],$params['id_str']);
	  $query=$this->db->delete($params['table']);
	  $this->db->last_query();
	  
	  if($query)
		  return true;
	  else
		  return false;
		  
   }
   
   public function changeStatus($params){
	   
	  $this->db->where_in($params['field'],$params['id_str']);
	  $query=$this->db->update($params['table'],$params['data']);
  
	  if($query){
		return true;
	  }else{
		return false;
	  }
   }
   
  public function recordList($params){
	  
	  if(isset($params['select']) && $params['select']!=""){
		  $select = implode("," , $params['select']);
		  $this->db->select($select, FALSE);
	  }
	  
	  if(isset($params['where'])){
			$this->db->where($params['where']);
	   }
	   if(isset($params['jointbl']))
	   {
			$cnt=count($params['jointbl']);
			for($i=0;$i<$cnt;$i++)
			{
					$this->db->join($params['jointbl'][$i],$params['joinfield']);
			}
	   
	   }
	  $this->db->order_by($params['sortvar']);
	  
	  if($params['option']!="" and $params['keyword'] !=""){
		  
		 $this->db->like($params['option'],$params['keyword'],'both');
	  }
	  $this->db->limit($params['limit'], $params['start']);
	  $query=$this->db->get_where($params['table']);
	  return $query->result_array();
  }
  
  public function login($params){
	
	  $this -> db -> limit(1);
	  $query = $this -> db ->get_where($params['table'],$params['where']);
	  if($query -> num_rows() == 1){
		  
		  $loginfo=$query->result_array();
		  $udata = array('logged_in_datetime' => date('Y-m-d h:m:s'));
		  $whr=array("id"=>$loginfo[0]['id']);
		  $result = $this->db->update($params['table'], $udata, $whr);
		  return $query->result();
	  }else
		 return false;
  }
  
  function writeAdminLog($module,$action,$comment='') {
  
	  $params=array('admin_id'    =>  $this->sessionObj->data['admin']['id'],
					'admin_email' =>  $this->sessionObj->data['admin']['email'],
					'module' 	  =>  $module,
					'action' 	  =>  $action,
					'ip_address'  =>  $this->sessionObj->data['CUSTOMER_IP']);
						
	  $this->dbObj->insert(TABLE_PREFIX."admin_log",$params);
	}
	public function lastQuery()
	{
		$query=$this->db->last_query();
		return $query;
	}
	public function paginationdata($params) {
        $this->db->limit($params['limit'], $params['start']);
		if(isset($params['where']))
	   {
			$this->db->where($params['where']);
	   }
        $query = $this->db->get($params['table']);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
   }
   public function lastrecord($params)
   {
	   $this->db->from($params['table']);
	    $this->db->order_by($params['field'],'DESC');
		$this->db->limit(1);
	   $query=$this->db->get();
	   return $query->result_array();
   } 

}
?>
