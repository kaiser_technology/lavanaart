<?php
class MY_Loader extends CI_Loader {
    public function template($template_name, $vars = array(), $return = FALSE)
    {
         $CI =& get_instance();
		//$vars['scripts']=$this->jsCss();
		$vars['menu'] = $this->getmenu();
		$vars['session_data'] = $CI->session->userdata('admin');
		$vars['pageName'] =ucfirst($CI->uri->segment(1));
        
		//print_r($vars);exit;
        if($return):
        $content  = $this->view('templates/header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('templates/footer', $vars, $return);

        return $content;
    else:
        $this->view('include/header', $vars);
        $this->view($template_name, $vars);
        $this->view('include/footer', $vars);
    endif;
    }
	public function getmenu()
	{
		$param['table'] ="nv_menu_tbl";
        $param['where'] =array("parent_menu_id"=>"0");
		$this->model('CorModel');
		 $CI =& get_instance();
		$menu = $CI->CorModel->getRecords($param);

	return $menu;
		
	}
    public function isSignedIn()
	{
		$CI =& get_instance();
		if(!$CI->session->userdata('admin'))
		{
			 redirect(base_url().'login');
		}
		else
		{
			$session_data = $CI->session->userdata('admin');
			return $session_data;
		}
	}
	
}
?>