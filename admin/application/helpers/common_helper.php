<?php


function validPhoneNumber($value, $phoneCountryArray, $countryCode)
{
    $matches = array();

    $value = preg_replace("/[^A-Za-z0-9]/", "", $value);

    preg_match('/(0|\+?\d{2})(\d{7,15})/', $value , $matches);

    if(count($matches) ==  3)
    {
        //if country code not in config array
        if(!in_array('+'.$matches[1], array_keys($phoneCountryArray)))
        {
            $setFlag = 0;

            foreach($phoneCountryArray as $c)
            {
                if(strpos("+". $value, $c["d_code"]) === 0)
                {
                    $matches[2] = str_replace($c["d_code"], "", "+". $value);

                    $matches[1] = str_replace("+", "", $c["d_code"]);
                    
                    $setFlag++;
                }
            }

            if($setFlag == 0)
            {
                $matches[1] = $countryCode;
            }
        }

        if (substr($matches[2], 0, 1) === '0') { 
            $matches[2] = substr($matches[2], 1);
            $matches[0] = $matches[1]."".$matches[2];

            if (substr($matches[2], 0, 1) === '0') { 
                $matches[2] = substr($matches[2], 1);
                $matches[0] = $matches[1]."".$matches[2];
            }
        }
    }

    
    return $matches;
}






function isDevKen($key = 'ken', $isExit = 0)
{
    $CI = get_instance();

    if($CI->input->get_post('dev') == $key)
    {
        if($isExit == 1)
        {
            $CI->benchmark->mark('code_end');
            dump("ELAPSED: ". $CI->benchmark->elapsed_time('code_start', 'code_end'), 1);
        }

        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

 

function getDeviceName($device)
{
    switch ($device) {

    case "iPod5,1":                                 return "iPod Touch 5";
    break;
   
    case "iPod7,1":                                 return "iPod Touch 6";
    break;

    case "iPhone3,1":                               return "iPhone 4";
    case "iPhone3,2":                               return "iPhone 4";
    case "iPhone3,3":                               return "iPhone 4";
    break;
 
    case "iPhone4,1":                               return "iPhone 4s";
    break;
  
    case "iPhone5,1" :                              return "iPhone 5";
    case "iPhone5,2":                               return "iPhone 5";
    break;

    case "iPhone5,3":                               return "iPhone 5c";
    case "iPhone5,4":                               return "iPhone 5c";
    break;
 
    case "iPhone6,1":                               return "iPhone 5s";
    case "iPhone6,2":                               return "iPhone 5s";
    break;
 
    case "iPhone7,2":                               return "iPhone 6";
    break;

    case "iPhone7,1":                               return "iPhone 6 Plus";
    break;

    case "iPhone8,1":                               return "iPhone 6s";
    break;
 
    case "iPhone8,2":                               return "iPhone 6s Plus";
    break;

    case "iPhone8,4":                               return "iPhone SE";
    break;
 
    case "iPad2,1":                               return "iPad 2";
    case "iPad2,2":                               return "iPad 2";
    case "iPad2,3":                               return "iPad 2";
    case "iPad2,4":                               return "iPad 2";
    break;

    case "iPad3,1":                               return "iPad 3";
    case "iPad3,2":                               return "iPad 3";
    case "iPad3,3":                               return "iPad 3";
    break;


    case "iPad3,4":                               return "iPad 4";
    case "iPad3,5":                               return "iPad 4";
    case "iPad3,6":                               return "iPad 4";
    break;
 
    case "iPad4,1":                               return "iPad Air";
    case "iPad4,2":                               return "iPad Air";
    case "iPad4,3":                               return "iPad Air";
    break;

    case "iPad5,3":                               return "iPad Air 2";
    case "iPad5,4":                               return "iPad Air 2";
    break;
   
    case "iPad2,5" :                              return "iPad Mini";
    case "iPad2,6" :                              return "iPad Mini";
    case "iPad2,7" :                              return "iPad Mini";
    break;
 
    case "iPad4,4" :                               return "iPad Mini 2";
    case "iPad4,5" :                               return "iPad Mini 2";
    case "iPad4,6" :                               return "iPad Mini 2";
    break;
  
    case "iPad4,7":                                return "iPad Mini 3";
    case "iPad4,8":                                return "iPad Mini 3";
    case "iPad4,9":                                return "iPad Mini 3";
    break;

    case "iPad5,1":                                return "iPad Mini 4";
    case "iPad5,2":                                return "iPad Mini 4";
    break;

    case "iPad6,3":                               return "iPad Pro";
    case "iPad6,4":                               return "iPad Pro";
    case "iPad6,7":                               return "iPad Pro";
    case "iPad6,8":                               return "iPad Pro";
    break;
 
    case "AppleTV5,3":                            return "Apple TV";
    break;

    case "i386":                                  return "Simulator";
    case "x86_64":                                return "Simulator";
    break;
 
    default:                                       return "";
    }
}







function suid()
{
	$CI = &get_instance();
	return getSessVal('uid');
}




function getSessVal($key)
{
    $CI = &get_instance();
    return $CI->session->userdata($key);
}


function generateToken($number)
{
	$CI = &get_instance();
	return md5($number.date(now()).$CI->config->item('uniqueString'));
}

function sendsms($number, $message_body, $return = '0') 
{
	$number = "9594844632";
	$sender = '9930797308';  // Need to change
	$smsGatewayUrl = 'http://bulkpush.mytoday.com';
	$feedid = '360340';  // Need to change   
	$password = 'pdmgm';  // Need to change   

	$textmessage = urlencode($message_body);
	$api_element = '/BulkSms/SingleMsgApi';
	$api_params = $api_element.'?feedid='.$feedid.'&username='.$sender.'&password='.$password.'&To='.$number.'&Text='.$textmessage;    
	$smsgatewaydata = $smsGatewayUrl.$api_params;

	$url = $smsgatewaydata;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, false);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	curl_close($ch);

	if(!$output){
	   $output =  file_get_contents($smsgatewaydata);
	}

	if($return == '1')
		return $output;        
}
function send_mail($to_email,$message,$subject)
{ 
	$ci = get_instance();
	//$this->email->initialize($config);
	$ci->load->library('email');
	$ci->email->set_mailtype("html");
	
	$ci->email->from('DoNotReply@delriques.com','Delriques Canine Institute');
	$ci->email->to($to_email);
	$ci->email->subject($subject);
	
	$ci->email->message($message);
	$ci->email->send();
} 
function generateOTP()
{
    $CI = &get_instance();

    $digits = $CI->config->item('OTPdigits');

    $otp = '';
    $i = 0;

    while ($i < $digits):
      $otp = $otp . mt_rand(0,9);
      $i++;
    endwhile;

	return $otp;
}
function uniqueCode()
{
	return bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
}

function formatDate($date)
{
	return date("d M, Y",strtotime($date));
}


function is_valid_url($url = NULL)
{
	$isValidUrl = 0;

	if($url == NULL || $url == '')
	{
		return false;
	}
	else
	{	
		if (!preg_match("/\b(?:(?:https?|ftp?|http):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) {
		 	return false; 
		}
		else{
			return true;
		}
	}
}



function getFileExtension($fileKey)
{
	$CI = &get_instance();

	$extension 	= 	explode('.', $_FILES[$fileKey]['name']);
       
	if(count($extension) == 1)
	{	
		return '';
	}
	else
	{
		$extension 	=	$extension[count($extension) - 1];

		return strtolower($extension);
	}
}


function fileUrlGen($fileName)
{
	return base_url() .'uploads/'. $fileName;
}

function in_array_r($needle, $haystack, $strict = false) {
	foreach ($haystack as $key => $item) {
		if (in_array($needle,$item)) {
			return $key;
		}
	}
	return false;
}
function addDayswithdate($date,$days){

    $date = strtotime("+".$days." days", strtotime($date));
    return  date("d-m-Y", $date);
}
function dogInfo($dob,$size_key=null,$type)
{
	$ci = get_instance();
	$breed_size = $ci->config->item('breed_size');
	$breed_days = $ci->config->item('days_count');
	
	$now = time(); // or your date as well
	$your_date = strtotime($dob);
	$datediff = $now - $your_date;
	$dob_days = floor($datediff/(60*60*24));
	 
	foreach($breed_days as $days_val)
	{
		if($dob_days <= $days_val)
		{
			$dd = $days_val;
			break;
		}
	}
	if($type == "vaccination" || $type == "tick" || $type == "deworming" || $type == "grooming")
	{
		if($type == "grooming")
			return $ci->config->config[$type][$size_key];
		else
			return $ci->config->config[$type][$dd];
	}
	else
		return $ci->config->config[$size_key][$dd][$type];
}
function dateDiff($dob)
{
	$now = time(); // or your date as well
	$your_date = strtotime($dob);
	$datediff = $now - $your_date;
	return $dob_days = floor($datediff/(60*60*24));
}
function notification()
{
	$CI = &get_instance();
    $uid = $CI->session->userdata('uid');
	$CI->load->model('Home_model');
	$data = $CI->Home_model->notification($uid);
	
	$curr_dt = date("Y-m-d",strtotime(date("Y-m-d")));
	$befor_one_day = date("Y-m-d",strtotime("-1 days", strtotime($curr_dt)));
	$after_one_day = date("Y-m-d",strtotime("+1 days", strtotime($curr_dt)));
	
	$date_array = array("1" => $befor_one_day, "2" => $curr_dt, "3" => $after_one_day);
	foreach($data as $val)
	{
		/***********Vaccination***********/
		
		$vac_date = $val['last_vac_date'];
		$res = array_search($vac_date,$date_array);
		
		if($res)
		{
			$added = $vac_date;
			$check = checkExistNotification($val['id'], 1, $added);
			if($check == 0)
			{
				$params = array("dog_id" => $val['id'],
								"type" => 1,
								"added" => $added);
				$CI->db->insert("notification", $params);
			}
		}
		
		/***********Vaccination***********/	
		/***********Dewarming***********/
			
		$dewarm_date = $val['last_deworm_date'];
		$res = array_search($dewarm_date,$date_array);
		if($res)
		{
			$added = $dewarm_date;
			$check = checkExistNotification($val['id'], 2, $added);
			if($check == 0)
			{
				$params = array("dog_id" => $val['id'],
								"type" => 2,
								"added" => $added);
				$CI->db->insert("notification", $params);
			}
		}
		
		/***********Dewarming***********/
		
		/************Tick**************/
		
		$tick_date = $val['last_tick_date'];
		$res = array_search($tick_date,$date_array);
		if($res)
		{
			$added = $tick_date;
			$check = checkExistNotification($val['id'], 3, $added);
			if($check == 0)
			{
				$params = array("dog_id" => $val['id'],
								"type" => 3,
								"added" => $added);
				$CI->db->insert("notification", $params);
			}
		}
		
		/************Tick**************/
	}
	
	$notific = $CI->db->
					select('dg.*, dn.id as nid, dn.type')->
					from('dogs dg')->
					join('notification dn', 'dn.dog_id = dg.id')->
					where('dn.status','0')->
					where('dg.status','0')->
					where('dg.uid',$uid)->
					where('dn.added >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)')->
					get()->result_array(); 
	return $notific;
}
function checkExistNotification($did,$type,$date)
{
	$CI = &get_instance();
	$data = $CI->db->where('dog_id',$did)->where('type',$type)->where('added',$date)->get("notification")->num_rows();
	return $data;
}

function showNotification($notify)
{
	$disply_data = "";
	
	usort($notify, function($a1, $a2) {
	   $value1 = strtotime($a1['nofifytime']);
	   $value2 = strtotime($a2['nofifytime']);
	   return $value1 - $value2;
	});
	
	if($notify)
	{
		$disply_data = '<ul class="dropdown-menu hdropdown notification animated flipInX">';
		foreach($notify as $nval)
		{
			if($nval['type'] == 1)
			{
				$lbl = "V";	
				$type = "Vacciation";	
				$col = "success";
				$next_date = $nval['last_vac_date'];
			}
			else if($nval['type'] == 2)
			{
				$lbl = "D";	
				$type = "Deworming";	
				$col = "warning";
				$next_date = $nval['last_deworm_date'];	
			}
			else
			{
				$lbl = "T";	
				$type = "Tick Control";	
				$col = "info";
				$next_date = $nval['last_tick_date'];	
			}
			$nxt_date = strtotime($next_date);
			$cdater = strtotime(date("Y-m-d"));
			
			$next_date = date("d-m-Y",$nxt_date);
			
			if($cdater < $nxt_date)
				$txt = "is due on ".$next_date;
			else if($cdater == $nxt_date)
				$txt = "is due Today ";
			else if($cdater > $nxt_date)
				$txt = "was due on ".$next_date;
			
			$disply_data .= "
			<li>
		<a>
			<span class='label label-".$col."'>".$lbl."</span> ".ucwords(strtolower($nval['name']))."'s next ".$type." ".$txt."
		</a>
	</li>";
	
		}
		$disply_data .= "</ul>";
	}
	return $disply_data;
}
?>