<?php


function getSiteName()
{
	$CI = get_instance();

	return $CI->config->item('siteName');
}


function dump($value,$isExit = 0)
{	print_r('<pre>');
	print_r($value);
	print_r('</pre>');

	//invalidateToken();
	
	if($isExit == 1)
	{
		exit();
	}
}
function singleFile($path,$file_types,$filename)
{
	$config['upload_path']   = $path;
	$config['allowed_types'] = $file_types;
	$config['max_size']      = 2048;
	$config['encrypt_name'] = TRUE;
	
	$CI =& get_instance();
	$CI->load->library('upload', $config);
	//$CI->upload->initialize($config);
	if ( ! $CI->upload->do_upload($filename)) {
		$error = array('error' => $CI->upload->display_errors());
		//dump($_FILES[$filename]);
		//dump($file_types);
		//dump($error,1);
		//return $error;
		return "error";
	}
	else {
		$data = $CI->upload->data();
		//dump($data);
		return $data['file_name'];
	}
}
function multiFile($path,$file_type,$filename)
{
	$CI =& get_instance();
	$filesCount = count($_FILES[$filename]['name']);
	$files = $_FILES[$filename];
	
	for($i = 0; $i < $filesCount; $i++){
		//dump($_FILES[$filename]);die;
		$_FILES[$filename]['name'] = $files['name'][$i];
		$_FILES[$filename]['type'] = $files['type'][$i];
		$_FILES[$filename]['tmp_name'] = $files['tmp_name'][$i];
		$_FILES[$filename]['error'] = $files['error'][$i];
		$_FILES[$filename]['size'] = $files['size'][$i];
	
		$res = singleFile($path,$file_type,$filename);
		$datarr[] = $res;
	}
	return $datarr;
}

function isDev($key = 'ken', $isExit = 0)
{
    $CI = get_instance();

    if($CI->input->get_post('dev') == $key)
    {
        if($isExit == 1)
        {
            $CI->benchmark->mark('code_end');
            dump("ELAPSED: ". $CI->benchmark->elapsed_time('code_start', 'code_end'), 1);       
        }

        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


function jsonSuccess($data)
{
	$CI= &get_instance();
	
	$data['status'] 	= 	'success';
	//$data['size'] 		= 	$_SERVER['CONTENT_LENGTH'];
	//invalidateToken();
	$data['time']		=	time();

	echo 	json_encode($data);

	if($CI->input->get_post("profiler") == "yes")
	{
		echo $CI->profiler->run();
	}

	exit();
}


	
function jsonError($message = '',$code='')
{
	$CI= &get_instance();
	
	//invalidateToken();
	
	echo 	json_encode(
							array(
									'status' 	=> 'failure',
									'message' 	=> $message,
									'code'		=>	$code,
									'request' 	=> 	$_REQUEST
									//,'size' 		=> 	$_SERVER['CONTENT_LENGTH']
							)
						);	

	exit();
}



function invalidateToken()
{
	$CI=& get_instance();

	if(verifyToken())
	{
		destroySess();
	}
}

function verifyToken($return = NULL)
{
	$CI =& get_instance();

	$sessionToken = $CI->session->userdata('token');

	if($CI->input->get_post('token') && $CI->input->get_post('token') != null && $CI->input->get_post('token') != '')
	{
		if($return == 1)
		{
			return $CI->input->get_post('token');
		}

		return TRUE;
	}
	else if(! empty($sessionToken))
	{
		if($return == 1)
		{
			return $sessionToken;
		}

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

	
function destroySess()
{
	$CI=& get_instance();

	$CI->session->sess_destroy();
}

function currDateGMT()
{
	return date('Y-m-d H:i:s', local_to_gmt(now()));
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

function flattenObjToArr($obj, $compareKey, $returnLabel, $dumpFlag=0)
{
	$CI=& get_instance();

	$intColumnsArray = $CI->config->item('intColumns');

	$returnArr = array();

	foreach($obj as $item)
	{
		$keyVal = '';

		$temp = array();

		foreach($item as $k => $v)
		{
			if($k == $compareKey)
			{
				$keyval = $v;
			}

			if(is_null($v))
			{
				if(in_array($k, $intColumnsArray))
				{
					$temp[$k] = 0;
				}
				else
				{
					$temp[$k] = '';
				}
			}
			else
			{
				$temp[$k] = $v;
			}
		}
		
		$returnArr[$returnLabel . $keyval] = $temp;

	}

	return $returnArr;
}


function randomString($length = 8)
{
	$characters 	= 	'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
	$randstring 	= 	'';

	for ($i = 0; $i < $length; $i++) 
	{
		if(isset($characters[rand(0, (strlen($characters)-1))]))
		{
			$randstring .= $characters[rand(0, (strlen($characters)-1))];
        }
        else
        {
        	$i--;
        }
    }

    return $randstring;
}



function gdizFormatNumber($number, $decimals = 0)
{
	return number_format($number, $decimals);
}




function pageTitleGenerator($pageArr= array())
{
	$CI=& get_instance();
	
	$pageTitle = "";

	$separator = $CI->config->item('titleSeparator');
	
	$siteName =  $CI->config->item('siteName');

	if(count($pageArr) > 0)
	{
		$pageTitle = $siteName.$separator.implode($separator,$pageArr);
	}
	else
	{
		$pageTitle = $siteName;
	}

	return $pageTitle;
}


function getRowData($table, $compareCol, $compareVal, $returnField)
{
	 
	$CI = &get_instance();

	$query	 =	$CI->db->select($returnField)->where($compareCol, $compareVal)->limit(1)->get($table)->row();
					
	if (count($query)	==	1) 
	{
    	return $query->$returnField;
	} 
	else 
	{
    	return '';
	}
	
}
function getRowDataMulti($query)
{
	$CI = &get_instance();

	$res	 =	$CI->db->query($query)->result_array();
		
	if (count($res)	>	0)
	{
		return $res;
	}
	else
	{
		return '';
	}

}
function getRowDataSingle($query)
{
	$CI = &get_instance();

	$res	=	$CI->db->query($query)->row();

	if (count($res)	>	0)
	{
		return $res;
	}
	else
	{
		return '';
	}

}

function verifyRowMulti($table, $fields,  $isArchived = 1, $returnField = 0, $time='')
{
	$CI = &get_instance();

	if($isArchived === 1)
	{
		$CI->db->where('archived is NULL');
	}

	if($time != '')
	{
		$CI->db->where($time . ' >= ', date('Y-m-d H:i:s', local_to_gmt(strtotime('-' . $CI->config->item($time)))));
	}

	foreach($fields as $k => $v)
	{
		$CI->db->where($k,$v);
	}

	$q = $CI->db->limit(1)->get($table)->row();

	if(count($q) ==	1) 
	{

		if($returnField === 0)
		{
			return TRUE;
		}
		else
		{
			return $q;
		}
    	
	} 
	else 
	{
    	return FALSE;
	}
}

function verifyRow($table, $field, $id, $returnRow = 0)
{
	$CI = &get_instance();

	if($returnRow == 0)
	{
		$CI->db->select($field);
	}

	$query	 =	$CI->db->where($field, $id)->where('archived is NULL')->limit(1)->get($table)->row();
					
	if (count($query)	==	1) 
	{
		if($returnRow == 0)
		{
			return TRUE;
		}
		else
		{
			return $query;
		}
    	
	} 
	else 
	{
    	return FALSE;
	}
	
}

function getUserInfo($userId)
{
	$CI = &get_instance();

	return  $CI->db->where('id', $userId)->limit(1)->get('users')->row_array();
}


function checkUnique($table, $col, $colValue, $notCol = 0, $notColId = 0, $returnField = '')
{
	$CI = &get_instance();

	if($notCol !== 0)
	{
		$CI->db->where($notCol .' !=', $notColId);
	}

	$isReturn = 1;
	if($returnField == '')
	{
		$returnField = $col;
		$isReturn = 0;
	}

	$query = $CI->db->select($returnField)
						->where($col, $colValue)
							->limit(1)
								->where('archived is NULL')
									->get($table)
										->row();

	
	if(count($query) == 1)
	{
		if($isReturn == 0)
		{
			return TRUE;
		}
		else
		{
			
			return $query->$returnField;
		}
	}
	else
	{
		return FALSE;		
	}
}
function galImages($pid)
{
	$CI = &get_instance();
	
	$data = $CI->db->where('pid',$pid)->get("stk_product_gallery")->result_array();
	return $data;
}
function Oprator_eval($val)
{
	$test = preg_replace('/\s+/', '', $val);

	$number = '(?:\d+(?:[,.]\d+)?|pi|π)'; // What is a number
	$functions = '(?:sinh?|cosh?|tanh?|abs|acosh?|asinh?|atanh?|exp|log10|deg2rad|rad2deg|sqrt|ceil|floor|round)'; // Allowed PHP functions
	$operators = '[+\/*\^%-]'; // Allowed math operators
	$regexp = '/^(('.$number.'|'.$functions.'\s*\((?1)+\)|\((?1)+\))(?:'.$operators.'(?2))?)+$/'; // Final regexp, heavily using recursive patterns
	$test = preg_replace('!pi|π!', 'pi()', $test); // Replace pi with pi function
    eval('$result = '.$test.';');
	return $result;
}
function objectToArray($d)
{
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		 * Return array converted to object
		 * Using __FUNCTION__ (Magic constant)
		 * for recursive call
		 */
		return array_map(__FUNCTION__, $d);
	} else {
		// Return array
		return $d;
	}
}
function output($menu,$parent=0)
{
	global $menu_array;
	foreach ($menu as $handle)
	{
		//dump($handle);
		if(isset($handle->children))
		{
			//echo 'node : '.$handle->id.' - Pid : '.$parent.'<br>';
			if($parent != 0)
				$menu_array[$parent][] = $handle->id;
				// Insert into DataBase Code (menuId = $handle->id and ParentId = $parent)
				output($handle->children,$handle->id);
		}
		else if(isset($handle->id))
		{
			//echo 'node : '.$handle->id.' - Pid : '.$parent.'<br>';
			//if($parent != 0)
				$menu_array[$parent][] = $handle->id;
				// Insert into DataBase Code (menuId = $handle->id and ParentId = $parent)
		}
	}

	if(is_array($menu_array))
		return $menu_array;
}
function setDiv($r,$new_arr,$exist_key)
{
	$CI = &get_instance();
	if(is_array($r[$exist_key]))
	{
		$color = $CI->config->item('color');
		echo '<ol class="dd-list">';
		foreach ($r[$exist_key] as $rpt)
		{
			$randIndex = array_rand($color);
			$src = base_url()."assets/images/user.png";
			if(!empty($new_arr[$rpt]['emp_photo']) && $new_arr[$rpt]['emp_photo'] != "error")
				$src = base_url()."application/upload/empPhoto/".$new_arr[$rpt]['emp_photo'];
			echo '<li class="dd-item" data-id="'.$rpt.'">
                    <div class="dd-handle" style="border-top:2px solid '.$color[$randIndex].';">
                    <img alt="logo" height="30" width="30" style="margin:0;" class="img-circle m-b m-t-md" src="'.$src.'">
                    '.$new_arr[$rpt]['emp_fullname'].' ('.$new_arr[$rpt]['emp_refcode'].') - '.$new_arr[$rpt]['designation'].'
                    </div>';
				setDiv($r,$new_arr,$rpt);
			 
			echo '</li>';
			 
		}
		echo '</ol>';
	}
}
function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d',$str )
{
	$dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current)."/".$str;
        $current = strtotime($step, $current);
    }

    return $dates;
}
?>