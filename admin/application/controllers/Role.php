<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class role extends CI_Controller {
	
	 public function __construct()
	{
		parent::__construct();
	}
	 
	public function roleList()
    {
       $this->load->library("pagination");
		try{
			if($this->session->flashdata('msg'))
			{
					$this->data['msg'] = $this->session->flashdata('msg');
			}
			$param['table'] ="role_tbl";
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "store/roleList/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			$this->load->template('role',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	 public function addRole()
    {
       
        $this->form_validation->set_rules('role_name', 'Access Type', 'trim|required|is_unique[role_tbl.role_name]');
        if($this->form_validation->run() != FALSE)
  		{
            $cname = $this->input->post("role_name");
            $params['table']="role_tbl";
            $params['data']=array("role_name"=>$cname);
            $result =$this->CorModel->insert($params);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			$this->session->set_flashdata('msg',$vars['msg']);
			  redirect('role/roleList');
        }
        else
        {
            
            	$this->load->template('role');
				
        }
      
    }
	
    
}
