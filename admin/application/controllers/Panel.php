<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function index()
	{
		
			$this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$this->load->model('ordermodel');
			$sdata = $this->session->userdata('admin');
			$skey = $sdata['store_key'];
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "Order/allorder/";
			$config["total_rows"] = COUNT($this->ordermodel->getRecords($skey));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$param['skey'] = $skey;
			$param['status'] = "not_confirm";
			$this->data['list'] = $this->ordermodel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('panel',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}			
		
	}
	public function status()
	{
		try{
			$id = $this->uri->segment(3);	
			$this->load->model('ordermodel');	
			$param['where'] =array("ord_id"=>$id);
			$param['table'] ="order_tbl";
			$data = $this->ordermodel->getOrder($param);
			$param['data'] = array("ord_status"=>'pending');
			$this->ordermodel->update($param);
			redirect('Order/pendingorder');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}
	public function deleteorder()
	{
		$id = $this->uri->segment(3);
		$this->load->model('ordermodel');
		$param['table'] ="order_tbl";
		$param['where'] =array("ord_id"=>$id);
		$param['data'] = array("ord_status"=>'Cancel');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}
}
