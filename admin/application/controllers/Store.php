<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 
	 <ul class="pagination"><li class="paginate_button active"><a tabindex="0" aria-controls="example1" href="#" data-dt-idx="1">1</a></li><li class="paginate_button next disabled" id="example1_next"><a tabindex="0" aria-controls="example1" href="#" data-dt-idx="2">Next</a></li></ul>
	 */
    private $session_data;
    public $data ="";
	function __construct(){
		parent::__construct();
		
		$this->session_data=$this->load->isSignedIn();	
		//print_r($this->session_data);exit;
        //$this->load->library('grocery_CRUD');

	}
	public function test()
	{
			echo $this->pro->dycript('test');
	}
    public function storelist()
	{
		 $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="store_information";
			$param['where'] =array('status !=' => 'Drop');
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "store/storelist/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 25;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			

			//$this->CorModel->update($param);
			$this->load->template('store',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	}
	public function editStore()
	{
				  $param['table'] ="store_category";
		$this->data['catlist'] = $this->CorModel->getRecords($param);
		$id = $this->uri->segment(3);
		$param['table'] ="store_information";
        $param['where'] =array("store_id"=>$id);
		$this->data['info'] = $this->CorModel->getRecords($param);

		$this->load->template('editstore',$this->data);
	}
	public function deletestore()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="store_information";
        $param['where'] =array("store_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		
		$param['data'] = array("status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}
    public function status()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="store_information";
        $param['where'] =array("store_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		if($data[0]['status'] == "Active")
		{
			$param['data'] = array("status"=>'Inactive');
			$this->CorModel->update($param);
		}
		else
		{
			
			$param['data'] = array("status"=>'Active','approval_by' => $this->session_data['id']);
			$this->CorModel->update($param);
			$password = $this->randomPassword(8);
			
			$param['where'] = array('store_key'=>$data[0]['store_key']);
			$param['table'] ="login_credential";
			$ldata = $this->CorModel->getRecords($param);
			if(count($ldata)==0)
			{
				$param['data'] =  array('user_name'=>$data[0]['store_name'], 'email'=>$data[0]['contact_email'], 'password'=>sha1($password), 'role_id' => '3', 'status'=>'1', 'store_key'=>$data[0]['store_key']);
				$this->CorModel->insert($param);
				 //Load email library 
				$this->load->library('email'); 
				$from_email = "seals@navavepar.com";
				$this->email->from($from_email, 'Nava Vepar'); 
				$this->email->to($data[0]['contact_email']);
				$this->email->subject('Store create successfully'); 
				$data['info'] = $data; 
				$data['info']['password'] = $password; 
				
				$message=$this->load->view('emailtemplate/create_store',$data,TRUE);
				
				$this->email->message($message);
				$this->email->send();		
			}
			
			//echo $this->CorModel->lastQuery();exit;
		}
		//$this->CorModel->update($param);
		//redirect('store/');
	}
	function randomPassword($num) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $num; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
    public function addStore()
    {
        $param['table'] ="store_category";
		$this->data['catlist'] = $this->CorModel->getRecords($param);
        $this->load->template('addstore',$this->data);
    } 
	public function insertstore()
    {
		$this->form_validation->set_rules('storename', 'Store Name', 'trim|required');
		$this->form_validation->set_rules('storecategory', 'Number', 'trim|required');
		 if($this->form_validation->run() == FALSE)
		{
			$param['table'] ="store_category";
			$this->data['catlist'] = $this->CorModel->getRecords($param);
			$this->load->template('addstore',$this->data);
		}
		else
		{
			$param['table'] ="store_information";
			$param['field'] ="store_id";
			
			$sdata = $this->CorModel->lastrecord($param);
			//print_r($sdata);exit;
			
			$key = explode('NVDS',$sdata[0]['store_key']);
			if($key[1] != "0")
			{	
			$storekey ='NVDS'. ($key[1] + 1); 
			}
			else{
				$storekey = 'NVDS1101'; 
			}
			$config['upload_path'] = './assets/images/store/';
			$config['file_name']=$storekey;
			$config['allowed_types'] = 'gif|jpg|png';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('storelogo');
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
	 
			$param['data'] =array(	
												'store_key'			=>			$storekey, 
												'store_name'		=>			$this->input->post('storename'), 
												'store_logo'		=>			$imgname, 
												'store_category'	=>			$this->input->post('storecategory'), 
												'store_address'		=>			$this->input->post('storeaddress'), 
												'store_city'		=>			$this->input->post('storecity'), 
												'store_zipcode'		=>			$this->input->post('storezip'), 
												'contact_person'	=>			$this->input->post('contactper'), 
												'contact_email'		=>			$this->input->post('contactemail'), 
												'contact_number'	=>			$this->input->post('contactnum'), 
												'create_date'		=>			date('Y-m-d'),  
												'status'			=>			'Inactive', 
												'store_create_by'	=>			$this->session_data['id']);
			
			$result = $this->CorModel->insert($param);
			
				if($result == true)
					{
						$vars['msg']=array("success"=>MSG_UPDATE);
					}
					else{
						$vars['msg']=array("error"=>MSG_UPDATE_ERR);
					}
			 redirect('store/storelist');
		}		 
    }
	public function updatestore()
    {
		$id = $this->input->post('id');
		$param['table'] ="store_information";
		
        $param['where'] =array("store_id"=>$id);
        $param['data'] =array(	
												
												'store_name'		=>			$this->input->post('storename'), 
												'store_category'	=>			$this->input->post('storecategory'), 
												'store_address'		=>			$this->input->post('storeaddress'), 
												'store_city'		=>			$this->input->post('storecity'), 
												'store_zipcode'		=>			$this->input->post('storezip'), 
												'contact_person'	=>			$this->input->post('contactper'), 
												'contact_number'	=>			$this->input->post('contactnum'));
			
			$result = $this->CorModel->update($param);
				if($result == true)
					{
						$vars['msg']=array("success"=>MSG_UPDATE);
					}
					else{
						$vars['msg']=array("error"=>MSG_UPDATE_ERR);
					}
					$this->session->set_flashdata('msg',$vars['msg']);
			 redirect('store/storelist');
	
		
    }
	public function storeCategoryList()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="store_category";
			$param['where'] =array('status !=' => 'Drop');
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "store/storeCategoryList/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('storecategory',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function categoryStatus()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="store_category";
        $param['where'] =array("id"=>$id);
		$data = $this->CorModel->getRecords($param);
		if($data[0]['status'] == "Active")
		{
			$param['data'] = array("status"=>'Inactive');
		}
		else
		{
			$param['data'] = array("status"=>'Active');
		}
		$this->CorModel->update($param);
		redirect('store/storeCategoryList');
	}
	 public function addCategory()
    {
       
        $this->form_validation->set_rules('category_name', 'Store Category', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
            $cname = $this->input->post("category_name");
            $params['table']="store_category";
            $params['data']=array("category_name"=>$cname,"status"=>'Yes');
            $result =$this->CorModel->insert($params);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
        }
        else
        {
            
            	$this->load->template('storecategory',$this->data);
        }
        redirect('store/storeCategoryList');
    }
	 
   
	public function deleteStoreCategory()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="store_category";
        $param['where'] =array("id"=>$id);
		$param['data'] = array("status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}
    
	//webcredantial

	public function webCredential()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="store_information";
			$param['where'] =array('status !=' => 'Drop',"webservices"=>"");	
			$this->data['storelist']=$this->CorModel->getRecords($param);	
			$param['table'] ="store_webservices_credential";
			$param['where'] =array('status !=' => 'Drop');
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "store/webCredential/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('webCredential',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function webCredentialStatus()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="store_webservices_credential";
        $param['where'] =array("id"=>$id);
		$data = $this->CorModel->getRecords($param);
		if($data[0]['status'] == "Active")
		{
			$param['data'] = array("status"=>'Inactive');
		}
		else
		{
			$param['data'] = array("status"=>'Active');
		}
		$this->CorModel->update($param);
		redirect('store/webCredential');
	}
	 public function addwebCredential()
    {
       
        $this->form_validation->set_rules('storekey', 'Store Name', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
            $skey = $this->input->post("storekey");
			$api_key = $this->randomPassword(6);
			$secret_key = $this->randomPassword(16);
			
            $params['table']="store_webservices_credential";
            $params['data']=array("api_key"=>$api_key,"secret_key"=>$secret_key,"status"=>'Active',"store_key"=>$skey);
            $result =$this->CorModel->insert($params);
			
			$params['table']="store_information";
            $params['data']=array("webservices"=>"Yes");
            $params['where']=array("store_key"=>$skey);
            $this->CorModel->update($params);
			
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
        }
        else
        {
            
            	$this->load->template('storecategory',$this->data);
        }
        redirect('store/webCredential');
    }
	 
   
	public function deletewebCredential()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="store_webservices_credential";
        $param['where'] =array("id"=>$id);
		$param['data'] = array("status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}
    	
}
