<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteUsers extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct()
	{
		parent::__construct();

		$this->session_data=$this->load->isSignedIn();	
		//print_r($this->session_data);exit;
	}
	function _remap($param)
	{
		 $this->index($param);
	}
	public function index()
	{
		$this->load->library("pagination");
		try{
			$this->load->model('SiteUsermodel');
			$skey = $this->session_data['store_key'];
			$config = array();
			$config["base_url"] = base_url() . "SiteUsers";
			$config["total_rows"] = COUNT($this->SiteUsermodel->userdetail('',$skey));
			$config["per_page"] = 10;
			$config["uri_segment"] = 2;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$param['skey'] = $skey;
			$productdata['list'] = $this->SiteUsermodel->paginationdata($param);			
			$productdata["links"] = $this->pagination->create_links();
			// For Pagination
			//$productdata['list']=$this->productmodel->productdetail('',$skey);
			
			$this->load->template('SiteUsers',$productdata);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}				
	}
	
}