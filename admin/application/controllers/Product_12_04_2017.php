<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct()
	{
		parent::__construct();

		$this->session_data=$this->load->isSignedIn();	
		//print_r($this->session_data);exit;
	}
	 
	public function productList()
	{
	 $this->load->library("pagination");
		try{
				$this->load->model('productmodel');
			$skey = $this->session_data['store_key'];
			$productdata['list']=$this->productmodel->productdetail('',$skey);
	
			$this->load->template('productlist',$productdata);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}				
	}
	public function productStatus()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="product_tbl";
        $param['where'] =array("pro_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		if($data[0]['pro_status'] == "Active")
		{
			$param['data'] = array("pro_status"=>'Inactive');
		}
		else
		{
			$param['data'] = array("pro_status"=>'Active');
		}
		$this->CorModel->update($param);
		//echo $this->CorModel->last_query();exit;
		redirect('store/storeCategoryList');
	}	
	public function productAdd()
	{
		$where=array("parent_id"=>0);
			$id="2";
			$wherepro=array("pro_cat"=>$id);
			$this->load->model('productmodel');
			$categorydata['cat']=$this->productmodel->maincategory('category_tbl',$where);
			//$categorydata['brand']=$this->productmodel->viewbrand('manufacturer');
			$categorydata['code']=$this->productmodel->code('product_tbl');
		$this->load->template('productadd',$categorydata);
	}
	function get_sub_category()
	{
		
			$this->load->model('productmodel');
		
			if ($catid = $this->input->post('parent_id'))
			{
				$catid = $this->input->post('parent_id');
				$where=array("parent_id"=>$catid);
				$query = $this->productmodel->get_sub_cat('category_tbl',$where);
				
				header('Content-Type: application/json');
				echo  json_encode($query);
			} 
			else
			{
				redirect('product/');
    		}

	}
	function insertproduct()
	{
			date_default_timezone_set('Asia/Calcutta');
			
			
					$pcode=$this->input->post("productCode");
					$cat=$this->input->post("Category");
					$proname=$this->input->post("productName");
					$brandname=$this->input->post("brandName");
					$tax=$this->input->post("tax");
					$dispdate=$this->input->post("dispdate");
					
					
						$imgn=url_title($proname, 'dash', true);
							$config['upload_path'] = './assets/images/product/';
							$config['file_name']=$imgn;
							$config['allowed_types'] = 'gif|jpg|png';
							
							$this->load->library('upload', $config);
							$this->upload->do_upload('pimg');
							$data = array('upload_data' => $this->upload->data());
							$imgname=$data['upload_data']['file_name'];	
						//	$buffer = file_get_contents($url);
					
				 		$filename=$imgn.".jpg";
					
					
					
					//$relpro=$this->input->post("relpro");
					
					
					//$relpros=implode(",",$relpro);
				
					$pro=array(	
								"pro_code"=>$pcode,
								"pro_cat"=>$cat,
								"pro_name"=>$proname,
								"brand"=>$brandname,
								"pro_tax"=>$tax,
								"pro_display_date"=>$dispdate,
								"pro_image"=>$filename,
								"pro_status"=>"Active",
								"add_date"=>date("Y-m-d h:i:s"),
								"modify_date"=>date("Y-m-d h:i:s"),
								"store_key"=>$this->session_data['store_key']
								);
					
						$this->load->model("productmodel");
					$insertid=$this->productmodel->insertpro("product_tbl",$pro);
					
					$mtitle=$this->input->post("metatagTitle");
					$mdesc=$this->input->post("metatagDescription");
					$mkeyword=$this->input->post("metatagKeywords");
					$detail=$this->input->post("prodetail");
					
						$prodetail =array("pro_id"=>$insertid,
											"m_title"=>$mtitle,
											"m_desc"=>$mdesc,
											"m_keyword"=>$mkeyword,
											"pro_detail"=>$detail,
											"add_date"=>date("Y-m-d h:i:s"),
											"modify_date"=>date("Y-m-d h:i:s")
										);
					$this->productmodel->addpro("prodcut_detail_tbl",$prodetail);
									
					$unit=$this->input->post("unit");
					$no_unit=$this->input->post("no_unit");
					$mrp=$this->input->post("mrp");
					$purchase=$this->input->post("purchase");
					//$profit=$this->input->post("profit");
					$seal=$this->input->post("seal");
					$qty=$this->input->post("qty");
					
					
					$sno=count($unit);
					for($i=0;$i<$sno;$i++)
					{
						
						if($no_unit[$i]!=0)
						{
						$proprice=array("pro_id"=>$insertid,
									"pro_unit"=>$unit[$i],
									"pro_no_unit"=>$no_unit[$i],
									"pro_mrp_price"=>$mrp[$i],
									"pro_purchase_price"=>$purchase[$i],
									//"pro_profit_price"=>$profit[$i],
									"pro_sale_price"=>$seal[$i],
									"pro_qty"=>$qty[$i],
									"add_dat"=>date("Y-m-d h:i:s"),
									"modify_date"=>date("Y-m-d h:i:s")
									);
						$this->productmodel->addpro("product_price_tbl",$proprice);
						}
						
					}
					redirect('product/productList');
				
			
	
	}
	public function productCategoryList()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="category_tbl";
			$param['where'] =array('cat_status  !=' => 'Drop');
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "product/productCategoryList/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('categorylist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function categoryStatus()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="category_tbl";
			$param['where'] =array("cat_id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['cat_status'] == "Active")
			{
				$param['data'] = array("cat_status"=>'Inactive');
			}
			else
			{
				$param['data'] = array("cat_status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('product/productCategoryList');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}
	 public function addProductCategory()
    {
       
        $this->form_validation->set_rules('categoryname', 'Category Name', 'trim|required');
        $this->form_validation->set_rules('mainCategory', 'Parent Category', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
		
            $cat_name = $this->input->post("categoryname");
            $parent_id = $this->input->post("mainCategory");
			$config['upload_path'] = './assets/images/category/';
			$config['file_name']=url_title($cat_name, 'dash', true);
			$config['allowed_types'] = 'gif|jpg|png';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('img');
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
            $cat_image = $imgname;
            $cat_status = "Active";
            $user_id = $this->session_data['id'];
            //$cat_description = $this->input->post("categoryname");
            $meta_tag_title = $this->input->post("metatagTitle");
            $meta_tag_description = $this->input->post("metatagDescription");
            $meta_tag_keywords = $this->input->post("metatagKeywords");
			$store_key = $this->session_data['store_key'];
			$param['table'] ="category_tbl";
            $param['data']=array(	"cat_name"			=>		$cat_name,
									"user_id"			=>		$user_id,
									"parent_id"			=>		$parent_id,
									"cat_image"			=>		$cat_image,
									"cat_status"		=>		$cat_status,
									"meta_tag_title"	=>		$meta_tag_title,
									"meta_tag_description"=>	$meta_tag_description,
									"meta_tag_keywords"	=>		$meta_tag_keywords,
									"store_key"			=>		$store_key,
									"menu"				=> 		"0"
									);
            $result =$this->CorModel->insert($param);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			redirect('product/productCategoryList');
        }
        else
        {
			
				$param['table'] ="category_tbl";
				$param['where'] =array("parent_id"=>"0");
				$this->data['catlist'] = $this->CorModel->getRecords($param);
            	$this->load->template('addproductcategory',$this->data);
        }
        
    }
	public function deleteCategory()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="category_tbl";
			$param['where'] =array("cat_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		
		$param['data'] = array("cat_status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}	
	
	public function manufactureList()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="manufacturer";
			$param['where'] =array('m_status  !=' => 'Drop');
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "product/productManufactureList/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('manufacturelist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function manufacturerStatus()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="manufacturer";
			$param['where'] =array("m_id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['m_status'] == "Active")
			{
				$param['data'] = array("m_status"=>'Inactive');
			}
			else
			{
				$param['data'] = array("m_status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('product/manufactureList');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}
	 public function addProductManufacturer()
    {
       
        $this->form_validation->set_rules('m_name', 'Manufacturer Name', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
		
            $m_name = $this->input->post("m_name");
			$config['upload_path'] = './assets/images/Manufacturer/';
			$config['file_name']=url_title($cat_name, 'dash', true);
			$config['allowed_types'] = 'gif|jpg|png';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('img');
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
            $m_image = $imgname;
            $m_status = "Active";
			$store_key = $this->session_data['store_key'];
			$param['table'] ="manufacturer";
            $param['data']=array(	"m_name"			=>		$m_name,
									"m_image"			=>		$m_image,
									"m_status"			=>		$m_status,
									"store_key"			=>		$store_key
									);
            $result =$this->CorModel->insert($param);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			redirect('product/manufactureList');
        }
        else
        {			
			$this->load->template('addproductmanufacturer');
        }
        
    }
	public function deleteManufacturer()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="manufacturer";
			$param['where'] =array("m_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		
		$param['data'] = array("m_status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}	
	
	
	public function bannerList()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="banner";
			$param['where'] =array('status  !=' => 'Drop');
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "product/productBannerList/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('bannerlist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function bannerStatus()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="banner";
			$param['where'] =array("banner_id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['status'] == "Active")
			{
				$param['data'] = array("status"=>'Inactive');
			}
			else
			{
				$param['data'] = array("status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('product/bannerList');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}
	 public function addProductBanner()
    {
       
        $this->form_validation->set_rules('main_title', 'Banner Title', 'required');
		
        if($this->form_validation->run() != FALSE)
  		{
		
            $m_name = $this->input->post("m_name");
			$config['upload_path'] = './assets/images/banner/';
			$config['file_name']=url_title($cat_name, 'dash', true);
			$config['allowed_types'] = 'gif|jpg|png';
			$config['min_width']     = '1170';
			$config['min_height']    = '520';

			$this->load->library('upload', $config);
			$this->upload->do_upload('img');
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
            $img = $imgname;
            $bindex = $this->input->post("bindex");
            $sort_title = $this->input->post("sort_title");
            $main_title = $this->input->post("main_title");
            $sale = $this->input->post("sale");
            $status = "Active";
			$store_key = $this->session_data['store_key'];
			$param['table'] ="banner";
            $param['data']=array(	"img"				=>		$img,
									"bindex"			=>		$bindex,
									"sort_title"		=>		$sort_title,
									"main_title"		=>		$main_title,
									"sale"				=>		$sale,
									"status"			=>		$status,
									"store_key"			=>		$store_key
									);
            $result =$this->CorModel->insert($param);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			redirect('product/bannerList');
        }
        else
        {			
			$this->load->template('addproductbanner');
        }
        
    }
	public function deleteBanner()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="banner";
		$data = $this->CorModel->getRecords($param);
			$param['where'] =array("banner_id"=>$id);
		
		$param['data'] = array("status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}
}