<?php
class SoapserversAction extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		
	 $ns = 'http://' . $_SERVER['HTTP_HOST'] . '/SoapserversAction/';
        $this->load->library("nusoap_library"); // load nusoap toolkit library in controller
        $this->load->library("pro");
        $this->prolib        = new pro();
        // load nusoap toolkit library in controller
        $this->nusoap_server = new soap_server(); // create soap server object
        $this->nusoap_server->configureWSDL("Document", "urn:Document"); // wsdl cinfiguration
        $this->nusoap_server->wsdl->schemaTargetNamespace = $ns; // server namespace
        
        $input_array  = array(
            'param' => "xsd:string"
        ); // "addnumbers" method parameters
        $return_array = array(
            "return" => "xsd:string"
        );
        $this->nusoap_server->register('document', $input_array, $return_array);
        $this->nusoap_server->register('instDocument', $input_array, $return_array);
       
    }
    function index()
    {
        function getdata($dbparma)
        {
			    
            $CI =& get_instance();
            $query  = $CI->db->get_where($dbparma['table'], $dbparma['where']);
            $result = $query->result_array();
            return $result;
        }
		function insertData($dbparma)
		{
			$CI =& get_instance();
			
            $CI->db->insert($dbparma['table'], $dbparma['data']);
            $insert_id = $CI->db->insert_id();
            return $insert_id;	
		}
		function updateData($dbparma)
		{
			//return $dbparma;
			$CI =& get_instance();
			
				$result = $CI->db->update($dbparma['table'], $dbparma['data'],$dbparma['where']);
            if($result){
            return true;
			}else{ return false; }			
		}
		function checkcredential($k)
        {
			return json_encode($k);
            $a                = explode("|", unserialize(gzuncompress(base64_decode($k))));
            $dbparma['table'] = "store_webservices_credential";
            $dbparma['where'] = array(
                "api_key" => $a[0],
                "secret_key" => $a[1],
                "store_key" => $a[2]
            );
            $result           = $this->getdata($dbparma);
            if (!empty($result)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
		function document($param)
        {
			
            $params = json_decode($param);
            $k      = $params->akey;
			
			$result =checkcredential($k);
            if (empty($result)) {
                return "Invalid Authentication";
            } else {
				//return json_encode($params);
                $stkey            = $params->where;
				$updata  			=$params->urec;
                $dbparma['table'] = $params->table;
				
                $dbparma['where'] = (array)$stkey;
				$dbparma['data'] = (array)$updata;
				
                $result           = updateData($dbparma);
				
				if($result){
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "true",
						"detail" => $result
					));
				}else{
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "false",
						"detail" => ""
					));
				}				
                return $data;
            }
        }
		function instDocument($param){
		
			$params = json_decode($param);
            $k      = $params->akey;
			$result =checkcredential($k);
			
			if (empty($result)) {
                return "Invalid Authentication";
            } else {
				
				$dbparma['table'] = $params->table;
			   	$dbparma['data'] = $params->shipping;;
				
				$result           = insertData($dbparma);
				
				if($result){
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "true",
						"detail" => $result
					));
				}else{
				//return json_encode($CI->db->last_query());
					$data             = json_encode(array(
						"response" => "false",
						"detail" => ""
					));
				}				
                return $data;
			}	
		}
        $this->nusoap_server->service(file_get_contents("php://input")); // read raw data from request body
    }
}
?>