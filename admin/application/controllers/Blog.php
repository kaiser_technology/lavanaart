<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->session_data=$this->load->isSignedIn();	
		//print_r($this->session_data);exit;
	}
	public function index()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="blog";
			
			$param['where'] =array('status  !=' => 'Deactive');
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "blog/index/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('bloglist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function add()
    {
		  $this->form_validation->set_rules('blog_name', 'Blog Name', 'required');
		  $this->form_validation->set_rules('sortdesc', 'Sort Description', 'required');
		  
        if($this->form_validation->run() != FALSE)
  		{
		
            $b_name = $this->input->post("short_name");
			$config['upload_path'] = './assets/images/blog/';
			$config['file_name']=url_title($b_name, 'dash', true);
			$config['allowed_types'] = 'gif|jpg|png';
			$config['min_width']     = '1170';
			$config['min_height']    = '520';
			$imgn=url_title($b_name, 'dash', true);
			$this->load->library('upload', $config);
			$path = FCPATH.'assets/images/blog/';
			//$this->upload->do_upload('img');
			move_uploaded_file($_FILES["img"]["tmp_name"], $path . $imgn.".jpg");
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
            $img=$imgn.".jpg";
			
			if(file_exists($_FILES['img1']['tmp_name'])){
				move_uploaded_file($_FILES["img1"]["tmp_name"], $path . $imgn."1.jpg");
				$filename1=$imgn."1.jpg";
			}
			
			if(file_exists($_FILES['img2']['tmp_name'])){
				move_uploaded_file($_FILES["img2"]["tmp_name"], $path . $imgn."2.jpg");
				$filename2=$imgn."2.jpg";
			}
			
			if(file_exists($_FILES['img3']['tmp_name'])){
				move_uploaded_file($_FILES["img3"]["tmp_name"], $path . $imgn."3.jpg");
				$filename3=$imgn."3.jpg";
			}
			
			if(file_exists($_FILES['img4']['tmp_name'])){
				move_uploaded_file($_FILES["img4"]["tmp_name"], $path . $imgn."4.jpg");
				$filename4=$imgn."4.jpg";
			}
			if(file_exists($_FILES['img5']['tmp_name'])){
				move_uploaded_file($_FILES["img4"]["tmp_name"], $path . $imgn."5.jpg");
				$filename5=$imgn."5.jpg";
			}
			if(file_exists($_FILES['img6']['tmp_name'])){
				move_uploaded_file($_FILES["img6"]["tmp_name"], $path . $imgn."6.jpg");
				$filename6=$imgn."7.jpg";
			}
			if(file_exists($_FILES['img7']['tmp_name'])){
				move_uploaded_file($_FILES["img7"]["tmp_name"], $path . $imgn."7.jpg");
				$filename7=$imgn."7.jpg";
			}
			
			
			
            $sortdesc = $this->input->post("sortdesc");
            $blog_name=$this->input->post("blog_name");
            $status = "Active";
			$description = $this->input->post("editor1");
			$param['table'] ="blog";
            $param['data']=array(	
									"blogName"			=>		$blog_name,
									"sortDesc"		=>		$sortdesc,
									"mainImage"		=>		$img,
									"img1"	=> $filename1,
									"img2"	=> $filename2,
									"img3"	=> $filename3,
									"img4"	=> $filename4,
									"img5"	=> $filename5,
									"img6"	=> $filename6,
									"img7"	=> $filename7,
									"status"			=>		$status,
									'description'	=> $description,
									);
            $result =$this->CorModel->insert($param);
		
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			redirect('blog');
        }
        else
        {			
			$this->load->template('addblog');
        }
	}		
	public function status()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="blog";
			$param['where'] =array("id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['status'] == "Active")
			{
				$param['data'] = array("status"=>'Deactive');
			}
			else
			{
				$param['data'] = array("status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('blog/');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	
	
	}
	public function bdelete()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="blog";
		$data = $this->CorModel->getRecords($param);
			$param['where'] =array("id"=>$id);
		
		$param['data'] = array("status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}
}
?>