<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct()
	{
		parent::__construct();

		$this->session_data=$this->load->isSignedIn();	
		//print_r($this->session_data);exit;
	}
		
	public function blist()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="banner";
			$skey = $this->session_data['store_key'];
			$param['where'] =array('status  !=' => 'Drop','store_key ='=> $skey);
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "banner/blist/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$param['skey'] = $skey;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('bannerlist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function status()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="banner";
			$param['where'] =array("banner_id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['status'] == "Active")
			{
				$param['data'] = array("status"=>'Inactive');
			}
			else
			{
				$param['data'] = array("status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('Banner/blist');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}
	 public function add()
    {
       
        $this->form_validation->set_rules('main_title', 'Banner Title', 'required');
		
        if($this->form_validation->run() != FALSE)
  		{
		
            $m_name = $this->input->post("main_title");
			$config['upload_path'] = './assets/images/banner/';
			$config['file_name']=url_title($m_name, 'dash', true);
			$config['allowed_types'] = 'gif|jpg|png';
			$config['min_width']     = '1170';
			$config['min_height']    = '520';
			$imgn=url_title($m_name, 'dash', true);
			$this->load->library('upload', $config);
			$path = FCPATH.'assets/images/banner/';
			//$this->upload->do_upload('img');
			move_uploaded_file($_FILES["img"]["tmp_name"], $path . $imgn.".jpg");
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
            $img=$imgn.".jpg";
            $bindex = $this->input->post("bindex");
            $sort_title = $this->input->post("sort_title");
            $type = $this->input->post("type");
            $url = $this->input->post("url");
            $main_title = $this->input->post("main_title");
            $sale = $this->input->post("sale");
            $status = "Active";
			$store_key = $this->session_data['store_key'];
			$param['table'] ="banner";
            $param['data']=array(	"img"				=>		$img,
									"bindex"			=>		$bindex,
									"sort_title"		=>		$sort_title,
									"bannerType"		=>		$type,
									"url"		=>		$url,
									"main_title"		=>		$main_title,
									"sale"				=>		$sale,
									"status"			=>		$status,
									"store_key"			=>		$store_key
									);
            $result =$this->CorModel->insert($param);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			redirect('Banner/blist');
        }
        else
        {			
			$this->load->template('addbanner');
        }
        
    }
	public function bdelete()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="banner";
		$data = $this->CorModel->getRecords($param);
			$param['where'] =array("banner_id"=>$id);
		
		$param['data'] = array("status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}
}