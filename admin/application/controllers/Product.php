<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct()
	{
		parent::__construct();

		$this->session_data=$this->load->isSignedIn();	
		//print_r($this->session_data);exit;
	}
	 
	public function productList()
	{
	 $this->load->library("pagination");
		try{
			$this->load->model('productmodel');
			$skey = $this->session_data['store_key'];
			$config = array();
			$config["base_url"] = base_url() . "product/productList/";
			$config["total_rows"] = COUNT($this->productmodel->productdetail('',$skey));
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$param['skey'] = $skey;
			$productdata['list'] = $this->productmodel->paginationdata($param);			
			$productdata["links"] = $this->pagination->create_links();
			// For Pagination
			//$productdata['list']=$this->productmodel->productdetail('',$skey);
			
			$this->load->template('productlist',$productdata);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}				
	}
	public function productStatus()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="product_tbl";
        $param['where'] =array("pro_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		if($data[0]['pro_status'] == "Active")
		{
			$param['data'] = array("pro_status"=>'Inactive');
		}
		else
		{
			$param['data'] = array("pro_status"=>'Active');
		}
		$this->CorModel->update($param);
		//echo $this->CorModel->last_query();exit;
		redirect('store/storeCategoryList');
	}	
	public function deleteProduct()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="product_tbl";
			$param['where'] =array("pro_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		
		$param['data'] = array("pro_status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	} 
	public function productAdd()
	{
		$where=array("parent_id"=>0,"cat_status"=>"Active", "store_key"=>$this->session_data['store_key']);			
			$this->load->model('productmodel');
			$categorydata['cat']=$this->productmodel->maincategory('category_tbl',$where);
			//$categorydata['brand']=$this->productmodel->viewbrand('manufacturer');
			$categorydata['code']=$this->productmodel->code('product_tbl');
		$this->load->template('productadd',$categorydata);
	}
	function get_sub_category()
	{
		
			$this->load->model('productmodel');
		
			if ($catid = $this->input->post('parent_id'))
			{
				$catid = $this->input->post('parent_id');
				$where=array("parent_id"=>$catid,"cat_status"=>"Active");
				$query = $this->productmodel->get_sub_cat('category_tbl',$where);
				
				header('Content-Type: application/json');
				echo  json_encode($query);
			} 
			else
			{
				redirect('product/');
    		}

	}
	function insertproduct()
	{
		$postdata = $this->input->post();
		date_default_timezone_set('Asia/Calcutta');
			
			
					$pcode=$this->input->post("productCode");
					$cat=$this->input->post("Category");
					$subcat=$this->input->post("subcategory");
					$proname=$this->input->post("productName");
					$brandname=$this->input->post("brandName");
					$tax=$this->input->post("tax");
					$wrhouse=$this->input->post("wrhouse");
					$dispdate=$this->input->post("dispdate");
					$gstrate=$this->input->post("gstrate");
					$dhome=$this->input->post("home");
					
					
						$imgn=url_title($proname, 'dash', true);
							$config['upload_path'] = './assets/images/product/';
							$config['file_name']=$imgn;
							$config['allowed_types'] = 'gif|jpg|png';
							$path = FCPATH.'assets/images/product/';
							$this->load->library('upload', $config);
							
							 move_uploaded_file($_FILES["pimg"]["tmp_name"], $path . $imgn.".jpg");
							//$this->upload->do_upload('pimg');
							$data = array('upload_data' => $this->upload->data());
							$imgname=$data['upload_data']['file_name'];	
						//	$buffer = file_get_contents($url);
					
				 		$mainimage=$imgn.".jpg";
				 		//$filename="";
				 		
					
						
				 		if(file_exists($_FILES['pimg1']['tmp_name'])){
							move_uploaded_file($_FILES["pimg1"]["tmp_name"], $path . $imgn."1.jpg");
							$filename1=$imgn."1.jpg";
						}
						
						if(file_exists($_FILES['pimg2']['tmp_name'])){
							move_uploaded_file($_FILES["pimg2"]["tmp_name"], $path . $imgn."2.jpg");
							$filename2=$imgn."2.jpg";
						}
						
						if(file_exists($_FILES['pimg3']['tmp_name'])){
							move_uploaded_file($_FILES["pimg3"]["tmp_name"], $path . $imgn."3.jpg");
							$filename3=$imgn."3.jpg";
						}
						
						if(file_exists($_FILES['pimg4']['tmp_name'])){
							move_uploaded_file($_FILES["pimg4"]["tmp_name"], $path . $imgn."4.jpg");
							$filename4=$imgn."4.jpg";
						}
						
					
					//$relpro=$this->input->post("relpro");
					
					
					//$relpros=implode(",",$relpro);
						if($subcat !=""){$cat = $subcat;}
					$pro=array(	
								"pro_code"=>$pcode,
								"pro_cat"=>$cat,
								"pro_name"=>$proname,
								"brand"=>$brandname,
								"pro_tax"=>$tax,
								"wrhouse"=>$wrhouse,
								"pro_display_date"=>$dispdate,
								"pro_image"=>$mainimage,
								"pro_image1"=>$filename1,
								"pro_image2"=>$filename2,
								"pro_image3"=>$filename3,
								"pro_image4"=>$filename4,
								"related_pro"=>$gstrate,
								"pro_status"=>"Active",
								"displayOnHome"=>$dhome,
								"add_date"=>date("Y-m-d h:i:s"),
								"modify_date"=>date("Y-m-d h:i:s"),
								"store_key"=>$this->session_data['store_key']
								);
					
						$this->load->model("productmodel");
					$insertid=$this->productmodel->insertpro("product_tbl",$pro);
					
					$mtitle=$this->input->post("metatagTitle");
					$mdesc=$this->input->post("metatagDescription");
					$mkeyword=$this->input->post("metatagKeywords");
					$detail=$this->input->post("productDesc");
					
						$prodetail =array("pro_id"=>$insertid,
											"m_title"=>$mtitle,
											"m_desc"=>$mdesc,
											"m_keyword"=>$mkeyword,
											"pro_detail"=>$detail,
											"add_date"=>date("Y-m-d h:i:s"),
											"modify_date"=>date("Y-m-d h:i:s")
										);
					$this->productmodel->addpro("prodcut_detail_tbl",$prodetail);
									
					$unit=$this->input->post("unit");
					$no_unit=$this->input->post("no_unit");
					$mrp=$this->input->post("mrp");
					$purchase=$this->input->post("purchase");
					//$profit=$this->input->post("profit");
					$seal=$this->input->post("seal");
					$qty=$this->input->post("qty");
					
					
					$sno=count($unit);
					for($i=0;$i<$sno;$i++)
					{
						
						if($no_unit[$i]!=0)
						{
						$proprice=array("pro_id"=>$insertid,
									"pro_unit"=>$unit[$i],
									"pro_no_unit"=>$no_unit[$i],
									"pro_mrp_price"=>$mrp[$i],
									//"pro_purchase_price"=>$purchase[$i],
									"pro_purchase_price"=>'100',
									//"pro_profit_price"=>$profit[$i],
									"pro_sale_price"=>($seal[$i])+($seal[$i]*$gstrate/100),
									"pro_qty"=>$qty[$i],
									"add_dat"=>date("Y-m-d h:i:s"),
									"modify_date"=>date("Y-m-d h:i:s")
									);
						$this->productmodel->addpro("product_price_tbl",$proprice);
						}
						
						//print_r($insertid);
						//exit;
						
					}
					redirect('product/productList');
				
			
	
	}
	function removePrice()
	{
		$postdata = $this->input->post();
		$this->db->where("pro_price_id",$postdata['pid'])->delete("product_price_tbl");
	}
	function editCat($cat_id)
	{
		//dump($cat_id);
		$this->form_validation->set_rules('categoryname', 'Category Name', 'trim|required');
	        //$this->form_validation->set_rules('mainCategory', 'Parent Category', 'trim|required');
	        if($this->form_validation->run() != FALSE)
	  	{
			//dump($this->input->post(),1);	
	            $cat_name = $this->input->post("categoryname");
	            //$parent_id = $this->input->post("mainCategory");
	            //$sub_id = $this->input->post("Category");
	            $filename = $this->input->post("old_img");
	            	if(isset($_FILES['img']['name']) && !empty($_FILES['img']['name']))
	            	{
				//echo FCPATH .'./assets/images/category/';exit;
				$config['upload_path'] = './assets/images/category/';
				$config['file_name']=url_title($cat_name, 'dash', true);
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']             = 100;
				$config['max_width']            = 1024;
				$config['max_height']           = 768;
				$this->load->library('upload', $config);
				//print_r($this->upload->do_upload);exit;
				//$this->upload->do_upload('img');
				$path=FCPATH .'assets/images/category/';
				$cat_image = url_title($cat_name, 'dash', true);
				 move_uploaded_file($_FILES["img"]["tmp_name"], $path . $cat_image.".jpg");
				/*if ($this->upload->do_upload('img'))
	             {
				$data = array('upload_data' => $this->upload->data());
				$imgname=$data['upload_data']['file_name'];	
	            $cat_image = $imgname;
				}*/
				$filename = $cat_image.'.jpg';
			}
	            $cat_status = "Active";
	            $user_id = $this->session_data['id'];
			
				
	            //$cat_description = $this->input->post("categoryname");
	            $meta_tag_title = $this->input->post("metatagTitle");
	            $meta_tag_description = $this->input->post("metatagDescription");
	            $meta_tag_keywords = $this->input->post("metatagKeywords");
		   $store_key = $this->session_data['store_key'];
				
				//if($sub_id != ""){$parent_id = $sub_id; }
			$param['table'] ="category_tbl";
	            $param['data']=array(	"cat_name"			=>		$cat_name,
										"cat_image"			=>		$filename,
										"meta_tag_title"	=>		$meta_tag_title,
										"meta_tag_description"=>	$meta_tag_description,
										"meta_tag_keywords"	=>		$meta_tag_keywords,
										"menu"				=> 		"0"
										);
			$this->load->model("productmodel");					
	            $result = $this->productmodel->Updpro("category_tbl",$param['data'],"cat_id =".$this->input->post("exist_cid"));
	            if($result == true)
				{
					$this->data['msg']=array("success"=>MSG_UPDATE);
				}
				else{
					$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
				}
				
				redirect('product/categoryList');
	        }
	        else
	        {
				
			$param['table'] ="category_tbl";
			$param['where'] =array("parent_id"=>"0","cat_status"=>"Active");
			$this->data['catlist'] = $this->CorModel->getRecords($param);
			$this->data['catData'] = getRowDataSingle("select * from category_tbl where cat_id =".$cat_id);
	            	$this->load->template('editcategory',$this->data);
	        }
	}
	function updateProduct()
	{
		$postdata = $this->input->post();
		//dump($_FILES);
		//dump($postdata,1);
		date_default_timezone_set('Asia/Calcutta');
			
			
		$pcode=$this->input->post("productCode");
		$cat=$this->input->post("Category");
		$subcat=$this->input->post("subcategory");
		$proname=$this->input->post("productName");
		$brandname=$this->input->post("brandName");
		$tax=$this->input->post("tax");
		$wrhouse=$this->input->post("wrhouse");
		$gstrate=$this->input->post("gstrate");
		$dispdate=$this->input->post("dispdate");
		$dhome=$this->input->post("home");
		$filename = $postdata['pimg_old'];
		$filename1 = $postdata['pimg_old1'];
		$filename2 = $postdata['pimg_old2'];
		$filename3 = $postdata['pimg_old3'];
		$filename4 = $postdata['pimg_old4'];
		$imgn=url_title($proname, 'dash', true);
		$path = FCPATH.'assets/images/product/';
		if(isset($_FILES["pimg"]["name"]) && !empty($_FILES["pimg"]["name"]))
		{
			/*$imgn = url_title($proname, 'dash', true);
			echo $return_file = singleFile(FCPATH."assets/images/product","gif|jpg|png","pimg");die;
			$ext = end(explode(".",$return_file));
			$filename = $imgn.".".$ext;*/
			
			
			
			$config['upload_path'] = './assets/images/product/';
			$config['file_name']=$imgn;
			$config['allowed_types'] = 'gif|jpg|png';
			//$path = FCPATH.'assets/images/product/';
			$this->load->library('upload', $config);
			
			 move_uploaded_file($_FILES["pimg"]["tmp_name"], $path . $imgn.".jpg");
			//$this->upload->do_upload('pimg');
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
			//	$buffer = file_get_contents($url);
		
 			$mainimage = $imgn.".jpg";
 			//unlink(FCPATH.'assets/images/product/'.$postdata['pimg_old']);
			
		}
		//$relpro=$this->input->post("relpro");
		if(file_exists($_FILES['pimg1']['tmp_name'])){
			move_uploaded_file($_FILES["pimg1"]["tmp_name"], $path . $imgn."1.jpg");
			$filename1=$imgn."1.jpg";
		}
		
		if(file_exists($_FILES['pimg2']['tmp_name'])){
			move_uploaded_file($_FILES["pimg2"]["tmp_name"], $path . $imgn."2.jpg");
			$filename2=$imgn."2.jpg";
		}
		
		if(file_exists($_FILES['pimg3']['tmp_name'])){
			move_uploaded_file($_FILES["pimg3"]["tmp_name"], $path . $imgn."3.jpg");
			$filename3=$imgn."3.jpg";
		}
		
		if(file_exists($_FILES['pimg4']['tmp_name'])){
			move_uploaded_file($_FILES["pimg4"]["tmp_name"], $path . $imgn."4.jpg");
			$filename4=$imgn."4.jpg";
		}
		
		//$relpros=implode(",",$relpro);
			if($subcat !=""){$cat = $subcat;}
		$pro=array(	
					"pro_cat"=>$cat,
					"pro_name"=>$proname,
					"brand"=>$brandname,
					"pro_tax"=>$tax,
					"wrhouse"=>$wrhouse,
					"pro_display_date"=>$dispdate,
					"related_pro"=>$gstrate,
					"displayOnHome"=>$dhome,
					"pro_image"=>$filename,
					"pro_image1"=>$filename1,
					"pro_image2"=>$filename2,
					"pro_image3"=>$filename3,
					"pro_image4"=>$filename4,
					"modify_date"=>date("Y-m-d h:i:s"),
					);
		
		$this->load->model("productmodel");
		$this->productmodel->Updpro("product_tbl",$pro,"pro_id =".$postdata['exist_pro_id']);
		
		$mtitle=$this->input->post("metatagTitle");
		$mdesc=$this->input->post("metatagDescription");
		$mkeyword=$this->input->post("metatagKeywords");
		$detail=$this->input->post("productDesc");
		
			$prodetail =array("m_title"=>$mtitle,
					  "m_desc"=>$mdesc,
					  "m_keyword"=>$mkeyword,
					  "pro_detail"=>$detail,
					 "modify_date"=>date("Y-m-d h:i:s")
				);
		$this->productmodel->Updpro("prodcut_detail_tbl",$prodetail, "pro_id =".$postdata['exist_pro_id']);
						
		$unit=$this->input->post("unit");
		$no_unit=$this->input->post("no_unit");
		$mrp=$this->input->post("mrp");
		$purchase=$this->input->post("purchase");
		//$profit=$this->input->post("profit");
		$seal=$this->input->post("seal");
		$qty=$this->input->post("qty");
		
		
		$sno=count($unit);
		for($i=0;$i<$sno;$i++)
		{
			
			if($no_unit[$i]!=0)
			{
			$proprice = array("pro_id"=>$postdata['exist_pro_id'],
						"pro_unit"=>$unit[$i],
						"pro_no_unit"=>$no_unit[$i],
						"pro_mrp_price"=>$mrp[$i],
						"pro_purchase_price"=>$purchase[$i],
						//"pro_profit_price"=>$profit[$i],
						"pro_sale_price"=>($seal[$i])+($seal[$i]*$gstrate/100),
						"pro_qty"=>$qty[$i],
						"add_dat"=>date("Y-m-d h:i:s"),
						"modify_date"=>date("Y-m-d h:i:s")
						);
			$this->productmodel->addpro("product_price_tbl",$proprice);
			}
			
		}
		
		if(isset($postdata['unit_old']))
		{
			$k = 0;
			foreach($postdata['unit_old'] as $uo_val):
				$proprice = array("pro_unit"=>$uo_val,
						"pro_no_unit"=>$postdata['no_unit_old'][$k],
						"pro_mrp_price"=>$postdata['mrp_old'][$k],
						"pro_purchase_price"=>$postdata['purchase_old'][$k],
						"pro_sale_price"=>$postdata['seal_old'][$k],
						"pro_qty"=>$postdata['qty_old'][$k],
						"modify_date"=>date("Y-m-d h:i:s")
						);
			$this->productmodel->Updpro("product_price_tbl",$proprice, "pro_price_id =".$postdata['pro_price_id'][$k]);
			$k++;	
			endforeach;
		}
		redirect('product/productList');
	}
	public function editProduct($id){
		
		$where=array("parent_id"=>0,"cat_status"=>"Active", "store_key"=>$this->session_data['store_key']);	
					
		$this->load->model('productmodel');
		
		$proData = $this->productmodel->editProData($id);
		
		if($proData->first_parent)
		{
			$where1 = array("parent_id"=>$proData->first_parent,"cat_status"=>"Active", "store_key"=>$this->session_data['store_key']);	
			$categorydata['scat'] = $this->productmodel->maincategory('category_tbl',$where1);
			
			if($proData->second_parent)
			{
				$where2 = array("parent_id"=>$proData->second_parent,"cat_status"=>"Active", "store_key"=>$this->session_data['store_key']);	
				$categorydata['sscat'] = $this->productmodel->maincategory('category_tbl',$where2);
			}
			
		}
		
		
		$categorydata['editProData'] = $proData;
		$categorydata['cat'] = $this->productmodel->maincategory('category_tbl',$where);
		
		//$categorydata['brand']=$this->productmodel->viewbrand('manufacturer');
		$categorydata['code']=$this->productmodel->code('product_tbl');
		
		$this->load->template('productedit',$categorydata);
	}		
			
	public function categoryList()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$param['table'] ="category_tbl";
			$skey = $this->session_data['store_key'];
			$param['where'] =array('cat_status  !=' => 'Drop','store_key ='=> $skey);
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "product/categoryList/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$param['skey'] = $skey;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('categorylist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function categoryStatus()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="category_tbl";
			$param['where'] =array("cat_id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['cat_status'] == "Active")
			{
				$param['data'] = array("cat_status"=>'Inactive');
			}
			else
			{
				$param['data'] = array("cat_status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('product/categoryList');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}
	 public function addCategory()
    {
        $this->form_validation->set_rules('categoryname', 'Category Name', 'trim|required');
        $this->form_validation->set_rules('mainCategory', 'Parent Category', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
		
            $cat_name = $this->input->post("categoryname");
            $parent_id = $this->input->post("mainCategory");
            $sub_id = $this->input->post("Category");
			//echo FCPATH .'./assets/images/category/';exit;
			$config['upload_path'] = './assets/images/category/';
			$config['file_name']=url_title($cat_name, 'dash', true);
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']             = 100;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$this->load->library('upload', $config);
			//print_r($this->upload->do_upload);exit;
			//$this->upload->do_upload('img');
			$path=FCPATH .'assets/images/category/';
			$cat_image = url_title($cat_name, 'dash', true);
			 move_uploaded_file($_FILES["img"]["tmp_name"], $path . $cat_image.".jpg");
			/*if ($this->upload->do_upload('img'))
             {
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
            $cat_image = $imgname;
			}*/
			$filename = $cat_image.'.jpg';
            $cat_status = "Active";
            $user_id = $this->session_data['id'];
		
			
            //$cat_description = $this->input->post("categoryname");
            $meta_tag_title = $this->input->post("metatagTitle");
            $meta_tag_description = $this->input->post("metatagDescription");
            $meta_tag_keywords = $this->input->post("metatagKeywords");
			$store_key = $this->session_data['store_key'];
			
			if($sub_id != ""){$parent_id = $sub_id; }
			$param['table'] ="category_tbl";
            $param['data']=array(	"cat_name"			=>		$cat_name,
									"user_id"			=>		$user_id,
									"parent_id"			=>		$parent_id,
									"cat_image"			=>		$filename,
									"cat_status"		=>		$cat_status,
									"cat_dtadded"		=>		date("Y-m-d H:i:s"),
									"meta_tag_title"	=>		$meta_tag_title,
									"meta_tag_description"=>	$meta_tag_description,
									"meta_tag_keywords"	=>		$meta_tag_keywords,
									"store_key"			=>		$store_key,
									"menu"				=> 		"0"
									);
							
            $result =$this->CorModel->insert($param);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			
			redirect('product/categoryList');
        }
        else
        {
			
				$param['table'] ="category_tbl";
				$param['where'] =array("parent_id"=>"0","cat_status"=>"Active");
				$this->data['catlist'] = $this->CorModel->getRecords($param);
            	$this->load->template('addcategory',$this->data);
        }
        
    }
	public function deleteCategory()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="category_tbl";
			$param['where'] =array("cat_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		
		$param['data'] = array("cat_status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}	
	
	public function manufactureList()
    {
       $this->load->library("pagination");
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			$skey = $this->session_data['store_key'];	
			$param['table'] ="manufacturer";
			$param['where'] =array('m_status  !=' => 'Drop','store_key ='=> $skey);
			// For Pagination 
			$config = array();
			$config["base_url"] = base_url() . "product/manufactureList/";
			$config["total_rows"] = COUNT($this->CorModel->getRecords($param));
			$config["per_page"] = 15;
			$config["uri_segment"] = 3;
			$config['full_tag_open'] = "<ul class='pagination'>";
			$config['full_tag_close']="</ul>";
			$config['prev_tag_open'] = '<li class="paginate_button previous" id="example1_previous">';
			$config['prev_tag_close'] = '<li>';
			$config['next_tag_open'] = '<li class="paginate_button next" id="example1_next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
			$config['cur_tag_close'] = '</a></li>';	
			$config['num_tag_open'] = '<li >';
			$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$param['limit'] = $config["per_page"];
			$param['start'] = $page;
			$param['skey'] = $skey;
			$this->data['list'] = $this->CorModel->paginationdata($param);
			$this->data["links"] = $this->pagination->create_links();
			// For Pagination 			
			/*if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$param['table'] ="store_category";
			$param['where'] =array('id' => $id);
			$this->data[]	$this->CorModel->getRecords($param)
			}*/
			
			//$this->CorModel->update($param);
			$this->load->template('manufacturelist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
    }
	public function manufacturerStatus()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="manufacturer";
			$param['where'] =array("m_id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['m_status'] == "Active")
			{
				$param['data'] = array("m_status"=>'Inactive');
			}
			else
			{
				$param['data'] = array("m_status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('product/manufactureList');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}
	 public function addManufacturer()
    {
       
        $this->form_validation->set_rules('m_name', 'Manufacturer Name', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
		
            $m_name = $this->input->post("m_name");
			$config['upload_path'] = './assets/images/Manufacturer/';
			$config['file_name']=url_title($cat_name, 'dash', true);
			$config['allowed_types'] = 'gif|jpg|png';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('img');
			$data = array('upload_data' => $this->upload->data());
			$imgname=$data['upload_data']['file_name'];	
            $m_image = $imgname;
            $m_status = "Active";
			$store_key = $this->session_data['store_key'];
			$param['table'] ="manufacturer";
            $param['data']=array(	"m_name"			=>		$m_name,
									"m_image"			=>		$m_image,
									"m_status"			=>		$m_status,
									"store_key"			=>		$store_key
									);
            $result =$this->CorModel->insert($param);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			redirect('product/manufactureList');
        }
        else
        {			
			$this->load->template('addmanufacturer');
        }
        
    }
	public function deleteManufacturer()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="manufacturer";
			$param['where'] =array("m_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		
		$param['data'] = array("m_status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}	
	public function offerList()
    {
       
		try{
				if($this->session->flashdata('msg'))
				{
						$this->data['msg'] = $this->session->flashdata('msg');
				}
			
			$param['table'] ="promocode";
			$param['where'] =array("status !=" => 'Drop');
			
			$this->data['list'] = $this->CorModel->getRecords($param);
			$this->load->template('offerlist',$this->data);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	
		
	}
	public function addoffer()
    {
       
		$this->form_validation->set_rules('pcode', 'Promo code', 'trim|required');
		$this->form_validation->set_rules('type', 'Promo type', 'trim|required');
		$this->form_validation->set_rules('amount', 'Promo type value', 'trim|required');
		$this->form_validation->set_rules('sdate', 'Start Date', 'trim|required');
		$this->form_validation->set_rules('edate', 'end date', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
		
			$pcode = $this->input->post("pcode");
			$type = $this->input->post("type");
			$sdate = $this->input->post("sdate");
			$edate = $this->input->post("edate");
			$amount = $this->input->post("amount");
			$status = 'Active';
		
			
			$param['table'] ="promocode";
            $param['data']=array(	"promocode"			=>		$pcode,
									"type"			=>		$type,
									"amount"			=>		$amount,
									"status"			=>		$status,
									"startDate"			=>		$sdate,
									"endDate"			=>		$edate,
								);
            $result =$this->CorModel->insert($param);
            if($result == true)
			{
				$this->data['msg']=array("success"=>MSG_UPDATE);
			}
			else{
				$this->data['msg']=array("error"=>MSG_UPDATE_ERR);
			}
			redirect('product/offerList');
        }
        else
        {			
			$this->load->template('offerlist');
        }
        
	}
	public function deleteoffer()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="promocode";
			$param['where'] =array("id"=>$id);
		$data = $this->CorModel->getRecords($param);
		
		$param['data'] = array("status"=>'Drop');
		
		$this->CorModel->update($param);
		//redirect('store/');
	}	
	public function offerStatus()
	{
		try{
			$id = $this->uri->segment(3);
			$param['table'] ="promocode";
			$param['where'] =array("id"=>$id);
			$data = $this->CorModel->getRecords($param);
			if($data[0]['status'] == "Active")
			{
				$param['data'] = array("status"=>'Inactive');
			}
			else
			{
				$param['data'] = array("status"=>'Active');
			}
			$this->CorModel->update($param);
			redirect('product/offerList');

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	

	}	
}