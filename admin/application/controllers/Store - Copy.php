<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    private $session_data;
    public $data ="";
	function __construct(){
		parent::__construct();
		$this->session_data=$this->load->isSignedIn();	
        $this->load->library('grocery_CRUD');

	}
    public function index()
	{
		try{
			$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
			$crud->set_table('store_information');			
			$crud->set_subject('Store Information');	
			$crud->required_fields('store_name','store_category','contact_person','contact_email','contact_number');
			$crud->columns('store_key','store_name','store_category','contact_person','contact_email','contact_number','status');
			$crud->set_field_upload('store_logo','assets/images/store');			
			$crud->set_relation('store_category','store_category','category_name');
			$crud->fields( 'store_name', 'store_logo', 'store_category', 'store_address', 'store_city', 'store_zipcode', 'contact_person', 'contact_email', 'contact_number', 'bank_name', 'account_number', 'IFSC_code');	
			$crud->add_action('Status', base_url().'design/images/img.png', 'store/status');
			
			$crud->callback_after_insert(array($this, 'store_data'));
			$output = $crud->render();
			$this->_example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}		
	}
	public function store_data($post_array,$primary_key)
	{
		$param['table'] ="store_information";
        $param['where'] =array("store_id"=>$primary_key);
		$data = $this->CorModel->getRecords($param);
		if($data[0]['status'] == "Active")
		{
			$param['data'] = array("status"=>'Inactive');
		}
		else
		{
			$param['data'] = array("status"=>'Active');
		}
		$this->CorModel->update($param);
		
	}
    public function status()
	{
		$id = $this->uri->segment(3);
		$param['table'] ="store_information";
        $param['where'] =array("store_id"=>$id);
		$data = $this->CorModel->getRecords($param);
		if($data[0]['status'] == "Active")
		{
			$param['data'] = array("status"=>'Inactive');
		}
		else
		{
			$param['data'] = array("status"=>'Active');
		}
		$this->CorModel->update($param);
		redirect('store/');
	}
    public function add()
    {
        
        $this->load->template('deshboard');
    }
	public function _example_output($output = null)
	{
		//$this->load->view('example.php',$output);
		$this->load->template('store',(array)$output);
	}
    public function store_category()
    {
        /*$vars['test']="testing";
              // echo $this->session->flashdata('emessage');exit;
        if($this->session->flashdata('smessage'))
        {
            $vars['msg']=array("error"=>$this->session->flashdata('smessage'),"type"=>"success");
        }
        else if($this->session->flashdata('emessage'))
        {
            $vars['msg']=array("error"=>$this->session->flashdata('emessage'),"type"=>"danger");
        }
        $params['table']="store_category";
       $vars['scat']= $this->CorModel->getRecords($params);
        */
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		
    }
    public function addCategory()
    {
       
        $this->form_validation->set_rules('storeCategory', 'Store Category', 'trim|required');
        if($this->form_validation->run() != FALSE)
  		{
            $cname = $this->input->post("storeCategory");
            $params['table']="store_category";
            $params['data']=array("category_name"=>$cname,"status"=>'Yes');
            $this->CorModel->insert($params);
            $this->session->set_flashdata('smessage', MSG_ADD);
        }
        else
        {
            
            $this->session->set_flashdata('emessage', MSG_ADD_ERR);
        }
        redirect('store/store_category');
    }
}
