<?php
class Soapserver extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$a = array("asdff");
		echo "asdf";exit;
		return json_encode($a);
        $ns = 'http://' . $_SERVER['HTTP_HOST'] . '/soapserver/';
        $this->load->library("Nusoap_library"); // load nusoap toolkit library in controller
        $this->load->library("pro");
        $this->prolib        = new pro();
        // load nusoap toolkit library in controller
        $this->nusoap_server = new soap_server(); // create soap server object
        $this->nusoap_server->configureWSDL("demo", "urn:demo"); // wsdl cinfiguration
        $this->nusoap_server->wsdl->schemaTargetNamespace = $ns; // server namespace
        
        $input_array  = array(
            'param' => "xsd:string"
        ); // "addnumbers" method parameters
        $return_array = array(
            "return" => "xsd:string"
        );
        $this->nusoap_server->register('getCategory', $input_array, $return_array);
        $this->nusoap_server->register('storeInfo', $input_array, $return_array);
        $this->nusoap_server->register('productList', $input_array, $return_array);
        $this->nusoap_server->register('bannerList', $input_array, $return_array);
        $this->nusoap_server->register('customListing', $input_array, $return_array);
        $this->nusoap_server->register('userRagistration', $input_array, $return_array);
		$this->nusoap_server->register('updateRec', $input_array, $return_array);
		$this->nusoap_server->register('addShipping', $input_array, $return_array);
        
    }
    function index()
    {
        function getdata($dbparma)
        {
			
            $CI =& get_instance();
            $query  = $CI->db->get_where($dbparma['table'], $dbparma['where']);
            $result = $query->result_array();
            return $result;
        }
		function insertData($dbparma)
		{
			$CI =& get_instance();
            $CI->db->insert($dbparma['table'], $dbparma['data']);
            $insert_id = $CI->db->insert_id();

            return $insert_id;	
		}
		function updateData($dbparma)
		{
			$CI =& get_instance();
            $CI->db->update($dbparma['table'], $dbparma['data'],$dbparma['where']);
            
            return true;	
		}
		function productdata($dbparma)
		{
			//return $dbparma;
			$date= date("Y-m-d");

		  $CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('product_tbl');
		$CI->db->join('product_price_tbl','product_tbl.pro_id = product_price_tbl.pro_id');
		
		$CI->db->join('prodcut_detail_tbl','product_tbl.pro_id = prodcut_detail_tbl.pro_id');
		$CI->db->where("product_tbl.pro_display_date <=", $date);
		//$this->db->where("product_tbl.pro_cat",$id);
		$CI->db->where("pro_status",'Active');
		/*if($bid!=0)
		{
		$this->db->where("brand",$bid);
		}	*/	
		$CI->db->where($dbparma['where']);
		if(!empty($dbparma['inarray']))
		{
			$CI->db->where_in("product_tbl.pro_cat",$dbparma['inarray']);
		}
		
		$CI->db->order_by("product_price_tbl.pro_qty", "desc");
		$CI->db->order_by("product_tbl.pro_id", "desc"); 
			//$this->db->limit($resultsPerPage, $ord);
		 $CI->db->group_by('product_tbl.pro_id');
   		 $query = $CI->db->get_where();
		
	  return $query->result_array();
		
		}
		function getchield($cid,$stkey)
		{
			 $CI =& get_instance();
			 $where = array(
                    "store_key" => $stkey,
					"parent_id" => $cid
                );
			$CI->db->select('cat_id');
            $query  = $CI->db->get_where('category_tbl', $where);
            $result = $query->result_array();
			$catarray[]=$cid;
			foreach($result as $res){ $catarray[] = $res['cat_id'];}
            return $catarray;
		}
        function checkcredential($k)
        {
            $a                = explode("|", unserialize(gzuncompress(base64_decode($k))));
            $dbparma['table'] = "store_webservices_credential";
            $dbparma['where'] = array(
                "api_key" => $a[0],
                "secret_key" => $a[1],
                "store_key" => $a[2]
            );
            $result           = getdata($dbparma);
            if (!empty($result)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
		
        function getCategory($param)
        {
			
            $params = json_decode($param);
            $k      = $params->akey;
            if (checkcredential($k) == FALSE) {
                return "Invalid Authentication";
            } else {
                $stkey            = $params->where;
                $dbparma['table'] = "category_tbl";
				
                $dbparma['where'] = (array)$stkey;
                $result           = getdata($dbparma);
                $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
                return $data;
            }
        } 
		function customListing($param)
        {
			//return $param;
            $params = json_decode($param);
            $k      = $params->akey;
            if (checkcredential($k) == FALSE) {
                return "Invalid Authentication";
            } else {
                $stkey            = $params->where;
                $dbparma['table'] = $params->table;
				
                $dbparma['where'] = (array)$stkey;
                $result           = getdata($dbparma);
                $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
                return $data;
            }
        }
		function updateRec($param)
        {
			//return $param;
            $params = json_decode($param);
            $k      = $params->akey;
            if (checkcredential($k) == FALSE) {
                return "Invalid Authentication";
            } else {
                $stkey            = $params->where;
                $dbparma['table'] = $params->table;;
				
                $dbparma['where'] = (array)$stkey;
				$dbparma['data'] = (array)$params->data;
                $result           = updateData($dbparma);
				if($result)
				{
					$data             = json_encode(array(
						"response" => "true",
						"detail" => $result
					));
				}else{
					$data             = json_encode(array(
						"response" => "false",
						"detail" => array()
					));
				}					
                return $data;
            }
        }
		function storeInfo($param)
        {
            $params = json_decode($param);
            $k      = $params->akey;
            if (checkcredential($k) == FALSE) {
                return "Invalid Authentication";
            } else {
                $stkey            = $params->storekey;
                $dbparma['table'] = "store_information";
                $dbparma['where'] = array(
                    "store_key" => $stkey
                );
							$result           = getdata($dbparma);
							$data             = json_encode(array(
								"response" => "true",
                    "detail" => $result
                ));
                return $data;
            }
        }
		function bannerList($param)
        {
            $params = json_decode($param);
            $k      = $params->akey;
            if (checkcredential($k) == FALSE) {
                return "Invalid Authentication";
            } else {
                $stkey            = $params->storekey;
                $dbparma['table'] = "banner";
                $dbparma['where'] = array(
                    "store_key" => $stkey,
					"status" => "Active"
                );
                $result           = getdata($dbparma);
                $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
                return $data;
            }
        }
		function productList($param)
        {
			
            $params = json_decode($param);
            $k      = $params->akey;
            if (checkcredential($k) == FALSE) {
                return "Invalid Authentication";
            } else {
                $stkey            = $params->storekey;
                $dbparma['table'] = "store_information";
                $dbparma['where'] = (array)$params->where;
				if(!empty($params->cid)){
                $dbparma['inarray'] = getchield($params->cid,$stkey);
				}
				$result           = productdata($dbparma);
                $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
                return $data;
            }
        }
		function userRagistration($param)
		{
			//return $param;
			 $params = json_decode($param);
			 $dbparma['table']		= "user_tbl";
			 $dbparma['data']    	= $params->udata;
			 insertData( $dbparma);
			 unset($dbparma);
			 $dbparma['table'] 		= "login_credential";
			 $dbparma['data']		= $params->ldata;
			 $result	=	insertData( $dbparma);
			  $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
				return $data;
		}
		
		function addShipping($param)
		{			
			 $params = json_decode($param);
			 $dbparma['table']		= $params->table;
			 $dbparma['billing']    	= $params->billing;
			 insertData( $dbparma);
			 $dbparma['shipping']    	= $params->shipping;			 
			 $result	=	insertData( $dbparma);
			  $data             = json_encode(array(
                    "response" => "true",
                    "detail" => $result
                ));
				return $data;
		}
		
        $this->nusoap_server->service(file_get_contents("php://input")); // read raw data from request body
    }
}
?>