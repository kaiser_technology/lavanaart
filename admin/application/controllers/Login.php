<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct()
	 {
	   parent::__construct();

	 }
	public function index()
	{
		
		if(!$this->session->userdata('admin'))
		{
            
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
		  if($this->form_validation->run() == FALSE)
  			{
					
					$message = $this->session->flashdata('message');
					if($message)
					{
						$admindata['msg']=array("error"=>$message);
						$this->load->view('login',$admindata);
					}
					else
					{
						
						$this->load->view('login');
					}
			}
			else
			{
				$username=$this->input->post("username");	
				$password=sha1($this->input->post("password"));
				
				$param['table'] ="login_credential";
				$param['where'] = array('email'=>$username,'password'=>$password,'status'=>1);
				
				$result= $this->CorModel->login($param);
				 if($result)
				   {
					 
					 $sess_array = array();
					 foreach($result as $row)
					 {
					 
					   $sess_array = array(
						 'rolid' => $row->role_id,
						 'id' => $row->id,
						 'name'=> $row->user_name,
						 'store_key'=> $row->store_key,
					   );
					   $this->session->set_userdata("admin",$sess_array);
					 }
					$session_data=$this->session->userdata('admin');
						 if(!empty($session_data['rolid']))
						 {
							
								redirect('dashboard');
							 
						 }
						
						else
						{
							redirect("login");
						}
				   }
				   else
				   {
					   	$this->session->set_flashdata('message', 'You are not Authenticate for Login');
					   	redirect('login');
						$this->form_validation->set_message('check_database', 'Invalid username or password');
						return false;
				   }
				 
				 
			}
		}
		else{
            $session_data=$this->session->userdata('admin');
           
			 if($session_data['rolid']==1)
			 {
				
					redirect('dashboard');
				 
			 }
            else
            {
				
                redirect("login");
            }
		}
	}
	public function profile()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('number', 'Number', 'trim|required');
		
	  if($this->form_validation->run() == FALSE)
		{
				
				
					$session_data=$this->session->userdata('admin');
					$store_key = $session_data['store_key'];
					$param['table'] ="store_information";
					$param['where'] = array('store_key'=>$store_key,'status'=>'Active');
					$result= $this->CorModel->getRecords($param);
					$vars['result'] = $result;
					$this->load->template('profile',$vars);
				
		}
		else
		{
			if($this->input->post('update'))
			{
				
				$session_data=$this->session->userdata('admin');
				$store_key = $session_data['store_key'];
				$name = $this->input->post('name');
				$number = $this->input->post('number');
				$param['table'] ="store_information";
				$param['data'] =array("contact_person"=>$name,"contact_number"=>$number);
				$param['where'] = array('store_key'=>$store_key,'status'=>'Active');
				$result= $this->CorModel->update($param);
				if($result == true)
				{
					$vars['msg']=array("success"=>MSG_UPDATE);
				}
				else{
					$vars['msg']=array("error"=>MSG_UPDATE_ERR);
				}
			}
			$session_data=$this->session->userdata('admin');
			$store_key = $session_data['store_key'];
			$param['table'] ="store_information";
			$param['where'] = array('store_key'=>$store_key,'status'=>'Active');
			$result= $this->CorModel->getRecords($param);
			$vars['result'] = $result;
			$this->load->template('profile',$vars);
		}	
	}
}
