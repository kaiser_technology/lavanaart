<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Fetch Website Settings */
$CI =& get_instance();
/*$CI->load->database();
$where=array("status"=>'1');
$query = $CI->db->get_where("site_settings",$where);
if ($query->num_rows() > 0)
{
	foreach ($query->result_array() as $row)
	{
		$config[$row['var_name']] = $row['setting'];
	}
}*/
$config['ADMIN_URL'] = config_item("base_url");
$config['ADMIN_CSS_URL'] = config_item("base_url")."assets/css/";
$config['ADMIN_JS_URL'] = config_item("base_url")."assets/js/";
$config['VENDOR_JS_URL'] = config_item("base_url")."assets/vendor/";
$config['PAGE_JS_URL'] = config_item("base_url")."assets/pagejs/";
$config['ADMIN_IMAGES_URL'] = config_item("base_url")."assets/images/";
define('ADMIN_URL',config_item("base_url"));
define('ADMIN_IMAGES_URL',config_item("base_url").'assets/images/');

define('CATEGORY_IMG_PATH', "./assets/images/category/");
define('CATEGORY_IMG_URL', config_item("base_url")."/assets/images/category/");
	define('SSL_ENABLED', false); 	// true for ssl enabled -- false for ssl not enabled  
	define('SITE_BASE', '');  // for live this value must be null
	
//echo config_item("base_url");
/* Fetch Website Settings */

if(ENVIRONMENT == 'Local') 
	{
		// Root Directory
		define('DIR_ROOT',$_SERVER['DOCUMENT_ROOT'].'/'.SITE_BASE.'/');
		
		$site_url = "http://".$_SERVER["HTTP_HOST"].'/'.SITE_BASE.'/';
		
		if(isset($_SERVER["HTTPS"])) {
			if($_SERVER["HTTPS"]=='1' or $_SERVER["HTTPS"]=='on')
				$site_url = "https://".$_SERVER["HTTP_HOST"].'/'.SITE_BASE.'/';
		}	
		
		$secure_url = "https://".$_SERVER["HTTP_HOST"].'/'.SITE_BASE.'/';
		
		if(SSL_ENABLED)
			$secure_url = "https://".$_SERVER["HTTP_HOST"].'/'.SITE_BASE.'/';
		
		$insecure_url = "https://".$_SERVER["HTTP_HOST"].'/'.SITE_BASE.'/';
		
	} 
	else 
	{
	
		// Root Directory
		define('DIR_ROOT', '');
		
		$site_url = "https://".$_SERVER["HTTP_HOST"].'/';
		
		if(isset($_SERVER["HTTPS"])) {
			if($_SERVER["HTTPS"]=='1' or $_SERVER["HTTPS"]=='on')
				$site_url = "https://".$_SERVER["HTTP_HOST"].'/';
		}		
		
		$secure_url = "https://".$_SERVER["HTTP_HOST"].'/';
		if(SSL_ENABLED)
			$secure_url = "https://".$_SERVER["HTTP_HOST"].'/';
		
		$insecure_url = "https://".$_SERVER["HTTP_HOST"].'/';
	}	
	// PATH
	define('SITE_URL',$site_url);  
	define('SECURE_URL',$secure_url);  
	define('INSECURE_URL',$insecure_url);  
	define('TITLE','Welcome To Lavana Art');  
