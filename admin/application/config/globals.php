<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



$config['siteName'] 			= 'Asmita Farm Fresh';

$config['globalDateFormat'] 	= "d F, Y";


$config['uniqueString']			=	"okjjahjsjkj78465$%@#&^$*kdgfzjxc574dfiylkjojlxfjug``iwierP:OLK||{<>dfjugshdf][]0-89*&*(4";

$config['uid']					=	0;

$config['appId']				=	0;

$config['userInfo'] 			= 	array();

$config['quotes']       		=  "'";

$config['OTPmessage'] 			= 	'Zala verification code: ';

$config['newTaskUsermessage'] 	= 	'Invite user';

$config['defaultList'] 			= 	'Tasks';

$config['defaultGroupName'] 	= 	'Default Group';

$config['OTPdigits']			=	6;

$config['limit'] 				=   20;

$config['selfThreadName'] 		=   'Me';

$config['selfActivityName'] 	=   'You';

$config['titleSeparator']       = 	' | ';

$config['reportsAutoUpdateInterval']       = 	60;


$config['imageResizeDimensions'] = 	array(
											array("h" => 512, 	"w" => 512, 	"suffix" => 'l'),
											array("h" => 200, 	"w" => 200, 	"suffix" => 'm'),
											array("h" => 72, 	"w" => 72, 		"suffix" => 's'),
										);

$config['fileType'] 			= 	array(
										'1' => 'Image',
										'2' => 'Video',
										'3' => 'Others'
									 );


$config['intColumns'] 			= 	array(
											'1' => 'open',
											'2' => 'closed',
											'3' => 'archived'
										);

$config['maxLogins'] 			=   3;

// user token validity'
$config['created'] 		=   '6 months';

$config['otpTime'] 		=   '30 min';

$config['color'] 		=	array('#9b59b6','#34495e','#9b59b6','#3498db','#62cb31','#ffb606','#e67e22','#e74c3c','#c0392b');

$config['imageExtensions'] 		=	array('jpeg','jpg','png','gif');

$config['videoExtension'] 		=	array(
									   '1'  => 'webm',
									   '2'	=> 'mkv',
									   '3'	=> 'flv',
									   '4'	=> 'vob',
									   '5'	=> 'ogv',
									   '6'  => 'ogg',
									   '7'	=> 'drc',
									   '8'	=> 'mng',
									   '9'	=> 'avi',
									   '10'	=> 'mov',
									   '11' => 'qt',
									   '12'	=> 'wmv',
									   '13'	=> 'yuv',
									   '14'	=> 'rm',
									   '15'	=> 'rmvb',
									   '16' => 'asf',
									   '17'	=> 'mp4',
									   '18'	=> 'mpg',
									   '19'	=> 'mpeg',
									   '20'	=> 'm2v',
									   '21' => 'm4v',
									   '22'	=> 'svi',
									   '23'	=> '3gp',
									   '24'	=> '3g2',
									   '25'	=> 'mxf',
									   '26'	=> 'roq',
									   '27'	=> 'nsv',		
										);	

$config['gdiz_phpass_hash_portable'] = FALSE;
$config['gdiz_phpass_hash_strength'] = 8;
$config['verificationCallSecret'] = "dfkjbdlkjfbdjkfTTUYYUJBJKBJdb483654835643sjhlfbds.kfbdskjfbds.kjfdbhj,";