-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2017 at 04:40 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nayavepar`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_tbl`
--

CREATE TABLE `category_tbl` (
  `cat_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `cat_name` varchar(200) NOT NULL,
  `cat_image` varchar(500) NOT NULL,
  `cat_status` enum('Active','Inactive','Hold','Drop') NOT NULL,
  `cat_dtadded` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_description` text NOT NULL,
  `meta_tag_title` varchar(255) NOT NULL,
  `meta_tag_description` text NOT NULL,
  `meta_tag_keywords` text NOT NULL,
  `store_key` varchar(12) NOT NULL,
  `menu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_tbl`
--

INSERT INTO `category_tbl` (`cat_id`, `parent_id`, `cat_name`, `cat_image`, `cat_status`, `cat_dtadded`, `user_id`, `cat_description`, `meta_tag_title`, `meta_tag_description`, `meta_tag_keywords`, `store_key`, `menu`) VALUES
(1, 0, 'test', 'test', 'Active', '2017-03-08 00:00:00', 12, 'asdf', 'asdfs', 'asfdas', 'asdfas', 's', 'aasdf'),
(2, 1, 'test2', '8ffe430c8a49eeb3552cfaed25577413.jpg', 'Drop', '0000-00-00 00:00:00', 5, '', '', '', '', 'NVDS1102', '0'),
(3, 1, 'test2s', 'test2s.jpg', 'Drop', '0000-00-00 00:00:00', 5, '', 'test', 'test', 'teswte', 'NVDS1102', '0'),
(4, 1, 'test2', 'test2.jpg', 'Active', '0000-00-00 00:00:00', 5, '', 'test', '', '', 'NVDS1102', '0');

-- --------------------------------------------------------

--
-- Table structure for table `login_credential`
--

CREATE TABLE `login_credential` (
  `id` int(11) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `email` varchar(265) NOT NULL,
  `password` varchar(70) NOT NULL,
  `role_id` int(8) NOT NULL,
  `status` int(11) NOT NULL,
  `store_key` varchar(54) NOT NULL,
  `first_time_login` varchar(10) NOT NULL,
  `addDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logged_in_datetime` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_credential`
--

INSERT INTO `login_credential` (`id`, `user_name`, `email`, `password`, `role_id`, `status`, `store_key`, `first_time_login`, `addDate`, `logged_in_datetime`) VALUES
(1, 'Nikunj Patel', 'nikunj@gmail.com', '20a84576d1ebbaa48311e66171286b50980ea601', 1, 1, 'NVDS0000', '', '2016-12-16 19:08:01', '2017-03-27 12:03:38'),
(5, 'asdf', 'nikunj9721@gmail.com', '2e7464a5e9bac192f1251866fea0c255db0cbd83', 3, 1, 'NVDS1102', '', '2017-03-26 09:44:59', '2017-03-28 02:03:40');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `m_id` int(11) NOT NULL,
  `m_name` varchar(150) NOT NULL,
  `m_image` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nv_menu_tbl`
--

CREATE TABLE `nv_menu_tbl` (
  `id` int(11) NOT NULL,
  `parent_menu_id` int(11) NOT NULL,
  `menu_name` varchar(80) NOT NULL,
  `icon` varchar(35) NOT NULL,
  `menu_link` varchar(100) NOT NULL,
  `act_name` varchar(80) NOT NULL,
  `role_id` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nv_menu_tbl`
--

INSERT INTO `nv_menu_tbl` (`id`, `parent_menu_id`, `menu_name`, `icon`, `menu_link`, `act_name`, `role_id`) VALUES
(1, 0, 'Dashboard', '', 'dashboard', 'dashboard', '1,2'),
(2, 0, 'Store', '', '', 'store', '1'),
(3, 2, 'List', '', 'store/storelist/', 'store', '1'),
(4, 0, 'Add', '', 'store/add', 'store', '0'),
(5, 2, 'Category', '', 'store/storeCategoryList', 'store', '1'),
(6, 0, 'ACCESS TYPE', '', 'role/roleList', 'role', '1'),
(7, 0, 'MASTER PRODUCT', '', 'product/', 'product', '1'),
(8, 0, 'PRODUCT', '', 'product/', 'product', '3'),
(9, 8, 'PRODUCT LIST', '', 'product/productList/', 'product', '3'),
(10, 8, 'CATEGORY', '', 'product/productCategoryList/', 'category', '3'),
(11, 8, 'MANUFACTURE', '', 'manufacture/', 'manufacture', '3'),
(12, 8, 'DISCOUNT', '', 'discount/', 'discount', '3'),
(13, 0, 'ORDER', '', '', 'order', '3'),
(14, 13, 'PENDING ORDER', '', 'order/pending-order/', 'order', '3'),
(15, 13, 'DISPATCH ORDER', '', 'order/dispatch-order/', 'order', '3'),
(16, 13, 'DELIVERED ORDER', '', 'order/delivered-order/', 'order', '3'),
(17, 13, 'CANCEL ORDER', '', 'order/cancel-order/', 'order', '3'),
(18, 13, 'COMPLETED ORDER', '', 'order/completed-order/', 'order', '3'),
(19, 0, 'REPORTS', '', '', 'report', '1,2,3'),
(20, 2, 'Web Credential', '', 'store/webCredential', 'store', '1');

-- --------------------------------------------------------

--
-- Table structure for table `nv_site_settings_form`
--

CREATE TABLE `nv_site_settings_form` (
  `site_settings_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `var_name` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Define variable name',
  `description` text COLLATE latin1_general_ci NOT NULL COMMENT 'Description of variable',
  `setting` varchar(1000) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Setting value',
  `display_order` int(11) NOT NULL DEFAULT '0',
  `section` int(3) NOT NULL DEFAULT '0' COMMENT 'Section of settings..i.e. Payment section, shipping section, etc..',
  `html_element` enum('Text','Select','Textarea','File') COLLATE latin1_general_ci NOT NULL DEFAULT 'Text',
  `html_element_value` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `status` enum('0','1') COLLATE latin1_general_ci NOT NULL COMMENT '0=Inactive and will not display in admin panel 1= Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `nv_site_settings_form`
--

INSERT INTO `nv_site_settings_form` (`site_settings_id`, `title`, `var_name`, `description`, `setting`, `display_order`, `section`, `html_element`, `html_element_value`, `status`) VALUES
(1, 'WebSite Title', 'SITE_TITLE', '', 'Demo Project', 1, 1, 'Text', '', '1'),
(2, 'Email address for administrator', 'ADMIN_MAIL', 'Email address for administrator e-mails ', 'customer.service@carpetexpress.com', 2, 1, 'Text', '', '1'),
(3, 'Contact Person Mail Address', 'CONTACT_MAIL', 'E-mail Address for your Customer Service ', 'customer.service@carpetexpress.com', 3, 1, 'Text', '', '1'),
(4, 'TOLL FREE NO', 'TOLL_FREE_NO', 'Toll Free Phone Number', '800-922-5582', 4, 1, 'Text', '', '1'),
(5, 'Google Analytics Tracking ID', 'GOOGLE_ANALYTICS_TRACKING_ID', 'Set your Google Analytics Tracking ID here. Ex UA-XXXXXXX-X', 'UA-36115126-1', 5, 1, 'Text', '', '1'),
(6, 'Show coming soon page?', 'SHOW_COMING_SOON_PAGE', 'Set you website on coming soon page.', '', 6, 1, 'Select', 'No#Yes', '1'),
(7, 'Home Page Banner Speed', 'HOME_BANNER_SPEED', 'Home Page Banner Sliding Speed in Second', '15', 7, 1, 'Text', '', '1'),
(8, 'Home Page Text', 'HOME_PAGE_TEXT', 'Text that display after Featured Items', '<h3>Love your floors</h3><span style=\"font-size: medium;\">\r\nAt Carpet Express, we believe the floor is the foundation for beautiful interior design. You could say, â€œthe floor is the most important wall in any home.â€ Thatâ€™s why Carpet Express is committed to providing the most trusted brands from our nation\'s top flooring manufacturers at the lowest possible price. So whether you are shopping for carpet, hardwood, laminate, vinyl flooring, or area rugs, you can be sure that CarpetExpress.com is your best source for all your floor covering needs.</span>', 8, 1, 'Textarea', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `prodcut_detail_tbl`
--

CREATE TABLE `prodcut_detail_tbl` (
  `pro_detail_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `m_title` varchar(250) NOT NULL,
  `m_desc` varchar(500) NOT NULL,
  `m_keyword` varchar(500) NOT NULL,
  `pro_detail` varchar(1500) DEFAULT NULL,
  `add_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodcut_detail_tbl`
--

INSERT INTO `prodcut_detail_tbl` (`pro_detail_id`, `pro_id`, `m_title`, `m_desc`, `m_keyword`, `pro_detail`, `add_date`, `modify_date`) VALUES
(1, 2, 'ASDFA', 'dbhsgsdf', 'sdfgsdfg', NULL, '2017-03-28 06:48:40', '2017-03-28 06:48:40'),
(2, 3, '', '', '', NULL, '2017-03-28 07:32:50', '2017-03-28 07:32:50'),
(3, 4, '', '', '', NULL, '2017-03-28 07:33:23', '2017-03-28 07:33:23'),
(4, 5, '', '', '', NULL, '2017-03-28 07:33:47', '2017-03-28 07:33:47'),
(5, 6, '', '', '', NULL, '2017-03-28 07:35:45', '2017-03-28 07:35:45'),
(6, 7, '', '', '', NULL, '2017-03-28 07:43:47', '2017-03-28 07:43:47'),
(7, 8, '', '', '', NULL, '2017-03-28 07:47:46', '2017-03-28 07:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `product_price_tbl`
--

CREATE TABLE `product_price_tbl` (
  `pro_price_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `pro_unit` varchar(50) NOT NULL,
  `pro_no_unit` int(11) NOT NULL,
  `pro_mrp_price` decimal(6,2) NOT NULL,
  `pro_purchase_price` decimal(6,2) NOT NULL,
  `pro_profit_price` decimal(6,2) NOT NULL,
  `pro_seal_price` decimal(6,2) NOT NULL,
  `pro_qty` int(11) NOT NULL,
  `add_dat` datetime NOT NULL,
  `modify_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_price_tbl`
--

INSERT INTO `product_price_tbl` (`pro_price_id`, `pro_id`, `pro_unit`, `pro_no_unit`, `pro_mrp_price`, `pro_purchase_price`, `pro_profit_price`, `pro_seal_price`, `pro_qty`, `add_dat`, `modify_date`) VALUES
(1, 2, 'kg', 1, '500.00', '400.00', '0.00', '600.00', 50, '2017-03-28 06:48:40', '2017-03-28 06:48:40');

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl`
--

CREATE TABLE `product_tbl` (
  `pro_id` int(11) NOT NULL,
  `pro_code` varchar(50) NOT NULL,
  `pro_cat` int(11) NOT NULL,
  `pro_name` varchar(150) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `pro_tax` varchar(255) NOT NULL,
  `pro_display_date` varchar(15) NOT NULL,
  `pro_image` varchar(175) NOT NULL,
  `related_pro` varchar(150) NOT NULL,
  `pro_status` enum('Active','Inactive','Hold','Drop') NOT NULL,
  `add_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `store_key` varchar(18) NOT NULL,
  `offer` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tbl`
--

INSERT INTO `product_tbl` (`pro_id`, `pro_code`, `pro_cat`, `pro_name`, `brand`, `pro_tax`, `pro_display_date`, `pro_image`, `related_pro`, `pro_status`, `add_date`, `modify_date`, `store_key`, `offer`) VALUES
(1, 'GR-10001', 2, 'asdfasd', 'ASF', 'Downloadable Products', '03/29/2017 6:37', 'asdfasd.jpg', '', 'Active', '2017-03-28 06:42:01', '2017-03-28 06:42:01', 'NVDS1102', 0),
(2, 'GR-10002', 2, 'asdfasd', 'ASF', 'Taxable Goods', '03/29/2017 6:37', 'asdfasd.jpg', '', 'Active', '2017-03-28 06:48:40', '2017-03-28 06:48:40', 'NVDS1102', 0),
(3, 'GR-10003', 0, '', '', 'None', '', '.jpg', '', 'Active', '2017-03-28 07:32:50', '2017-03-28 07:32:50', 'NVDS1102', 0),
(4, 'GR-10004', 0, '', '', 'None', '', '.jpg', '', 'Active', '2017-03-28 07:33:23', '2017-03-28 07:33:23', 'NVDS1102', 0),
(5, 'GR-10005', 0, '', '', 'None', '', '.jpg', '', 'Active', '2017-03-28 07:33:47', '2017-03-28 07:33:47', 'NVDS1102', 0),
(6, 'GR-10006', 0, '', '', 'None', '', '.jpg', '', 'Active', '2017-03-28 07:35:45', '2017-03-28 07:35:45', 'NVDS1102', 0),
(7, 'GR-10007', 0, '', '', 'None', '', '.jpg', '', 'Active', '2017-03-28 07:43:47', '2017-03-28 07:43:47', 'NVDS1102', 0),
(8, 'GR-10008', 0, '', '', 'None', '', '.jpg', '', 'Active', '2017-03-28 07:47:46', '2017-03-28 07:47:46', 'NVDS1102', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl_`
--

CREATE TABLE `product_tbl_` (
  `pro_id` int(11) NOT NULL,
  `pro_code` varchar(50) NOT NULL,
  `pro_cat` int(11) NOT NULL,
  `pro_name` varchar(150) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `pro_tax` varchar(255) NOT NULL,
  `pro_display_date` varchar(15) NOT NULL,
  `pro_image` varchar(175) NOT NULL,
  `related_pro` varchar(150) NOT NULL,
  `pro_status` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `offer` int(11) NOT NULL DEFAULT '0',
  `m_title` varchar(250) NOT NULL,
  `m_desc` varchar(500) NOT NULL,
  `m_keyword` varchar(500) NOT NULL,
  `pro_detail` varchar(1500) NOT NULL,
  `pro_unit` varchar(50) NOT NULL,
  `pro_no_unit` int(11) NOT NULL,
  `pro_mrp_price` decimal(6,2) NOT NULL,
  `pro_purchase_price` decimal(6,2) NOT NULL,
  `pro_profit_price` decimal(6,2) NOT NULL,
  `pro_seal_price` decimal(6,2) NOT NULL,
  `pro_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `product_tbl_`
--

INSERT INTO `product_tbl_` (`pro_id`, `pro_code`, `pro_cat`, `pro_name`, `brand`, `pro_tax`, `pro_display_date`, `pro_image`, `related_pro`, `pro_status`, `add_date`, `modify_date`, `user_id`, `offer`, `m_title`, `m_desc`, `m_keyword`, `pro_detail`, `pro_unit`, `pro_no_unit`, `pro_mrp_price`, `pro_purchase_price`, `pro_profit_price`, `pro_seal_price`, `pro_qty`) VALUES
(1, 'dyedrhdf', 1, 'dgdsgghfg dfdfg', 'fhchdf', 'cghd', 'chfgh', '3fe50-two.png', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '', '', '', '', 0, '0.00', '0.00', '0.00', '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE `role_tbl` (
  `id` int(11) NOT NULL,
  `role_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_tbl`
--

INSERT INTO `role_tbl` (`id`, `role_name`) VALUES
(1, 'super_admin'),
(2, 'admin'),
(3, 'aaa'),
(4, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `site_setting_value`
--

CREATE TABLE `site_setting_value` (
  `id` int(11) NOT NULL,
  `store_setting` varchar(120) NOT NULL,
  `store_key` varchar(265) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store_category`
--

CREATE TABLE `store_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(110) NOT NULL,
  `status` enum('Active','Inactive','Hold','Drop') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_category`
--

INSERT INTO `store_category` (`id`, `category_name`, `status`) VALUES
(1, 'Grocery', 'Active'),
(2, 'Furniture', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `store_information`
--

CREATE TABLE `store_information` (
  `store_id` int(11) NOT NULL,
  `store_key` varchar(50) NOT NULL,
  `store_name` varchar(250) NOT NULL,
  `store_logo` varchar(80) NOT NULL,
  `store_category` int(11) NOT NULL,
  `store_address` varchar(2500) NOT NULL,
  `store_city` varchar(50) NOT NULL,
  `store_zipcode` int(8) NOT NULL,
  `contact_person` varchar(150) NOT NULL,
  `contact_email` varchar(265) NOT NULL,
  `contact_number` varchar(13) NOT NULL,
  `webservices` varchar(10) NOT NULL,
  `create_date` date NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Hold','Drop') NOT NULL,
  `bank_name` varchar(360) NOT NULL,
  `account_number` varchar(32) NOT NULL,
  `IFSC_code` varchar(20) NOT NULL,
  `store_create_by` int(5) NOT NULL,
  `approval_by` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_information`
--

INSERT INTO `store_information` (`store_id`, `store_key`, `store_name`, `store_logo`, `store_category`, `store_address`, `store_city`, `store_zipcode`, `contact_person`, `contact_email`, `contact_number`, `webservices`, `create_date`, `modify_date`, `status`, `bank_name`, `account_number`, `IFSC_code`, `store_create_by`, `approval_by`) VALUES
(1, 'NVDS0000', 'Supper Admin', '', 0, '-', 'Mumbai', 400101, 'NavaVepar', 'admin@navavepar.com', '9879513314', '-', '2017-01-31', '2017-01-31 00:57:48', 'Inactive', '', '', '', 1, 5),
(2, '', 'xyz', 'c3b0d-ab_photography_by_gimmyfood.jpg', 1, 'sdf', 'asdf', 0, 'asdfasdf', 'asdfasdf', 'asdasdfas', '', '0000-00-00', '2017-02-01 16:55:36', 'Drop', 'asdfasdfasdf', 'asdfasd', 'asdfasdfasdf', 0, 0),
(3, 'NVDS1101', 'asadd', '1203170000122612', 1, 'ettest', 'Mumbai', 400101, 'asdfas', 'abc@gamil.com', '998899889', 'Yes', '2017-03-12', '2017-03-12 11:26:12', 'Active', '', '', '', 1, 0),
(5, 'NVDS1102', 'asdf', '1203171101124124.png', 1, 'asdfasdf', 'Mumbai', 4545454, 'asdasdf', 'nikunj9721@gmail.com', '75645645', 'Yes', '2017-03-12', '2017-03-12 11:41:24', 'Active', '', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `store_webservices_credential`
--

CREATE TABLE `store_webservices_credential` (
  `id` int(11) NOT NULL,
  `api_key` varchar(15) NOT NULL,
  `secret_key` varchar(15) NOT NULL,
  `status` enum('Active','Inactive','Drop','Hold') NOT NULL,
  `store_key` varchar(15) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_webservices_credential`
--

INSERT INTO `store_webservices_credential` (`id`, `api_key`, `secret_key`, `status`, `store_key`, `create_date`) VALUES
(1, '0', '100yyF9zhpAbEtL', 'Active', 'NVDS1101', '2017-03-26 17:30:19'),
(2, 'bRt8BZ', 'yLzxQ2ijkwwFskq', 'Drop', 'NVDS1102', '2017-03-26 17:31:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_tbl`
--
ALTER TABLE `category_tbl`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `login_credential`
--
ALTER TABLE `login_credential`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `nv_menu_tbl`
--
ALTER TABLE `nv_menu_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nv_site_settings_form`
--
ALTER TABLE `nv_site_settings_form`
  ADD PRIMARY KEY (`site_settings_id`),
  ADD UNIQUE KEY `var_name` (`var_name`);

--
-- Indexes for table `prodcut_detail_tbl`
--
ALTER TABLE `prodcut_detail_tbl`
  ADD PRIMARY KEY (`pro_detail_id`),
  ADD KEY `pro_id` (`pro_id`);

--
-- Indexes for table `product_price_tbl`
--
ALTER TABLE `product_price_tbl`
  ADD PRIMARY KEY (`pro_price_id`),
  ADD KEY `pro_id` (`pro_id`);

--
-- Indexes for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `product_tbl_`
--
ALTER TABLE `product_tbl_`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting_value`
--
ALTER TABLE `site_setting_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_key` (`store_key`);

--
-- Indexes for table `store_category`
--
ALTER TABLE `store_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_information`
--
ALTER TABLE `store_information`
  ADD PRIMARY KEY (`store_id`),
  ADD UNIQUE KEY `store_key` (`store_key`),
  ADD KEY `store_category` (`store_category`);

--
-- Indexes for table `store_webservices_credential`
--
ALTER TABLE `store_webservices_credential`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_tbl`
--
ALTER TABLE `category_tbl`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login_credential`
--
ALTER TABLE `login_credential`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `nv_menu_tbl`
--
ALTER TABLE `nv_menu_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `nv_site_settings_form`
--
ALTER TABLE `nv_site_settings_form`
  MODIFY `site_settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `prodcut_detail_tbl`
--
ALTER TABLE `prodcut_detail_tbl`
  MODIFY `pro_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `product_price_tbl`
--
ALTER TABLE `product_price_tbl`
  MODIFY `pro_price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_tbl`
--
ALTER TABLE `product_tbl`
  MODIFY `pro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site_setting_value`
--
ALTER TABLE `site_setting_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `store_category`
--
ALTER TABLE `store_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `store_information`
--
ALTER TABLE `store_information`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `store_webservices_credential`
--
ALTER TABLE `store_webservices_credential`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
