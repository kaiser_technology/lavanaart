-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2016 at 04:04 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nayavepar`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_credential`
--

CREATE TABLE `login_credential` (
  `id` int(11) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `email` varchar(265) NOT NULL,
  `password` varchar(70) NOT NULL,
  `role_id` int(8) NOT NULL,
  `status` int(11) NOT NULL,
  `store_key` varchar(54) NOT NULL,
  `addDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logged_in_datetime` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_credential`
--

INSERT INTO `login_credential` (`id`, `user_name`, `email`, `password`, `role_id`, `status`, `store_key`, `addDate`, `logged_in_datetime`) VALUES
(1, 'Nikunj Patel', 'nikunj@gmail.com', '20a84576d1ebbaa48311e66171286b50980ea601', 1, 1, '889a3a791b3875cfae413574b53da4bb8a90d53e', '2016-12-16 19:08:01', '2016-12-17 03:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `nv_site_settings_form`
--

CREATE TABLE `nv_site_settings_form` (
  `site_settings_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `var_name` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Define variable name',
  `description` text COLLATE latin1_general_ci NOT NULL COMMENT 'Description of variable',
  `setting` varchar(1000) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Setting value',
  `display_order` int(11) NOT NULL DEFAULT '0',
  `section` int(3) NOT NULL DEFAULT '0' COMMENT 'Section of settings..i.e. Payment section, shipping section, etc..',
  `html_element` enum('Text','Select','Textarea','File') COLLATE latin1_general_ci NOT NULL DEFAULT 'Text',
  `html_element_value` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `status` enum('0','1') COLLATE latin1_general_ci NOT NULL COMMENT '0=Inactive and will not display in admin panel 1= Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `nv_site_settings_form`
--

INSERT INTO `nv_site_settings_form` (`site_settings_id`, `title`, `var_name`, `description`, `setting`, `display_order`, `section`, `html_element`, `html_element_value`, `status`) VALUES
(1, 'WebSite Title', 'SITE_TITLE', '', 'Demo Project', 1, 1, 'Text', '', '1'),
(2, 'Email address for administrator', 'ADMIN_MAIL', 'Email address for administrator e-mails ', 'customer.service@carpetexpress.com', 2, 1, 'Text', '', '1'),
(3, 'Contact Person Mail Address', 'CONTACT_MAIL', 'E-mail Address for your Customer Service ', 'customer.service@carpetexpress.com', 3, 1, 'Text', '', '1'),
(4, 'TOLL FREE NO', 'TOLL_FREE_NO', 'Toll Free Phone Number', '800-922-5582', 4, 1, 'Text', '', '1'),
(5, 'Google Analytics Tracking ID', 'GOOGLE_ANALYTICS_TRACKING_ID', 'Set your Google Analytics Tracking ID here. Ex UA-XXXXXXX-X', 'UA-36115126-1', 5, 1, 'Text', '', '1'),
(6, 'Show coming soon page?', 'SHOW_COMING_SOON_PAGE', 'Set you website on coming soon page.', '', 6, 1, 'Select', 'No#Yes', '1'),
(7, 'Home Page Banner Speed', 'HOME_BANNER_SPEED', 'Home Page Banner Sliding Speed in Second', '15', 7, 1, 'Text', '', '1'),
(8, 'Home Page Text', 'HOME_PAGE_TEXT', 'Text that display after Featured Items', '<h3>Love your floors</h3><span style="font-size: medium;">\r\nAt Carpet Express, we believe the floor is the foundation for beautiful interior design. You could say, â€œthe floor is the most important wall in any home.â€ Thatâ€™s why Carpet Express is committed to providing the most trusted brands from our nation''s top flooring manufacturers at the lowest possible price. So whether you are shopping for carpet, hardwood, laminate, vinyl flooring, or area rugs, you can be sure that CarpetExpress.com is your best source for all your floor covering needs.</span>', 8, 1, 'Textarea', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `role_id`
--

CREATE TABLE `role_id` (
  `id` int(11) NOT NULL,
  `role_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_id`
--

INSERT INTO `role_id` (`id`, `role_name`) VALUES
(1, 'super_admin'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `site_setting_value`
--

CREATE TABLE `site_setting_value` (
  `id` int(11) NOT NULL,
  `store_setting` varchar(120) NOT NULL,
  `store_key` varchar(265) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_credential`
--
ALTER TABLE `login_credential`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- Indexes for table `nv_site_settings_form`
--
ALTER TABLE `nv_site_settings_form`
  ADD PRIMARY KEY (`site_settings_id`),
  ADD UNIQUE KEY `var_name` (`var_name`);

--
-- Indexes for table `role_id`
--
ALTER TABLE `role_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting_value`
--
ALTER TABLE `site_setting_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_key` (`store_key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_credential`
--
ALTER TABLE `login_credential`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nv_site_settings_form`
--
ALTER TABLE `nv_site_settings_form`
  MODIFY `site_settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `role_id`
--
ALTER TABLE `role_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `site_setting_value`
--
ALTER TABLE `site_setting_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
