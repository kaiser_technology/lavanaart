-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2017 at 01:50 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nayavepar`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_tbl`
--

CREATE TABLE `category_tbl` (
  `cat_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `cat_name` varchar(200) NOT NULL,
  `cat_image` varchar(500) NOT NULL,
  `cat_status` int(11) NOT NULL,
  `cat_dtadded` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_description` text NOT NULL,
  `meta_tag_title` varchar(255) NOT NULL,
  `meta_tag_description` text NOT NULL,
  `meta_tag_keywords` text NOT NULL,
  `menu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_tbl`
--

INSERT INTO `category_tbl` (`cat_id`, `parent_id`, `cat_name`, `cat_image`, `cat_status`, `cat_dtadded`, `user_id`, `cat_description`, `meta_tag_title`, `meta_tag_description`, `meta_tag_keywords`, `menu`) VALUES
(1, 0, 'women', '9cdf9-similar1.jpg', 1, '2017-01-15 00:00:00', 1, '<p>\r\n	test</p>\r\n', 'test', '<p>\r\n	test</p>\r\n', '<p>\r\n	test</p>\r\n', 'category'),
(2, 1, 'cloth', '73647-similar2.jpg', 1, '2017-01-15 00:00:00', 1, '<p>\r\n	hi</p>\r\n', '', '<p>\r\n	hi</p>\r\n', '<p>\r\n	hi</p>\r\n', 'cloth');

-- --------------------------------------------------------

--
-- Table structure for table `login_credential`
--

CREATE TABLE `login_credential` (
  `id` int(11) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `email` varchar(265) NOT NULL,
  `password` varchar(70) NOT NULL,
  `role_id` int(8) NOT NULL,
  `status` int(11) NOT NULL,
  `store_key` varchar(54) NOT NULL,
  `addDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logged_in_datetime` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_credential`
--

INSERT INTO `login_credential` (`id`, `user_name`, `email`, `password`, `role_id`, `status`, `store_key`, `addDate`, `logged_in_datetime`) VALUES
(1, 'Nikunj Patel', 'nikunj@gmail.com', '20a84576d1ebbaa48311e66171286b50980ea601', 1, 1, 'NVDS0000', '2016-12-16 19:08:01', '2017-02-01 04:02:28');

-- --------------------------------------------------------

--
-- Table structure for table `nv_menu_tbl`
--

CREATE TABLE `nv_menu_tbl` (
  `id` int(11) NOT NULL,
  `parent_menu_id` int(11) NOT NULL,
  `menu_name` varchar(80) NOT NULL,
  `icon` varchar(35) NOT NULL,
  `menu_link` varchar(100) NOT NULL,
  `act_name` varchar(80) NOT NULL,
  `role_id` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nv_menu_tbl`
--

INSERT INTO `nv_menu_tbl` (`id`, `parent_menu_id`, `menu_name`, `icon`, `menu_link`, `act_name`, `role_id`) VALUES
(1, 0, 'Dashboard', '', 'dashboard', 'dashboard', '1,2'),
(2, 0, 'Store', '', 'store', 'store', '1'),
(3, 2, 'List', '', 'store/list', 'store', '1'),
(4, 2, 'Add', '', 'store/add', 'store', '1'),
(5, 2, 'Category', '', 'store/store_category', 'store', '1'),
(6, 0, 'ACCESS TYPE', '', 'role/', 'role', '1'),
(7, 0, 'MASTER PRODUCT', '', 'product/', 'product', '1'),
(8, 0, 'PRODUCT', '', 'product/', 'product', '3'),
(9, 8, 'PRODUCT LIST', '', 'product/', 'product', '3'),
(10, 8, 'CATEGORY', '', 'category/', 'category', '3'),
(11, 8, 'MANUFACTURE', '', 'manufacture/', 'manufacture', '3'),
(12, 8, 'DISCOUNT', '', 'discount/', 'discount', '3'),
(13, 0, 'ORDER', '', '', 'order', '3'),
(14, 13, 'PENDING ORDER', '', 'order/pending-order/', 'order', '3'),
(15, 13, 'DISPATCH ORDER', '', 'order/dispatch-order/', 'order', '3'),
(16, 13, 'DELIVERED ORDER', '', 'order/delivered-order/', 'order', '3'),
(17, 13, 'CANCEL ORDER', '', 'order/cancel-order/', 'order', '3'),
(18, 13, 'COMPLETED ORDER', '', 'order/completed-order/', 'order', '3'),
(19, 0, 'REPORTS', '', '', 'report', '1,2,3');

-- --------------------------------------------------------

--
-- Table structure for table `nv_site_settings_form`
--

CREATE TABLE `nv_site_settings_form` (
  `site_settings_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `var_name` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Define variable name',
  `description` text COLLATE latin1_general_ci NOT NULL COMMENT 'Description of variable',
  `setting` varchar(1000) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Setting value',
  `display_order` int(11) NOT NULL DEFAULT '0',
  `section` int(3) NOT NULL DEFAULT '0' COMMENT 'Section of settings..i.e. Payment section, shipping section, etc..',
  `html_element` enum('Text','Select','Textarea','File') COLLATE latin1_general_ci NOT NULL DEFAULT 'Text',
  `html_element_value` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `status` enum('0','1') COLLATE latin1_general_ci NOT NULL COMMENT '0=Inactive and will not display in admin panel 1= Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `nv_site_settings_form`
--

INSERT INTO `nv_site_settings_form` (`site_settings_id`, `title`, `var_name`, `description`, `setting`, `display_order`, `section`, `html_element`, `html_element_value`, `status`) VALUES
(1, 'WebSite Title', 'SITE_TITLE', '', 'Demo Project', 1, 1, 'Text', '', '1'),
(2, 'Email address for administrator', 'ADMIN_MAIL', 'Email address for administrator e-mails ', 'customer.service@carpetexpress.com', 2, 1, 'Text', '', '1'),
(3, 'Contact Person Mail Address', 'CONTACT_MAIL', 'E-mail Address for your Customer Service ', 'customer.service@carpetexpress.com', 3, 1, 'Text', '', '1'),
(4, 'TOLL FREE NO', 'TOLL_FREE_NO', 'Toll Free Phone Number', '800-922-5582', 4, 1, 'Text', '', '1'),
(5, 'Google Analytics Tracking ID', 'GOOGLE_ANALYTICS_TRACKING_ID', 'Set your Google Analytics Tracking ID here. Ex UA-XXXXXXX-X', 'UA-36115126-1', 5, 1, 'Text', '', '1'),
(6, 'Show coming soon page?', 'SHOW_COMING_SOON_PAGE', 'Set you website on coming soon page.', '', 6, 1, 'Select', 'No#Yes', '1'),
(7, 'Home Page Banner Speed', 'HOME_BANNER_SPEED', 'Home Page Banner Sliding Speed in Second', '15', 7, 1, 'Text', '', '1'),
(8, 'Home Page Text', 'HOME_PAGE_TEXT', 'Text that display after Featured Items', '<h3>Love your floors</h3><span style="font-size: medium;">\r\nAt Carpet Express, we believe the floor is the foundation for beautiful interior design. You could say, â€œthe floor is the most important wall in any home.â€ Thatâ€™s why Carpet Express is committed to providing the most trusted brands from our nation''s top flooring manufacturers at the lowest possible price. So whether you are shopping for carpet, hardwood, laminate, vinyl flooring, or area rugs, you can be sure that CarpetExpress.com is your best source for all your floor covering needs.</span>', 8, 1, 'Textarea', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl`
--

CREATE TABLE `product_tbl` (
  `pro_id` int(11) NOT NULL,
  `pro_code` varchar(50) NOT NULL,
  `pro_cat` int(11) NOT NULL,
  `pro_name` varchar(150) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `pro_tax` varchar(255) NOT NULL,
  `pro_display_date` varchar(15) NOT NULL,
  `pro_image` varchar(175) NOT NULL,
  `related_pro` varchar(150) NOT NULL,
  `pro_status` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `offer` int(11) NOT NULL DEFAULT '0',
  `m_title` varchar(250) NOT NULL,
  `m_desc` varchar(500) NOT NULL,
  `m_keyword` varchar(500) NOT NULL,
  `pro_detail` varchar(1500) NOT NULL,
  `pro_unit` varchar(50) NOT NULL,
  `pro_no_unit` int(11) NOT NULL,
  `pro_mrp_price` decimal(6,2) NOT NULL,
  `pro_purchase_price` decimal(6,2) NOT NULL,
  `pro_profit_price` decimal(6,2) NOT NULL,
  `pro_seal_price` decimal(6,2) NOT NULL,
  `pro_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tbl`
--

INSERT INTO `product_tbl` (`pro_id`, `pro_code`, `pro_cat`, `pro_name`, `brand`, `pro_tax`, `pro_display_date`, `pro_image`, `related_pro`, `pro_status`, `add_date`, `modify_date`, `user_id`, `offer`, `m_title`, `m_desc`, `m_keyword`, `pro_detail`, `pro_unit`, `pro_no_unit`, `pro_mrp_price`, `pro_purchase_price`, `pro_profit_price`, `pro_seal_price`, `pro_qty`) VALUES
(1, 'dyedrhdf', 1, 'dgdsgghfg dfdfg', 'fhchdf', 'cghd', 'chfgh', '3fe50-two.png', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '', '', '', '', 0, '0.00', '0.00', '0.00', '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE `role_tbl` (
  `id` int(11) NOT NULL,
  `role_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_tbl`
--

INSERT INTO `role_tbl` (`id`, `role_name`) VALUES
(1, 'super_admin'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `site_setting_value`
--

CREATE TABLE `site_setting_value` (
  `id` int(11) NOT NULL,
  `store_setting` varchar(120) NOT NULL,
  `store_key` varchar(265) NOT NULL,
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store_category`
--

CREATE TABLE `store_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(110) NOT NULL,
  `status` enum('Yes','No') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_category`
--

INSERT INTO `store_category` (`id`, `category_name`, `status`) VALUES
(1, 'Grocery', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `store_information`
--

CREATE TABLE `store_information` (
  `store_id` int(11) NOT NULL,
  `store_key` varchar(50) NOT NULL,
  `store_name` varchar(250) NOT NULL,
  `store_logo` varchar(80) NOT NULL,
  `store_category` int(11) NOT NULL,
  `store_address` varchar(2500) NOT NULL,
  `store_city` varchar(50) NOT NULL,
  `store_zipcode` int(8) NOT NULL,
  `contact_person` varchar(150) NOT NULL,
  `contact_email` varchar(265) NOT NULL,
  `contact_number` varchar(13) NOT NULL,
  `api_key` varchar(80) NOT NULL,
  `secret_key` varchar(80) NOT NULL,
  `create_date` date NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive','Hold','Drop') NOT NULL,
  `bank_name` varchar(360) NOT NULL,
  `account_number` varchar(32) NOT NULL,
  `IFSC_code` varchar(20) NOT NULL,
  `store_create_by` int(5) NOT NULL,
  `approval_by` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_information`
--

INSERT INTO `store_information` (`store_id`, `store_key`, `store_name`, `store_logo`, `store_category`, `store_address`, `store_city`, `store_zipcode`, `contact_person`, `contact_email`, `contact_number`, `api_key`, `secret_key`, `create_date`, `modify_date`, `status`, `bank_name`, `account_number`, `IFSC_code`, `store_create_by`, `approval_by`) VALUES
(1, 'NVDS0000', 'Supper Admin', '', 0, '-', 'Mumbai', 400101, 'NavaVepar', 'admin@navavepar.com', '9879513314', '-', '-', '2017-01-31', '2017-01-31 00:57:48', 'Active', '', '', '', 1, 1),
(2, '', 'xyz', '', 1, 'sdf', 'asdf', 0, 'asdfasdf', 'asdfasdf', 'asdasdfas', '', '', '0000-00-00', '2017-02-01 16:55:36', 'Active', 'asdfasdfasdf', 'asdfasd', 'asdfasdfasdf', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_tbl`
--
ALTER TABLE `category_tbl`
  ADD PRIMARY KEY (`cat_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `login_credential`
--
ALTER TABLE `login_credential`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- Indexes for table `nv_menu_tbl`
--
ALTER TABLE `nv_menu_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nv_site_settings_form`
--
ALTER TABLE `nv_site_settings_form`
  ADD PRIMARY KEY (`site_settings_id`),
  ADD UNIQUE KEY `var_name` (`var_name`);

--
-- Indexes for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting_value`
--
ALTER TABLE `site_setting_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_key` (`store_key`);

--
-- Indexes for table `store_category`
--
ALTER TABLE `store_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_information`
--
ALTER TABLE `store_information`
  ADD PRIMARY KEY (`store_id`),
  ADD UNIQUE KEY `store_key` (`store_key`),
  ADD KEY `store_category` (`store_category`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_credential`
--
ALTER TABLE `login_credential`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nv_menu_tbl`
--
ALTER TABLE `nv_menu_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `nv_site_settings_form`
--
ALTER TABLE `nv_site_settings_form`
  MODIFY `site_settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `site_setting_value`
--
ALTER TABLE `site_setting_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `store_category`
--
ALTER TABLE `store_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `store_information`
--
ALTER TABLE `store_information`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
